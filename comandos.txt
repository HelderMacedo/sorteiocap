## 
Horários limites de 
JACOBINA - DOAÇÂO : 8:00
JACOBINA - SOLIDARIEDADE: 8:00
SOBRAL - DOAÇÃO (Guara) : 10:00

export DOCKERHOST=$(ifconfig | grep -E "([0-9]{1,3}\.){3}[0-9]{1,3}" | grep -v 127.0.0.1 | awk '{ print $2 }' | cut -f2 -d: | head -n1)


#### Executar a aplicação e criar o banco (desenvolvimento) baseado no liquibase

gradlew

#### Em desenvolvimento, verifica as diferenças entre o banco e as anotações das entidades JPA

./gradlew clean liquibaseDiffChangelog

#### Empacota o arquivo war (em build/libs)

./gradlew clean bootRepackage -x test
./gradlew -Pprod-sobral_sort clean bootRepackage -x test
./gradlew -Pprod-sobral_esp clean bootRepackage -x test
./gradlew -Pprod-solid_sort clean bootRepackage -x test
./gradlew -Pprod-solid_rasp clean bootRepackage -x test
./gradlew -Pprod-solid_apae clean bootRepackage -x test
./gradlew -Pprod-solid_esp clean bootRepackage -x test
./gradlew -Pprod-ceara_solid clean bootRepackage -x test
./gradlew -Pprod-rasp_norte clean bootRepackage -x test


### Geração de comandos sql a partir do liquibase
java -jar /opt/liquibase-3.5.3-bin/liquibase.jar --driver=com.mysql.jdbc.Driver  --classpath=/opt/java/mysql-connector-java-5.1.42/mysql-connector-java-5.1.42-bin.jar --url="jdbc:mysql://localhost/sorteioapp?useUnicode=true&characterEncoding=UTF-8" --username=dbadmin --password=usort2017 --changeLogFile=/home/rodrigo/dev/freelancer/sorteioapp/src/main/resources/config/liquibase/changelog/20170501072030_changelog.xml updateSQL > /home/rodrigo/dev/freelancer/script.sql

