create table if not exists sorteio_concurso (
   sorteio_id int(11) NOT NULL AUTO_INCREMENT,
   concurso_id int(11) NOT NULL,
   ordem_premio int(6)  UNSIGNED NOT NULL,
   descricao_premio VARCHAR(255) NOT NULL,
   valor_premio DECIMAL NOT NULL,
   data_sorteio timestamp NULL DEFAULT current_timestamp(),
   quantidade_bolas_chamadas int(6) DEFAULT 0,
   bolas varchar(1000) DEFAULT '0',
   PRIMARY KEY (sorteio_id),
   FOREIGN KEY (concurso_id) REFERENCES concurso(concurso_id)  
);

CREATE TABLE IF NOT EXISTS ganhador_concurso (
   ganhador_id int(11) NOT NULL AUTO_INCREMENT,
   sorteio_id int(11) NOT NULL,
   numero_bilhete int(11) NOT NULL,
   identificacao_bilhete VARCHAR(25) NOT NULL,
   nome_ganhador VARCHAR(255) DEFAULT NULL,
   endereco_bairro varchar(255) DEFAULT NULL,
   endereco_cidade varchar(255) DEFAULT NULL,
   endereco_uf varchar(255) DEFAULT NULL,
  PRIMARY KEY (ganhador_id),
  FOREIGN KEY (sorteio_id) REFERENCES sorteio_concurso(sorteio_id)
);

CREATE TABLE IF NOT EXISTS giro_sorte (
   giro_id int(11) NOT NULL AUTO_INCREMENT,
   concurso_id int(11) NOT NULL,
   quantidade_giros int (5) NOT NULL,
   valor_premio DECIMAL NOT NULL,
   PRIMARY KEY (giro_id),
   FOREIGN KEY (concurso_id) REFERENCES concurso(concurso_id)  
);

CREATE TABLE IF NOT EXISTS ganhador_giro_sorte (
   giro_id int(11) NOT NULL,
   ordem_giro int(11) NOT NULL,
   data_hora timestamp NULL DEFAULT current_timestamp(),
   identificacao_bilhete varchar(25) NOT NULL,
   numero_bilhete int(11) NOT NULL,
   nome_ganhador VARCHAR(255) NOT NULL,
   endereco_bairro varchar(255) DEFAULT NULL,
   endereco_cidade varchar(255) DEFAULT NULL,
   endereco_uf varchar(255) DEFAULT NULL,
   PRIMARY KEY (giro_id, ordem_giro)
);

