update concurso set perc_ajuste = 0 where concurso_id <= 2019014;
update concurso set perc_ajuste = 0 where concurso_id <= 2019901;
update concurso set perc_ajuste = 35 where concurso_id between 2019015 and 2019041;
update concurso set perc_ajuste = 40 where concurso_id = 2019042;

update regional set sofre_ajuste = False where nome like '%APAE%';

SELECT c2.concurso_id, c2.perc_ajuste, count(b2.validado) totvalidado, sum(b2.ajustado) totajustado,
		    round(count(b2.validado) * c2.perc_ajuste / 100) as meta from bilhete b2, concurso c2
		      where  b2.concurso_id = 2019039 and b2.concurso_id = c2.concurso_id group by c2.concurso_id, c2.perc_ajuste ;

select concurso_id, regional_id, count(*), sum(ajustado), sum(ajustado)/ count(*) as perc 
 from bilhete where concurso_id = 2019039 and validado = TRUE
  GROUP by concurso_id, regional_id
  ORDER BY perc;

select concurso_id, estabelecimento_id, count(*), sum(ajustado), sum(ajustado)/ count(*) as perc 
 from bilhete where concurso_id = 2019039 and validado = TRUE
  GROUP by concurso_id, estabelecimento_id
  ORDER BY perc;          


DELIMITER $$
CREATE PROCEDURE ajustar_bilhetes( concid int(11))
BEGIN

DECLARE percaju int(11);
set percaju := (select perc_ajuste from concurso where concurso_id = concid);
 
IF (percaju > 0) THEN

update bilhete set ajustado = False where validado = TRUE and concurso_id = concid;

update bilhete set ajustado = True where concurso_id = concid and validado = TRUE
                   and regional_id in (select regional_id from regional where sofre_ajuste = False)
			       and (concurso_id, numero) not in (select g0.concurso_id, g0.numero_bilhete from ganhadores g0)
			       and (concurso_id, numero) not in (select g1.concurso_id, g1.numero_bilhete from giro_sorte g1);
                   
update bilhete set ajustado = TRUE
    where (concurso_id, numero) in
    (select s2.concurso_id, s2.numero
    from (
	    select rkb.concurso_id, rkb.numero, rkb.rrank,  (x.meta-x.totajustado) as falta,  (@row_number:=@row_number + 1) AS num

	    FROM
          (SELECT * 
           FROM
              (SELECT  rke.concurso_id, rke.regional_id, rke.estabelecimento_id, rke.numero, rke.erank, w2.totreg,
                    (( 
                      CASE rke.regional_id
                      WHEN @curReg
                      THEN @curRRow := @curRRow + 1 
                      ELSE @curRRow := 1 END
                      ) )/w2.totreg AS rrank,
                    @curReg := rke.regional_id
              FROM
                   (SELECT  b.concurso_id, b.regional_id, b.estabelecimento_id, totestab,  b.numero,
                        (( 
                          CASE b.estabelecimento_id
                          WHEN @curEst
                          THEN @curRow := @curRow + 1 
                          ELSE @curRow := 1 END
                          ) )/w.totestab AS erank,
                        @curEst := b.estabelecimento_id 
                    FROM bilhete b, 
                        (SELECT concurso_id, regional_id, estabelecimento_id, count(*) totestab 
                              FROM bilhete where  concurso_id = concid and validado = TRUE
                              group by concurso_id, regional_id, estabelecimento_id) w,
                        (SELECT @curRow := 1, @curEst := 0) r
                    WHERE b.concurso_id = w.concurso_id AND b.regional_id = w.regional_id AND b.estabelecimento_id = w.estabelecimento_id AND 
                        b.concurso_id = concid and validado = TRUE and ajustado = False
                    ORDER BY b.concurso_id, b.regional_id ASC, b.estabelecimento_id ASC, b.numero DESC) rke, 
                   (SELECT @curRRow := 	1, @curReg := 0) r2,
                   (SELECT concurso_id, regional_id, count(*) totreg 
                                  FROM bilhete where  concurso_id = concid and validado = TRUE 
                                  group by concurso_id, regional_id) w2
               WHERE rke.concurso_id = w2.concurso_id AND rke.regional_id = w2.regional_id
               ORDER BY rke.concurso_id, rke.regional_id ASC, rke.erank DESC, rke.totestab ASC, rke.numero ASC) rankbils ORDER BY rrank DESC, totreg ASC) rkb, 
	      (SELECT c2.concurso_id, c2.perc_ajuste, count(b2.validado) totvalidado, sum(b2.ajustado) totajustado,
		          round(count(b2.validado) * c2.perc_ajuste / 100) as meta from bilhete b2, concurso c2
		      where  b2.concurso_id = concid and b2.concurso_id = c2.concurso_id group by c2.concurso_id, c2.perc_ajuste) x,
	      (SELECT @row_number:=0) AS t 
	    -- WHERE x.concurso_id = rkb.concurso_id and  @row_number < (x.meta-x.totajustado) 
       -- ORDER BY rrank DESC
    ) s2 where num <= falta)
    ;
ELSEIF (percaju = 0) THEN 
   update bilhete set ajustado = TRUE where validado = TRUE and concurso_id = concid;
END IF;

 END $$
DELIMITER ;

