     delimiter |
DROP TRIGGER  insertregionalbilhetes|    
CREATE TRIGGER insertregionalbilhetes AFTER INSERT ON loteregionalbilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set bilhete.regional_id = NEW.regional_id, bilhete.data_modificacao = now()
    where bilhete.concurso_id = NEW.concurso_id and bilhete.validado <> TRUE
          bilhete.numero between NEW.numinicio and NEW.numfim;
  END|
DROP TRIGGER  updateregionalbilhetes|  
CREATE TRIGGER updateregionalbilhetes AFTER UPDATE ON loteregionalbilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set bilhete.regional_id = NEW.regional_id, bilhete.data_modificacao = now()
    where bilhete.concurso_id = NEW.concurso_id and
          bilhete.numero between NEW.numinicio and NEW.numfim;
  END|
DROP TRIGGER  deleteregionalbilhetes|  
CREATE TRIGGER deleteregionalbilhetes BEFORE DELETE ON loteregionalbilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set regional_id = NULL, data_modificacao = now()
    where bilhete.concurso_id = OLD.concurso_id and
          bilhete.numero between OLD.numinicio and OLD.numfim;
  END|
  DROP TRIGGER  insertvendabilhetes|  
CREATE TRIGGER insertvendabilhetes AFTER INSERT ON lotevendabilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set bilhete.estabelecimento_id = NEW.estabelecimento_id, bilhete.data_modificacao = now()
    where bilhete.concurso_id = NEW.concurso_id and
          bilhete.numero between NEW.numinicio and NEW.numfim;
  END|
  DROP TRIGGER  updatevendabilhetes|  
CREATE TRIGGER updatevendabilhetes AFTER UPDATE ON lotevendabilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set bilhete.estabelecimento_id = NEW.estabelecimento_id, bilhete.data_modificacao = now()
    where bilhete.concurso_id = NEW.concurso_id and
          bilhete.numero between NEW.numinicio and NEW.numfim;
  END|
  DROP TRIGGER  deletevendabilhetes|  
CREATE TRIGGER deletevendabilhetes BEFORE DELETE ON lotevendabilhete
  FOR EACH ROW
  BEGIN
    UPDATE bilhete set bilhete.estabelecimento_id = NULL, data_modificacao = now()
    where bilhete.concurso_id = OLD.concurso_id and
          bilhete.numero between OLD.numinicio and OLD.numfim;
  END|
delimiter ;
