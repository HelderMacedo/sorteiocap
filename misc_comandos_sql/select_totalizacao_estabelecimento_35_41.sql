select r.regional_id as COD_REGIONAL, r.nome as NOME_REGIONAL,
       e.estabelecimento_id as COD_ESTABELECIMENTO, e.nome as NOME_ESTABELECIMENTO,
		 concat (t.label, ' - ' , c.num_sorteio),
		 count(*) as VALIDADOS,
		 count(*) * t.valor AS VALOR,
		 e.percentualrepasse as PERC_COMISSAO,
		 count(*) * t.valor * e.percentualrepasse /100.0 AS COMISSAO
from bilhete b
join concurso c on c.concurso_id = b.concurso_id
join tipobilhete t on t.tipo_id = b.tipo_id 
join regional r on r.regional_id = b.regional_id
join estabelecimento e on e.estabelecimento_id = b.estabelecimento_id 
where (c.concurso_id between 2019035 and 2019041) AND ( b.reservado = TRUE OR b.validado = TRUE)

GROUP BY c.concurso_id, e.estabelecimento_id
ORDER BY c.concurso_id, r.nome, e.nome ;
