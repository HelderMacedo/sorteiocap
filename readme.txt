Pré-requisitos:

Install Java 8 from the Oracle website.
Install Node.js from the Node.js website (prefer an LTS version)
Install Yarn from the Yarn website
Install Bower: yarn global add bower
Install Gulp: yarn global add gulp-cli
If you want to use the JHipster Marketplace, install Yeoman: yarn global add yo
Install JHipster: yarn global add generator-jhipster

Obs.: After all, run:

rm -rf node_modules
rm -rf package-lock.json
npm cache clean
npm install --unsafe-perm=true  (not sure)
yarn update gulp                (almost sure)
