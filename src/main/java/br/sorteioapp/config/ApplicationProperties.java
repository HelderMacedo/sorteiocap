package br.sorteioapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to JHipster.
 *
 * <p>
 *     Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private Boolean verAjuste;

    private Boolean comissaoProgressiva;

    public Boolean getVerAjuste() {
        return verAjuste;
    }

    public void setVerAjuste(Boolean verAjuste) {
        this.verAjuste = verAjuste;
    }


    public Boolean getComissaoProgressiva() {
        return comissaoProgressiva;
    }

    public void setComissaoProgressiva(Boolean comissaoProgressiva) {
        this.comissaoProgressiva = comissaoProgressiva;
    }
}
