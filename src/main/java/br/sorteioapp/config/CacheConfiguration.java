package br.sorteioapp.config;

import java.util.concurrent.TimeUnit;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.Eh107Configuration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.sorteioapp.service.ConcursoService;
import br.sorteioapp.service.EstabelecimentoService;
import io.github.jhipster.config.JHipsterProperties;

@Configuration
@EnableCaching
@AutoConfigureAfter(value = { MetricsConfiguration.class })
@AutoConfigureBefore(value = { WebConfigurer.class, DatabaseConfiguration.class })
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(Expirations.timeToLiveExpiration(Duration.of(ehcache.getTimeToLiveSeconds(), TimeUnit.SECONDS)))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(br.sorteioapp.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.User.class.getName() + ".persistentTokens", jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.PersistentToken.class.getName(), jcacheConfiguration);

            cm.createCache(br.sorteioapp.domain.TipoBilhete.class.getName() , jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Bilhete.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Concurso.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Regional.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.AreaRegional.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Estabelecimento.class.getName(), jcacheConfiguration);
            cm.createCache(br.sorteioapp.domain.Concurso.class.getName() + ".lotebilhetes", jcacheConfiguration);
            cm.createCache(ConcursoService.QUERY_CACHE_TODOS_CONCURSOS, jcacheConfiguration);
            cm.createCache(ConcursoService.QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS, jcacheConfiguration);
            //cm.createCache(RegionalService.QUERY_CACHE_TODAS_REGIONAIS, jcacheConfiguration);
            //cm.createCache(RegionalService.QUERY_CACHE_TODAS_REGIONAIS_ATIVAS, jcacheConfiguration);
            cm.createCache(EstabelecimentoService.QUERY_CACHE_TODAS_CIDADES_ESTABELECIMENTOS, jcacheConfiguration);
            cm.createCache(EstabelecimentoService.QUERY_CACHE_TODOS_BAIRROS_ESTABELECIMENTOS, jcacheConfiguration);

            // jhipster-needle-ehcache-add-entry
        };
    }
}
