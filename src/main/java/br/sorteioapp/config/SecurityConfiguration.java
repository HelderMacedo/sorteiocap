package br.sorteioapp.config;

//import static br.sorteioapp.security.AuthoritiesConstants.ROLE_ADMIN;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.data.repository.query.SecurityEvaluationContextExtension;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import io.github.jhipster.config.JHipsterProperties;
import io.github.jhipster.security.AjaxAuthenticationFailureHandler;
import io.github.jhipster.security.AjaxAuthenticationSuccessHandler;
import io.github.jhipster.security.AjaxLogoutSuccessHandler;
import io.github.jhipster.security.Http401UnauthorizedEntryPoint;

    import static br.sorteioapp.security.AuthoritiesConstants.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private final AuthenticationManagerBuilder authenticationManagerBuilder;

	private final UserDetailsService userDetailsService;

	private final JHipsterProperties jHipsterProperties;

	private final RememberMeServices rememberMeServices;

	private final CorsFilter corsFilter;

	public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder,
			UserDetailsService userDetailsService, JHipsterProperties jHipsterProperties,
			RememberMeServices rememberMeServices) {

		this.authenticationManagerBuilder = authenticationManagerBuilder;
		this.userDetailsService = userDetailsService;
		this.jHipsterProperties = jHipsterProperties;
		this.rememberMeServices = rememberMeServices;
		this.corsFilter = corsFilter();
	}

	@PostConstruct
	public void init() {
		try {
			authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		} catch (Exception e) {
			throw new BeanInitializationException("Security configuration failed", e);
		}
	}

	@Bean
	public AjaxAuthenticationSuccessHandler ajaxAuthenticationSuccessHandler() {
		return new AjaxAuthenticationSuccessHandler();
	}

	@Bean
	public AjaxAuthenticationFailureHandler ajaxAuthenticationFailureHandler() {
		return new AjaxAuthenticationFailureHandler();
	}

	@Bean
	public AjaxLogoutSuccessHandler ajaxLogoutSuccessHandler() {
		return new AjaxLogoutSuccessHandler();
	}

	@Bean
	public Http401UnauthorizedEntryPoint http401UnauthorizedEntryPoint() {
		return new Http401UnauthorizedEntryPoint();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	// @Bean
	public CorsFilter corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(false);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		// source.registerCorsConfiguration("/api/jogador", config);
		// source.registerCorsConfiguration("/api/estabelecimentos-enderecos", config);
		// source.registerCorsConfiguration("/api/estabelecimentos-por-enderecos",
		// config);
		return new CorsFilter(source);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**").antMatchers("/app/**/*.{js,html}")
				.antMatchers("/bower_components/**").antMatchers("/i18n/**").antMatchers("/content/**")
				.antMatchers("/swagger-ui/index.html").antMatchers("/test/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).and()
				.addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class).exceptionHandling()
				.authenticationEntryPoint(http401UnauthorizedEntryPoint()).and().rememberMe()
				.rememberMeServices(rememberMeServices).rememberMeParameter("remember-me")
				.key(jHipsterProperties.getSecurity().getRememberMe().getKey()).and().formLogin()
				.loginProcessingUrl("/api/authentication").successHandler(ajaxAuthenticationSuccessHandler())
				.failureHandler(ajaxAuthenticationFailureHandler()).usernameParameter("j_username")
				.passwordParameter("j_password").permitAll().and().logout().logoutUrl("/api/logout")
				.logoutSuccessHandler(ajaxLogoutSuccessHandler()).permitAll().and().headers().frameOptions().disable()
				.and().authorizeRequests()
				//.antMatchers("/api/jogador").permitAll()
				//.antMatchers("/api/estabelecimentos-enderecos/**").permitAll()
				//.antMatchers("/api/estabelecimentos-por-enderecos/**").permitAll()
				//.antMatchers("/api/register").denyAll()
				.antMatchers("/api/activate").permitAll()
				.antMatchers("/api/authenticate").permitAll()
				.antMatchers("/api/account/reset_password/init").permitAll()
				.antMatchers("/api/account/reset_password/finish").permitAll()
				.antMatchers("/api/profile-info").permitAll()
                .antMatchers(HttpMethod.POST,"/api/concursos/**").hasAuthority(PERM_GERACAO_CONCURSO)
                .antMatchers(HttpMethod.PUT,"/api/concursos/**").hasAnyAuthority(PERM_EDICAO_CONCURSO, PERM_GERACAO_CONCURSO)
                .antMatchers("/api/lotebilhete-geracao").hasAnyAuthority(PERM_GERACAO_CONCURSO)
				.antMatchers(HttpMethod.GET, "/api/concursos/**").authenticated()
				.antMatchers("/api/concursos-short").authenticated()
                .antMatchers("/api/concursos-naosinc").authenticated()
                .antMatchers(HttpMethod.GET,"/api/estabelecimentos/**").hasAnyAuthority(PERM_CONSULTAR_ESTABELECIMENTO)
				.antMatchers("/api/estabelecimentos/**").hasAnyAuthority(PERM_CADASTRO_ESTABELECIMENTO)
				.antMatchers(HttpMethod.GET,"/api/arearegional/**").hasAnyAuthority(PERM_CONSULTAR_AREA_REGIONAL)
				.antMatchers("/api/arearegional/**").hasAnyAuthority(PERM_CADASTRO_AREA_REGIONAL)				
				.antMatchers(HttpMethod.GET,"/api/arearegional/regional/**").hasAnyAuthority(PERM_CONSULTAR_AREA_REGIONAL)
				
				.antMatchers(HttpMethod.GET,"/api/regionais/**").hasAnyAuthority(PERM_CONSULTAR_REGIONAL)
				.antMatchers("/api/regionais/**").hasAnyAuthority(PERM_CADASTRO_REGIONAL)
				.antMatchers("/api/regionais-ativas").hasAnyAuthority(ACESSO_REGIONAL_TODOS, ACESSO_REGIONAL_COM_LOGIN, PERM_CADASTRO_REGIONAL,
						PERM_REL_DIST_REGIONAIS, PERM_REL_DIST_REGIONAIS_SEM_EST, PERM_REL_DIST_ESTABELECIMENTO,
						PERM_REL_REG_EST_ENDERECO, PERM_REL_FINANCEIRO)
				.antMatchers("/api/regionais-todas")
				.hasAnyAuthority(ACESSO_REGIONAL_TODOS, ACESSO_REGIONAL_COM_LOGIN, PERM_CADASTRO_REGIONAL,
						PERM_REL_DIST_REGIONAIS, PERM_REL_DIST_REGIONAIS_SEM_EST, PERM_REL_DIST_ESTABELECIMENTO,
						PERM_REL_REG_EST_ENDERECO, PERM_REL_FINANCEIRO)
				//.antMatchers("/api/lotebilhete/faixas-disponiveis/**").hasAnyAuthority(PERM_CONFECCAO_LOTES)
				.antMatchers("/api/lotesregionais/distribui/**").hasAnyAuthority(PERM_DISTRIBUICAO_REGIONAL)
				.antMatchers("/api/lotesregionais/concurso/livres/**").hasAnyAuthority(PERM_DISTRIBUICAO_REGIONAL)
				.antMatchers(HttpMethod.GET, "/api/grupo-lancamento").hasAnyAuthority(PERM_CADASTRO_GRUPO_LANCAMENTO,PERM_CADASTRO_LANCAMENTO,PERM_REL_LANCAMENTO)
				.antMatchers("/api/grupo-lancamento").hasAnyAuthority(PERM_CADASTRO_GRUPO_LANCAMENTO)
				.antMatchers(HttpMethod.GET, "/api/lancamento").hasAnyAuthority(PERM_CADASTRO_LANCAMENTO,PERM_REL_LANCAMENTO)
				.antMatchers("/api/lancamento").hasAnyAuthority(PERM_CADASTRO_LANCAMENTO)

				.antMatchers("/api/distribuicaobilhetes/loteregionais/**").hasAnyAuthority(PERM_REL_DIST_REGIONAIS)
				.antMatchers("/api/distribuicaobilhetes/loteestabelecimentos-disponiveis/**").hasAnyAuthority(PERM_REL_DIST_REGIONAIS_SEM_EST)
				.antMatchers("/api/distribuicaobilhetes/loteregionais-totalizacao/**").hasAnyAuthority(PERM_REL_FINANCEIRO)
                .antMatchers("/api/distribuicaobilhetes/loteregionais-tendencia/**").hasAnyAuthority(PERM_REL_FINANCEIRO)
                .antMatchers("/api/distribuicaobilhetes/loteregionais-tendencia-periodo/**").hasAnyAuthority(PERM_REL_FINANCEIRO)
                .antMatchers("/api/distribuicaobilhetes/loteestabelecimentos-totalizacao/**").hasAnyAuthority(PERM_REL_FINANCEIRO, ROLE_AREA_REGIONAL)
				.antMatchers("/api/distribuicaobilhetes").hasAnyAuthority(PERM_CONFECCAO_LOTES)
				.antMatchers("/api/distribuicaobilhetes/bilhete").hasAnyAuthority(PERM_CONFECCAO_LOTES)
				.antMatchers("/api/bilhetes-status-dist").hasAnyAuthority(PERM_DISTRIBUICAO_ESTABELECIMENTO)

                .antMatchers("/api/distribuicaoselos/**").hasAnyAuthority(PERM_REL_DIST_REGIONAIS)
                .antMatchers("/api/comissoes/**").hasAnyAuthority(PERM_EDICAO_COMISSOES)

				.antMatchers("/api/lotesregionais/**").hasAnyAuthority(PERM_CONFECCAO_LOTES)
				.antMatchers("/api/lotevenda").hasAnyAuthority(PERM_DISTRIBUICAO_ESTABELECIMENTO)
				.antMatchers("/api/distribuicaobilhetes/loteestabelecimentos-est/**").hasAnyAuthority(PERM_CADASTRO_ESTABELECIMENTO,PERM_REL_DIST_ESTABELECIMENTO)
				.antMatchers("/api/distribuicaobilhetes/loteestabelecimentos-area/**").hasAnyAuthority(PERM_CADASTRO_AREA_REGIONAL,PERM_CONFECCAO_LOTES,PERM_REL_DIST_ESTABELECIMENTO)
				.antMatchers("/api/distribuicaobilhetes/loteestabelecimentos-reg/**").hasAnyAuthority(PERM_CADASTRO_REGIONAL,PERM_CONFECCAO_LOTES,PERM_REL_DIST_ESTABELECIMENTO)

				.antMatchers("/api/validarbilhete").hasAnyAuthority(PERM_VALIDAR_BILHETES)
				.antMatchers("/api/tiposbilhete").authenticated()
				//.antMatchers("/api/bilhetesvendidos/ultimos/**").hasAnyAuthority(ROLE_SYSTEM)
				.antMatchers("/api/users/**").hasAuthority(PERM_CADASTRO_USUARIOS)
				.antMatchers("/websocket/tracker").hasAuthority(ROLE_SYSTEM)
				.antMatchers("/websocket/**").authenticated()
				.antMatchers("/management/health").permitAll()
				.antMatchers("/management/**").hasAuthority(ROLE_SYSTEM)
				.antMatchers("/v2/api-docs/**").hasAuthority(ROLE_SYSTEM)
				.antMatchers("/swagger-resources/configuration/ui").hasAuthority(ROLE_SYSTEM)
				.antMatchers("/swagger-ui/index.html").hasAuthority(ROLE_SYSTEM);

	}

	@Bean
	public SecurityEvaluationContextExtension securityEvaluationContextExtension() {
		return new SecurityEvaluationContextExtension();
	}
}
