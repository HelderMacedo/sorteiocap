package br.sorteioapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.sorteioapp.util.NaiveXSSFilter;

/**
 * A Regional.
 */
@Entity
@Table(name = "arearegional")
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class AreaRegional extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String COL_NOME = "nome";

	@Id
	@Column(name = "area_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = COL_NOME, nullable = false)
	@NotNull
	@Size(max = 64)
	private String nome;

	@Column(name = "responsavel")
	@NotNull
	@Size(max = 64)
	private String responsavel;

	@Column(name = "cpf", nullable = false)
	@NotNull
	// @Pattern(regexp="(\\d{3})[.](\\d{3})[.](\\d{3})-(\\d{2})")
	private String cpf;

	@Column(name = "telefone")
	@Size(max = 16)
	private String telefone;
 

	@Column(name = "ativo")
	private boolean ativo;

	@OneToOne
	@JoinColumn(name = "usuario_id")
	private User usuario;

	@Column(name = "percentualrepasse")
	private float percentualRepasse;

	@ManyToOne
	@JoinColumn(name = "regional_id", nullable = false)
	private Regional regional;

	public AreaRegional() {
		
	}

	public AreaRegional(Long idarea) {
		this();
		this.id = idarea;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public Regional getRegional() {
		return regional;
	}

	public void setRegional(Regional regional) {
		this.regional = regional;
	}

	public AreaRegional nome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null) this.nome = this.nome.toUpperCase();		
		return this;
	}

	public void setNome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null) this.nome = this.nome.toUpperCase();		
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		AreaRegional area = (AreaRegional) o;
		if (area.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, area.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Regional{" + "id=" + id + ", nome='" + nome + "'" + '}';
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = NaiveXSSFilter.getSafeValue(responsavel);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = NaiveXSSFilter.getSafeValue(cpf);
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public User getUsuario() {
		return usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

}
