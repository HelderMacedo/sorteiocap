package br.sorteioapp.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A Bilhete.
 */
@Entity
@Table(name = Bilhete.TABLE_NAME, indexes = { @Index(columnList = "concurso_id, numero, regional_id, estabelecimento_id"),
		@Index(columnList = "concurso_id, numero, validado"), @Index(columnList = "concurso_id, online") })
@IdClass(Bilhete.BilheteKey.class)
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Bilhete implements Serializable {

	public static final String TABLE_NAME = "bilhete";
	public static final String COL_CONCURSO = "concurso_id";
	public static final String COL_NUMERO = "numero";
	public static final String COL_NUMORDER = "numorder";
    public static final String COL_NUMORDER_2 = "numorder2";
	public static final String COL_COMBINADICS = "combinadics";
	public static final String COL_BITSET = "bit_set";
	public static final String COL_TIPO = "tipo_id";
	public static final String COL_IDENTIFICACAO = "identificacao";
	public static final String COL_DEZENAS = "dezenas";
    public static final String COL_DEZENAS_2 = "dezenas2";

	public static final String COL_NUMERO_SORTEIO = "numero_sorteio";
	// public static final String COL_INT_NUM_SORTEIO = "intsorteio";

	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne
	@JoinColumn(name = COL_CONCURSO)
	@NotNull
	private Concurso concurso;

	@Id
	@Column(name = COL_NUMERO)
	@NotNull
	private Long numero;

	@Column(name = COL_NUMORDER)
	private Long numorder;

    @Column(name = COL_NUMORDER_2)
    private Long numorder2;

	@ManyToOne
	@JoinColumn(name = COL_TIPO)
	@NotNull
	private TipoBilhete tipo;

	@Column(name = COL_IDENTIFICACAO)
	@Size(min = 1, max = 20)
	private String identificacao;

	@Column(name = COL_DEZENAS)
	@Size(min = 1, max = 150)
	private String dezenas;

    @Column(name = COL_DEZENAS_2)
    @Size(min = 1, max = 150)
    private String dezenas2;

	private Boolean validado;

	@Column(name = "lote_validacao")
	private Long loteValidacao;

	@Column(name = "data_validacao")	
	private LocalDateTime dataValidacao ;
	
	private Boolean online;

	@Column(name = "estabelecimento_id")
	private Long estabelecimento_id;
	@Column(name = "regional_id")
	private Long regional_id;
	@Column(name = "numero_conjugado")
	private Long numeroConjugado;

    private Boolean ajustado;

	@Column(name = "area_id")
	private Long area_id;
	
	public Long getAreaId(){
		return this.area_id;
	}

	public void setAreaId(Long area_id){
		this.area_id = area_id;
	}

	public Long getEstabelecimento_id() {
		return estabelecimento_id;
	}

	public void setEstabelecimento_id(Long estabelecimento_id) {
		this.estabelecimento_id = estabelecimento_id;
	}
	
	public Long getRegional_id() {
		return regional_id;
	}

	public void setRegional_id(Long regional_id) {
		this.regional_id = regional_id;
	}

	public Long getNumeroConjugado() {
		return numeroConjugado;
	}

	public void setNumeroConjugado(Long numero_conjugado) {
		this.numeroConjugado = numero_conjugado;
	}

	public Long getNumero() {
		return numero;
	}

	public Bilhete numero(Long numero) {
		this.numero = numero;
		return this;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Concurso getConcurso() {
		return concurso;
	}

	public Bilhete concurso(Concurso concurso) {
		this.concurso = concurso;
		return this;
	}

	public void setConcurso(Concurso concurso) {
		this.concurso = concurso;
	}

	public Long getLoteValidacao() {
		return loteValidacao;
	}

	public void setLoteValidacao(Long loteValidacao) {
		this.loteValidacao = loteValidacao;
	}

	public Boolean getOnline() {
		return online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}

    public Long getNumorder2() {
        return numorder2;
    }

    public void setNumorder2(Long numorder2) {
        this.numorder2 = numorder2;
    }

    public String getDezenas2() {
        return dezenas2;
    }

	public Bilhete dezenas(String dezenas){
		this.dezenas = dezenas;
		return this;
	}

    public void setDezenas2(String dezenas2) {
        this.dezenas2 = dezenas2;
    }

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Bilhete bilhete = (Bilhete) o;
		if (bilhete.numero == null || numero == null || concurso == null || concurso.getId() == null
				|| bilhete.concurso == null || bilhete.concurso.getId() == null) {
			return false;
		}
		return Objects.equals(concurso, bilhete.concurso) && Objects.equals(numero, bilhete.numero);
	}

	@Override
	public int hashCode() {
		// como as comparações serão geralmente entre bilhetes de um mesmo concurso,
		// colocar o hashcode baseado apenas no numero para evitar muitas colisões;
		return (int) (numero ^ (numero >> 32));
	}

	@Override
	public String toString() {
		return "Bilhete{" + "concurso=" + concurso.getId() + ", numero='" + numero + "'" + ", tipo='" + getTipo() + "'"
				+ '}';
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public Long getNumorder() {
		return numorder;
	}

	public void setNumorder(Long numorder) {
		this.numorder = numorder;
	}

	public void setTipo(TipoBilhete tipo) {
		this.tipo = tipo;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public String getDezenas() {
		return dezenas;
	}

	public void setDezenas(String dezenas) {
		this.dezenas = dezenas;
	}

	public Boolean getValidado() {
		return validado;
	}

	public boolean isValidado() {
		return validado != null && validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

    public LocalDateTime getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(LocalDateTime dataValidacao) {
        this.dataValidacao = dataValidacao;
    }

    public Boolean getAjustado() {
        return ajustado;
    }

    public void setAjustado(Boolean ajustado) {
        this.ajustado = ajustado;
    }

    public static class BilheteKey implements Serializable {
		public Concurso concurso;
		public Long numero;
		public BilheteKey() {}
		public BilheteKey(Concurso conc, Long num) {
			this.concurso = conc;
			this.numero = num;
		}
		@Override
		public boolean equals(Object o) {
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;

			BilheteKey that = (BilheteKey) o;

			if (concurso != null ? !concurso.equals(that.concurso) : that.concurso != null)
				return false;
			return numero != null ? numero.equals(that.numero) : that.numero == null;
		}

		@Override
		public int hashCode() {
			int result = concurso != null ? concurso.hashCode() : 0;
			result = 31 * result + (numero != null ? numero.hashCode() : 0);
			return result;
		}
	}
}
