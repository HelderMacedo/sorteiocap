package br.sorteioapp.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.DynamicInsert;

/**
 * A Concurso.
 */
@Entity
@DynamicInsert
@Table(name = "concurso")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Concurso extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "concurso_id")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "tipo_id")
	@NotNull
	private TipoBilhete tipo;

	@Column(name = "ano")
	@NotNull
	private int ano;

	@Column(name = "num_sorteio")
	@NotNull
	private int numSorteio;

    @Column(name = "data_sorteio")
    private LocalDate dataSorteio;
    
	@JsonIgnore
	@Column(name = "chave_criptografica")
	@NotNull
	private String chaveCriptografica;

	@Column(name = "data_inicio_vigencia")
	@NotNull
	private ZonedDateTime dataInicioVigencia;

	@Column(name = "data_fim_vigencia")
	@NotNull
	private ZonedDateTime dataFimVigencia;

	@Column(name = "sequencia_lote")
	private Long sequenciaLote;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "concurso_id", referencedColumnName = "concurso_id")
	private Set<LoteGeracaoBilhete> lotebilhetes = new HashSet<>();

    @Column(name = "perc_ajuste")
    private Integer percAjuste;

    @Column(name = "Sincronizado")
    private Boolean sincronizado;

    @Column(name = "perc_fixo_regional")
    private Double percentualFixoRegional;

    @Column(name = "qte_premios")
    private Integer quantidadePremios;

    @Column(name = "premio_01")
    private String premio1;

    @Column(name = "premio_02")
    private String premio2;

    @Column(name = "premio_03")
    private String premio3;

    @Column(name = "premio_04")
    private String premio4;

    @Column(name = "premio_05")
    private String premio5;

    @Column(name = "dupla_chance")
    private Boolean duplaChance;

    @Column(name = "lim_faixa_comissao_reg1")
    private Float limiteFaixaComissaoRegional1;

    @Column(name = "lim_faixa_comissao_est1")
    private Float limiteFaixaComissaoEstabelecimento1;

    @Column(name = "lim_faixa_comissao_est2")
    private Float limiteFaixaComissaoEstabelecimento2;

	@Transient
	private Long quantidadeBilhetes;

	private transient byte[] byteChaveCriptografica;

	public Concurso() {
	}

	public Concurso(Integer id) {
		this.id = id;
	}

    public Boolean getDuplaChance() {
        return (duplaChance==null)?Boolean.FALSE:duplaChance;
    }

    public void setDuplaChance(Boolean duplaChance) {
        this.duplaChance = duplaChance;
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(LocalDate dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public void setTipo(TipoBilhete tipo) {
		this.tipo = tipo;
	}

	public String getChaveCriptografica() {
		return chaveCriptografica;
	}

	public Concurso chaveCriptografica(String chaveCriptografica) {
		this.chaveCriptografica = chaveCriptografica;
		return this;
	}

	public void setChaveCriptografica(String chaveCriptografica) {
		this.chaveCriptografica = chaveCriptografica;
	}

	@JsonIgnore
	public byte[] getByteChaveCriptografica() {
		if (byteChaveCriptografica == null) {
			// O tamanho da chave indefere para para o algoritmo FPE. Assim, o custo abaixo,
			// torna-se denecessário.
			// byteChaveCriptografica = Base64.getDecoder().decode(chaveCriptografica);
			byteChaveCriptografica = chaveCriptografica.getBytes();
		}
		return byteChaveCriptografica;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Concurso concurso = (Concurso) o;
		if (concurso.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, concurso.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Concurso{" + "id=" + id +
		// ", chaveCriptografica='" + chaveCriptografica + "'" +
				", tipo='" + tipo + "'" + '}';
	}

	public Set<LoteGeracaoBilhete> getLotebilhetes() {
		return lotebilhetes;
	}

	public void setLotebilhetes(Set<LoteGeracaoBilhete> lotebilhetes) {
		this.lotebilhetes = lotebilhetes;
	}

	public Long getSequenciaLote() {
		return sequenciaLote;
	}

	public void setSequenciaLote(Long sequenciaLote) {
		this.sequenciaLote = sequenciaLote;
	}

	public ZonedDateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(ZonedDateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public ZonedDateTime getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(ZonedDateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getNumSorteio() {
		return numSorteio;
	}

	public void setNumSorteio(int numSorteio) {
		this.numSorteio = numSorteio;
	}

	public void setQuantidadeBilhetes(Long qtd){
		this.quantidadeBilhetes = qtd;
	}
	public Long getQuantidadeBilhetes(){
		return this.quantidadeBilhetes;
	}

    public Integer getPercAjuste() {
        return percAjuste;
    }

    public void setPercAjuste(Integer perecAjuste) {
        this.percAjuste = perecAjuste;
    }

    public Boolean getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(Boolean sincronizado) {
        this.sincronizado = sincronizado;
    }

    public Double getPercentualFixoRegional() {
        return percentualFixoRegional;
    }

    public void setPercentualFixoRegional(Double percentualFixoRegional) {
        this.percentualFixoRegional = percentualFixoRegional;
    }

    public Integer getQuantidadePremios() {
        return (quantidadePremios == null)?0:quantidadePremios;
    }

    public void setQuantidadePremios(Integer quantidadePremios) {
	    if (quantidadePremios == null || quantidadePremios < 0 )
            quantidadePremios = 0;
	    else if (quantidadePremios > 5)
            quantidadePremios = 5;
        this.quantidadePremios = quantidadePremios;
    }

    public String getPremio1() {
        return premio1;
    }

    public void setPremio1(String premio1) {
        this.premio1 = premio1;
    }

    public String getPremio2() {
        return premio2;
    }

    public void setPremio2(String premio2) {
        this.premio2 = premio2;
    }

    public String getPremio3() {
        return premio3;
    }

    public void setPremio3(String premio3) {
        this.premio3 = premio3;
    }

    public String getPremio4() {
        return premio4;
    }

    public void setPremio4(String premio4) {
        this.premio4 = premio4;
    }

    public String getPremio5() {
        return premio5;
    }

    public void setPremio5(String premio5) {
        this.premio5 = premio5;
    }

    public float getLimiteFaixaComissaoRegional1() {
        return limiteFaixaComissaoRegional1==null?0:limiteFaixaComissaoRegional1;
    }

    public void setLimiteFaixaComissaoRegional1(float limiteFaixaComissaoRegional1) {
        this.limiteFaixaComissaoRegional1 = limiteFaixaComissaoRegional1;
    }

    public float getLimiteFaixaComissaoEstabelecimento1() {
        return limiteFaixaComissaoEstabelecimento1==null?0:limiteFaixaComissaoEstabelecimento1;
    }

    public void setLimiteFaixaComissaoEstabelecimento1(float limiteFaixaComissaoEstabelecimento1) {
        this.limiteFaixaComissaoEstabelecimento1 = limiteFaixaComissaoEstabelecimento1;
    }

    public float getLimiteFaixaComissaoEstabelecimento2() {
        return limiteFaixaComissaoEstabelecimento2==null?0:limiteFaixaComissaoEstabelecimento2;
    }

    public void setLimiteFaixaComissaoEstabelecimento2(float limiteFaixaComissaoEstabelecimento2) {
        this.limiteFaixaComissaoEstabelecimento2 = limiteFaixaComissaoEstabelecimento2;
    }
}
