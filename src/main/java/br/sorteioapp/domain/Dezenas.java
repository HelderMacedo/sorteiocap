package br.sorteioapp.domain;

public class Dezenas {

    private Integer concurso;
    private Integer numero;
    private String dezenas;

    public void setConcurso(Integer concurso){
        this.concurso = concurso;
    }

    public void setNumero(Integer numero){
        this.numero = numero;
    }

    public void setDezenas(String dezenas){
        this.dezenas = dezenas;
    }

    public Dezenas concurso(Integer concurso){
        this.concurso = concurso;
        return this;
    }

    public Dezenas numero(Integer numero){
        this.numero = numero;
        return this;
    }

    public Dezenas dezenas(String dezenas){
        this.dezenas = dezenas;
        return this;
    }

}