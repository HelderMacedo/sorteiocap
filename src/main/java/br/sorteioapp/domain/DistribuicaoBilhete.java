package br.sorteioapp.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "distribuicao_bilhete",
	indexes = {@Index(columnList = "concurso_id, numInicio, numFim, data_modificacao, data_modificacao"),
	        @Index(columnList = "concurso_id, antiga_regional_id, nova_regional_id, data_modificacao")
	})
public class DistribuicaoBilhete implements Serializable {
    public final static char MOTIVO_DISTRIBUICAO_REGIONAL = 'R';
    public final static char MOTIVO_DISTRIBUICAO_ESTABELECIMENTO = 'E';
	public final static char MOTIVO_DISTRIBUICAO_SUBREGIONAL= 'S';
    public final static char MOTIVO_DEVOLUCAO = 'D';
    public final static char MOTIVO_REDISTRIBUICAO_CONTINUA = 'X';
    public final static char MOTIVO_REDISTRIBUICAO_NAO_CONTINUA = 'Y';
	



    @Id
    @Column(name="transferencia_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne
    @JoinColumn(name = "concurso_id", nullable = false)
    @NotNull
    private Concurso concurso;

    @LastModifiedBy
    @Column(name = "usuario_modificacao", length = 50, nullable = false)
    @NotNull
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "data_modificacao", nullable = false)
    @NotNull
    private ZonedDateTime lastModifiedDate = ZonedDateTime.now();
    
    @Column(name = "numInicio", nullable = false)
    @NotNull
    protected Long numeroInicio;

    @Column(name = "numFim", nullable = false)
    @NotNull
    protected Long numeroFim;
    
    @ManyToOne
    @JoinColumn(name="antigo_estabelecimento_id", referencedColumnName = "estabelecimento_id")
    private Estabelecimento antigoEstabelecimento;

    @ManyToOne
    @JoinColumn(name="antiga_regional_id", referencedColumnName = "regional_id")
    private Regional antigaRegional;

    @ManyToOne
    @JoinColumn(name="novo_estabelecimento_id", referencedColumnName = "estabelecimento_id")
    private Estabelecimento novoEstabelecimento;

    @ManyToOne
    @JoinColumn(name="nova_regional_id", referencedColumnName = "regional_id")
    private Regional novaRegional;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name="concurso_id", referencedColumnName = "concurso_id", insertable = false, updatable = false),
        @JoinColumn(name="numInicio", referencedColumnName = "numero", insertable = false, updatable = false)
    })
    private Bilhete bilheteInicio;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name="concurso_id", referencedColumnName = "concurso_id", insertable = false, updatable = false),
        @JoinColumn(name="numFim", referencedColumnName = "numero", insertable = false, updatable = false)
    })
    private Bilhete bilheteFim;

    @Column(name = "motivo")
    private Character motivoDistribuicao;
	

	@ManyToOne
    @JoinColumn(name="antiga_area_id", referencedColumnName = "area_id")
	private AreaRegional antigaArea;
	
	
	@ManyToOne
    @JoinColumn(name="nova_area_id", referencedColumnName = "area_id")
	private AreaRegional novaArea;

	public AreaRegional getAntigaArea(){
		return this.antigaArea;
	}

	public void setAntigaArea(AreaRegional antigaArea){
		this.antigaArea = antigaArea;
	}

	public AreaRegional getNovaArea(){
		return this.novaArea;
	}

	public void setNovaArea(AreaRegional novaArea){
		this.novaArea = novaArea;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Concurso getConcurso() {
		return concurso;
	}

	public void setConcurso(Concurso concurso) {
		this.concurso = concurso;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public ZonedDateTime getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Estabelecimento getAntigoEstabelecimento() {
		return antigoEstabelecimento;
	}

	public void setAntigoEstabelecimento(Estabelecimento antigoEstabelecimento) {
		this.antigoEstabelecimento = antigoEstabelecimento;
	}

	public Regional getAntigaRegional() {
		return antigaRegional;
	}

	public void setAntigaRegional(Regional antigaRegional) {
		this.antigaRegional = antigaRegional;
	}

	public Estabelecimento getNovoEstabelecimento() {
		return novoEstabelecimento;
	}

	public void setNovoEstabelecimento(Estabelecimento novoEstabelecimento) {
		this.novoEstabelecimento = novoEstabelecimento;
	}

	public Regional getNovaRegional() {
		return novaRegional;
	}

	public void setNovaRegional(Regional novaRegional) {
		this.novaRegional = novaRegional;
	}

	public Long getNumeroInicio() {
		return numeroInicio;
	}

	public void setNumeroInicio(Long numeroInicio) {
		this.numeroInicio = numeroInicio;
	}

	public Long getNumeroFim() {
		return numeroFim;
	}

	public void setNumeroFim(Long numeroFim) {
		this.numeroFim = numeroFim;
	}

	public Bilhete getBilheteInicio() {
		return bilheteInicio;
	}

	public void setBilheteInicio(Bilhete bilheteInicio) {
		this.bilheteInicio = bilheteInicio;
	}

	public Bilhete getBilheteFim() {
		return bilheteFim;
	}

	public void setBilheteFim(Bilhete bilheteFim) {
		this.bilheteFim = bilheteFim;
	}

    public Character getMotivoDistribuicao() {
        return motivoDistribuicao;
    }

    public void setMotivoDistribuicao(Character motivoDistribuicao) {
        this.motivoDistribuicao = motivoDistribuicao;
    }
}
