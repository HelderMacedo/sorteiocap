package br.sorteioapp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Table(name = "distribuicao_selo")
public class DistribuicaoSelo implements Serializable {

    public final static char OPERACAO_INSERCAO = 'I';
    public final static char OPERACAO_REMOCAO = 'R';

    @Id
    @Column(name="transferencia_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @ManyToOne
    @JoinColumn(name = "concurso_id", nullable = false)
    @NotNull
    private Concurso concurso;

    @LastModifiedBy
    @Column(name = "usuario_modificacao", length = 50, nullable = false)
    @NotNull
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "data_modificacao", nullable = false)
    @NotNull
    private ZonedDateTime lastModifiedDate = ZonedDateTime.now();

    @Column(name = "numInicio", nullable = false)
    @NotNull
    protected Integer numeroInicio;

    @Column(name = "numFim", nullable = false)
    @NotNull
    protected Integer numeroFim;

    @ManyToOne
    @JoinColumn(name="estabelecimento_id", referencedColumnName = "estabelecimento_id")
    private Estabelecimento estabelecimento;

    @ManyToOne
    @JoinColumn(name="regional_id", referencedColumnName = "regional_id")
    private Regional regional;

    @Column(name = "operacao", nullable = false)
    private Character operacao;

    public DistribuicaoSelo(){}
    public DistribuicaoSelo(Concurso concurso, Regional regional, Estabelecimento estabelecimento,
                            Integer numeroInicio, Integer numeroFim, Character operacao) {
        this.concurso = concurso;
        this.numeroInicio = numeroInicio;
        this.numeroFim = numeroFim;
        this.estabelecimento = estabelecimento;
        this.regional = regional;
        this.operacao = operacao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistribuicaoSelo that = (DistribuicaoSelo) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public ZonedDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(ZonedDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Integer getNumeroInicio() {
        return numeroInicio;
    }

    public void setNumeroInicio(Integer numeroInicio) {
        this.numeroInicio = numeroInicio;
    }

    public Integer getNumeroFim() {
        return numeroFim;
    }

    public void setNumeroFim(Integer numeroFim) {
        this.numeroFim = numeroFim;
    }

    public Estabelecimento getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public Regional getRegional() {
        return regional;
    }

    public void setRegional(Regional regional) {
        this.regional = regional;
    }


    public Character getOperacao() {
        return operacao;
    }

    public void setOperacao(Character operacao) {
        this.operacao = operacao;
    }
}
