package br.sorteioapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CacheConcurrencyStrategy;

import br.sorteioapp.util.NaiveXSSFilter;

/**
 * A Estabelecimento.
 */
@Entity
@Table(name = "estabelecimento", indexes = { @Index(columnList = "endereco_cidade") }, uniqueConstraints = {
		@UniqueConstraint(columnNames = { "regional_id", "padrao" }) })
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Estabelecimento extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "estabelecimento_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "cpf", nullable = false)
	@NotNull
	private String cpf;

	@Column(name = "nome", nullable = false)
	@NotNull
	@Size(max = 128)
	private String nome;

	@Column(name = "responsavel")
	@Size(max = 64)
	private String responsavel;

	@Column(name = "telefone")
	@Size(max = 16)
	private String telefone;

	/*
	 * @Column(name = "email")
	 * 
	 * @Pattern(regexp = "^(.+)@(.+)$")
	 * 
	 * @Size(min=5, max=100) private String email;
	 */

	@Column(name = "endereco_logradouro")
	@Size(max = 128)
	private String logradouro;

	@Column(name = "endereco_numero")
	@Size(max = 8)
	@Pattern(regexp = "^[0-9]*$")
	private String numero;

	@Column(name = "endereco_bairro", nullable = false)
	@NotNull
	@Size(max = 32)
	private String bairro;

	@Column(name = "endereco_cidade", nullable = false)
	@NotNull
	@Size(max = 32)
	private String cidade;

	@Column(name = "endereco_uf", nullable = false)
	@NotNull
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "endereco_cep")
	@Pattern(regexp = "(\\d{8})")
	private String cep;

	@Column(name = "ponto_referencia")
	private String pontoReferencia;

	@Column(name = "ativo")
	private boolean ativo;

	@ManyToOne
	@JoinColumn(name = "regional_id", nullable = false)
	private Regional regional;

	@ManyToOne
	@JoinColumn(name = "area_id")
	private AreaRegional area;

	@Column(name = "percentualrepasse")
	private float percentualRepasse;

	@Column(name = "padrao")
	private Boolean padraoRegional;

	@Column(name = "online")
	private Boolean online;

	public Estabelecimento() {
	}

	public Estabelecimento(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isPadraoRegional() {
		return Boolean.TRUE.equals(padraoRegional);
	}

	public void setPadraoRegional(Boolean padraoRegional) {
		this.padraoRegional = (Boolean.TRUE.equals(padraoRegional) ? Boolean.TRUE : null);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = NaiveXSSFilter.getSafeValue(cpf);
	}

	public String getNome() {
		return nome;
	}

	public Estabelecimento nome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null)
			this.nome = this.nome.toUpperCase();
		return this;
	}

	public void setNome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null)
			this.nome = this.nome.toUpperCase();
	}

	public String getTelefone() {
		return telefone;
	}

	public Estabelecimento telefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
		return this;
	}

	public void setTelefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
	}

	public String getNumero() {
		return numero;
	}

	public Estabelecimento numero(String numero) {
		this.numero = NaiveXSSFilter.getSafeValue(numero);
		return this;
	}

	public void setNumero(String numero) {
		this.numero = NaiveXSSFilter.getSafeValue(numero);
	}

	public String getBairro() {
		return bairro;
	}

	public Estabelecimento bairro(String bairro) {
		this.bairro = NaiveXSSFilter.getSafeValue(bairro);
		return this;
	}

	public void setBairro(String bairro) {
		this.bairro = NaiveXSSFilter.getSafeValue(bairro);
	}

	public String getCidade() {
		return cidade;
	}

	public Estabelecimento cidade(String cidade) {
		this.cidade = NaiveXSSFilter.getSafeValue(cidade);
		return this;
	}

	public void setCidade(String cidade) {
		this.cidade = NaiveXSSFilter.getSafeValue(cidade);
	}

	public String getCep() {
		return cep;
	}

	public Estabelecimento cep(String cep) {
		this.cep = NaiveXSSFilter.getSafeValue(cep);
		return this;
	}

	public void setCep(String cep) {
		this.cep = NaiveXSSFilter.getSafeValue(cep);
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public Estabelecimento pontoReferencia(String pontoReferencia) {
		this.pontoReferencia = NaiveXSSFilter.getSafeValue(pontoReferencia);
		return this;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = NaiveXSSFilter.getSafeValue(pontoReferencia);
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = NaiveXSSFilter.getSafeValue(uf);
		if (this.uf != null)
			this.uf = this.uf.toUpperCase();
	}

	public AreaRegional getArea() {
		return area;
	}

	public void setArea(AreaRegional area) {
		this.area = area;
	}

	public Boolean getPadraoRegional() {
		return padraoRegional;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Estabelecimento estabelecimento = (Estabelecimento) o;
		if (estabelecimento.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), estabelecimento.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "Estabelecimento{" + "id=" + getId() + ", cpf='" + cpf + "'" + ", nome='" + nome + "'" + ", telefone='"
				+ telefone + "'" + ", logradouro='" + getLogradouro() + "'" + ", numero='" + numero + "'" + ", bairro='"
				+ bairro + "'" + ", cidade='" + cidade + "'" + ", cep='" + cep + "'" + ", pontoReferencia='"
				+ pontoReferencia + "'" + '}';
	}

	public Regional getRegional() {
		return regional;
	}

	public void setRegional(Regional regional) {
		this.regional = regional;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getOnline() {
		return this.online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = NaiveXSSFilter.getSafeValue(logradouro);
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
}
