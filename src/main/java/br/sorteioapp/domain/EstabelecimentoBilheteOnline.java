package br.sorteioapp.domain;

public class EstabelecimentoBilheteOnline {
    private Integer estabalecimento_id;
    private String nome;
    private String bairro;
    private String cidade;
    private String uf;
    private Integer bilhetesDistribuidos;
    private Integer bilhetesValidados;
    private Integer bilhetesDistribuidosOnline;
    private Integer validados_on;
    private Integer validados_off;
    private Integer distribuidos;

    public Integer getBilhetesDistribuidos() {
        return this.bilhetesDistribuidos;
    }

    public void setBilhetesDistribuidos(Integer bilhetesDistribuidos) {
        this.bilhetesDistribuidos = bilhetesDistribuidos;
    }

    public Integer getBilhetesValidados() {
        return this.bilhetesValidados;
    }

    public void setBilhetesValidados(Integer bilhetesValidados) {
        this.bilhetesValidados = bilhetesValidados;
    }

    public Integer getBilhetesDistribuidosOnline() {
        return this.bilhetesDistribuidosOnline;
    }

    public void setBilhetesDistribuidosOnline(Integer bilhetesDistribuidosOnline) {
        this.bilhetesDistribuidosOnline = bilhetesDistribuidosOnline;
    }

    public Integer getEstabalecimento_id() {
        return this.estabalecimento_id;
    }

    public void setEstabalecimento_id(Integer estabalecimento_id) {
        this.estabalecimento_id = estabalecimento_id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getBairro() {
        return this.bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return this.cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return this.uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Integer getValidados_on() {
        return this.validados_on;
    }

    public void setValidados_on(Integer validados_on) {
        this.validados_on = validados_on;
    }

    public Integer getValidados_off() {
        return this.validados_off;
    }

    public void setValidados_off(Integer validados_off) {
        this.validados_off = validados_off;
    }

    public Integer getDistribuidos() {
        return this.distribuidos;
    }

    public void setDistribuidos(Integer distribuidos) {
        this.distribuidos = distribuidos;
    }

}