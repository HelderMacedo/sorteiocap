package br.sorteioapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import br.sorteioapp.util.NaiveXSSFilter;

/**
 * A GrupoLancamentoFinanceiro.
 */
@Entity
@Table(name = "grupo_lancamento_financeiro",
	uniqueConstraints={
		    @UniqueConstraint(columnNames = {"nome"})})

// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class GrupoLancamentoFinanceiro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "nome")
	@NotNull
	private String nomeGrupo;

    @ManyToOne
    @JoinColumn(name = "tipo_operacao_id")
    @NotNull
    private TipoOperacaoLancamentoFinanceiro tipoOperacao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomeGrupo() {
		return nomeGrupo;
	}

	public void setNomeGrupo(String nomeGrupo) {
		this.nomeGrupo = NaiveXSSFilter.getSafeValue(nomeGrupo);
	}

	public TipoOperacaoLancamentoFinanceiro getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(TipoOperacaoLancamentoFinanceiro tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		GrupoLancamentoFinanceiro grupoLancamento = (GrupoLancamentoFinanceiro) o;
		if (grupoLancamento.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), grupoLancamento.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "GrupoLancamentoFinanceiro{" + "id=" + getId() + "}";
	}
}
