package br.sorteioapp.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "historico_comissao_estabelecimento",
    indexes = { @Index(columnList = "concurso_id, regional_id, estabelecimento_id")})
public class HistoricoComissaoEstabelecimento implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp ;

    @Column(name = "regional_id")
    private Long regional_id;

    @Column(name = "regional_nome")
    private String regionalNome;

    @Column(name = "estabelecimento_id")
    private Long estabelecimento_id;

    @Column(name = "estabelecimento_nome")
    private String estabelecimentoNome;

    @Column(name = "estabelecimento_bairro")
    private String estabelecimentoBairro;

    @Column(name = "estabelecimento_cidade")
    private String estabelecimentoCidade;

    @Column(name = "estabelecimento_uf")
    private String estabelecimentoUF;

    @Column(name = "concurso_id")
    private Long concursoId;

    @Column(name = "faixa_inicio")
    private Long faixaInicio;

    @Column(name = "faixa_fim")
    private Long faixaFim;

    @Column(name = "valor_comissao")
    private Double valorComissao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getRegional_id() {
        return regional_id;
    }

    public void setRegional_id(Long regional_id) {
        this.regional_id = regional_id;
    }

    public String getRegionalNome() {
        return regionalNome;
    }

    public void setRegionalNome(String regionalNome) {
        this.regionalNome = regionalNome;
    }

    public Long getEstabelecimento_id() {
        return estabelecimento_id;
    }

    public void setEstabelecimento_id(Long estabelecimento_id) {
        this.estabelecimento_id = estabelecimento_id;
    }

    public String getEstabelecimentoNome() {
        return estabelecimentoNome;
    }

    public void setEstabelecimentoNome(String estabelecimentoNome) {
        this.estabelecimentoNome = estabelecimentoNome;
    }

    public String getEstabelecimentoBairro() {
        return estabelecimentoBairro;
    }

    public void setEstabelecimentoBairro(String estabelecimentoBairro) {
        this.estabelecimentoBairro = estabelecimentoBairro;
    }

    public String getEstabelecimentoCidade() {
        return estabelecimentoCidade;
    }

    public void setEstabelecimentoCidade(String estabelecimentoCidade) {
        this.estabelecimentoCidade = estabelecimentoCidade;
    }

    public String getEstabelecimentoUF() {
        return estabelecimentoUF;
    }

    public void setEstabelecimentoUF(String estabelecimentoUF) {
        this.estabelecimentoUF = estabelecimentoUF;
    }

    public Long getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(Long concursoId) {
        this.concursoId = concursoId;
    }

    public Long getFaixaInicio() {
        return faixaInicio;
    }

    public void setFaixaInicio(Long faixaInicio) {
        this.faixaInicio = faixaInicio;
    }

    public Long getFaixaFim() {
        return faixaFim;
    }

    public void setFaixaFim(Long faixaFim) {
        this.faixaFim = faixaFim;
    }

    public Double getValorComissao() {
        return valorComissao;
    }

    public void setValorComissao(Double valorComissao) {
        this.valorComissao = valorComissao;
    }
}
