package br.sorteioapp.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "historico_comissao_regional",
    indexes = { @Index(columnList = "concurso_id, regional_id")})
public class HistoricoComissaoRegional implements Serializable {


    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp ;

    @Column(name = "regional_id")
    private Long regional_id;

    @Column(name = "regional_nome")
    private String regionalNome;

    @Column(name = "concurso_id")
    private Long concursoId;

    @Column(name = "faixa_inicio")
    private Long faixaInicio;

    @Column(name = "faixa_fim")
    private Long faixaFim;

    @Column(name = "valor_comissao")
    private Double valorComissao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(Long concursoId) {
        this.concursoId = concursoId;
    }

    public Long getRegional_id() {
        return regional_id;
    }

    public void setRegional_id(Long regional_id) {
        this.regional_id = regional_id;
    }

    public String getRegionalNome() {
        return regionalNome;
    }

    public void setRegionalNome(String regionalNome) {
        this.regionalNome = regionalNome;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getFaixaInicio() {
        return faixaInicio;
    }

    public void setFaixaInicio(Long faixaInicio) {
        this.faixaInicio = faixaInicio;
    }

    public Long getFaixaFim() {
        return faixaFim;
    }

    public void setFaixaFim(Long faixaFim) {
        this.faixaFim = faixaFim;
    }

    public Double getValorComissao() {
        return valorComissao;
    }

    public void setValorComissao(Double valorComissao) {
        this.valorComissao = valorComissao;
    }
}
