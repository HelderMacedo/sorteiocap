package br.sorteioapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import br.sorteioapp.util.NaiveXSSFilter;

/**
 * A Jogador.
 */
@Embeddable
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Jogador implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "jogador_nome")
	private String nome;

	@Column(name = "jogador_cpf")
	private String cpf;

	@Column(name = "jogador_telefone")
	private String telefone;

	public String getNome() {
		return nome;
	}

	public Jogador nome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		return this;
	}

	public void setNome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
	}

	public String getCpf() {
		return cpf;
	}

	public Jogador cpf(String cpf) {
		this.cpf = NaiveXSSFilter.getSafeValue(cpf);
		return this;
	}

	public void setCpf(String cpf) {
		this.cpf = NaiveXSSFilter.getSafeValue(cpf);
	}

	public String getTelefone() {
		return telefone;
	}

	public Jogador telefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
		return this;
	}

	public void setTelefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
	}

	@Override
	public String toString() {
		return "Jogador{" + ", nome='" + nome + "'" + ", cpf='" + cpf + "'" + ", telefone='" + telefone + "'" + '}';
	}

}
