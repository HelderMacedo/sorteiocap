package br.sorteioapp.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.sorteioapp.util.NaiveXSSFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A LancamentoFinanceiro.
 */
@Entity
@Table(name = "lancamento_financeiro")
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class LancamentoFinanceiro implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "grupo_lancamento_id")
	@NotNull
	private GrupoLancamentoFinanceiro grupoLancamento;

	@Column(name = "descricao")
	private String descricao;

    @Column(name = "dataLancamento", nullable = false)
    private LocalDate dataLancamento;

    @Column(name = "valor", nullable = false)
	private float valor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = NaiveXSSFilter.getSafeValue(descricao);
	}

	public GrupoLancamentoFinanceiro getGrupoLancamento() {
		return grupoLancamento;
	}

	public void setGrupoLancamento(GrupoLancamentoFinanceiro grupoLancamento) {
		this.grupoLancamento = grupoLancamento;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(LocalDate dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    @Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		LancamentoFinanceiro lancamento = (LancamentoFinanceiro) o;
		if (lancamento.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), lancamento.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "LancamentoFinanceiro{" + "id=" + getId() + ", valor='" + getValor() + "'" + ", dataLancamento=" + getDataLancamento() + "}";
	}
}
