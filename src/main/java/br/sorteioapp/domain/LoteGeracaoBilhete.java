package br.sorteioapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lotegeracaobilhete")
public class LoteGeracaoBilhete extends AbstractAuditingEntity implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(name = "concurso_id", nullable = false)
	private int concursoId;

	@ManyToOne
	@JoinColumn(name = "tipo_id", nullable = false)
	protected TipoBilhete tipo;

	@Column(name = "numinicio", nullable = false)
	protected Long numeroInicial;

	@Column(name = "numfim", nullable = false)
	protected Long numeroFinal;

	@OneToOne
	@JoinColumns({
			@JoinColumn(name = "concurso_id", referencedColumnName = "concurso_id", insertable = false, updatable = false),
			@JoinColumn(name = "numinicio", referencedColumnName = "numero", insertable = false, updatable = false) })
	private Bilhete bilheteInicial;

	@OneToOne
	@JoinColumns({
			@JoinColumn(name = "concurso_id", referencedColumnName = "concurso_id", insertable = false, updatable = false),
			@JoinColumn(name = "numfim", referencedColumnName = "numero", insertable = false, updatable = false) })
	private Bilhete bilheteFinal;

	private Boolean online;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public void setTipo(TipoBilhete tipo) {
		this.tipo = tipo;
	}

	public Long getNumeroInicial() {
		return numeroInicial;
	}

	public void setNumeroInicial(Long numeroInicial) {
		this.numeroInicial = numeroInicial;
	}

	public Long getNumeroFinal() {
		return numeroFinal;
	}

	public void setNumeroFinal(Long numeroFinal) {
		this.numeroFinal = numeroFinal;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		LoteGeracaoBilhete bloco = (LoteGeracaoBilhete) o;
		if (bloco.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, bloco.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	public int getConcursoId() {
		return concursoId;
	}

	public void setConcursoId(int concursoId) {
		this.concursoId = concursoId;
	}

	public Bilhete getBilheteInicial() {
		return bilheteInicial;
	}

	public void setBilheteInicial(Bilhete bilheteInicial) {
		this.bilheteInicial = bilheteInicial;
	}

	public Bilhete getBilheteFinal() {
		return bilheteFinal;
	}

	public void setBilheteFinal(Bilhete bilheteFinal) {
		this.bilheteFinal = bilheteFinal;
	}

	public Boolean getOnline() {
		return online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}
}
