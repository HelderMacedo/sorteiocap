package br.sorteioapp.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.sorteioapp.util.NaiveXSSFilter;
import org.hibernate.annotations.DynamicInsert;

/**
 * A Regional.
 */
@Entity
@Table(name = "regional")
@DynamicInsert
@Inheritance
@DiscriminatorColumn(name = "tipo_regional", discriminatorType = DiscriminatorType.STRING, length = 10)
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Regional extends AbstractAuditingEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String COL_NOME = "nome";

	@Id
	@Column(name = "regional_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = COL_NOME, nullable = false)
	@NotNull
	@Size(max = 64)
	private String nome;

	@Column(name = "responsavel")
	@NotNull
	@Size(max = 64)
	private String responsavel;

	@Column(name = "cpf", nullable = false)
	@NotNull
	// @Pattern(regexp="(\\d{3})[.](\\d{3})[.](\\d{3})-(\\d{2})")
	private String cpf;

    @ManyToOne
    @JoinColumn(name = "regiao_id")
    private Regiao regiao;

	@Column(name = "telefone")
	@Size(max = 16)
	private String telefone;
	/*
	 * @Column(name = "email")
	 * 
	 * @Pattern(regexp = "^(.+)@(.+)$")
	 * 
	 * @Size(min=5, max=100) private String email;
	 */
	@Column(name = "endereco_logradouro")
	@Size(max = 128)
	private String logradouro;

	@Column(name = "endereco_numero")
	@Size(max = 8)
	@Pattern(regexp = "^[0-9]*$")
	private String numero;

	@Column(name = "endereco_bairro")
	@Size(max = 32)
	private String bairro;

	@Column(name = "endereco_cidade")
	@Size(max = 32)
	private String cidade;

	@Column(name = "endereco_uf")
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "endereco_cep")
	@Pattern(regexp = "(\\d{8})")
	private String cep;

	@Column(name = "ativo")
	private boolean ativo;

	@OneToOne
	@JoinColumn(name = "usuario_id")
	private User usuario;

	@Column(name = "percentualrepasse")
	private float percentualRepasse;

    @Column(name = "sofre_ajuste")
    private boolean sofreAjuste;

    @Column(name = "val_comissao_reg_faixa1")
    private Float valorComissaoRegionalFaixa1;

    @Column(name = "val_comissao_reg_faixa2")
    private Float valorComissaoRegionalFaixa2;

    @Column(name = "val_comissao_est_faixa1")
    private Float valorComissaoEstabelecimenotFaixa1;

    @Column(name = "val_comissao_est_faixa2")
    private Float valorComissaoEstabelecimenotFaixa2;

    @Column(name = "val_comissao_est_faixa3")
    private Float valorComissaoEstabelecimenotFaixa3;

	public Regional() {
		super();
	}

	public Regional(Long idregional) {
		this();
		this.id = idregional;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public Regional nome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null) this.nome = this.nome.toUpperCase();		
		return this;
	}

	public void setNome(String nome) {
		this.nome = NaiveXSSFilter.getSafeValue(nome);
		if (this.nome != null) this.nome = this.nome.toUpperCase();		
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = NaiveXSSFilter.getSafeValue(uf);
		if (this.uf != null) this.uf = this.uf.toUpperCase();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Regional reg = (Regional) o;
		if (reg.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, reg.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Regional{" + "id=" + id + ", nome='" + nome + "'" + '}';
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = NaiveXSSFilter.getSafeValue(responsavel);
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = NaiveXSSFilter.getSafeValue(cpf);
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = NaiveXSSFilter.getSafeValue(telefone);
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = NaiveXSSFilter.getSafeValue(logradouro);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = NaiveXSSFilter.getSafeValue(numero);
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = NaiveXSSFilter.getSafeValue(bairro);
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = NaiveXSSFilter.getSafeValue(cidade);
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = NaiveXSSFilter.getSafeValue(cep);
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public User getUsuario() {
		return usuario;
	}

	public void setUsuario(User usuario) {
		this.usuario = usuario;
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

    public boolean isSofreAjuste() {
        return sofreAjuste;
    }

    public void setSofreAjuste(boolean sofreAjuste) {
        this.sofreAjuste = sofreAjuste;
    }

    public float getValorComissaoRegionalFaixa1() {
        return valorComissaoRegionalFaixa1==null?0:valorComissaoRegionalFaixa1;
    }

    public void setValorComissaoRegionalFaixa1(float valorComissaoRegionalFaixa1) {
        this.valorComissaoRegionalFaixa1 = valorComissaoRegionalFaixa1;
    }

    public float getValorComissaoRegionalFaixa2() {
        return valorComissaoRegionalFaixa2==null?0:valorComissaoRegionalFaixa2;
    }

    public void setValorComissaoRegionalFaixa2(float valorComissaoRegionalFaixa2) {
        this.valorComissaoRegionalFaixa2 = valorComissaoRegionalFaixa2;
    }

    public float getValorComissaoEstabelecimenotFaixa1() {
        return valorComissaoEstabelecimenotFaixa1==null?0:valorComissaoEstabelecimenotFaixa1;
    }

    public void setValorComissaoEstabelecimenotFaixa1(float valorComissaoEstabelecimenotFaixa1) {
        this.valorComissaoEstabelecimenotFaixa1 = valorComissaoEstabelecimenotFaixa1;
    }

    public float getValorComissaoEstabelecimenotFaixa2() {
        return valorComissaoEstabelecimenotFaixa2==null?0:valorComissaoEstabelecimenotFaixa2;
    }

    public void setValorComissaoEstabelecimenotFaixa2(float valorComissaoEstabelecimenotFaixa2) {
        this.valorComissaoEstabelecimenotFaixa2 = valorComissaoEstabelecimenotFaixa2;
    }

    public float getValorComissaoEstabelecimenotFaixa3() {
        return valorComissaoEstabelecimenotFaixa3==null?0:valorComissaoEstabelecimenotFaixa3;
    }

    public void setValorComissaoEstabelecimenotFaixa3(float valorComissaoEstabelecimenotFaixa3) {
        this.valorComissaoEstabelecimenotFaixa3 = valorComissaoEstabelecimenotFaixa3;
    }

	public Regiao getRegiao() {
        return regiao;
    }

    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }
}
