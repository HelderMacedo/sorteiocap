package br.sorteioapp.domain;

public class RegionalBilheteOnline {
    private Integer concurso_id;
    private Integer regional_id;
    private String  nome;
    private Integer distribuidos;
    private Integer validados_on;
    private Integer validados_off;

	public Integer getConcurso_id() {
		return this.concurso_id;
    }
    
    public RegionalBilheteOnline concurso_id(Integer concurso_id){
        this.concurso_id = concurso_id;
        return this;
    }

	public void setConcurso_id(Integer concurso_id) {
		this.concurso_id = concurso_id;
	}

	public Integer getRegional_id() {
		return this.regional_id;
    }
    
    public RegionalBilheteOnline regional_id(Integer regional_id){
        this.regional_id = regional_id;
        return this;
    }

	public void setRegional_id(Integer regional_id) {
		this.regional_id = regional_id;
	}

	public String getNome() {
		return this.nome;
    }
    
    public RegionalBilheteOnline nome(String nome){
        this.nome = nome;
        return this;
    }

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getDistribuidos() {
		return this.distribuidos;
    }
    
    public RegionalBilheteOnline distribuidos(Integer distribuidos){
        this.distribuidos = distribuidos;
        return this;
    }

	public void setDistribuidos(Integer distribuidos) {
		this.distribuidos = distribuidos;
	}

	public Integer getValidados_on() {
		return this.validados_on;
	}

    public RegionalBilheteOnline validados_on(Integer validados_on){
        this.validados_on = validados_on;
        return this;
    }

	public void setValidados_on(Integer validados_on) {
		this.validados_on = validados_on;
	}

	public Integer getValidados_off() {
		return this.validados_off;
    }
    
    public RegionalBilheteOnline validados_off(Integer validados_off){
        this.validados_off = validados_off;
        return this;
    }

	public void setValidados_off(Integer validados_off) {
		this.validados_off = validados_off;
	}



}