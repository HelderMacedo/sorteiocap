package br.sorteioapp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by rodrigo on 06/05/17.
 */
@Entity
@IdClass(Sorteio.SorteioKey.class)
public class Sorteio implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name="concurso_id", nullable = false)
    @NotNull
    private
    Concurso concurso;

    @Id
    @Column(name = "data_sorteio", nullable = false)
    @NotNull
    private LocalDate dataSorteio;

    @Id
    @Column(name = "sequencial_data", nullable = false)
    @NotNull
    private Integer sequencialDia;


    @Column(name = "numero_bilhete", nullable = false)
    @NotNull
    private Long numeroBilhete;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumns({
        @JoinColumn(name = "concurso_id", referencedColumnName = "concurso_id", insertable = false, updatable = false),
        @JoinColumn(name = "numero_bilhete", referencedColumnName = "numero", insertable = false, updatable = false)
    })
    private Bilhete bilheteSorteado;

    @Column(name = "tipo_sorteio", nullable = false)
    @NotNull
    private Integer tipoSorteio;

    public Concurso getConcurso() {
        return concurso;
    }

    public void setConcurso(Concurso concurso) {
        this.concurso = concurso;
    }

    public LocalDate getDataSorteoi() {
        return dataSorteio;
    }

    public void setDataSorteoi(LocalDate dataSorteoi) {
        this.dataSorteio = dataSorteoi;
    }

    public Integer getSequencialDia() {
        return sequencialDia;
    }

    public void setSequencialDia(Integer sequencialDia) {
        this.sequencialDia = sequencialDia;
    }

    public Long getNumeroBilhete() {
        return numeroBilhete;
    }

    public void setNumeroBilhete(Long numeroBilhete) {
        this.numeroBilhete = numeroBilhete;
    }

    public Bilhete getBilheteSorteado() {
        return bilheteSorteado;
    }

    public void setBilheteSorteado(Bilhete bilheteSorteado) {
        this.bilheteSorteado = bilheteSorteado;
    }

    public Integer getTipoSorteio() {
        return tipoSorteio;
    }

    public void setTipoSorteio(Integer tipoSorteio) {
        this.tipoSorteio = tipoSorteio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sorteio sorteio = (Sorteio) o;

        if (concurso != null ? !concurso.equals(sorteio.concurso) : sorteio.concurso != null) return false;
        if (dataSorteio != null ? !dataSorteio.equals(sorteio.dataSorteio) : sorteio.dataSorteio != null) return false;
        return sequencialDia != null ? sequencialDia.equals(sorteio.sequencialDia) : sorteio.sequencialDia == null;
    }

    @Override
    public int hashCode() {
        int result = concurso != null ? concurso.hashCode() : 0;
        result = 31 * result + (dataSorteio != null ? dataSorteio.hashCode() : 0);
        result = 31 * result + (sequencialDia != null ? sequencialDia.hashCode() : 0);
        return result;
    }

    public static class SorteioKey implements Serializable {
        Concurso concurso;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SorteioKey that = (SorteioKey) o;

            if (concurso != null ? !concurso.equals(that.concurso) : that.concurso != null) return false;
            if (dataSorteio != null ? !dataSorteio.equals(that.dataSorteio) : that.dataSorteio != null) return false;
            return sequencialDia != null ? sequencialDia.equals(that.sequencialDia) : that.sequencialDia == null;
        }

        @Override
        public int hashCode() {
            int result = concurso != null ? concurso.hashCode() : 0;
            result = 31 * result + (dataSorteio != null ? dataSorteio.hashCode() : 0);
            result = 31 * result + (sequencialDia != null ? sequencialDia.hashCode() : 0);
            return result;
        }

        private LocalDate dataSorteio;
        private Integer sequencialDia;
    }
}
