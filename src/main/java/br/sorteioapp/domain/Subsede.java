package br.sorteioapp.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rodrigo on 10/06/17.
 */
@Entity
@DiscriminatorValue("SUBSEDE")
public class Subsede extends Regional{

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "superRegional")
    private Set<Vendedor> subRegionais = new HashSet<>();

    public Set<Vendedor> getSubRegionais() {
        return subRegionais;
    }

    public void setSubRegionais(Set<Vendedor> subRegionais) {
        this.subRegionais = subRegionais;
    }
}
