package br.sorteioapp.domain;

import br.sorteioapp.util.combinatoria.CombinatoriaUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;
import java.math.BigInteger;

/**
 * A Bilhete.
 INSERT INTO sorteioapp.tipobilhete (id,label, valor) VALUES (1,'R$ 0,25', 0.25);
 INSERT INTO sorteioapp.tipobilhete (id,label, valor) VALUES (2,'R$ 0,50', 0.50);
 INSERT INTO sorteioapp.tipobilhete (id,label, valor) VALUES (3,'R$ 1,00', 1);
 INSERT INTO sorteioapp.tipobilhete (id,label, valor) VALUES (4,'R$ 2,00', 2);
 */
@Entity
@Table(name = "tipobilhete")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TipoBilhete implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="tipo_id")
    private Long id;

    @Column(name = "label")
    private String label;

    @Column(name = "valor")
    private float valor;

    @Column(name = "quantidade_numeros")
    private Integer quantidadeNumeros;

    @Column(name = "numeros_bilhete")
    private Integer numerosBilhete;

    @Column(name = "ativo", nullable = false)
    private Boolean ativo;

    private transient BigInteger totalCombinacoes;

    public BigInteger getTotalCombinacoes(){
        if (totalCombinacoes == null){
            totalCombinacoes = CombinatoriaUtil.totalCombinacoes(quantidadeNumeros, numerosBilhete);
        }
        return totalCombinacoes;
    }

    public TipoBilhete(){}
    public TipoBilhete(long id){
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getQuantidadeNumeros() {
        return quantidadeNumeros;
    }

    public TipoBilhete quantidadeNumeros(Integer quantidadeNumeros) {
        this.quantidadeNumeros = quantidadeNumeros;
        return this;
    }

    public void setQuantidadeNumeros(Integer quantidadeNumeros) {
        this.quantidadeNumeros = quantidadeNumeros;
    }

    public Integer getNumerosBilhete() {
        return numerosBilhete;
    }

    public TipoBilhete numerosBilhete(Integer numerosBilhete) {
        this.numerosBilhete = numerosBilhete;
        return this;
    }

    public void setNumerosBilhete(Integer numerosBilhete) {
        this.numerosBilhete = numerosBilhete;
    }

    public Boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoBilhete tipo = (TipoBilhete) o;
        if (tipo.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, tipo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TipoBilhete{" +
            "id=" + id +
            ", label='" + label + "'" +
            '}';
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
}
