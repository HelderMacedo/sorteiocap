package br.sorteioapp.domain;

/**
 * Created by rodrigo on 22/05/17.
 */
public enum TipoIndisponibilidadeBilhete {
    Devolucao_Regional,
    Devolucao_Estabelecimento,
    Inutilizacao_bilhete
    /* Adicionar novos enums ao final (para evitar problemas no banco de dados)*/
}
