package br.sorteioapp.domain;

import br.sorteioapp.util.NaiveXSSFilter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "tipo_lancamento_financeiro")
public class TipoOperacaoLancamentoFinanceiro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;

    @Column(name = "nome")
    @NotNull
    public String nome;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = NaiveXSSFilter.getSafeValue(nome);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TipoOperacaoLancamentoFinanceiro tipooperacao = (TipoOperacaoLancamentoFinanceiro) o;
        if (tipooperacao.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipooperacao.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TipoOperacaoLancamentoFinanceiro{" + "id=" + getId() + "}";
    }
}
