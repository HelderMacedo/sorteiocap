package br.sorteioapp.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "totalizacao_concurso_estabelecimento",
    indexes = { @Index(columnList = "concurso_id, regional_id, estabelecimento_id")})
public class TotalizacaoConcursoEstabelecimento {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp ;

    @Column(name = "regional_id")
    private Long regional_id;

    @Column(name = "regional_nome")
    private String regionalNome;

    @Column(name = "estabelecimento_id")
    private Long estabelecimento_id;

    @Column(name = "estabelecimento_nome")
    private String estabelecimentoNome;

    @Column(name = "estabelecimento_bairro")
    private String estabelecimentoBairro;

    @Column(name = "estabelecimento_cidade")
    private String estabelecimentoCidade;

    @Column(name = "estabelecimento_uf")
    private String estabelecimentoUF;

    @Column(name = "concurso_id")
    private Long concursoId;

    @Column(name = "total_distribuidos")
    private Long totalDistribuidos;

    @Column(name = "total_validados")
    private Long totalValidados;

    @Column(name = "valor_total_arrecadado")
    private Double valorTotalArrecadadao;

    @Column(name = "valor_total_comissao")
    private Double valorTotalComissao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getRegional_id() {
        return regional_id;
    }

    public void setRegional_id(Long regional_id) {
        this.regional_id = regional_id;
    }

    public String getRegionalNome() {
        return regionalNome;
    }

    public void setRegionalNome(String regionalNome) {
        this.regionalNome = regionalNome;
    }

    public Long getEstabelecimento_id() {
        return estabelecimento_id;
    }

    public void setEstabelecimento_id(Long estabelecimento_id) {
        this.estabelecimento_id = estabelecimento_id;
    }

    public String getEstabelecimentoNome() {
        return estabelecimentoNome;
    }

    public void setEstabelecimentoNome(String estabelecimentoNome) {
        this.estabelecimentoNome = estabelecimentoNome;
    }

    public String getEstabelecimentoBairro() {
        return estabelecimentoBairro;
    }

    public void setEstabelecimentoBairro(String estabelecimentoBairro) {
        this.estabelecimentoBairro = estabelecimentoBairro;
    }

    public String getEstabelecimentoCidade() {
        return estabelecimentoCidade;
    }

    public void setEstabelecimentoCidade(String estabelecimentoCidade) {
        this.estabelecimentoCidade = estabelecimentoCidade;
    }

    public String getEstabelecimentoUF() {
        return estabelecimentoUF;
    }

    public void setEstabelecimentoUF(String estabelecimentoUF) {
        this.estabelecimentoUF = estabelecimentoUF;
    }

    public Long getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(Long concursoId) {
        this.concursoId = concursoId;
    }

    public Long getTotalDistribuidos() {
        return totalDistribuidos;
    }

    public void setTotalDistribuidos(Long totalDistribuidos) {
        this.totalDistribuidos = totalDistribuidos;
    }

    public Long getTotalValidados() {
        return totalValidados;
    }

    public void setTotalValidados(Long totalValidados) {
        this.totalValidados = totalValidados;
    }

    public Double getValorTotalArrecadadao() {
        return valorTotalArrecadadao;
    }

    public void setValorTotalArrecadadao(Double valorTotalArrecadadao) {
        this.valorTotalArrecadadao = valorTotalArrecadadao;
    }

    public Double getValorTotalComissao() {
        return valorTotalComissao;
    }

    public void setValorTotalComissao(Double valorTotalComissao) {
        this.valorTotalComissao = valorTotalComissao;
    }
}
