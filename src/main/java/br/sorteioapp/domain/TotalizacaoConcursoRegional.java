package br.sorteioapp.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "totalizacao_concurso_regional",
    indexes = { @Index(columnList = "concurso_id, regional_id")})
public class TotalizacaoConcursoRegional {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp ;

    @Column(name = "regional_id")
    private Long regional_id;

    @Column(name = "regional_nome")
    private String regionalNome;

    @Column(name = "concurso_id")
    private Long concursoId;

    @Column(name = "total_distribuidos")
    private Long totalDistribuidos;

    @Column(name = "total_validados")
    private Long totalValidados;

    @Column(name = "valor_total_arrecadado")
    private Double valorTotalArrecadadao;

    @Column(name = "valor_total_comissao")
    private Double valorTotalComissao;

    @Column(name = "valor_total_faturamento")
    private Double valorTotalFaturamento;

    public Double getValorTotalFaturamento() {
        return valorTotalFaturamento;
    }

    public void setValorTotalFaturamento(Double valorTotalFaturamento) {
        this.valorTotalFaturamento = valorTotalFaturamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Long getRegional_id() {
        return regional_id;
    }

    public void setRegional_id(Long regional_id) {
        this.regional_id = regional_id;
    }

    public String getRegionalNome() {
        return regionalNome;
    }

    public void setRegionalNome(String regionalNome) {
        this.regionalNome = regionalNome;
    }

    public Long getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(Long concursoId) {
        this.concursoId = concursoId;
    }

    public Long getTotalDistribuidos() {
        return totalDistribuidos;
    }

    public void setTotalDistribuidos(Long totalDistribuidos) {
        this.totalDistribuidos = totalDistribuidos;
    }

    public Long getTotalValidados() {
        return totalValidados;
    }

    public void setTotalValidados(Long totalValidados) {
        this.totalValidados = totalValidados;
    }

    public Double getValorTotalArrecadadao() {
        return valorTotalArrecadadao;
    }

    public void setValorTotalArrecadadao(Double valorTotalArrecadadao) {
        this.valorTotalArrecadadao = valorTotalArrecadadao;
    }

    public Double getValorTotalComissao() {
        return valorTotalComissao;
    }

    public void setValorTotalComissao(Double valorTotalComissao) {
        this.valorTotalComissao = valorTotalComissao;
    }
}
