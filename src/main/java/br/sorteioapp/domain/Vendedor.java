package br.sorteioapp.domain;

import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Created by rodrigo on 10/06/17.
 */
@Entity
@DiscriminatorValue("VENDEDOR")
public class Vendedor extends Regional{

    @ManyToOne
    @JoinColumn(name="super_regional", referencedColumnName="regional_id", insertable = false, updatable = false)
    @Where(clause="tipo_regional='SUBSEDE'")
    private Subsede superRegional;

    public Subsede getSuperRegional() {
        return superRegional;
    }

    public void setSuperRegional(Subsede superRegional) {
        this.superRegional = superRegional;
    }

}
