package br.sorteioapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import java.util.List;

import com.querydsl.core.types.dsl.StringPath;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.QAreaRegional;
import br.sorteioapp.domain.User;

/**
 * Created by rodrigo on 10/06/17.
 */
public interface AreaRegionalRepository extends JpaRepository<AreaRegional, Long>,
        QueryDslPredicateExecutor<AreaRegional>,
        QuerydslBinderCustomizer<QAreaRegional> {

    @Query("select ar from AreaRegional ar inner join ar.usuario u where u.login = ?1")
    AreaRegional findByLogin(String login);

    @Query("select ar from AreaRegional ar where ar.usuario = ?1")
    List<AreaRegional> findByUsuario(User user);

    @Query("select ar from AreaRegional ar where ar.ativo = 1")
    List<AreaRegional> findAllSubRegionalAtivos();

    @Override
    default public void customize(QuerydslBindings bindings, QAreaRegional root) {
        bindings.bind(String.class).first(
                (StringPath path, String value) -> path.containsIgnoreCase(value));
        bindings.excluding(root.usuario);
    }
}
