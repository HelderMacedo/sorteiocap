package br.sorteioapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Regional;

/**
 * Spring Data JPA repository for the Bilhete entity.
 */
public interface BilheteRepository extends JpaRepository<Bilhete, Bilhete.BilheteKey> {
	Page<Bilhete> findByConcurso(Concurso concurso, Pageable pageable);

	Bilhete findTopByConcursoOrderByNumeroDesc(Concurso concurso);

	@Query("select bil from Bilhete bil where concurso = ?1 and numero between ?2 and ?3 and estabelecimento_id != ?4")
	List<Bilhete> findAllByConcursoAndNumeroInicioNumeroFimBetween(Concurso concurso, Long numeroInicio, Long numeroFim,
			Long estabelecimento);

	@Query("select numero from Bilhete where concurso = ?1 and area_id = ?2")
	List<Bilhete> findAllBySubRegionalDistribuidos(Concurso concurso, Long area_id);

	@Query(nativeQuery = true, value = "select b.concurso_id, b.numero, b.dezenas from bilhete b where b.concurso_id = ?1 and b.numero = ?2")
	List<Bilhete> findByConcursoAndNumero(Long concurso, Long numero);

	@Query(nativeQuery = true, value = " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero and bil3.numero between ?2 and ?3 "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1) and (bil2.numero between ?2 and ?3) "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim , bil1.regional_id, bil1.estabelecimento_id "
			+ " from bilhete bil1 "
			+ " where bil1.concurso_id = ?1 and (bil1.numero between ?2 and ?3) "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1) and (bil2.numero between ?2 and ?3)  "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	List<Object[]> listaIntervalosDistribuicao(Integer concurso_id, Long numInicio, Long numFim);

	@Query(nativeQuery = true, value = " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero and bil3.numero between ?2 and ?3 "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and  bil3.validado is null "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1) and (bil2.numero between ?2 and ?3) and bil2.validado is null "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim , bil1.regional_id, bil1.estabelecimento_id "
			+ " from bilhete bil1 "
			+ " where bil1.concurso_id = ?1 and (bil1.numero between ?2 and ?3) and bil1.validado is null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1) and (bil2.numero between ?2 and ?3)  and bil2.validado is null "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	List<Object[]> listaIntervalosDistribuicaoNaoValidado(Integer concurso_id, Long numInicio, Long numFim);

	@Query(nativeQuery = true, value = " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero and bil3.numero between ?2 and ?3 "
			+ " and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null))  "
			+ " and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is NULL)) "
			+ " AND (bil3.area_id = bil1.area_id OR (bil3.area_id IS NULL AND bil1.area_id IS null)) "
			+ " and  bil3.validado is null  "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id  "
			+ " and bil2.numero = (bil3.numero+1) and (bil2.numero between ?2 and ?3) and bil2.validado is null  "
			+ " and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ " and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null)) "
			+ " AND (bil3.area_id = bil2.area_id OR (bil3.area_id IS NULL AND bil2.area_id IS null))) "
			+ " ) as numfim , bil1.regional_id, bil1.estabelecimento_id, bil1.area_id  "
			+ " from bilhete bil1 "
			+ " where bil1.concurso_id = ?1 and (bil1.numero between ?2 and ?3) and bil1.validado is null  "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id  "
			+ " and bil2.numero = (bil1.numero -1) and (bil2.numero between ?2 and ?3)  and bil2.validado is null "
			+ " and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ " and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)) "
			+ " AND (bil1.area_id = bil2.area_id OR (bil1.area_id IS NULL AND bil2.area_id IS null))) ")
	List<Object[]> listaIntervalosDistribuicaoNaoValidadoSubRegional(Integer concurso_id, Long numInicio, Long numFim);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, "
			+ "bil3.regional_id, bil3.regional_nome "
			+ "from ("
			+ "SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome"
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id "
			+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable} ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoTodasRegionais(Integer concurso_id, Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id "
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable} ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoRegional(Integer concurso_id, Long regional_id, Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id "
			+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null and bil1.estabelecimento_id is null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable} ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null and bil1.estabelecimento_id is null "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoDisponivelTodasRegional(Integer concurso_id, Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id "
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id is  null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable} ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id is  null "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoDisponivelRegional(Integer concurso_id, Long regional_id,
			Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome  "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome  "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
			+ " left join arearegional area on area.area_id = est.area_id"
			+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null and bil1.estabelecimento_id is not null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable} ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id is not null and bil1.estabelecimento_id is not null "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentosTodasRegional(Integer concurso_id,
			Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome  "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome  "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
			+ " left join arearegional area on area.area_id = est.area_id"
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id is not null "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable}  ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id is not null "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentosRegional(Integer concurso_id, Long regional_id,
			Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
			+ " left join arearegional area on area.area_id = est.area_id"
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id = ?3 "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable}  ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and bil1.estabelecimento_id = ?3 "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoEstabelecimentoRegional(Integer concurso_id, Long regional_id,
			Long estabelecimento_id, Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
			+ " left join arearegional area on area.area_id = est.area_id"
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and area.area_id = ?3 "
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable}  ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
					+ " left join arearegional area on area.area_id = est.area_id "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and area.area_id = ?3 "
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentoAreaRegional(Integer concurso_id, Long regional_id,
			Long area_id, Pageable pageable);

	@Query(nativeQuery = true, value = " select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome "
			+ "from ("
			+ " SELECT bil1.concurso_id, bil1.numero as numinicio, "
			+ " (SELECT min(bil3.numero) from bilhete bil3 "
			+ " where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero  "
			+ "     and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null)) "
			+ "     and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null)) "
			+ "     and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id "
			+ "             and bil2.numero = (bil3.numero+1)  "
			+ "			  and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
			+ " ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome "
			+ " from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
			+ " left join arearegional area on area.area_id = est.area_id"
			+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and area.area_id = ?3 and bil1.estabelecimento_id = ?4"
			+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
			+ "             and bil2.numero = (bil1.numero -1)   "
			+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
			+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))"
			+ ") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ORDER BY ?#{#pageable}  ", countQuery = " SELECT count(*) from bilhete bil1 "
					+ " left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id "
					+ " left join arearegional area on area.area_id = est.area_id "
					+ " where bil1.concurso_id = ?1 and bil1.regional_id = ?2 and area.area_id = ?3 and bil1.estabelecimento_id = ?4"
					+ " and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id "
					+ "             and bil2.numero = (bil1.numero -1)   "
					+ "             and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null)) "
					+ "             and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null)))")
	Page<Object[]> listaIntervalosDistribuicaoTodosOsFiltros(Integer concurso_id, Long regional_id, Long area_id,
			Long estabelecimento_id, Pageable pageable);

	Page<Bilhete> findByConcursoAndNumeroBetween(Concurso concurso, long numero, Pageable pageable);

	@Query("select bil, reg, est " +
			" from Bilhete bil LEFT JOIN Regional reg ON reg.id = bil.regional_id LEFT JOIN Estabelecimento est ON est.id = bil.estabelecimento_id "
			+
			" where bil.concurso = ?1 and bil.numero >= ?2 and bil.numero <= ?3")
	Page<Object[]> getBilheteStatusFaixa(Concurso concurso, Long numincio, Long numfim, Pageable pageable);

	@Query("select bil, reg, est " +
			" from Bilhete bil JOIN Regional reg ON reg.id = bil.regional_id LEFT JOIN Estabelecimento est ON est.id = bil.estabelecimento_id "
			+ " where bil.concurso = ?1 and bil.numero >= ?2 and bil.numero <= ?3 and "
			+ " reg in ?4 ")
	Page<Object[]> getBilheteStatusDistribuidos(Concurso concurso, Long numincio, Long numfim, List<Regional> regs,
			Pageable pageable);

	@Query("select bil, reg, est " +
			" from Bilhete bil JOIN Regional reg ON reg.id = bil.regional_id LEFT JOIN Estabelecimento est ON est.id = bil.estabelecimento_id "
			+ " where bil.concurso = ?1 and bil.loteValidacao = ?2 and bil.validado = TRUE and "
			+ " reg in ?3 ")
	Page<Object[]> getBilheteStatusDistribuidos(Concurso concurso, Long loteValidacao, List<Regional> regs,
			Pageable pageable);

	@Query("select count(bil.numero) from Bilhete bil "
			+ " where bil.concurso = ?1 and bil.numero <= ?3 and bil.numero >= ?2 and "
			+ " bil.regional_id in ?4 and bil.validado = TRUE ")
	Long quantidadeValidados(Concurso concurso, Long numinicio, Long numfim, List<Long> regIds);

	@Query("select count(bil.numero) from Bilhete bil "
			+ " where bil.concurso = ?1 and bil.loteValidacao = ?2 and "
			+ " bil.regional_id in ?3 and bil.validado = TRUE ")
	Long quantidadeValidados(Concurso concurso, Long loteValidacao, List<Long> regIds);

	@Query("select count(bil.numero) from Bilhete bil "
			+ " where bil.concurso = ?1 and bil.regional_id in ?2 and bil.validado = TRUE ")
	Long quantidadeValidados(Concurso concurso, List<Long> regIds);

	@Query("select count(bil.numero) from Bilhete bil "
			+ " where bil.concurso = ?1  ")
	Long quantidadeBilhetes(Concurso concurso);

	@Query("select bil.concurso, bil.loteValidacao, count(*), max(bil.dataValidacao) from Bilhete bil "
			+ " where bil.concurso = ?1 and bil.regional_id in ?2 and bil.validado = TRUE GROUP BY bil.concurso, bil.loteValidacao ORDER BY bil.loteValidacao ")
	Page<Object[]> consultaLotesValidacao(Concurso concurso, List<Long> regIds, Pageable pageable);

	@Query("select count(b.numero) from Bilhete b where b.concurso = ?1 and b.regional_id is null and b.estabelecimento_id is null and area_id is null and b.online = TRUE and (b.validado != TRUE or b.validado is null) ORDER BY b.numero ASC")
	Long bilhetesOnlineDisponiveisGeral(Concurso concurso_id);

	@Query("select count(b.numero) from Bilhete b where b.concurso = ?1 and b.regional_id = ?2 and b.area_id is null and b.estabelecimento_id is null and b.online = TRUE and (b.validado != TRUE or b.validado is null) ORDER BY b.numero ASC")
	Long quantidadeOnlineDisponivelParaArea(Concurso concurso, Long regionalId);

	@Query("select count(b.numero) from Bilhete b where b.concurso = ?1 and b.regional_id = ?2 and b.area_id = ?3 and b.estabelecimento_id is null and b.online = TRUE and (b.validado != TRUE or b.validado is null) ORDER BY b.numero ASC")
	Long quantidadeOnlineDisponivelParaEstabelecimentoComArea(Concurso concurso, Long regionalId, Long areaId);

	@Query(value = "SELECT * FROM bilhete b WHERE b.concurso_id = ?1 AND b.regional_id = ?2 AND b.area_id IS NULL AND b.estabelecimento_id IS NULL AND b.online = TRUE AND (b.validado != TRUE OR b.validado IS NULL) ORDER BY b.numero ASC LIMIT ?3", nativeQuery = true)
	List<Bilhete> obterBilhetesOnlineParaEstabelecimentoSemArea(Concurso concurso, Long regionalId, Integer quantidade);

	@Query(value = "SELECT * FROM bilhete b WHERE b.concurso_id = ?1 AND b.regional_id = ?2 AND b.area_id = ?3 AND b.estabelecimento_id IS NULL AND b.online = TRUE AND (b.validado != TRUE OR b.validado IS NULL) ORDER BY b.numero ASC LIMIT ?4", nativeQuery = true)
	List<Bilhete> obterBilhetesOnlineParaEstabelecimentoComArea(Concurso concurso, Long regionalId, Long areaId,
			Integer quantidade);

	@Query(value = "SELECT * FROM bilhete b WHERE b.concurso_id = ?1 and b.regional_id is null and b.estabelecimento_id is null and area_id is null and b.online = TRUE and (b.validado != TRUE or b.validado is null) ORDER BY b.numero ASC LIMIT ?2", nativeQuery = true)
	List<Bilhete> obterBilhetesOnlineParaRegional(Concurso concurso_id, Integer quantidade);

	@Query(value = "SELECT * FROM bilhete b WHERE b.concurso_id=?1 AND b.regional_id=?2	AND b.area_id IS NULL AND b.estabelecimento_id IS NULL AND b.online=TRUE AND (b.validado != TRUE OR b.validado IS NULL) ORDER BY b.numero ASC LIMIT ?3", nativeQuery = true)
	List<Bilhete> obterBilhetesOnlineParaArea(Concurso concurso, Long regionalId, Integer quantidade);
}
