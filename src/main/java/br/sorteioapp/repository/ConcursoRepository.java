package br.sorteioapp.repository;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;

import br.sorteioapp.domain.QConcurso;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;

import java.util.List;

/**
 * Spring Data JPA repository for the Concurso entity.
 */
@SuppressWarnings("unused")
public interface ConcursoRepository extends JpaRepository<Concurso,Integer>,
    QueryDslPredicateExecutor<Concurso>{

    @Query("select max(id) from Concurso conc " +
        " where conc.id >= ?1 and conc.id <= ?2")
    Integer queryMaxIdInRange(int inicio, int fim);

    List<Concurso> findByOrderByIdDesc(Pageable pageable);

}
