package br.sorteioapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.DistribuicaoBilhete;
import br.sorteioapp.domain.Regional;

@SuppressWarnings("unused")
public interface DistribuicaoBilheteRepository extends JpaRepository<DistribuicaoBilhete, Long> {

	@Query("select dist from DistribuicaoBilhete dist where " + " dist.concurso = ?1 and "+
		       " (?2 between dist.numeroInicio and dist.numeroFim) order by dist.lastModifiedDate desc, dist.antigaRegional.id")	
			
	Page<DistribuicaoBilhete> findAllByBilheteOrderByLastModifiedDate(Concurso conc, Long numero, Pageable pageable);

	@Query("select dist from DistribuicaoBilhete dist where " + " dist.concurso = ?1 and "+
	       " (dist.antigaRegional = ?2 or dist.novaRegional = ?2) order by dist.lastModifiedDate desc, dist.antigaRegional.id")	
	Page<DistribuicaoBilhete> findAllByConcursoRegionalOrderByLastModifiedDate(Concurso concurso, Regional reg, Pageable pageable);

	@Query("select dist from DistribuicaoBilhete dist where " + " dist.concurso = ?1 "+
		       " order by dist.lastModifiedDate desc, dist.antigaRegional.id")		
	Page<DistribuicaoBilhete> findAllByConcursoOrderByLastModifiedDate(Concurso concurso, Pageable pageable);

}
