package br.sorteioapp.repository;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.DistribuicaoSelo;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.Estabelecimento;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DistribuicaoSeloRepository extends JpaRepository<DistribuicaoSelo, Long> {

    @Query("select dist from DistribuicaoSelo dist where " + " dist.concurso = ?1 and "+
        " dist.regional = ?2 and dist.estabelecimento is null order by dist.lastModifiedDate desc, dist.numeroInicio asc")
    List<DistribuicaoSelo> findAllByConcursoRegionalOrderByLastModifiedDate(Concurso concurso, Regional reg);

    @Query("select dist from DistribuicaoSelo dist where " + " dist.concurso = ?1 and "+
        " dist.regional is not null and dist.estabelecimento = ?2 order by dist.lastModifiedDate desc, dist.numeroInicio asc")
    List<DistribuicaoSelo> findAllByConcursoEstabelecimentoOrderByLastModifiedDate(Concurso concurso, Estabelecimento est);

    @Query("select dist from DistribuicaoSelo dist where " + " dist.concurso = ?1 and "+
        " dist.regional = ?2 and numeroFim >= ?3 and numeroInicio <= ?4 order by dist.lastModifiedDate desc, dist.numeroInicio asc")
    List<DistribuicaoSelo> findAllSelosRegionalInRange(Concurso concurso, Regional reg, int ni, int nf);
    @Query("select dist from DistribuicaoSelo dist where " + " dist.concurso = ?1 and "+
        " dist.estabelecimento = ?2 and numeroFim >= ?3 and numeroInicio <= ?4 order by dist.lastModifiedDate desc, dist.numeroInicio asc")
    List<DistribuicaoSelo> findAllSelosEstabelecimentoInRange(Concurso concurso, Estabelecimento est, int ni, int nf);                                                                            

}
