package br.sorteioapp.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import com.querydsl.core.types.dsl.StringPath;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.QEstabelecimento;
import br.sorteioapp.service.dto.EstabelecimentoDTO;

/**
 * Spring Data JPA repository for the Estabelecimento entity.
 */
public interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, Long>,
                QueryDslPredicateExecutor<Estabelecimento>,
                QuerydslBinderCustomizer<QEstabelecimento> {

        // Page<Estabelecimento> findByRegional(Regional regional, Pageable pageable);

        // Page<Estabelecimento> findByLastModifiedByGreaterThanEqual(LocalDateTime
        // lastModifiedBy, Pageable pageable);

        // @Query("SELECT est FROM Estabelecimento est WHERE est.regional = ?1 and ativo
        // = TRUE ORDER BY est.nome")
        // public List<Estabelecimento> queryAllActiveByRegional(Regional reg);

        @Query("SELECT DISTINCT est.cidade FROM Estabelecimento est ORDER BY est.cidade")
        public List<String> queryAllCidades();

        @Query("SELECT DISTINCT est.bairro FROM Estabelecimento est WHERE est.cidade = ?1 ORDER BY est.bairro")
        public List<String> queryAllBairros(String cidade);

        @Query("SELECT e FROM Estabelecimento e WHERE e.ativo = true and e.area = ?1")
        public List<Estabelecimento> estabelecimentosPorSubRegional(AreaRegional area);

        public Page<Estabelecimento> queryAllByCidadeAndAtivo(String cidade, boolean ativo, Pageable pageable);

        public Page<Estabelecimento> queryAllByCidadeAndBairroAndAtivo(String cidade, String bairro, boolean ativo,
                        Pageable pageable);

        @Override
        default public void customize(QuerydslBindings bindings, QEstabelecimento root) {
                bindings.bind(String.class).first(
                                (StringPath path, String value) -> path.containsIgnoreCase(value));
                bindings.excluding(root.area.usuario);
                bindings.excluding(root.regional.usuario);
        }

        List<Estabelecimento> findByAtivoTrueAndRegionalIdAndOnline(Long regionalId, boolean online);

        List<Estabelecimento> findByAtivoTrueAndRegionalIdAndAreaIdAndOnline(Long regionalId, Long areaId,
                        boolean online);

}
