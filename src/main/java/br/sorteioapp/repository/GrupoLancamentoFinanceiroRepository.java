package br.sorteioapp.repository;

import java.util.Optional;
import java.util.List;

import br.sorteioapp.domain.GrupoLancamentoFinanceiro;
import br.sorteioapp.domain.TipoOperacaoLancamentoFinanceiro;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data repository for the GrupoLancamentoFinanceiro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface GrupoLancamentoFinanceiroRepository extends JpaRepository<GrupoLancamentoFinanceiro, Long> {

	Optional<GrupoLancamentoFinanceiro> findById(Long id);

	Page<GrupoLancamentoFinanceiro> findByOrderByNomeGrupoAsc(Pageable page);

	List<GrupoLancamentoFinanceiro> findByTipoOperacaoOrderByNomeGrupoAsc(TipoOperacaoLancamentoFinanceiro tipoOperacao);

	@Query("SELECT DISTINCT tp FROM TipoOperacaoLancamentoFinanceiro tp order by nome")
	List<TipoOperacaoLancamentoFinanceiro> listaTiposOperacoes();

	@Query("SELECT tp FROM TipoOperacaoLancamentoFinanceiro tp where tp.id = ?1")
	Optional<TipoOperacaoLancamentoFinanceiro> findTipoById(Long id);	
}
