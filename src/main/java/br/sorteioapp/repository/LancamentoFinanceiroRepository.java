package br.sorteioapp.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import br.sorteioapp.domain.LancamentoFinanceiro;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.dsl.StringPath;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.QLancamentoFinanceiro;
import br.sorteioapp.domain.Regional;
import org.springframework.data.domain.Page;
/**
 * Spring Data repository for the LancamentoFinanceiro entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LancamentoFinanceiroRepository
		extends JpaRepository<LancamentoFinanceiro, Long>, QueryDslPredicateExecutor<LancamentoFinanceiro>, QuerydslBinderCustomizer<QLancamentoFinanceiro> {

	Optional<LancamentoFinanceiro> findById(Long id);

	Page<LancamentoFinanceiro> findByDataLancamentoBetween(LocalDate dataInicio, LocalDate dataFim, Pageable page);

    @Query("select gru, sum(lanc.valor) "
        + " from LancamentoFinanceiro lanc join lanc.grupoLancamento gru"
        + " where lanc.dataLancamento between ?1 and ?2 GROUP BY  gru.id ORDER BY gru.tipoOperacao.nome asc, gru.nomeGrupo asc")
    List<Object[]> consultaTotalizacaoLancamentos(LocalDate dataInicio, LocalDate dataFim);

	@Override
	default public void customize(QuerydslBindings bindings, QLancamentoFinanceiro root) {
		bindings.bind(String.class).first((StringPath path, String value) -> path.containsIgnoreCase(value));

		// https://www.logicbig.com/tutorials/spring-framework/spring-data/query-dsl-customized-binding.html
        bindings.bind(root.dataLancamento)
              .all((path, value) -> {
                  List<? extends LocalDate> dates = new ArrayList<>(value);
                  if (dates.size() == 1) {
                      return path.eq(dates.get(0));
                  } else {
                      LocalDate from = dates.get(0);
                      LocalDate to = dates.get(1);
                      return path.between(from, to);
                  }
              });		
	}
}
