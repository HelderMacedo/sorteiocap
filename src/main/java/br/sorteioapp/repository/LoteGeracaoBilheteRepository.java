package br.sorteioapp.repository;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.LoteGeracaoBilhete;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by rodrigo on 27/03/17.
 */
public interface LoteGeracaoBilheteRepository extends JpaRepository<LoteGeracaoBilhete,Long> {


    LoteGeracaoBilhete findTopByConcursoIdOrderByNumeroInicialDesc(int concursoId);

    List<LoteGeracaoBilhete> findByConcursoIdOrderByNumeroInicial(int concursoId);

}
