package br.sorteioapp.repository;

import br.sorteioapp.domain.Regiao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RegiaoRepository extends JpaRepository<Regiao,Long> {

}
