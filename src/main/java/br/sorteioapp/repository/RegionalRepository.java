package br.sorteioapp.repository;

import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.QEstabelecimento;
import br.sorteioapp.domain.QRegional;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.User;
import br.sorteioapp.service.dto.RegionalDTO;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;

import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Spring Data JPA repository for the Regional entity.
 */
@SuppressWarnings("unused")
public interface RegionalRepository extends JpaRepository<Regional,Long>,
                                      QueryDslPredicateExecutor<Regional>,
                                      QuerydslBinderCustomizer<QRegional> {

    @Query("select reg from Regional reg inner join reg.usuario u where u.login = ?1")
    Regional findByLogin(String login);

    @Query("select reg from Regional reg where reg.usuario = ?1")
    List<Regional> findByRegionalPorUsuario(User user);

    List<Regional> findByAtivoOrderByNome(boolean ativo);

    @Override
    default public void customize(QuerydslBindings bindings, QRegional root) {
        bindings.bind(String.class).first(
            (StringPath path, String value) -> path.containsIgnoreCase(value));
        bindings.excluding(root.usuario);
    }
}
