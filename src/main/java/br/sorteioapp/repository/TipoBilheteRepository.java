package br.sorteioapp.repository;

import br.sorteioapp.domain.TipoBilhete;
import org.springframework.data.jpa.repository.*;
import java.util.*;

/**
 * Created by rodrigo on 27/03/17.
 */
public interface TipoBilheteRepository extends JpaRepository<TipoBilhete,Long> {

    @Query("select t from TipoBilhete t where t.ativo = true ")
    public List<TipoBilhete> findAllActive();

}
