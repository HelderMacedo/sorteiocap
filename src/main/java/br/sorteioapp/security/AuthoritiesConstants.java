package br.sorteioapp.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

	public static final String ANONYMOUS = "ROLE_ANONYMOUS";
	/* usuario sem nenhuma permissao */
	public static final String ROLE_SYSTEM = "ROLE_SYSTEM";
	public static final String ROLE_ADMIN = "ROLE_ADMIN";
	public static final String ROLE_SECRETARIA = "ROLE_SECRETARIA";
	public static final String ROLE_LOGISTICA = "ROLE_LOGISTICA";
	public static final String ROLE_VALIDADOR = "ROLE_VALIDADOR";
	/*
	 * Os 3 papeis abaixo tem limitações ROLE_SUBSEDE: Vê somente os seus lotes e
	 * estabelecimentos ROLE_REGIONAL: Vê somente os seus lotes e estabelecimentos
	 * ROLE_COMERCIAL: Vê somente os seus lotes e estabelecimentos não associados a
	 * uma subsede
	 */
	public static final String ROLE_COMERCIAL = "ROLE_COMERCIAL";
	public static final String ROLE_SUBSEDE = "ROLE_SUBSEDE";
	public static final String ROLE_TECNOLOGIA = "ROLE_COORDENADOR";
	public static final String ROLE_REGIONAL = "ROLE_REGIONAL";
	public static final String ROLE_AREA_REGIONAL = "ROLE_AREA_REGIONAL";

	public static final String PERM_GERACAO_CONCURSO = "PERM_GERACAO_CONCURSO";
    public static final String PERM_EDICAO_CONCURSO = "PERM_EDICAO_CONCURSO";
	public static final String PERM_CONSULTAR_REGIONAL = "PERM_CONSULTAR_REGIONAL";
	public static final String PERM_CONSULTAR_AREA_REGIONAL = "PERM_CONSULTAR_AREA_REGIONAL";	
	public static final String PERM_CONSULTAR_ESTABELECIMENTO = "PERM_CONSULTAR_ESTABELECIMENTO";	
	public static final String PERM_CADASTRO_REGIONAL = "PERM_CADASTRO_REGIONAL";
	public static final String PERM_CADASTRO_AREA_REGIONAL = "PERM_CADASTRO_AREA_REGIONAL";
	public static final String PERM_CADASTRO_ESTABELECIMENTO = "PERM_CADASTRO_ESTABELECIMENTO";
	public static final String PERM_CONFECCAO_LOTES = "PERM_CONFECCAO_LOTES";
	public static final String PERM_CONFECCAO_SUBLOTES = "PERM_CONFECCAO_SUBLOTES";
	public static final String PERM_DISTRIBUICAO_REGIONAL = "PERM_DISTRIBUICAO_REGIONAL";
	public static final String PERM_DISTRIBUICAO_ESTABELECIMENTO = "PERM_DISTRIBUICAO_ESTABELECIMENTO";
	public static final String PERM_CONSULTA_BILHETES = "PERM_CONSULTA_BILHETES";
	public static final String PERM_PRESTACAO_CONTAS_REGIONAIS = "PERM_PRESTACAO_CONTAS_REGIONAIS";
	public static final String PERM_CADASTRO_USUARIOS = "PERM_CADASTRO_USUARIOS";
	public static final String PERM_VALIDAR_BILHETES = "PERM_VALIDAR_BILHETES";
	public static final String PERM_REL_DIST_REGIONAIS = "PERM_REL_DIST_REGIONAIS";
	public static final String PERM_REL_DIST_REGIONAIS_SEM_EST = "PERM_REL_DIST_REGIONAIS_SEM_EST";
	public static final String PERM_REL_DIST_ESTABELECIMENTO = "PERM_REL_DIST_ESTABELECIMENTO";
	public static final String PERM_REL_REG_EST_ENDERECO = "PERM_REL_REG_EST_ENDERECO";
	public static final String PERM_REL_FINANCEIRO = "PERM_REL_FINANCEIRO";
	public static final String PERM_CADASTRO_DESPESAS = "PERM_CADASTRO_DESPESAS";

	public static final String PERM_CADASTRO_GRUPO_LANCAMENTO = "PERM_CADASTRO_GRUPO_LANCAMENTO";
	public static final String PERM_CADASTRO_LANCAMENTO = "PERM_CADASTRO_LANCAMENTO";
	public static final String PERM_REL_LANCAMENTO = "PERM_REL_LANCAMENTO";
    public static final String PERM_EDICAO_COMISSOES = "PERM_EDICAO_COMISSOES";

	public static final String ACESSO_REGIONAL_TODOS = "ACESSO_REGIONAL_TODOS";
	public static final String ACESSO_REGIONAL_COM_LOGIN = "ACESSO_REGIONAL_COM_LOGIN";
	public static final String ACESSO_REGIONAL_DA_AREA = "ACESSO_REGIONAL_DA_AREA";
	// public static final String ACESSO_REGIONAL_SEM_SUPER = "ACESSO_REGIONAL_SEM_SUPER";
	public static final String BLOQUEIA_VISUALIZ_TOTALIZACAO_TODAS_REGIONAIS = "BLOQUEIA_VISUALIZ_TOTALIZACAO_TODAS_REGIONAIS";
	public static final String PERM_VISUALIZ_TOTALIZACAO = "PERM_VISUALIZ_TOTALIZACAO";


	private AuthoritiesConstants() {
	}
}
