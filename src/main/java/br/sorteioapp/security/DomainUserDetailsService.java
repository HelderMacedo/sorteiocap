package br.sorteioapp.security;

import br.sorteioapp.config.ApplicationProperties;
import br.sorteioapp.domain.Authority;
import br.sorteioapp.domain.Permission;
import br.sorteioapp.domain.User;
import br.sorteioapp.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DomainUserDetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);
    private final ApplicationProperties applicationProperties;
    private final UserRepository userRepository;

    public DomainUserDetailsService(UserRepository userRepository, ApplicationProperties applicationProperties) {
        this.userRepository = userRepository;
        this.applicationProperties = applicationProperties;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase(Locale.ENGLISH);
        Optional<User> userFromDatabase = userRepository.findOneWithAuthoritiesByLogin(lowercaseLogin);
        return userFromDatabase.map(user -> {
            if (! (user.getActivated() &&
                     ((applicationProperties.getVerAjuste()  &&  user.isAcessoSistemaAjustado()) || (!applicationProperties.getVerAjuste()  &&  user.isAcessoSistemaSorteio()))
                   )) {
                throw new UserNotActivatedException("User " + lowercaseLogin + " was not activated");
            }
            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
            for (Authority role : user.getAuthorities()){
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
                for (Permission perm : role.getPermissions()){
                    grantedAuthorities.add(new SimpleGrantedAuthority(perm.getName()));
                }
            }
            /*
                user.getAuthorities().stream()
                    .map(authority -> new SimpleGrantedAuthority(authority.getName()))
                .collect(Collectors.toList());
            */
            return new org.springframework.security.core.userdetails.User(lowercaseLogin,
                user.getPassword(),
                grantedAuthorities);
        }).orElseThrow(() -> new UsernameNotFoundException("User " + lowercaseLogin + " was not found in the " +
        "database"));
    }
}
