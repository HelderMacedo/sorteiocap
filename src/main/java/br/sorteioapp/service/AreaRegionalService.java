package br.sorteioapp.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.QAreaRegional;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.User;
import br.sorteioapp.repository.AreaRegionalRepository;
import br.sorteioapp.repository.UserRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.AreaRegionalDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;

/**
 * Service Implementation for managing Regional.
 */
@Service
@Transactional
public class AreaRegionalService {

	private final Logger log = LoggerFactory.getLogger(RegionalService.class);

	private final RegionalService regionalService;

	private final AreaRegionalRepository arearegionalRepository;
	private final UserService userService;
	private final UserRepository userRepository;

	public AreaRegionalService(AreaRegionalRepository arearegionalRepository, RegionalService regionalService,
			UserService userService, UserRepository userRepository) {
		this.arearegionalRepository = arearegionalRepository;
		this.regionalService = regionalService;
		this.userService = userService;
		this.userRepository = userRepository;
	}

	/**
	 * Save a regional.
	 *
	 * @param dto
	 *            the entity to save
	 * @return the persisted entity
	 */
	// @Todo avaliar necessidade do cache evict abaixo do cache da entidade
	@Transactional(rollbackFor = { UsuarioSemPermissaoException.class, ErroCriarUsuarioException.class })
	@CacheEvict(value = { "br.sorteioapp.domain.AreaRegional" }, beforeInvocation = true, allEntries = true)
	public AreaRegional save(AreaRegionalDTO dto) throws UsuarioSemPermissaoException, ErroCriarUsuarioException {
		log.debug("Request to save Regional : {}", dto);
		boolean updating = dto.getId() != null;
		boolean newuser = false;
		AreaRegional area = null;
		User user = null;
		if (updating) {
			area = arearegionalRepository.findOne(dto.getId());
			user = area.getUsuario();
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(area.getRegional());
		} else {
			area = new AreaRegional();
		}

		if (dto.getLogin() != null && dto.getSenha() != null) {
			if (updating) {
				user = area.getUsuario();
				if (area.getUsuario() == null || !dto.getLogin().equals(area.getUsuario().getLogin())) {
					if (userService.loginIsPresent(dto.getLogin())) {
						throw new ErroCriarUsuarioException("Login já existe!");
					}
					try {
						if (area.getUsuario() != null) {
							userService.deleteUser(user.getLogin());
						}
						user = userService.createTrustfulUser(dto.getLogin(), dto.getSenha(),
								dto.getNome(), dto.getEmail(), AuthoritiesConstants.ROLE_AREA_REGIONAL, dto.isAtivo());
					} catch (Throwable t) {
						throw new ErroCriarUsuarioException(
								"Confirme os dados do usuário. Ou contacte o administrador.");
					}
					newuser = true;
				} else {
					userService.forcePasswordReset(user.getLogin(), dto.getSenha());
				}
			} else {
				try {
					user = userService.createTrustfulUser(dto.getLogin(), dto.getSenha(),
							dto.getNome(), dto.getEmail(), AuthoritiesConstants.ROLE_AREA_REGIONAL, dto.isAtivo());
				} catch (Throwable t) {
					throw new ErroCriarUsuarioException("Verifique se já não existe o usuário!");
				}
				newuser = true;
			}
		} else if (user != null && dto.getLogin() != null && dto.getSenha() == null
				&& !dto.getLogin().equals(area.getUsuario().getLogin())) {
			throw new ErroCriarUsuarioException("Não é possível alterar o usuário sem suas informações de senha!");
		}

		if (user != null && updating && !newuser) {
			user.setActivated(dto.isAtivo());
			if (dto.getEmail() != null)
				user.setEmail(dto.getEmail());
			userService.updateUser(user);
		}

		area.setAtivo(dto.isAtivo());
		area.setCpf(dto.getCpf());
		area.setNome(dto.getNome());
		area.setPercentualRepasse(dto.getPercentualRepasse());
		area.setResponsavel(dto.getResponsavel());
		area.setTelefone(dto.getTelefone());
		area.setId(dto.getId());
		area.setRegional(new Regional(dto.getRegional().getId()));
		area.setUsuario(user);

		AreaRegional result = arearegionalRepository.save(area);
		return result;
	}

	/**
	 * Get all the regionais.
	 *
	 * @param pageable
	 *                 the pagination information
	 * @return the list of entities
	 */
	// @todo melhorar o desempenho da consulta
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<AreaRegional> findAll(Predicate predicate, Pageable pageable) {
		log.debug("Request to get all Area Regionais");
		QAreaRegional qarea = QAreaRegional.areaRegional;
		BooleanExpression bexp = qarea.regional.in(regionalService.regionaisComAcesso());
		predicate = bexp.and(predicate);
		return arearegionalRepository.findAll(predicate, pageable);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<AreaRegional> findAll(Regional reg) {
		log.debug("Request to get all Area Regionais");
		List<AreaRegional> aregs = new ArrayList<>();
		QAreaRegional qarea = QAreaRegional.areaRegional;
		BooleanExpression bexp = qarea.regional.in(regionalService.regionaisComAcesso());
		bexp = bexp.and(qarea.regional.eq(reg));

		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
			bexp = bexp.and(qarea.usuario.login.eq(SecurityUtils.getCurrentUserLogin()));
		}
		arearegionalRepository.findAll(bexp, new Sort(Sort.Direction.ASC, "nome")).forEach(
				area -> {
					if (area.isAtivo()) {
						aregs.add(area);

					}
				});
		return aregs;
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<AreaRegional> findAllActive(Regional reg) {
		log.debug("Request to get all AreaRegionais");
		List<AreaRegional> aregs = new ArrayList<>();
		try {
			if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
				AreaRegional area = arearegionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
				if (area.getRegional().getId().equals(reg))
					aregs.add(area);
			} else {
				regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);

				QAreaRegional qarea = QAreaRegional.areaRegional;
				Predicate predicate = qarea.regional.eq(reg).and(qarea.ativo.eq(true));
				arearegionalRepository.findAll(predicate, new Sort(Sort.Direction.ASC, "nome")).forEach(aregs::add);
			}
		} catch (UsuarioSemPermissaoException e) {
			// retorna uma lista vazia
		}
		return aregs;
	}

	@Transactional()
	public List<AreaRegional> findByUsuario(Long usuarioId) {
		log.debug("Request to get AreaRegional por usuario");

		User usuario = userRepository.findOne(usuarioId);

		return arearegionalRepository.findByUsuario(usuario);
	}

	@Transactional()
	public List<AreaRegional> findAllAtivos() {
		return arearegionalRepository.findAllSubRegionalAtivos();
	}

	/**
	 * Get one regional by id.
	 *
	 * @param id
	 *           the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public AreaRegional findOne(Long id) {
		log.debug("Request to get AreaRegional : {}", id);
		AreaRegional area = arearegionalRepository.findOne(id);
		return area;
	}

	/**
	 * Delete the regional by id.
	 */
	@Transactional
	public void delete(Long id) {
		log.debug("Request to delete Area Regional : {}",
				id);
		AreaRegional reg = arearegionalRepository.findOne(id);
		if (reg != null && reg.getUsuario() != null) {
			userService.deleteUser(reg.getUsuario().getLogin());
		}
		arearegionalRepository.delete(id);
	}

	@Transactional(readOnly = true)
	public AreaRegional findByLogin(String login) {
		return arearegionalRepository.findByLogin(login);
	}

}
