package br.sorteioapp.service;

import static br.sorteioapp.domain.Bilhete.COL_BITSET;
import static br.sorteioapp.domain.Bilhete.COL_COMBINADICS;
import static br.sorteioapp.domain.Bilhete.COL_CONCURSO;
import static br.sorteioapp.domain.Bilhete.COL_DEZENAS;
import static br.sorteioapp.domain.Bilhete.COL_DEZENAS_2;
import static br.sorteioapp.domain.Bilhete.COL_IDENTIFICACAO;
import static br.sorteioapp.domain.Bilhete.COL_NUMERO;
import static br.sorteioapp.domain.Bilhete.COL_NUMORDER;
import static br.sorteioapp.domain.Bilhete.COL_NUMORDER_2;
import static br.sorteioapp.domain.Bilhete.COL_TIPO;
import static br.sorteioapp.service.ConcursoService.QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS;
import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import br.sorteioapp.service.dto.LoteValidacaoStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Bilhete.BilheteKey;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Dezenas;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.TipoBilhete;
import br.sorteioapp.repository.BilheteRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.BilheteStatusDTO;
import br.sorteioapp.service.erros.BilheteJaValidadoException;
import br.sorteioapp.service.erros.FaixaNaoCorrespondeAUmLoteException;
import br.sorteioapp.service.erros.IdentificacaoBilheteInvalida;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import br.sorteioapp.service.erros.ValidacaoForaPeriodoPermitido;
import br.sorteioapp.util.EAN13;
import br.sorteioapp.util.combinatoria.CombinatoriaUtil;
import br.sorteioapp.util.criptografia.FPE;
/**
 * Created by rodrigo on 23/04/17.
 */
@Service
@Transactional
public class BilheteService {
	//@PersistenceContext
	private EntityManager entityManager;
	@Value("${spring.jpa.properties.hibernate.jdbc.batch_size}")
	private int batchSize;

	private final Logger log = LoggerFactory.getLogger(ConcursoService.class);

	private final BilheteRepository bilheteRepository;
	

	private final RegionalService regionalService;

	public BilheteService(BilheteRepository bilheteRepository, 
			RegionalService regionalService, EntityManager entityManager) {

		this.bilheteRepository = bilheteRepository;
		this.regionalService = regionalService;
		this.entityManager = entityManager;
	}

	public List<Dezenas> getConcursoAndNumero(Integer conc, Integer num, String identificador){
		
		String sql = "select b.concurso_id ,b.identificacao, b.dezenas, b.dezenas2 from bilhete b where b.concurso_id = :conc and b.numero = :num and b.identificacao = :identi";
		
		/*String strIdentificador = Integer.toString(identificador);
		String identificadorNumber = strIdentificador.substring(0,11)+"-"+strIdentificador.substring(11);
		System.out.println("primeiro "+strIdentificador.substring(0,11));
		System.out.println("segundo "+strIdentificador.substring(11));*/
		Query q = entityManager.createNativeQuery(sql);
		q.setParameter("conc", conc);
		q.setParameter("num", num);
		q.setParameter("identi", identificador);
		
		return q.getResultList();
	}

	public static class CombOrder implements Comparable<CombOrder> {
		BigInteger combinadics;
		BigInteger bitset;
		long longbitSet;		

		public CombOrder(BigInteger c, BigInteger b) {
			this.combinadics = c;
			this.bitset = b;
		}

		public CombOrder(BigInteger c, BigInteger b, long l) {
			this(c,  b);
			this.longbitSet = l;
		}

		@Override
		public int compareTo(CombOrder o) {
			return combinadics.compareTo(o.combinadics);
		}
	}

	public Bilhete getOne(Concurso conc, Long num) {
		return bilheteRepository.getOne(new BilheteKey(conc, num));
	}
	@Transactional
	@CacheEvict(value = { ConcursoService.QUERY_CACHE_TODOS_CONCURSOS, QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS,
			"br.sorteioapp.domain.Concurso" }, beforeInvocation = true, allEntries = true)

	public int geraBilhetes(Concurso conc, long ultimoNumero, long ultimoNumOrder, int quant, boolean online)
			throws Exception {
		int x = 0;
		int numbflushs = 0;

		// BigInteger quantPossivesNumeros =
		// BigInteger.TEN.pow(7).subtract(BigInteger.ONE);
		// FPE.MultiEncryptorX encComp = new FPE.MultiEncryptorX(quantPossivesNumeros,
		// conc.getChaveCriptografica());

        if (quant > 0) {
            try {

                long no = ultimoNumOrder + 1;
                // privilegia os calculos com números long, se a quantidade de números for menor que 64 (tamanho long)
                boolean aplicaRestricoes = (conc.getTipo().getQuantidadeNumeros() <= 60);
                GeradorDezenasIterator gerador = new GeradorDezenasIterator(no, conc, aplicaRestricoes);

                FPE.EncryptorX encryptorSub = new FPE.EncryptorX(new BigInteger("999999"), conc.getChaveCriptografica().getBytes());

                while (x < quant) {

                    int y = Math.min(50, quant - x);
                    StringBuffer sb = new StringBuffer();
                    sb.append("INSERT INTO " + Bilhete.TABLE_NAME + " (" + COL_CONCURSO + "," + COL_NUMERO + ","
                        + COL_IDENTIFICACAO + "," + COL_TIPO + ","
                        + COL_NUMORDER + "," + COL_DEZENAS + "," + COL_NUMORDER_2 + "," + COL_DEZENAS_2 + ","
                        + "online) values ");
                    for (int a = 0; a < y; a++) {
                        if (a!=0) sb.append(",");
                        sb.append("(:conc_" + a + ", :num_" + a + ", :ident_" + a + ", :tipo_" + a
                            + ", :numorder_" + a + ", :dez_" + a + ", :numorder2_" + a + ", :dez2_" + a
                            + ", :online_" + a + ") ");
                    }
                    log.debug("sqlinsert bilhete " + sb.toString());
                    Query q = entityManager.createNativeQuery(sb.toString());

                    for (int a = 0; a < y; ) {
                        long numero = ultimoNumero + x + a + 1;
                        //long numOrder = no++;
                        GeradorDezenasIterator.CombOrderDezena codez1 = gerador.next();
                        GeradorDezenasIterator.CombOrderDezena codez2;
                        String identificacao = calcIdentificacao(conc, conc.getTipo(), numero, codez1.getBitset());
                       
                        if (conc.getDuplaChance()) {
                            codez2 = gerador.next();
                        } else {
                            codez2 = new GeradorDezenasIterator.CombOrderDezena(0l, BigInteger.ZERO, BigInteger.ZERO, 0l);
                        }



                        q.setParameter("conc_" + a, conc.getId());
                        q.setParameter("num_" + a, numero);
                        //q.setParameter("comb_" + a, printHexBinary(combinadics.toByteArray()));
                        //q.setParameter("bitset_" + a, printHexBinary(bitSet.toByteArray()));
                        q.setParameter("tipo_" + a, conc.getTipo().getId());
                        q.setParameter("ident_" + a, identificacao);
                        q.setParameter("dez_" + a, codez1.getDezenas());
                        q.setParameter("dez2_" + a, codez2.getDezenas());
                        q.setParameter("numorder_" + a, (codez1.getNum()));
                        q.setParameter("numorder2_" + a, (codez2.getNum()));
                        q.setParameter("online_" + a, online);
                        a++;
                    }
                    q.executeUpdate();

                    x += y;
                    if (x / y % batchSize > numbflushs) {
                        // Flush a batch of inserts and release memory.
                        entityManager.flush();
                        entityManager.clear();
                        numbflushs++;
                    }
                }
            } catch (NoSuchElementException nse){
                nse.printStackTrace();
            }

			entityManager.flush();
			entityManager.clear();
		}
		return x;
	}


	List<Object[]> listaIntervalosDistribuicao(Integer concurso_id, Long numInicio, Long numFim) {
		return bilheteRepository.listaIntervalosDistribuicao(concurso_id, numInicio, numFim);
	}
    List<Object[]> listaIntervalosDistribuicaoNaoValidado(Integer concurso_id, Long numInicio, Long numFim) {
        return bilheteRepository.listaIntervalosDistribuicaoNaoValidado(concurso_id, numInicio, numFim);
    }
	List<Object[]> listaIntervalosDistribuicaoNaoValidadoArea(Integer concurso_id, Long numInicio, Long numFim) {
        return bilheteRepository.listaIntervalosDistribuicaoNaoValidadoSubRegional(concurso_id, numInicio, numFim);
    }
	Page<Object[]> listaIntervalosDistribuicaoRegional(Integer concurso_id, Long regional_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "numinicio")));
		return bilheteRepository.listaIntervalosDistribuicaoRegional(concurso_id, regional_id, pagerequest);
	}
	Page<Object[]> listaIntervalosDistribuicaoTodasRegionais(Integer concurso_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "numinicio")));		
		return bilheteRepository.listaIntervalosDistribuicaoTodasRegionais(concurso_id, pagerequest);
	}
	Page<Object[]> listaIntervalosDistribuicaoDisponivelTodasRegional(Integer concurso_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "numinicio")));		
		return bilheteRepository.listaIntervalosDistribuicaoDisponivelTodasRegional(concurso_id, pagerequest);
	}	
	Page<Object[]> listaIntervalosDistribuicaoDisponivelRegional(Integer concurso_id, Long regional_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "numinicio")));		
		return bilheteRepository.listaIntervalosDistribuicaoDisponivelRegional(concurso_id, regional_id, pagerequest);
	}
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentosRegional(Integer concurso_id, Long regional_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "estabelecimento_nome")).and(new Sort(Sort.Direction.ASC, "numinicio")));		
		return bilheteRepository.listaIntervalosDistribuicaoTodosEstabelecimentosRegional(concurso_id, regional_id, pagerequest);
	}
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentosTodasRegional(Integer concurso_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "estabelecimento_nome")).and(new Sort(Sort.Direction.ASC, "numinicio")));		
		return bilheteRepository.listaIntervalosDistribuicaoTodosEstabelecimentosTodasRegional(concurso_id, pagerequest);
	}	
	Page<Object[]> listaIntervalosDistribuicaoEstabelecimentoRegional(Integer concurso_id, Long regional_id, Long estabelecimento_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "estabelecimento_nome")).and(new Sort(Sort.Direction.ASC, "numinicio")));
		return bilheteRepository.listaIntervalosDistribuicaoEstabelecimentoRegional(concurso_id, regional_id, estabelecimento_id, pagerequest);
	}		
	Page<Object[]> listaIntervalosDistribuicaoTodosEstabelecimentoAreaRegional(Integer concurso_id, Long regional_id, Long area_id, Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "estabelecimento_nome")).and(new Sort(Sort.Direction.ASC, "numinicio")));
		return bilheteRepository.listaIntervalosDistribuicaoTodosEstabelecimentoAreaRegional(concurso_id, regional_id, area_id, pagerequest);
	}
	
	Page<Object[]>listaIntervalosDistribuicaoTodosOsFiltros(Integer concurso_id, Long regional_id, Long area_id, Long estabelecimento_id,Pageable pageable) {
		Pageable pagerequest = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),
				new Sort(Sort.Direction.ASC, "regional_nome").and(new Sort(Sort.Direction.ASC, "estabelecimento_nome")).and(new Sort(Sort.Direction.ASC, "numinicio")));
		return bilheteRepository.listaIntervalosDistribuicaoTodosOsFiltros(concurso_id, regional_id, area_id, estabelecimento_id, pagerequest);
	}
	/*
	private boolean checkSimilaridadeVizinhos(NavigableSet<CombOrder> ns, BigInteger bitsetf, int numerosporbilhete) {
		int t = 10, k = 0;
		int sim1 = (numerosporbilhete + 1) / 2;
		boolean skip = false;
		for (CombOrder ctmp : ns) {
			if (skip || k > 15)
				break;
			else if (k < 10)
				t = sim1;
			else if (k >= 10)
				t = sim1 + 1;
			skip = (ctmp.bitset.and(bitsetf).bitCount() >= t);
			k++;
		}
		return skip;
	}
	*/

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, FaixaNaoCorrespondeAUmLoteException.class,
			UsuarioSemPermissaoException.class, ValidacaoForaPeriodoPermitido.class, BilheteJaValidadoException.class })
	public String validaVenda(Integer concursoId, String identificacao)
			throws IdentificacaoBilheteInvalida, FaixaNaoCorrespondeAUmLoteException, UsuarioSemPermissaoException,
			ValidacaoForaPeriodoPermitido, BilheteJaValidadoException {
		Bilhete bilhete = recuperaBilhetePelaIdentificacao(identificacao);

		if (bilhete == null || concursoId == null || !bilhete.getConcurso().getId().equals(concursoId)) {
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.INDEFINIDO, identificacao);
		}

		if (ZonedDateTime.now().isAfter(bilhete.getConcurso().getDataFimVigencia())
				|| ZonedDateTime.now().isBefore(bilhete.getConcurso().getDataInicioVigencia())) {
			throw new ValidacaoForaPeriodoPermitido(bilhete.getConcurso().getDataInicioVigencia(),
					bilhete.getConcurso().getDataFimVigencia());
		}

		if (bilhete.isValidado()) {
			throw new BilheteJaValidadoException(bilhete.getIdentificacao());
		}

		if (bilhete.getRegional_id() == null )
			throw new FaixaNaoCorrespondeAUmLoteException(bilhete.getIdentificacao(), bilhete.getIdentificacao());
		
		regionalService.verificaSeUsuarioTemAcessoSobreRegional(new Regional(bilhete.getRegional_id()));

		Query qu = entityManager.createQuery("update Bilhete bil set validado = :val , lastModifiedDate = :data  "
				+ " where bil.concurso = :conc and  bil.numero = :numero");
		qu.setParameter("conc", bilhete.getConcurso());
		qu.setParameter("numero", bilhete.getNumero());
		qu.setParameter("val", Boolean.TRUE);
		qu.setParameter("data", LocalDateTime.now());
		qu.executeUpdate();
		if (qu.executeUpdate() > 0) {
			return bilhete.getIdentificacao();
		} else {
			return null;
		}
	}

	private static String calcIdentificacao(Concurso con, TipoBilhete tipo, long numero, BigInteger comb) {
		DecimalFormat nf4 = new DecimalFormat("#0000");
		DecimalFormat nf7 = new DecimalFormat("#0000000");

		int prefixoConcursoETipo = (int) (con.getId() % 10000);
		if (con.getId() <= 201899) {
			// TODO Heuristica antiga de identificação do
			// concurso a partir do bilhete
			prefixoConcursoETipo = (int) (con.getId() % 1000) * 10 + 1;
		}
		String identificacaoNumero = nf4.format(prefixoConcursoETipo) + nf7.format(numero);

		int numeroAleatorioReferenteIdentificacao = comb.mod(BigInteger.TEN).intValue();

		int digitoVerificador = EAN13
				.calcDigitoVerificador(identificacaoNumero + numeroAleatorioReferenteIdentificacao);
		return identificacaoNumero + "-" + Integer.toString(numeroAleatorioReferenteIdentificacao)
				+ Integer.toString(digitoVerificador);
	}

	@Transactional(readOnly = true)
	public Bilhete recuperaBilhetePelaIdentificacao(String identificacao) throws IdentificacaoBilheteInvalida {
		if (identificacao == null)
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.INDEFINIDO, identificacao);
		identificacao = identificacao.replace("-", "");
		int concursoId = extraiConcursoDeIdentificacao(identificacao);
		long numero = extraiNumeroDeIdentificacao(identificacao);
		Bilhete.BilheteKey key = new Bilhete.BilheteKey();
		key.concurso = new Concurso(concursoId);
		key.numero = numero;
		log.debug("recuperaBilhetePelaIdentificacao '" + identificacao + "' " + concursoId + " " + numero);
		identificacao = identificacao.substring(0, 11) + "-" + identificacao.substring(11, 13);
		Bilhete bil = bilheteRepository.findOne(key);
		String identificacaoRecuperada = "";
		if (bil != null)
			identificacaoRecuperada = bil.getIdentificacao();
		if (bil == null || !identificacao.equals(identificacaoRecuperada))
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.INDEFINIDO, identificacao);
		return bil;
	}

/*	@Transactional(readOnly = true)
	public Page<BilheteDTO> consultaBilhetesVendidos(int concid, LocalDateTime data, Pageable pageable) {
		Page<BilheteDTO> res = bilheteRepository.findAllBilhetesVendidos(concid, data, pageable).map(BilheteDTO::new);
		return res;
	}
*/
	static int extraiNumeroDeIdentificacao(String identificacao) throws IdentificacaoBilheteInvalida {
		if (eTamanhoBilheteInvalido(identificacao))
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.TAM_INVALIDO, identificacao);
		try {
			return Integer.parseInt(identificacao.substring(4, 11));
		} catch (Exception e) {
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.SO_ALGARISMOS, identificacao);
		}
	}

	static int extraiConcursoDeIdentificacao(String identificacao) throws IdentificacaoBilheteInvalida {
		if (eTamanhoBilheteInvalido(identificacao))
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.TAM_INVALIDO, identificacao);

		try {

			int ano = Integer.parseInt(identificacao.substring(0, 1));
			int sequencial = Integer.parseInt(identificacao.substring(1, 4));
			// No momento da confecção deste sistema, foi solicitado que
			// o primeiro digito da identificação representasse os anos a partir de 2017.
			if (ano < 7)
				ano += 10;
			ano = 2010 + ano;
			if (ano == 2018 && sequencial >= 80) {
				// % TODO rever em 10 anos. Heuristica antiga de identificação do
				// concurso a partir do bilhete
				return ano * 100 + sequencial / 10;
			} else {
				return ano * 1000 + sequencial;
			}
		} catch (Exception e) {
			throw new IdentificacaoBilheteInvalida(IdentificacaoBilheteInvalida.SO_ALGARISMOS, identificacao);
		}
	}

	private static boolean eTamanhoBilheteInvalido(String identificacao) {
		return identificacao == null || identificacao.length() != 13;
	}

	@Transactional
	public Page<BilheteStatusDTO> getStatusBilhetesFaixa(Integer concursoId, Long numincio, Long numfim, Pageable pageable) {
		if (!SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_TODOS)) {
			throw new IllegalArgumentException("Sem permissao");
		}
		return bilheteRepository.getBilheteStatusFaixa(new Concurso(concursoId), numincio, numfim, pageable)
				.map(BilheteStatusDTO::new);
	}

	@Transactional
	public Page<BilheteStatusDTO> getStatusBilhetesDistribuidos(Integer concursoId, Long numincio, Long numfim,
			Pageable pageable) {
		List<Regional> regs = regionalService.regionaisComAcesso();
		return bilheteRepository
				.getBilheteStatusDistribuidos(new Concurso(concursoId), numincio, numfim, regs, pageable)
				.map(BilheteStatusDTO::new);
	}

	@Transactional
	public Page<BilheteStatusDTO> getStatusBilhetesDistribuidos(Integer concursoId, Long loteValidacao,
			Pageable pageable) {
		List<Regional> regs = regionalService.regionaisComAcesso();
		return bilheteRepository
				.getBilheteStatusDistribuidos(new Concurso(concursoId), loteValidacao, regs, pageable)
				.map(BilheteStatusDTO::new);
	}

	public Long quantidadeValidados(Integer concursoId, Long numinicio, Long numfim) {
		List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
		List<Long> regIdsComAcesso = new ArrayList<Long>();
		for(Regional reg: regsComAcesso) regIdsComAcesso.add(reg.getId());		
		return bilheteRepository.quantidadeValidados(new Concurso(concursoId), numinicio, numfim, regIdsComAcesso);
	}
	
	public Long quantidadeValidadosLote(Integer concursoId, Long loteValidacao) {
		List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
		List<Long> regIdsComAcesso = new ArrayList<Long>();
		for(Regional reg: regsComAcesso) regIdsComAcesso.add(reg.getId());		
		return bilheteRepository.quantidadeValidados(new Concurso(concursoId), loteValidacao, regIdsComAcesso);
	}

    @Transactional
    public Page<LoteValidacaoStatusDTO> consultaLotesValidacao(Integer concursoId, Long regId,
                                                               Pageable pageable) throws UsuarioSemPermissaoException {
        List<Long> regIdsComAcesso = new ArrayList<Long>();
	    if (regId == null){
            List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
            for(Regional reg: regsComAcesso) regIdsComAcesso.add(reg.getId());
        } else if (regionalService.verificaSeUsuarioTemAcessoSobreRegional(new Regional(regId))){
            regIdsComAcesso.add(regId);
        }
        return bilheteRepository
            .consultaLotesValidacao(new Concurso(concursoId), regIdsComAcesso, pageable)
            .map(LoteValidacaoStatusDTO::new);
    }

    public Long quantidadeValidadosRegional(Integer concursoId, Long regId) throws UsuarioSemPermissaoException {
	    List<Long> regIdsComAcesso = new ArrayList<Long>();
        if (regId == null){
            List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
            for(Regional reg: regsComAcesso) regIdsComAcesso.add(reg.getId());
        } else if (regionalService.verificaSeUsuarioTemAcessoSobreRegional(new Regional(regId))){
            regIdsComAcesso.add(regId);
        }

        return bilheteRepository.quantidadeValidados(new Concurso(concursoId), regIdsComAcesso);
    }
}
