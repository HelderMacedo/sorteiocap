package br.sorteioapp.service;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import br.sorteioapp.domain.Regional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.QConcurso;
import br.sorteioapp.repository.ConcursoRepository;
import br.sorteioapp.repository.BilheteRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.ConcursoDTO;
import br.sorteioapp.util.criptografia.GeradorChave;

import javax.persistence.EntityManager;

/**
 * Service Implementation for managing Concurso.
 */
@Service
@Transactional
public class ConcursoService {

	public static final String QUERY_CACHE_TODOS_CONCURSOS = "todosConcursos";
    public static final String QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS = "concursosNaoSincronizados";
	private final Logger log = LoggerFactory.getLogger(ConcursoService.class);

	private final ConcursoRepository concursoRepository;
	private final BilheteRepository bilheteRepository;	
    private final EntityManager entityManager;

	public ConcursoService(ConcursoRepository concursoRepository, BilheteRepository bilheteRepository, EntityManager entityManager) {
		this.concursoRepository = concursoRepository;
		this.bilheteRepository = bilheteRepository;
		this.entityManager = entityManager;
	}

	/**
	 * Adiciona um concurso.
	 *
	 * @param concursodto
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Transactional
	@CacheEvict(value = { QUERY_CACHE_TODOS_CONCURSOS, QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS,
			"br.sorteioapp.domain.Concurso" }, allEntries = true)
	public Concurso insert(ConcursoDTO concursodto) {
		log.debug("solicitação para a inserção de um Concurso : {}", concursodto);
		if (concursodto.getId() != null && concursodto.getId() > 0)
			throw new IllegalArgumentException("Um novo concurso não pode ter uma chave pré-definida");
		if (!isUsusarioPermitido()) {
			throw new IllegalArgumentException("Usuario não permitido");
		}
		// Evitar quantidades muito grandes, e que não fazem sentido para o negócio.
		// A quantidade de combinações de bilhetes com 50 números sobre um concurso de
		// 100 não excede 30 bits.
		// Caso decida-se alterar este número, avaliar as modificações de memoization
		// sobre o algoritmo de FPE e
		// sobre o calculo de fatoriais (rank para bitset); embora, as modificações
		// existentes suporte até 200.
		// if (concursodto.getQuantidadeNumeros() > 100 ||
		// concursodto.getQuantidadeNumeros() < 1 || concursodto.getNumerosBilhete() <
		// 1) {
		// throw new IllegalArgumentException("Quantidade de combinações não
		// suportada!");
		// }

		if (concursodto.getAno() < LocalDateTime.now().getYear()
				|| concursodto.getAno() > LocalDateTime.now().getYear() + 1) {
			throw new IllegalArgumentException("Só é permitido criar concursos do ano atual, ou do próximo ano.");
		}
		/*
		 * int initRange = concursodto.getAno()*100; int endRange =
		 * concursodto.getAno()*100+99; Integer maxid =
		 * concursoRepository.queryMaxIdInRange( initRange, endRange); if(maxid==null)
		 * maxid = initRange;
		 */
		if (concursodto.getNumSorteio() < 0 || concursodto.getNumSorteio() > 999) {
			throw new IllegalArgumentException("O sequencial do concurso deverá ser entre 1 e 999 concursos.");
		}

		int id = (concursodto.getAno()) * 1000 + (concursodto.getNumSorteio()) % 1000;
		Concurso conc = new Concurso(id);
		conc.setSincronizado(false);
		conc.setTipo(concursodto.getTipo());
		conc.setChaveCriptografica(GeradorChave.geraChaveBase64());
		conc.setDataInicioVigencia(ZonedDateTime.now());
		conc.setDataFimVigencia(concursodto.getDataSorteio().plusDays(1));
		conc.setDataSorteio(concursodto.getDataSorteio().toLocalDate());
		conc.setAno(concursodto.getAno());
		conc.setNumSorteio(concursodto.getNumSorteio());
		conc.setDuplaChance(concursodto.getDuplaChance());
		return concursoRepository.save(conc);
    }


    @CacheEvict(value = { QUERY_CACHE_TODOS_CONCURSOS, QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS,
        "br.sorteioapp.domain.Concurso" }, beforeInvocation = true, allEntries = true)
    public Concurso update(ConcursoDTO concursodto) {
        log.debug("solicitação para a inserção de um Concurso : {}", concursodto);
        if (concursodto.getId() == null)
            throw new IllegalArgumentException("Um novo concurso não pode ter uma chave pré-definida");
        if (!isUsusarioPermitido()) {
            throw new IllegalArgumentException("Usuario não permitido");
        }

        Concurso conc = concursoRepository.findOne(concursodto.getId());

        int qtdPremios = concursodto.getQuantidadePremios()==null?0:concursodto.getQuantidadePremios();
        if (qtdPremios < 5) concursodto.setPremio5(null);
        if (qtdPremios < 4) concursodto.setPremio4(null);
        if (qtdPremios < 3) concursodto.setPremio3(null);
        if (qtdPremios < 2) concursodto.setPremio2(null);
        if (qtdPremios < 1) concursodto.setPremio1(null);

        conc.setQuantidadePremios(qtdPremios);
        conc.setPremio5(concursodto.getPremio5());
        conc.setPremio4(concursodto.getPremio4());
        conc.setPremio3(concursodto.getPremio3());
        conc.setPremio2(concursodto.getPremio2());
        conc.setPremio1(concursodto.getPremio1());
        conc.setPercentualFixoRegional(concursodto.getPercentualFixoRegional());

        conc.setTipo(concursodto.getTipo());
        conc.setLimiteFaixaComissaoRegional1(concursodto.getLimiteFaixaComissaoRegional1());
        conc.setLimiteFaixaComissaoEstabelecimento1(concursodto.getLimiteFaixaComissaoEstabelecimento1());
        conc.setLimiteFaixaComissaoEstabelecimento2(concursodto.getLimiteFaixaComissaoEstabelecimento2());
        concursoRepository.save(conc);
        return conc;
    }

	/**
	 * Get all the concursos.
	 * @return the list of entities
	 */
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<Concurso> findAllQtd() {
		log.debug("Request to get all Concursos");
		Sort sort = new Sort(Sort.Direction.DESC, "id");
		List<Concurso> result = concursoRepository.findAll(new Sort(Sort.Direction.ASC, "id"));
		for(Concurso conc : result){
			conc.setQuantidadeBilhetes( bilheteRepository.quantidadeBilhetes(conc));
		}
		return result;
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	@Cacheable(value = QUERY_CACHE_TODOS_CONCURSOS)
	public List<Concurso> findAll() {
		List<Concurso> result = concursoRepository.findAll(new Sort(Sort.Direction.DESC, "id"));
		return result;
	}

    @Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
    @Cacheable(value = QUERY_CACHE_CONCURSOS_NAO_SINCRONIZADOS)
    public List<Concurso> findAllNaoSincronizado() {
        QConcurso qconc = QConcurso.concurso;
        Predicate predicate = qconc.sincronizado.eq(false);
        List<Concurso> concs = new ArrayList<>();

        concursoRepository.findAll(predicate, new Sort(Sort.Direction.ASC, "id")).forEach(concs::add);
        return concs ;
    }

	/**
	 * Get one concurso by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Concurso findOne(Integer id) {
		log.debug("Request to get Concurso : {}", id);
		Concurso concurso = concursoRepository.findOne(id);
		concurso.getLotebilhetes().size();
		return concurso;
	}

	/*
	 * public Concurso update(ConcursoDTO concursodto) {
	 * log.debug("Request to update Concurso : {}", concursodto);
	 * if(concursodto.getId()== null || concursodto.getId() <=0) throw new
	 * IllegalArgumentException("Não é possível atualizar um concurso com chave nula"
	 * ); if (concursodto.getQuantidadeNumeros() > 100 ||
	 * concursodto.getQuantidadeNumeros() < 1 || concursodto.getNumerosBilhete() <
	 * 1) { throw new
	 * IllegalArgumentException("Quantidade de combinações não suportada!"); }
	 * Concurso concurso = concursoRepository.getOne(concursodto.getId());
	 * concurso.setNumerosBilhete(concursodto.getNumerosBilhete());
	 * concurso.setQuantidadeNumeros(concursodto.getQuantidadeNumeros()); Concurso
	 * result = concursoRepository.save(concurso);
	 * 
	 * //@todo apagar bilhetes e lotes return result; }
	 * 
	 * public void delete(Integer id) { log.debug("Request to delete Concurso : {}",
	 * id); //@todo apagar bilhetes e lotes concursoRepository.delete(id); }
	 */

	private boolean isUsusarioPermitido() {
		return SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_GERACAO_CONCURSO);
	}

}
