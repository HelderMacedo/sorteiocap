package br.sorteioapp.service;

import java.time.ZonedDateTime;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.sorteioapp.config.ApplicationProperties;
import br.sorteioapp.service.erros.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.DistribuicaoBilhete;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.repository.AreaRegionalRepository;
import br.sorteioapp.repository.BilheteRepository;
import br.sorteioapp.repository.DistribuicaoBilheteRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.DistribuicaoBilheteDTO;
import br.sorteioapp.service.dto.RelatorioDistribuicaoBilheteDTO;
import br.sorteioapp.service.dto.RelatorioDistribuicaoSubRegionalDTO;
import br.sorteioapp.service.dto.ThinAreaRegionalDTO;
import br.sorteioapp.service.dto.ThinConcursoDTO;
import br.sorteioapp.service.dto.ThinDistribuicaoBilheteAtualDTO;
import br.sorteioapp.service.dto.ThinEstabelecimentoDTO;
import br.sorteioapp.service.dto.ThinRegionalDTO;
import br.sorteioapp.service.dto.DistribuicaoBilheteOnlineDTO;

@Service
@Transactional
public class DistribuicaoBilheteService {

	private final Logger log = LoggerFactory.getLogger(DistribuicaoBilheteService.class);

	private EntityManager entityManager;
	private final ConcursoService concursoService;
	private final BilheteService bilheteService;
	private final BilheteRepository bilheteRepository;
	private final RegionalService regionalService;
	private final EstabelecimentoService estabelecimentoService;
	private final AreaRegionalRepository arearegionalRepository;
	private final DistribuicaoBilheteRepository distribuicaoBilheteRepository;
	private final ApplicationProperties applicationProperties;
	private final DistribuicaoSeloService distribuicaoSeloService;

	public DistribuicaoBilheteService(EntityManager entityManager, ConcursoService concursoService,
			BilheteService bilheteService, RegionalService regionalService,
			EstabelecimentoService estabelecimentoService, AreaRegionalRepository arearegionalRepository,
			DistribuicaoBilheteRepository distribuicaoBilheteRepository, ApplicationProperties applicationProperties,
			DistribuicaoSeloService distribuicaoSeloService, BilheteRepository bilheteRepository) {

		this.entityManager = entityManager;
		this.concursoService = concursoService;
		this.bilheteService = bilheteService;
		this.regionalService = regionalService;
		this.estabelecimentoService = estabelecimentoService;
		this.arearegionalRepository = arearegionalRepository;
		this.distribuicaoBilheteRepository = distribuicaoBilheteRepository;
		this.applicationProperties = applicationProperties;
		this.distribuicaoSeloService = distribuicaoSeloService;
		this.bilheteRepository = bilheteRepository;
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	public void save(int concursoId, String indetificacaoBilheteInicial, String indetificacaoBilheteFinal,
			Estabelecimento estabelecimento, Character motivoDistribuicao, Long areaId)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			BilheteNaoCorrespondeConcurso, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {

		Estabelecimento est = estabelecimentoService.findOne(estabelecimento.getId());
		if (est == null) {
			throw new IllegalArgumentException("Estabelecimento inexistente");
		}
		save(concursoId, indetificacaoBilheteInicial, indetificacaoBilheteFinal, est.getRegional(), est,
				motivoDistribuicao, areaId);
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	public void saveDistSubRegional(int concursoId, String indetificacaoBilheteInicial,
			String indetificacaoBilheteFinal, AreaRegional areaRegional, Character motivoDistribuicao)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			BilheteNaoCorrespondeConcurso, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException,
			LotePertenceAOutraSubRegionalException {

		AreaRegional area = arearegionalRepository.findOne(areaRegional.getId());

		if (area == null) {
			throw new IllegalArgumentException("SubRegional inexistente");
		}

		save(concursoId, indetificacaoBilheteInicial, indetificacaoBilheteFinal, area.getRegional(), area,
				motivoDistribuicao);
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	public void save(int concursoId, String indetificacaoBilheteInicial, String indetificacaoBilheteFinal,
			Regional regional, AreaRegional areaRegional, Character motivoDistribuicao)
			throws IdentificacaoBilheteInvalida, BilheteNaoCorrespondeConcurso, UsuarioSemPermissaoException,
			BilheteJaValidadoException, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException,
			LotePertenceAOutraSubRegionalException {

		if (indetificacaoBilheteInicial == null || indetificacaoBilheteFinal == null
				|| indetificacaoBilheteInicial.isEmpty() || indetificacaoBilheteFinal.trim().isEmpty()) {
			throw new IllegalArgumentException("Valores obrigatorios não preenchidos");
		}

		Bilhete bilheteInicial = bilheteService.recuperaBilhetePelaIdentificacao(indetificacaoBilheteInicial);
		Bilhete bilheteFinal = bilheteService.recuperaBilhetePelaIdentificacao(indetificacaoBilheteFinal);
		if (!bilheteInicial.getConcurso().getId().equals(concursoId)) {
			throw new BilheteNaoCorrespondeConcurso(bilheteInicial.getIdentificacao());
		}
		if (!bilheteFinal.getConcurso().getId().equals(concursoId)) {
			throw new BilheteNaoCorrespondeConcurso(bilheteFinal.getIdentificacao());
		}

		if (bilheteFinal.getNumero().longValue() < bilheteInicial.getNumero().longValue()
				|| !bilheteInicial.getTipo().equals(bilheteFinal.getTipo())) {
			throw new IllegalArgumentException("Faixa inválida");
		}

		Query queryBils = entityManager.createQuery("select bil.identificacao "
				+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
				+ " bil.estabelecimento_id is not null ");
		queryBils.setMaxResults(1);
		queryBils.setParameter("conc", bilheteInicial.getConcurso());
		queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		queryBils.setParameter("numfim", bilheteFinal.getNumero());
		List lstresregId = queryBils.getResultList();
		if (!lstresregId.isEmpty())
			throw new FaixaJaCorrespondeAUmLoteException("---", "---");

		save(bilheteInicial, bilheteFinal, regional, areaRegional, motivoDistribuicao);
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	public void saveBilheteSeloRegional(int concursoId, String indetificacaoBilheteInicial,
			String indetificacaoBilheteFinal, String seloInicial, String seloFinal, Regional regional,
			Estabelecimento estabelecimento, Character motivoDistribuicao) throws IdentificacaoBilheteInvalida,
			UsuarioSemPermissaoException, BilheteJaValidadoException, BilheteNaoCorrespondeConcurso,
			LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException, SeloInvalidoException {

		save(concursoId, indetificacaoBilheteInicial, indetificacaoBilheteFinal, regional, estabelecimento,
				motivoDistribuicao, null);
		if (seloInicial != null || seloFinal != null) {
			distribuicaoSeloService.distribuirSelos(new Concurso(concursoId), regional, null, seloInicial, seloFinal);
		}
	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	public List<String> distiruirbilhetesOnlineService(DistribuicaoBilheteOnlineDTO distribuicaoBilheteOnlineDTO) {
		if (distribuicaoBilheteOnlineDTO.getConcurso_id() == null) {
			throw new IllegalArgumentException("Concurso não informado");
		}

		if (distribuicaoBilheteOnlineDTO.getRegional_id() == null) {
			throw new IllegalArgumentException("Regional não informado");
		}

		Concurso concurso = new Concurso(distribuicaoBilheteOnlineDTO.getConcurso_id());

		if (distribuicaoBilheteOnlineDTO.getEstabelecimento_id() == null
				&& distribuicaoBilheteOnlineDTO.getArea_id() == null) {
			return this.distribuirParaRegional(distribuicaoBilheteOnlineDTO, concurso);
		} else if (distribuicaoBilheteOnlineDTO.getEstabelecimento_id() == null) {
			return this.distribuirParaArea(distribuicaoBilheteOnlineDTO, concurso);
		} else if (distribuicaoBilheteOnlineDTO.getArea_id() != null) {
			return this.distribuirParaEstabelecimentoComArea(distribuicaoBilheteOnlineDTO, concurso);
		} else {
			return this.distribuirParaEstabelecimentoSemArea(distribuicaoBilheteOnlineDTO, concurso);
		}

	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> distribuirParaRegional(DistribuicaoBilheteOnlineDTO dto, Concurso concurso) {
		Long disponivel = bilheteRepository.bilhetesOnlineDisponiveisGeral(concurso);

		if (disponivel < dto.getQuantidade()) {
			throw new IllegalArgumentException(
					"Quantidade indisponível para Regional. Quantidade atual: " + disponivel);
		}

		List<Bilhete> bilhetes = bilheteRepository.obterBilhetesOnlineParaRegional(concurso, dto.getQuantidade());
		return atualizarBilhetesRegional(bilhetes, dto.getRegional_id(), concurso);
	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> distribuirParaArea(DistribuicaoBilheteOnlineDTO dto, Concurso concurso) {
		Long disponivelParaArea = bilheteRepository.quantidadeOnlineDisponivelParaArea(concurso, dto.getRegional_id());

		if (disponivelParaArea < dto.getQuantidade()) {
			throw new IllegalArgumentException(
					"Quantidade indisponível na Regional. Quaantidade atual: " + disponivelParaArea);
		}

		List<Bilhete> bilhetes = bilheteRepository.obterBilhetesOnlineParaArea(concurso, dto.getRegional_id(),
				dto.getQuantidade());
		return this.atualizarBilheresArea(bilhetes, concurso, dto.getRegional_id(), dto.getArea_id());

	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> distribuirParaEstabelecimentoComArea(DistribuicaoBilheteOnlineDTO dto, Concurso concurso) {
		Long disponivelParaArea = bilheteRepository.quantidadeOnlineDisponivelParaEstabelecimentoComArea(concurso,
				dto.getRegional_id(), dto.getArea_id());

		if (disponivelParaArea < dto.getQuantidade()) {
			throw new IllegalArgumentException(
					"Quantidade indisponível na área. Quantidade atual: " + disponivelParaArea);
		}

		List<Bilhete> bilhetes = bilheteRepository.obterBilhetesOnlineParaEstabelecimentoComArea(concurso,
				dto.getRegional_id(), dto.getArea_id(), dto.getQuantidade());
		return this.atualizarBilhetesEstabelecimento(bilhetes, concurso, dto.getRegional_id(),
				dto.getEstabelecimento_id(),
				dto.getArea_id());

	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	public List<String> distribuirParaEstabelecimentoSemArea(DistribuicaoBilheteOnlineDTO dto, Concurso concurso) {

		Long disponivelParaRegional = bilheteRepository.quantidadeOnlineDisponivelParaArea(concurso,
				dto.getRegional_id());
		if (disponivelParaRegional < dto.getQuantidade()) {
			System.out.println("entrou no if disponivelParaRegional < dto.getQuantidade()");
			throw new IllegalArgumentException(
					"Quantidade indisponível na Regional. Quantidade atual: " + disponivelParaRegional);
		}

		List<Bilhete> bilhetes = bilheteRepository.obterBilhetesOnlineParaEstabelecimentoSemArea(concurso,
				dto.getRegional_id(), dto.getQuantidade());

		return this.atualizarBilhetesEstabelecimento(bilhetes, concurso, dto.getRegional_id(),
				dto.getEstabelecimento_id(),
				null);
	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> atualizarBilhetesRegional(List<Bilhete> bilhetes, Long regionalId, Concurso concurso) {
		DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
		transferencia.setConcurso(concurso);
		transferencia.setNumeroInicio(((Number) bilhetes.get(0).getNumero()).longValue());
		transferencia.setNumeroFim(((Number) bilhetes.get(bilhetes.size() -
				1).getNumero()).longValue());
		transferencia.setNovaRegional(new Regional(regionalId));
		transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		transferencia.setLastModifiedDate(ZonedDateTime.now());
		distribuicaoBilheteRepository.save(transferencia);

		Query queryBils = entityManager.createNativeQuery(
				"update bilhete set regional_id = :regid where concurso_id = :concid and numero between :numinicio and :numfim and (validado != TRUE or validado is null)");
		queryBils.setMaxResults(1);
		queryBils.setParameter("concid", concurso.getId());
		queryBils.setParameter("numinicio", bilhetes.get(0).getNumero());
		queryBils.setParameter("numfim", bilhetes.get(bilhetes.size() -
				1).getNumero());
		queryBils.setParameter("regid", regionalId);
		queryBils.executeUpdate();

		List<String> intervaloDeBilhetes = new ArrayList<>();
		intervaloDeBilhetes.add(bilhetes.get(0).getIdentificacao());
		intervaloDeBilhetes.add(bilhetes.get(bilhetes.size() - 1).getIdentificacao());
		return intervaloDeBilhetes;
	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> atualizarBilheresArea(List<Bilhete> bilhetes, Concurso concurso, Long regionalId,
			Long areaId) {
		DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
		transferencia.setConcurso(concurso);
		transferencia.setNumeroInicio(((Number) bilhetes.get(0).getNumero()).longValue());
		transferencia.setNumeroFim(((Number) bilhetes.get(bilhetes.size() -
				1).getNumero()).longValue());
		transferencia.setNovaRegional(new Regional(regionalId));
		transferencia.setNovaArea(new AreaRegional(areaId));
		transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		transferencia.setLastModifiedDate(ZonedDateTime.now());
		distribuicaoBilheteRepository.save(transferencia);

		Query queryBils = entityManager.createNativeQuery(
				"update bilhete set area_id = :areaid where concurso_id = :concid and regional_id =:regionalId and numero between :numinicio and :numfim and (validado != TRUE or validado is null)");
		queryBils.setMaxResults(1);
		queryBils.setParameter("concid", concurso.getId());
		queryBils.setParameter("numinicio", ((Number) bilhetes.get(0).getNumero()).longValue());
		queryBils.setParameter("numfim", ((Number) bilhetes.get(bilhetes.size() - 1).getNumero()).longValue());
		queryBils.setParameter("areaid", areaId);
		queryBils.setParameter("regionalId", regionalId);
		queryBils.executeUpdate();

		List<String> intervaloDeBilhetes = new ArrayList<>();
		intervaloDeBilhetes.add(bilhetes.get(0).getIdentificacao());
		intervaloDeBilhetes.add(bilhetes.get(bilhetes.size() - 1).getIdentificacao());
		return intervaloDeBilhetes;
	}

	@Transactional(rollbackFor = { IllegalArgumentException.class, RuntimeException.class })
	private List<String> atualizarBilhetesEstabelecimento(List<Bilhete> bilhetes, Concurso concurso, Long regionalId,
			Long estabelecimentoId,
			Long areaId) {

		DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
		transferencia.setConcurso(concurso);
		transferencia.setNumeroInicio(((Number) bilhetes.get(0).getNumero()).longValue());
		transferencia.setNumeroFim(((Number) bilhetes.get(bilhetes.size() -
				1).getNumero()).longValue());
		transferencia.setNovaRegional(new Regional(regionalId));
		if (areaId != null) {
			transferencia.setNovaArea(new AreaRegional(areaId));
		}

		transferencia.setNovoEstabelecimento(new Estabelecimento(estabelecimentoId));
		transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		transferencia.setLastModifiedDate(ZonedDateTime.now());
		distribuicaoBilheteRepository.save(transferencia);

		String sql = "update bilhete set estabelecimento_id=:estabid";

		if (areaId != null) {
			sql += " ,area_id = :areaid";
		}
		sql += " where concurso_id = :concid and regional_id=:reg and numero between :numinicio and :numfim and (validado != TRUE or validado is null)";

		Query queryBils = entityManager
				.createNativeQuery(sql);
		queryBils.setMaxResults(1);
		queryBils.setParameter("concid", concurso.getId());
		queryBils.setParameter("reg", regionalId);
		queryBils.setParameter("numinicio", ((Number) bilhetes.get(0).getNumero()).longValue());
		queryBils.setParameter("numfim", ((Number) bilhetes.get(bilhetes.size() - 1).getNumero()).longValue());
		queryBils.setParameter("estabid", estabelecimentoId);
		if (areaId != null) {
			queryBils.setParameter("areaid", areaId);
		}
		queryBils.executeUpdate();

		List<String> intervaloDeBilhetes = new ArrayList<>();
		intervaloDeBilhetes.add(bilhetes.get(0).getIdentificacao());
		intervaloDeBilhetes.add(bilhetes.get(bilhetes.size() - 1).getIdentificacao());
		return intervaloDeBilhetes;

	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	public void save(int concursoId, String indetificacaoBilheteInicial, String indetificacaoBilheteFinal,
			Regional regional, Estabelecimento estabelecimento, Character motivoDistribuicao, Long areaId)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			BilheteNaoCorrespondeConcurso, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {
		if (indetificacaoBilheteInicial == null || indetificacaoBilheteFinal == null
				|| indetificacaoBilheteInicial.isEmpty() || indetificacaoBilheteFinal.trim().isEmpty()) {
			throw new IllegalArgumentException("Valores obrigatorios não preenchidos");
		}

		Bilhete bilheteInicial = bilheteService.recuperaBilhetePelaIdentificacao(indetificacaoBilheteInicial);
		Bilhete bilheteFinal = bilheteService.recuperaBilhetePelaIdentificacao(indetificacaoBilheteFinal);
		if (!bilheteInicial.getConcurso().getId().equals(concursoId)) {
			throw new BilheteNaoCorrespondeConcurso(bilheteInicial.getIdentificacao());
		}
		if (!bilheteFinal.getConcurso().getId().equals(concursoId)) {
			throw new BilheteNaoCorrespondeConcurso(bilheteFinal.getIdentificacao());
		}

		if (bilheteFinal.getNumero().longValue() < bilheteInicial.getNumero().longValue()
				|| !bilheteInicial.getTipo().equals(bilheteFinal.getTipo())) {
			throw new IllegalArgumentException("Faixa inválida");
		}
		save(bilheteInicial, bilheteFinal, regional, estabelecimento, motivoDistribuicao, areaId);
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void save(Bilhete bilheteInicial, Bilhete bilheteFinal, Regional novaRegional,
			AreaRegional novaSubregional, Character motivoDistribuicao)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException,
			LotePertenceAOutraSubRegionalException {

		if (bilheteInicial != null && bilheteFinal != null
				&& bilheteInicial.getConcurso().getId() == bilheteFinal.getConcurso().getId()
				&& bilheteInicial.getNumero() <= bilheteFinal.getNumero()) {
			if (bilheteInicial.getNumero() == bilheteFinal.getNumero()) {
				saveOneSubRegional(bilheteInicial, novaRegional, novaSubregional, motivoDistribuicao);
			} else {
				saveLoteSubRegional(bilheteInicial, bilheteFinal, novaRegional, novaSubregional, motivoDistribuicao);
			}
		} else {
			throw new IllegalArgumentException("Bilhetes inválidos");
		}
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void saveOneSubRegional(Bilhete bilhete, Regional novaRegional, AreaRegional areaRegional,
			Character motivoDistribuicao) throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException,
			BilheteJaValidadoException, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException,
			LotePertenceAOutraSubRegionalException {

		Bilhete bil = bilheteService.recuperaBilhetePelaIdentificacao(bilhete.getIdentificacao());
		if (bil == null || bil.getConcurso() == null) {
			throw new IdentificacaoBilheteInvalida("---");
		}
		if (bil.isValidado()) {
			throw new BilheteJaValidadoException(bil.getIdentificacao());
		}
		Concurso conc = bil.getConcurso();
		Regional regAntiga = null, regNova = null;
		AreaRegional areaAntiga = null, areaNova = null;

		if (bil.getRegional_id() != null) {
			regAntiga = new Regional(bil.getRegional_id());
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(regAntiga);
		} else if (!SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_DISTRIBUICAO_REGIONAL)) {
			throw new UsuarioSemPermissaoException(SecurityUtils.getCurrentUserLogin());
		}

		// área
		if (bil.getAreaId() != null) {
			areaAntiga = new AreaRegional(bil.getAreaId());
		}

		if (novaRegional != null) {
			regNova = new Regional(novaRegional.getId());
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(regNova);
		}

		// área
		if (areaRegional != null) {
			areaNova = new AreaRegional(areaRegional.getId());
		}

		if (regNova == null && motivoDistribuicao != DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			throw new IllegalArgumentException("Nenhuma regional foi definida!");
		}

		if (areaAntiga != null && motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_SUBREGIONAL) {
			throw new LotePertenceAOutraSubRegionalException(areaAntiga.getNome());
		}

		if (regAntiga != null && motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_REGIONAL) {
			throw new LotePertenceAOutraRegionalException();
		}

		if ((regAntiga == null || novaRegional == null || areaRegional == null)
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO) {
			throw new IllegalArgumentException("Nenhum estabelecimento foi definido");
		}

		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_SUBREGIONAL
				&& regAntiga.getId() != novaRegional.getId()) {
			throw new FaixaJaCorrespondeAUmLoteException(bilhete.getIdentificacao(), bilhete.getIdentificacao());
		}

		DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
		if (regAntiga != null)
			transferencia.setAntigaRegional(regAntiga);
		if (regNova != null)
			transferencia.setNovaRegional(regNova);
		if (bil.getEstabelecimento_id() != null)
			transferencia.setAntigoEstabelecimento(new Estabelecimento(bil.getEstabelecimento_id()));
		if (areaNova != null)
			transferencia.setNovaArea((new AreaRegional(areaRegional.getId())));
		if (areaAntiga != null)
			transferencia.setAntigaArea(areaAntiga);
		transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		transferencia.setLastModifiedDate(ZonedDateTime.now());
		transferencia.setConcurso(bil.getConcurso());
		transferencia.setNumeroInicio(bil.getNumero());
		transferencia.setNumeroFim(bil.getNumero());
		transferencia.setMotivoDistribuicao(motivoDistribuicao);
		transferencia = distribuicaoBilheteRepository.save(transferencia);

		bil.setRegional_id((regNova != null) ? regNova.getId() : null);
		bil.setAreaId((areaNova != null ? areaNova.getId() : null));
		// bil.setEstabelecimento_id((novoEstabelecimento != null) ?
		// novoEstabelecimento.getId() : null);
		entityManager.merge(bil);

	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void saveLoteSubRegional(Bilhete bilheteInicial, Bilhete bilheteFinal, Regional novaRegional,
			AreaRegional areaRegional, Character motivoDistribuicao)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException,
			BilheteJaValidadoException, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {

		if (bilheteInicial == null || bilheteFinal == null
				|| !bilheteFinal.getConcurso().equals(bilheteInicial.getConcurso())) {
			throw new IdentificacaoBilheteInvalida("---");
		}
		if (novaRegional == null && motivoDistribuicao != DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			throw new IllegalArgumentException("Nenhuma regional foi definida!");
		}

		if ((novaRegional == null || areaRegional == null)
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO) {
			throw new IllegalArgumentException("Nenhum estabelecimento foi definido");
		}
		if (novaRegional != null) {
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(novaRegional);
		}

		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_SUBREGIONAL) {
			Query queryBils = entityManager.createQuery("select bil.identificacao "
					+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
					+ " bil.area_id != :area ");
			queryBils.setMaxResults(1);
			queryBils.setParameter("conc", bilheteInicial.getConcurso());
			queryBils.setParameter("numinicio", bilheteInicial.getNumero());
			queryBils.setParameter("numfim", bilheteFinal.getNumero());
			queryBils.setParameter("area", areaRegional.getId());
			List lstresregId = queryBils.getResultList();
			if (!lstresregId.isEmpty())
				throw new FaixaJaCorrespondeAUmLoteException("---", "---");
		}
		List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
		List<Long> regIdsComAcesso = new ArrayList<Long>();
		for (Regional reg : regsComAcesso)
			regIdsComAcesso.add(reg.getId());
		Query queryBils = entityManager.createQuery("select bil.identificacao "
				+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
				+ " (bil.regional_id not in :regids"
				+ (!SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_DISTRIBUICAO_REGIONAL)
						? " or bil.regional_id is null)"
						: ")"));
		queryBils.setMaxResults(1);
		queryBils.setParameter("conc", bilheteInicial.getConcurso());
		queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		queryBils.setParameter("numfim", bilheteFinal.getNumero());
		queryBils.setParameter("regids", regIdsComAcesso);
		List lstresregId = queryBils.getResultList();
		if (!lstresregId.isEmpty())
			throw new UsuarioSemPermissaoException("---");

		if (motivoDistribuicao != DistribuicaoBilhete.MOTIVO_REDISTRIBUICAO_NAO_CONTINUA) {
			queryBils = entityManager.createQuery("select bil.identificacao "
					+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
					+ " bil.validado = TRUE ");
			queryBils.setMaxResults(1);
			queryBils.setParameter("conc", bilheteInicial.getConcurso());
			queryBils.setParameter("numinicio", bilheteInicial.getNumero());
			queryBils.setParameter("numfim", bilheteFinal.getNumero());
			List lstresValidados = queryBils.getResultList();
			if (!lstresValidados.isEmpty())
				throw new BilheteJaValidadoException("---");
		}
		List<Object[]> lstIntervalos = bilheteService.listaIntervalosDistribuicaoNaoValidadoArea(
				bilheteInicial.getConcurso().getId(), bilheteInicial.getNumero(), bilheteFinal.getNumero());
		for (Object[] intervalo : lstIntervalos) {
			DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
			transferencia.setConcurso(new Concurso((int) intervalo[0]));
			transferencia.setNumeroInicio(((Number) intervalo[1]).longValue());
			transferencia.setNumeroFim(((Number) intervalo[2]).longValue());
			if (intervalo[3] != null)
				transferencia.setAntigaRegional(new Regional(((Number) intervalo[3]).longValue()));
			if (intervalo[4] != null)
				transferencia.setAntigoEstabelecimento(new Estabelecimento(((Number) intervalo[4]).longValue()));
			if (intervalo[5] != null)
				transferencia.setAntigaArea(new AreaRegional(((Number) intervalo[5]).longValue()));
			if (novaRegional != null)
				transferencia.setNovaRegional(novaRegional);
			if (areaRegional != null)
				transferencia.setNovaArea(areaRegional);
			transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
			transferencia.setLastModifiedDate(ZonedDateTime.now());
			transferencia.setMotivoDistribuicao(motivoDistribuicao);
			distribuicaoBilheteRepository.save(transferencia);
		}

		queryBils = entityManager.createNativeQuery("update bilhete "
				+ " set regional_id = :regid, area_id = :area "
				+ " where concurso_id = :concid and numero between :numinicio and :numfim and (validado != TRUE or validado is null)");
		queryBils.setMaxResults(1);
		queryBils.setParameter("concid", bilheteInicial.getConcurso().getId());
		queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		queryBils.setParameter("numfim", bilheteFinal.getNumero());
		queryBils.setParameter("regid", (novaRegional != null) ? novaRegional.getId() : null);
		queryBils.setParameter("area", (areaRegional != null) ? areaRegional.getId() : null);
		queryBils.executeUpdate();
	}

	// areaIDDDddd
	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void save(Bilhete bilheteInicial, Bilhete bilheteFinal, Regional novaRegional,
			Estabelecimento novoEstabelecimento, Character motivoDistribuicao, Long areaId)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {
		if (bilheteInicial != null && bilheteFinal != null
				&& bilheteInicial.getConcurso().getId() == bilheteFinal.getConcurso().getId()
				&& bilheteInicial.getNumero() <= bilheteFinal.getNumero()) {
			if (bilheteInicial.getNumero() == bilheteFinal.getNumero()) {
				saveOne(bilheteInicial, novaRegional, novoEstabelecimento, motivoDistribuicao, areaId);
			} else {
				saveLote(bilheteInicial, bilheteFinal, novaRegional, novoEstabelecimento, motivoDistribuicao, areaId);
			}
		} else {
			throw new IllegalArgumentException("Bilhetes inválidos");
		}
	}

	// regional
	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void saveOne(Bilhete bilhete, Regional novaRegional, Estabelecimento novoEstabelecimento,
			Character motivoDistribuicao, Long areaId)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException,
			BilheteJaValidadoException, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {

		Bilhete bil = bilheteService.recuperaBilhetePelaIdentificacao(bilhete.getIdentificacao());
		if (bil == null || bil.getConcurso() == null) {
			throw new IdentificacaoBilheteInvalida("---");
		}
		if (bil.isValidado()) {
			throw new BilheteJaValidadoException(bil.getIdentificacao());
		}
		Concurso conc = bil.getConcurso();
		Regional regAntiga = null, regNova = null;
		AreaRegional areaAntiga = null, areaNova = null;
		if (bil.getRegional_id() != null) {
			regAntiga = new Regional(bil.getRegional_id());
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(regAntiga);
		} else if (!SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_DISTRIBUICAO_REGIONAL)) {
			throw new UsuarioSemPermissaoException(SecurityUtils.getCurrentUserLogin());
		}
		if (novaRegional != null) {
			regNova = new Regional(novaRegional.getId());
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(regNova);
		}

		if (regNova == null && motivoDistribuicao != DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			throw new IllegalArgumentException("Nenhuma regional foi definida!");
		}

		if (regAntiga != null && motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_REGIONAL) {
			throw new LotePertenceAOutraRegionalException();
		}

		if ((regAntiga == null || novaRegional == null || novoEstabelecimento == null)
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO) {
			throw new IllegalArgumentException("Nenhum estabelecimento foi definido");
		}

		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO
				&& regAntiga.getId() != novaRegional.getId()) {
			throw new FaixaJaCorrespondeAUmLoteException(bilhete.getIdentificacao(), bilhete.getIdentificacao());
		}

		if (bil.getAreaId() != null) {
			areaAntiga = new AreaRegional(bil.getAreaId());
		}

		if (areaId != null) {
			areaNova = new AreaRegional(areaId);
		}

		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO
				&& areaAntiga.getId() != areaNova.getId()) {
			throw new FaixaJaCorrespondeAUmLoteException(bilhete.getIdentificacao(), bilhete.getIdentificacao());
		}

		DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
		if (regAntiga != null)
			transferencia.setAntigaRegional(regAntiga);
		if (regNova != null)
			transferencia.setNovaRegional(regNova);
		if (bil.getEstabelecimento_id() != null)
			transferencia.setAntigoEstabelecimento(new Estabelecimento(bil.getEstabelecimento_id()));
		if (novoEstabelecimento != null)
			transferencia.setNovoEstabelecimento(new Estabelecimento(novoEstabelecimento.getId()));
		transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		transferencia.setLastModifiedDate(ZonedDateTime.now());
		transferencia.setConcurso(bil.getConcurso());
		transferencia.setNumeroInicio(bil.getNumero());
		transferencia.setNumeroFim(bil.getNumero());
		transferencia.setMotivoDistribuicao(motivoDistribuicao);
		transferencia = distribuicaoBilheteRepository.save(transferencia);

		bil.setRegional_id((regNova != null) ? regNova.getId() : null);
		bil.setEstabelecimento_id((novoEstabelecimento != null) ? novoEstabelecimento.getId() : null);

		if (regNova == null && novoEstabelecimento == null
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			bil.setAreaId(null);
		}

		if (regNova != null && novoEstabelecimento == null
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			bil.setAreaId(null);
		}

		entityManager.merge(bil);
	}

	@Transactional(rollbackFor = { IdentificacaoBilheteInvalida.class, UsuarioSemPermissaoException.class,
			BilheteJaValidadoException.class, RuntimeException.class })
	protected void saveLote(Bilhete bilheteInicial, Bilhete bilheteFinal, Regional novaRegional,
			Estabelecimento novoEstabelecimento, Character motivoDistribuicao, Long areaId)
			throws IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException {

		if (bilheteInicial == null || bilheteFinal == null
				|| !bilheteFinal.getConcurso().equals(bilheteInicial.getConcurso())) {
			throw new IdentificacaoBilheteInvalida("---");
		}
		if (novaRegional == null && motivoDistribuicao != DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			throw new IllegalArgumentException("Nenhuma regional foi definida!");
		}

		if ((novaRegional == null || novoEstabelecimento == null)
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO) {
			throw new IllegalArgumentException("Nenhum estabelecimento foi definido");
		}
		if (novaRegional != null) {
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(novaRegional);
		}
		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_REGIONAL) {
			Query queryBils = entityManager.createQuery("select bil.identificacao "
					+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
					+ " bil.regional_id is not null ");
			queryBils.setMaxResults(1);
			queryBils.setParameter("conc", bilheteInicial.getConcurso());
			queryBils.setParameter("numinicio", bilheteInicial.getNumero());
			queryBils.setParameter("numfim", bilheteFinal.getNumero());
			List lstresregId = queryBils.getResultList();
			if (!lstresregId.isEmpty())
				throw new LotePertenceAOutraRegionalException();
		}

		if (motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO) {
			Query queryBils = entityManager.createQuery("select bil.identificacao "
					+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
					+ " bil.regional_id != :regId ");
			queryBils.setMaxResults(1);
			queryBils.setParameter("conc", bilheteInicial.getConcurso());
			queryBils.setParameter("numinicio", bilheteInicial.getNumero());
			queryBils.setParameter("numfim", bilheteFinal.getNumero());
			queryBils.setParameter("regId", novaRegional.getId());
			List lstresregId = queryBils.getResultList();
			if (!lstresregId.isEmpty())
				throw new FaixaJaCorrespondeAUmLoteException("---", "---");
		}

		/*
		 * if (motivoDistribuicao ==
		 * DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO){
		 * Query queryBils =
		 * entityManager.createQuery("select bil.identificacao, bil.estabelecimento_id "
		 * +
		 * " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
		 * + " bil.estabelecimento_id != :estId ");
		 * queryBils.setMaxResults(1);
		 * queryBils.setParameter("conc", bilheteInicial.getConcurso());
		 * queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		 * queryBils.setParameter("numfim", bilheteFinal.getNumero());
		 * queryBils.setParameter("estId", novoEstabelecimento.getId());
		 * List lstestabId = queryBils.getResultList();
		 * if(!lstestabId.isEmpty()){
		 * throw new FaixaJaCorrespondeAUmLoteException("---", "---");
		 * }
		 * 
		 * }
		 */
		List<Regional> regsComAcesso = regionalService.regionaisComAcesso();
		List<Long> regIdsComAcesso = new ArrayList<Long>();
		for (Regional reg : regsComAcesso)
			regIdsComAcesso.add(reg.getId());
		Query queryBils = entityManager.createQuery("select bil.identificacao "
				+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
				+ " (bil.regional_id not in :regids"
				+ (!SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_DISTRIBUICAO_REGIONAL)
						? " or bil.regional_id is null)"
						: ")"));
		queryBils.setMaxResults(1);
		queryBils.setParameter("conc", bilheteInicial.getConcurso());
		queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		queryBils.setParameter("numfim", bilheteFinal.getNumero());
		queryBils.setParameter("regids", regIdsComAcesso);
		List lstresregId = queryBils.getResultList();
		if (!lstresregId.isEmpty())
			throw new UsuarioSemPermissaoException("---");

		if (motivoDistribuicao != DistribuicaoBilhete.MOTIVO_REDISTRIBUICAO_NAO_CONTINUA) {
			queryBils = entityManager.createQuery("select bil.identificacao "
					+ " from Bilhete bil where bil.concurso = :conc and numero between :numinicio and :numfim and "
					+ " bil.validado = TRUE ");
			queryBils.setMaxResults(1);
			queryBils.setParameter("conc", bilheteInicial.getConcurso());
			queryBils.setParameter("numinicio", bilheteInicial.getNumero());
			queryBils.setParameter("numfim", bilheteFinal.getNumero());
			List lstresValidados = queryBils.getResultList();
			if (!lstresValidados.isEmpty())
				throw new BilheteJaValidadoException("---");
		}
		List<Object[]> lstIntervalos = bilheteService.listaIntervalosDistribuicaoNaoValidado(
				bilheteInicial.getConcurso().getId(), bilheteInicial.getNumero(), bilheteFinal.getNumero());
		for (Object[] intervalo : lstIntervalos) {
			DistribuicaoBilhete transferencia = new DistribuicaoBilhete();
			transferencia.setConcurso(new Concurso((int) intervalo[0]));
			transferencia.setNumeroInicio(((Number) intervalo[1]).longValue());
			transferencia.setNumeroFim(((Number) intervalo[2]).longValue());
			if (intervalo[3] != null)
				transferencia.setAntigaRegional(new Regional(((Number) intervalo[3]).longValue()));
			if (intervalo[4] != null)
				transferencia.setAntigoEstabelecimento(new Estabelecimento(((Number) intervalo[4]).longValue()));
			if (novaRegional != null)
				transferencia.setNovaRegional(novaRegional);
			if (novoEstabelecimento != null)
				transferencia.setNovoEstabelecimento(novoEstabelecimento);
			transferencia.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
			transferencia.setLastModifiedDate(ZonedDateTime.now());
			transferencia.setMotivoDistribuicao(motivoDistribuicao);
			distribuicaoBilheteRepository.save(transferencia);
		}

		if ((novaRegional == null && novoEstabelecimento == null
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO)
				|| (novaRegional != null && novoEstabelecimento == null
						&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO)) {
			queryBils = entityManager.createNativeQuery("update bilhete "
					+ " set regional_id = :regid, estabelecimento_id = :estid, area_id = :area "
					+ " where concurso_id = :concid and numero between :numinicio and :numfim and (validado != TRUE or validado is null)");
		} else {
			queryBils = entityManager.createNativeQuery("update bilhete "
					+ " set regional_id = :regid, estabelecimento_id = :estid "
					+ " where concurso_id = :concid and numero between :numinicio and :numfim and (validado != TRUE or validado is null)");
		}
		queryBils.setMaxResults(1);
		queryBils.setParameter("concid", bilheteInicial.getConcurso().getId());
		queryBils.setParameter("numinicio", bilheteInicial.getNumero());
		queryBils.setParameter("numfim", bilheteFinal.getNumero());
		queryBils.setParameter("regid", (novaRegional != null) ? novaRegional.getId() : null);
		queryBils.setParameter("estid", (novoEstabelecimento != null) ? novoEstabelecimento.getId() : null);

		if (novaRegional == null && novoEstabelecimento == null
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			queryBils.setParameter("area", null);
		}

		if (novaRegional != null && novoEstabelecimento == null
				&& motivoDistribuicao == DistribuicaoBilhete.MOTIVO_DEVOLUCAO) {
			queryBils.setParameter("area", null);
		}

		queryBils.executeUpdate();
	}

	public List<Bilhete> findAllBilheteDistribuidos(Concurso concurso, Long numeroInicio, Long numFim,
			Long estabelecimento) {
		return bilheteRepository.findAllByConcursoAndNumeroInicioNumeroFimBetween(concurso, numeroInicio, numFim,
				estabelecimento);
	}

	public List<Bilhete> findAllSubRegionalDistribuidos(Concurso concurso, Long subRegional) {
		return bilheteRepository.findAllBySubRegionalDistribuidos(concurso, subRegional);
	}

	public Page<DistribuicaoBilheteDTO> findAll(Bilhete bil, Pageable pageable) {
		return distribuicaoBilheteRepository
				.findAllByBilheteOrderByLastModifiedDate(bil.getConcurso(), bil.getNumero(), pageable)
				.map(DistribuicaoBilheteDTO::new);
	}

	public Page<DistribuicaoBilheteDTO> findAll(Concurso concurso, Regional reg, Pageable pageable) {
		return distribuicaoBilheteRepository.findAllByConcursoRegionalOrderByLastModifiedDate(concurso, reg, pageable)
				.map(DistribuicaoBilheteDTO::new);
	}

	public Page<DistribuicaoBilheteDTO> findAll(Concurso concurso, Pageable pageable) {
		return distribuicaoBilheteRepository.findAllByConcursoOrderByLastModifiedDate(concurso, pageable)
				.map(DistribuicaoBilheteDTO::new);
	}

	ThinDistribuicaoBilheteAtualDTO converterlistaDistribuicaoRegional(ThinConcursoDTO thinconc, Object[] source) {
		return new ThinDistribuicaoBilheteAtualDTO(thinconc, (String) source[3], (String) source[4],
				new ThinRegionalDTO(((Number) source[5]).longValue(), (String) source[6]), null,
				((Number) source[2]).intValue() - ((Number) source[1]).intValue() + 1);
	}

	ThinDistribuicaoBilheteAtualDTO converterlistaDistribuicaoRegionalEstabelecimento(ThinConcursoDTO thinconc,
			Object[] source) {

		ThinRegionalDTO regdto = new ThinRegionalDTO(((Number) source[5]).longValue(), (String) source[6]);
		ThinEstabelecimentoDTO estdto = new ThinEstabelecimentoDTO(((Number) source[7]).longValue(),
				(String) source[8]);
		Number areaId = (Number) source[9];
		if (areaId != null) {
			estdto.setAreaRegional(new ThinAreaRegionalDTO(areaId.longValue(), (String) source[10]));
		}
		return new ThinDistribuicaoBilheteAtualDTO(thinconc, (String) source[3], (String) source[4], regdto, estdto,
				((Number) source[2]).intValue() - ((Number) source[1]).intValue() + 1);
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoAtualRegional(Integer idconcurso, Long idregional,
			Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoRegional(idconcurso, idregional, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegional(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoAtualTodasRegional(Integer idconcurso,
			Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoTodasRegionais(idconcurso, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegional(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoDisponivelRegional(Integer idconcurso,
			Long idregional, Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoDisponivelRegional(idconcurso, idregional, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegional(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoDisponivelTodasRegional(Integer idconcurso,
			Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoDisponivelTodasRegional(idconcurso, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegional(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoTodosEstabelecimentosRegional(Integer idconcurso,
			Long idregional, Pageable pageable) {
		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
			AreaRegional area = arearegionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
			return consultaDistribuicaoTodosEstabelecimentosAreaRegional(idconcurso, idregional, area.getId(),
					pageable);
		} else {
			Concurso conc = concursoService.findOne(idconcurso);
			ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
			return bilheteService
					.listaIntervalosDistribuicaoTodosEstabelecimentosRegional(idconcurso, idregional, pageable)
					.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
						@Override
						public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
							return converterlistaDistribuicaoRegionalEstabelecimento(thinconc, source);
						}
					});
		}
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoTodosEstabelecimentosAreaRegional(
			Integer idconcurso, Long idregional, Long idarea, Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService
				.listaIntervalosDistribuicaoTodosEstabelecimentoAreaRegional(idconcurso, idregional, idarea, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegionalEstabelecimento(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultarDistribuicaoEstabelecimentoTodosOsFiltros(Integer idconcurso,
			Long idregional, Long idarea, Long idestabelecimento, Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService
				.listaIntervalosDistribuicaoTodosOsFiltros(idconcurso, idregional, idarea, idestabelecimento, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegionalEstabelecimento(thinconc, source);
					}
				});
	}

	public List<RelatorioDistribuicaoBilheteDTO> consultarRelatorioDistribuicaoEstabelecimento(Integer concursoId,
			Integer regionalId, Integer areaId, Integer estabelecimentoId) {
		System.out.println("CONCURSO ----- " + concursoId);

		String sql = "select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.regional_id, bil3.regional_nome, bil3.estabelecimento_id, bil3.estabelecimento_nome, bil3.area_id, bil3.area_nome "
				+
				"from ( " +
				"SELECT bil1.concurso_id, bil1.numero as numinicio,  " +
				"(SELECT min(bil3.numero) from bilhete bil3  " +
				"where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero   " +
				"and (bil3.regional_id = bil1.regional_id or (bil3.regional_id is null and bil1.regional_id is null))  "
				+
				"and (bil3.estabelecimento_id = bil1.estabelecimento_id or (bil3.estabelecimento_id is null and bil1.estabelecimento_id is null))  "
				+
				"and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id  " +
				"and bil2.numero = (bil3.numero+1)   " +
				"and (bil3.regional_id = bil2.regional_id or (bil3.regional_id is null and bil2.regional_id is null))  "
				+
				"and (bil3.estabelecimento_id = bil2.estabelecimento_id or (bil3.estabelecimento_id is null and bil2.estabelecimento_id is null)))  "
				+
				" ) as numfim, bil1.identificacao as identificacaoinicial, bil1.regional_id, reg.nome as regional_nome, bil1.estabelecimento_id, est.nome as estabelecimento_nome, area.area_id, area.nome as area_nome  "
				+
				" from bilhete bil1 left join regional reg on bil1.regional_id = reg.regional_id left join estabelecimento est on est.estabelecimento_id = bil1.estabelecimento_id  "
				+
				"left join arearegional area on area.area_id = est.area_id " +
				"where bil1.concurso_id = :concurso ";
		if (regionalId != null) {
			sql += " and bil1.regional_id = :regional ";
		}

		if (areaId != null) {
			sql += " and area.area_id = :area ";
		}

		if (estabelecimentoId == null) {
			sql += " and bil1.estabelecimento_id is not null ";
		}

		if (estabelecimentoId != null) {
			sql += " and bil1.estabelecimento_id = :estabelecimento ";
		}
		sql += "and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id  " +
				"and bil2.numero = (bil1.numero -1) " +
				"and (bil1.regional_id = bil2.regional_id or (bil1.regional_id is null and bil2.regional_id is null))  "
				+
				"and (bil1.estabelecimento_id = bil2.estabelecimento_id or (bil1.estabelecimento_id is null and bil2.estabelecimento_id is null))) "
				+
				") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim ";

		Query q = entityManager.createNativeQuery(sql);

		q.setParameter("concurso", concursoId);
		if (regionalId != null) {
			q.setParameter("regional", regionalId);
		}

		if (areaId != null) {
			q.setParameter("area", areaId);
		}

		if (estabelecimentoId != null) {
			q.setParameter("estabelecimento", estabelecimentoId);
		}

		List<Object[]> lst = q.getResultList();

		List<RelatorioDistribuicaoBilheteDTO> res = new ArrayList<RelatorioDistribuicaoBilheteDTO>();

		for (Object[] objs : lst) {
			RelatorioDistribuicaoBilheteDTO relatorio = new RelatorioDistribuicaoBilheteDTO();

			Integer IdConcurso = Integer.valueOf(objs[0].toString());
			Concurso conc = new Concurso(IdConcurso);
			relatorio.setConcurso(conc);
			relatorio.setNumInicial(Integer.valueOf(objs[1].toString()));
			relatorio.setNumFinal(Integer.valueOf(objs[2].toString()));
			relatorio.setIdentificacaoInicial(objs[3].toString());
			relatorio.setIdentificacaoFinal(objs[4].toString());
			String regional = objs[6] == null ? "" : objs[6].toString();
			relatorio.setRegional(regional);
			String valorEstabelecimento = objs[8] == null ? "" : objs[8].toString();
			relatorio.setEstabelecimento(valorEstabelecimento);
			String valorArea = objs[10] == null ? "" : objs[10].toString();
			relatorio.setArea(valorArea);

			res.add(relatorio);
		}

		return res;
	}

	public List<RelatorioDistribuicaoSubRegionalDTO> consultarRelatorioDistribuicaoSubRegional(Integer concursoId,
			Integer areaId) {
		String sql = "select bil3.concurso_id, bil3.numinicio, bil3.numfim, bil3.identificacaoinicial, bil4.identificacao as identificacaofinal, bil3.area_id, bil3.area_nome "
				+
				"from ( " +
				"SELECT bil1.concurso_id, bil1.numero as numinicio,  " +
				"(SELECT min(bil3.numero) from bilhete bil3  " +
				"where bil3.concurso_id = bil1.concurso_id and bil3.numero >= bil1.numero   " +
				"and (bil3.area_id = bil1.area_id or (bil3.area_id is null and bil1.area_id is null))  " +
				"and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil3.concurso_id  " +
				"and bil2.numero = (bil3.numero+1)   " +
				"and (bil3.area_id = bil2.area_id or (bil3.area_id is null and bil2.area_id is null)))  " +
				") as numfim, bil1.identificacao as identificacaoinicial, bil1.area_id, a.nome as area_nome " +
				"from bilhete bil1 left join arearegional a on bil1.area_id = a.area_id " +
				"where bil1.concurso_id = :conc";
		if (areaId == null) {
			sql += " AND bil1.area_id IS NOT NULL ";
		}

		if (areaId != null) {
			sql += " and bil1.area_id = :area  ";
		}

		sql += "and not exists (select 1 from bilhete bil2 where bil2.concurso_id = bil1.concurso_id  " +
				"and bil2.numero = (bil1.numero -1) " +
				"and (bil1.area_id = bil2.area_id or (bil1.area_id is null and bil2.area_id is null))) " +
				") bil3, bilhete bil4 where bil4.concurso_id = bil3.concurso_id and bil4.numero = bil3.numfim";

		Query q = entityManager.createNativeQuery(sql);

		q.setParameter("conc", concursoId);
		if (areaId != null) {
			q.setParameter("area", areaId);
		}

		List<Object[]> lst = q.getResultList();

		List<RelatorioDistribuicaoSubRegionalDTO> listaRelatorio = new ArrayList<RelatorioDistribuicaoSubRegionalDTO>();

		for (Object[] objs : lst) {

			RelatorioDistribuicaoSubRegionalDTO r = new RelatorioDistribuicaoSubRegionalDTO();
			Integer idConcurso = Integer.valueOf(objs[0].toString());
			Concurso conc = new Concurso(idConcurso);
			r.setConcurso(conc);
			r.setNumInicial(Integer.valueOf(objs[1].toString()));
			r.setNumFinal(Integer.valueOf(objs[2].toString()));
			r.setIdentificacaoInicial(objs[3].toString());
			r.setIdentificacaoFinal(objs[4].toString());
			r.setArea(objs[6].toString());

			listaRelatorio.add(r);
		}

		return listaRelatorio;
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoTodosEstabelecimentosTodasRegionais(
			Integer idconcurso, Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoTodosEstabelecimentosTodasRegional(idconcurso, pageable)
				.map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegionalEstabelecimento(thinconc, source);
					}
				});
	}

	public Page<ThinDistribuicaoBilheteAtualDTO> consultaDistribuicaoEstabelecimentoRegional(Integer idconcurso,
			Long idestabelecimento, Pageable pageable) {
		Concurso conc = concursoService.findOne(idconcurso);
		Estabelecimento est = estabelecimentoService.findOne(idestabelecimento);
		ThinConcursoDTO thinconc = new ThinConcursoDTO(conc);
		return bilheteService.listaIntervalosDistribuicaoEstabelecimentoRegional(idconcurso, est.getRegional().getId(),
				idestabelecimento, pageable).map(new Converter<Object[], ThinDistribuicaoBilheteAtualDTO>() {
					@Override
					public ThinDistribuicaoBilheteAtualDTO convert(Object[] source) {
						return converterlistaDistribuicaoRegionalEstabelecimento(thinconc, source);
					}
				});
	}
}
