package br.sorteioapp.service;

import br.sorteioapp.config.ApplicationProperties;
import br.sorteioapp.domain.DistribuicaoSelo;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Regional;


import br.sorteioapp.repository.DistribuicaoSeloRepository;

import br.sorteioapp.service.erros.RemocaoSeloInsuficienteException;
import br.sorteioapp.service.erros.SeloInvalidoException;
import br.sorteioapp.security.SecurityUtils;

import java.util.List;
import java.util.Arrays; 
import java.time.ZonedDateTime;

import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DistribuicaoSeloService {

    private final Logger log = LoggerFactory.getLogger(DistribuicaoSeloService.class);

    private final ConcursoService concursoService;
    private final RegionalService regionalService;
    private final EstabelecimentoService estabelecimentoService;
    private final DistribuicaoSeloRepository distribuicaoSeloRepository;

    public DistribuicaoSeloService (
        DistribuicaoSeloRepository distribuicaoSeloRepository, ConcursoService concursoService,
        RegionalService regionalService, EstabelecimentoService estabelecimentoService ) {

        this.concursoService = concursoService;
        this.regionalService = regionalService;
        this.estabelecimentoService = estabelecimentoService;
        this.distribuicaoSeloRepository = distribuicaoSeloRepository;
    }

    public List<DistribuicaoSelo> listarSelos(Concurso conc, Regional reg, Estabelecimento est) throws UsuarioSemPermissaoException {
        if (est != null) {
            est = estabelecimentoService.findOne(est.getId());
            reg = est.getRegional();
        }
        regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);

        if (est == null) {            
            return distribuicaoSeloRepository.findAllByConcursoRegionalOrderByLastModifiedDate(conc, reg);
        } else {
            return distribuicaoSeloRepository.findAllByConcursoEstabelecimentoOrderByLastModifiedDate(conc, est);
        } 
    }

    public DistribuicaoSelo distribuirSelos(Concurso conc, Regional reg, Estabelecimento est,
                                            String strini, String strfim) throws SeloInvalidoException, UsuarioSemPermissaoException {
        if (strini == null || strfim == null)
            throw new SeloInvalidoException();
        try {
            int numini = Integer.parseInt(strini);
            int numfim = Integer.parseInt(strfim);

            return distribuirSelos(conc, reg, est, numini, numfim);
        } catch (NumberFormatException nfe){
            throw new SeloInvalidoException();
        }
    }

    public DistribuicaoSelo distribuirSelos(Concurso conc, Regional reg, Estabelecimento est,
                                            int numini, int numfim) throws SeloInvalidoException, UsuarioSemPermissaoException {
        if (conc == null || reg == null || numfim < numini)
            throw new SeloInvalidoException();

        regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);
        DistribuicaoSelo dist = new DistribuicaoSelo(conc, reg, est, numini, numfim, DistribuicaoSelo.OPERACAO_INSERCAO);
        dist.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		dist.setLastModifiedDate(ZonedDateTime.now());
        return distribuicaoSeloRepository.save(dist);
    }

    public void devolverSelos(Concurso conc, Regional reg, Estabelecimento est, int numini, int numfim)
    throws SeloInvalidoException, RemocaoSeloInsuficienteException, UsuarioSemPermissaoException {
        if (conc == null || reg == null || numfim < numini)
            throw new SeloInvalidoException();
        regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);

        List<DistribuicaoSelo> lst = null;
        if (reg != null)
            lst = distribuicaoSeloRepository.findAllSelosRegionalInRange(conc,reg,numini,numfim);
        else 
            lst = distribuicaoSeloRepository.findAllSelosEstabelecimentoInRange(conc,est,numini,numfim);

        int[] qtd = new int[numfim-numini+1];
        for (int i = 0; i < qtd.length; i++) {
            qtd[i] = 0;
        }
        log.debug("INICIO "+numini + " "+numfim+ " "+ Arrays.toString(qtd));
        for (DistribuicaoSelo dist : lst){
            for (int k = dist.getNumeroInicio(); k <= dist.getNumeroFim(); k++) {
                if (k>=numini && k<=numfim)
                    qtd[k-numini] += (dist.getOperacao().charValue() == DistribuicaoSelo.OPERACAO_INSERCAO)? 1: -1;
            }
            log.debug(dist.getOperacao()+ " " + dist.getNumeroInicio() + " " + dist.getNumeroFim() + " " +
                       Arrays.toString(qtd));
        }
        log.debug("FIM "+numini + " "+numfim+ " "+ Arrays.toString(qtd));        
        boolean possuiSelos = true;
        for (int i = 0; i < qtd.length; i++) {
            possuiSelos = possuiSelos & (qtd[i] > 0);
        }

        if(possuiSelos){
            DistribuicaoSelo dist = new DistribuicaoSelo(conc, reg, est, numini, numfim, DistribuicaoSelo.OPERACAO_REMOCAO);
            dist.setLastModifiedBy(SecurityUtils.getCurrentUserLogin());
		    dist.setLastModifiedDate(ZonedDateTime.now());
            distribuicaoSeloRepository.save(dist);
        } else {
            throw new RemocaoSeloInsuficienteException("Quantidade de selos insulficiente para serem devolvidos da reg "+ reg + " est "+ est+ " no intervalo "+numini + " - " + numfim);
        }
    }

}
