package br.sorteioapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.EstabelecimentoBilheteOnline;
import br.sorteioapp.domain.Regional;

@Service
@Transactional
public class EstabalecimentoBilheteOnlineService {

    private EntityManager entityManager;

    public EstabalecimentoBilheteOnlineService(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<EstabelecimentoBilheteOnline> buscarEstabelecimentoBilheteOnline(Concurso conc, Regional reg,
            Long areaId, Boolean isOnline) {

        String sql = "SELECT e.estabelecimento_id AS 'Estabelecimento ID', e.nome AS 'Estabelecimento', e.endereco_bairro AS 'Bairro', e.endereco_cidade AS \"Cidade\", e.endereco_uf AS \"UF\", "
                + "COUNT(CASE WHEN (b.online = FALSE or b.online is null) THEN b.numero END) AS \"Distribuídos Bilhetes\", "
                + "COUNT(CASE WHEN (b.online = FALSE or b.online is null) AND b.validado = 1 THEN b.numero END) AS \"Validados Bilhete\", "
                + "COUNT(CASE WHEN b.online = TRUE THEN b.numero END) AS \"Distribuídos Online\", "
                + "COUNT(CASE WHEN b.online = TRUE AND b.validado = TRUE THEN b.numero END) AS \"Validados Online\" "
                + " FROM estabelecimento e "
                + "INNER JOIN bilhete b ON e.estabelecimento_id = b.estabelecimento_id "
                + "WHERE b.concurso_id = :concursoId AND b.regional_id = :regionalId and e.ativo = TRUE ";
        if (areaId != null) {
            sql += "AND e.area_id = :areaId ";
        }

        if (isOnline) {
            sql += "AND (e.online = :isOnline) ";

        }

        sql += "GROUP BY e.estabelecimento_id "
                + "ORDER BY e.nome";

        Query q = entityManager.createNativeQuery(sql);

        q.setParameter("concursoId", conc.getId());
        q.setParameter("regionalId", reg.getId());
        if (areaId != null) {
            q.setParameter("areaId", areaId);
        }

        if (isOnline) {
            q.setParameter("isOnline", isOnline != null ? isOnline : Boolean.FALSE);
        }

        List<Object[]> lst = q.getResultList();

        List<EstabelecimentoBilheteOnline> res = new ArrayList<EstabelecimentoBilheteOnline>();

        for (Object[] objs : lst) {
            EstabelecimentoBilheteOnline objEstabelecimentoBilheteOnline = new EstabelecimentoBilheteOnline();
            Integer rowEstabelecimentoId = Integer.valueOf((objs[0] == null) ? "0" : objs[0].toString());
            objEstabelecimentoBilheteOnline.setEstabalecimento_id(rowEstabelecimentoId);
            String nome = objs[1].toString();
            objEstabelecimentoBilheteOnline.setNome(nome);
            String bairro = objs[2].toString();
            objEstabelecimentoBilheteOnline.setBairro(bairro);
            String cidade = objs[3].toString();
            objEstabelecimentoBilheteOnline.setCidade(cidade);
            String uf = objs[4].toString();
            objEstabelecimentoBilheteOnline.setUf(uf);
            Integer bilhetesDistribuidos = Integer.valueOf((objs[5] == null) ? "0" : objs[5].toString());
            objEstabelecimentoBilheteOnline.setBilhetesDistribuidos(bilhetesDistribuidos);
            Integer bilhetesValidados = Integer.valueOf((objs[6] == null) ? "0" : objs[6].toString());
            objEstabelecimentoBilheteOnline.setBilhetesValidados(bilhetesValidados);
            Integer distribuidosOnline = Integer.valueOf((objs[7] == null) ? "0" : objs[7].toString());
            objEstabelecimentoBilheteOnline.setBilhetesDistribuidosOnline(distribuidosOnline);
            Integer validadosOnline = Integer.valueOf((objs[8] == null) ? "0" : objs[8].toString());
            objEstabelecimentoBilheteOnline.setValidados_on(validadosOnline);
            res.add(objEstabelecimentoBilheteOnline);
        }

        return res;
    }
}