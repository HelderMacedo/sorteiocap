package br.sorteioapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.QEstabelecimento;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.repository.AreaRegionalRepository;
import br.sorteioapp.repository.EstabelecimentoRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.EstabelecimentoDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;

/**
 * Service Implementation for managing Estabelecimento.
 */
@Service
@Transactional
public class EstabelecimentoService {

	public static final String QUERY_CACHE_TODAS_CIDADES_ESTABELECIMENTOS = "todasCidadesEstabelecimentos";
	public static final String QUERY_CACHE_TODOS_BAIRROS_ESTABELECIMENTOS = "todosBairrosEstabelecimentos";
	private final Logger log = LoggerFactory.getLogger(EstabelecimentoService.class);

	@PersistenceContext
	private EntityManager entityManager;

	private final EstabelecimentoRepository estabelecimentoRepository;
	private final AreaRegionalRepository arearegionalRepository;
	private final RegionalService regionalService;

	public EstabelecimentoService(EstabelecimentoRepository estabelecimentoRepository,
			AreaRegionalRepository arearegionalRepository, RegionalService regionalService) {
		this.estabelecimentoRepository = estabelecimentoRepository;
		this.arearegionalRepository = arearegionalRepository;
		this.regionalService = regionalService;
	}

	/**
	 * Save a estabelecimento.
	 *
	 * @param estabelecimentodto
	 *                           the entity to save
	 * @return the persisted entity
	 */
	// @Todo avaliar necessidade do cache evict abaixo do cache da entidade
	@Transactional(rollbackFor = { UsuarioSemPermissaoException.class, ErroCriarUsuarioException.class })
	@CacheEvict(value = { QUERY_CACHE_TODAS_CIDADES_ESTABELECIMENTOS,
			"br.sorteioapp.domain.Estabelecimento" }, beforeInvocation = true, allEntries = true)
	public Estabelecimento save(EstabelecimentoDTO estabelecimentodto)
			throws UsuarioSemPermissaoException, ErroCriarUsuarioException {
		if (estabelecimentodto.getRegional() == null || estabelecimentodto.getRegional().getId() == null) {
			throw new IllegalArgumentException("Regional é obrigatório.");
		}
		boolean updating = estabelecimentodto.getId() != null;
		boolean newuser = false;
		Estabelecimento est = null;
		if (updating) {
			est = estabelecimentoRepository.findOne(estabelecimentodto.getId());
		} else {
			est = new Estabelecimento();
		}

		Regional newReg = null;
		if (estabelecimentodto.getRegional() != null) {
			newReg = regionalService.findOne(estabelecimentodto.getRegional().getId());
		}

		verificaSeUsuarioPodeAlterarEstabelecimento(updating, est, newReg);

		est.setId(estabelecimentodto.getId());
		est.setPadraoRegional(estabelecimentodto.isPadrao());
		est.setAtivo(estabelecimentodto.isAtivo());
		est.setOnline(estabelecimentodto.getOnline());
		est.setBairro(estabelecimentodto.getBairro());
		est.setCep(estabelecimentodto.getCep());
		est.setCidade(estabelecimentodto.getCidade());
		est.setUf(estabelecimentodto.getUf());
		est.setCpf(estabelecimentodto.getCpf());
		est.setLogradouro(estabelecimentodto.getLogradouro());
		est.setNome(estabelecimentodto.getNome());
		est.setResponsavel(estabelecimentodto.getResponsavel());
		est.setNumero(estabelecimentodto.getNumero());
		est.setPontoReferencia(estabelecimentodto.getPontoReferencia());
		est.setTelefone(estabelecimentodto.getTelefone());
		est.setRegional(newReg);
		if (estabelecimentodto.getArearegional() != null)
			est.setArea(new AreaRegional(estabelecimentodto.getArearegional().getId()));
		else
			est.setArea(null);
		est.setPercentualRepasse(estabelecimentodto.getPercentualRepasse());
		Estabelecimento result = estabelecimentoRepository.save(est);
		return result;
	}

	/**
	 * Delete the estabelecimento by id.
	 */
	public void delete(Long id) {
		log.debug("Request to delete Estabelecimento : {}", id);
		estabelecimentoRepository.delete(id);
	}

	/**
	 * Get one estabelecimento by id.
	 *
	 * @param id
	 *           the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Estabelecimento findOne(Long id) {
		log.debug("Request to get Estabelecimento : {}", id);
		// @todo verificar a necessidade de restringir o acesso aqui
		// QEstabelecimento qest = QEstabelecimento.estabelecimento;
		// BooleanExpression bexp =
		// qest.regional.in(regionalService.regionaisComAcesso());
		// Predicate predicate = bexp.and(qest.id.eq(id));

		return estabelecimentoRepository.findOne(id);
	}

	/**
	 * Get all the estabelecimentos.
	 *
	 * @param pageable
	 *                 the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<Estabelecimento> findAll(Predicate predicate, Pageable pageable) {
		log.debug("Request to get all Estabelecimentos");
		QEstabelecimento qest = QEstabelecimento.estabelecimento;
		BooleanExpression bexp = qest.regional.in(regionalService.regionaisComAcesso());
		bexp = bexp.and(predicate);

		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
			AreaRegional area = arearegionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
			bexp = bexp.and(qest.area.eq(area));
		}

		return estabelecimentoRepository.findAll(bexp, pageable);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<Estabelecimento> consultaEstabelecimentosPorRegional(Long regionalId, Pageable pageable) {
		QEstabelecimento qest = QEstabelecimento.estabelecimento;
		Predicate predicate = qest.regional.eq(new Regional(regionalId));

		return findAll(predicate, pageable);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<Estabelecimento> consultaEstabelecimentosAtivosPorRegional(Long regionalId) {
		// return estabelecimentoRepository.queryAllActiveByRegional(new
		// Regional(regionalId));
		List<Estabelecimento> lst = new ArrayList();

		try {
			regionalService.verificaSeUsuarioTemAcessoSobreRegional(new Regional(regionalId));

			QEstabelecimento qest = QEstabelecimento.estabelecimento;
			BooleanExpression bexp = qest.regional.eq(new Regional(regionalId));
			bexp = bexp.and(qest.ativo.eq(Boolean.TRUE));

			if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
				AreaRegional area = arearegionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin());
				bexp = bexp.and(qest.area.eq(area));
			}
			estabelecimentoRepository.findAll(bexp, new Sort(Sort.Direction.ASC, "nome")).forEach(lst::add);
		} catch (UsuarioSemPermissaoException e) {
			// retorna uma lista vazia
		}
		return lst;
	}

	public List<Estabelecimento> getEstabelecimentosOnlinePorRegionalEArea(Long regionalId, Long areaId,
			boolean online) {
		if (areaId == null) {
			return estabelecimentoRepository.findByAtivoTrueAndRegionalIdAndOnline(regionalId, online);
		} else {
			return estabelecimentoRepository.findByAtivoTrueAndRegionalIdAndAreaIdAndOnline(regionalId, areaId, online);
		}
	}

	public List<Estabelecimento> consultarEstabelecimentosPorSubRegional(Long areaId) {
		AreaRegional areaRegional = new AreaRegional(areaId);
		return estabelecimentoRepository.estabelecimentosPorSubRegional(areaRegional);
	}

	private boolean verificaSeUsuarioPodeAlterarEstabelecimento(boolean isupdating, Estabelecimento est,
			Regional newreg) throws UsuarioSemPermissaoException {
		boolean ok = false;
		boolean permCadEstabelecimento = (SecurityUtils
				.isCurrentUserWithAuthority(AuthoritiesConstants.PERM_CADASTRO_ESTABELECIMENTO));
		List<Regional> regionaisAcesso = regionalService.regionaisComAcesso();

		ok = permCadEstabelecimento && regionaisAcesso.contains(newreg) &&
				(!isupdating || regionaisAcesso.contains(est.getRegional()));

		if (!ok)
			throw new UsuarioSemPermissaoException(SecurityUtils.getCurrentUserLogin());

		return ok;
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	@Cacheable(value = QUERY_CACHE_TODAS_CIDADES_ESTABELECIMENTOS)
	public List<String> queryAllCidades() {
		return estabelecimentoRepository.queryAllCidades();
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	@Cacheable(value = QUERY_CACHE_TODOS_BAIRROS_ESTABELECIMENTOS, key = "#cidade")
	public List<String> queryAllBairros(String cidade) {
		return estabelecimentoRepository.queryAllBairros(cidade);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<Estabelecimento> queryAllByCidade(String cidade, Pageable pageable) {
		return estabelecimentoRepository.queryAllByCidadeAndAtivo(cidade, true, pageable);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<Estabelecimento> queryAllByCidadeAndBairro(String cidade, String bairro, Pageable pageable) {
		return estabelecimentoRepository.queryAllByCidadeAndBairroAndAtivo(cidade, bairro, true, pageable);
	}

}
