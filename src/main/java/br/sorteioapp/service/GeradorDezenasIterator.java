package br.sorteioapp.service;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.util.combinatoria.CombinatoriaUtil;
import br.sorteioapp.util.criptografia.FPE;

import java.math.BigInteger;
import java.util.*;

public class GeradorDezenasIterator implements Iterator<GeradorDezenasIterator.CombOrderDezena> {
    private final FPE.EncryptorX encryptor;
    private final int maxNumEmSequencia;
    private final int maxNumIntersecao;
    private final long[] simseq;
    private final Concurso conc;
    private final boolean apliqueRestricoes;

    List<GeradorDezenasIterator.CombOrderDezena> lstGerados;
    private long orderNumber;


    public  GeradorDezenasIterator (long startOrderNumber, Concurso conc, boolean apliqueRestricoes) throws Exception {
        this(new FPE.EncryptorX(conc.getTipo().getTotalCombinacoes(),
            conc.getChaveCriptografica().getBytes()), startOrderNumber, conc, apliqueRestricoes);
    }
    public GeradorDezenasIterator(FPE.EncryptorX encryptor, long startOrderNumber, Concurso conc, boolean apliqueRestricoes){
        this(encryptor, startOrderNumber, conc,
            conc.getTipo().getQuantidadeNumeros()/15,
            (int) Math.ceil(conc.getTipo().getNumerosBilhete() * 2.0 / 3.0), apliqueRestricoes);
    }

    public GeradorDezenasIterator(FPE.EncryptorX encryptor, long startOrderNumber, Concurso conc,
                                  int maxNumEmSequencia, int maxNumIntersecao, boolean apliqueRestricoes){

        this.lstGerados =  new ArrayList();
        this.encryptor = encryptor;
        this.orderNumber = startOrderNumber;
        this.conc = conc;
        this.maxNumEmSequencia = maxNumEmSequencia;
        this.maxNumIntersecao = maxNumIntersecao;
        this.apliqueRestricoes = apliqueRestricoes;

        // Inicializa mascara para verifica numeros em sequencia
        simseq = new long[conc.getTipo().getQuantidadeNumeros() - maxNumEmSequencia];
        long mask = 0b1111l;
        for (int i=0; i < simseq.length; i++) {
            simseq[i] = mask;
            mask = mask << 1;
        }
    }

    @Override
    public boolean hasNext() {
        return orderNumber < (encryptor.getModulus().longValue()-1);
    }

    @Override
    public GeradorDezenasIterator.CombOrderDezena next() {
        GeradorDezenasIterator.CombOrderDezena co = null;
        boolean ok = false;
        try {
            do {
                BigInteger combinadics = encryptor.encrypt(BigInteger.valueOf(orderNumber - 1));
                BigInteger bitSet = null;
                long longbitSet;
                if (conc.getTipo().getQuantidadeNumeros() <= 60){
                    longbitSet = CombinatoriaUtil.rankParaLongBitSet(combinadics, conc.getTipo().getNumerosBilhete());
                    bitSet = BigInteger.valueOf(longbitSet);
                } else {
                    bitSet = CombinatoriaUtil.rankParaBitSet(combinadics, conc.getTipo().getNumerosBilhete());
                    longbitSet = bitSet.longValue();
                }

                co = new GeradorDezenasIterator.CombOrderDezena(orderNumber, combinadics, bitSet, longbitSet);
                ok = (!apliqueRestricoes) || isOk(co); // ou não aplica restricoes ou as verifica
                orderNumber++;
            } while ( (!ok) && hasNext());
        } catch (Exception e) {
            e.printStackTrace();
            throw new NoSuchElementException("Erro na geração de dezenas.");
        }
        if (ok) {
            lstGerados.add(co);
            return co;
        } else {
            throw new NoSuchElementException("Nenhum novo elemento que atenda as condições requeridas.");
        }
     }

    private boolean isOk(CombOrderDezena co) {
        boolean ok = true;
        // verifica numeros nas dezenas em sequencia
        for (int k=0; ok && k < simseq.length; k++) {
            ok = ((simseq[k] & co.longbitSet) != simseq[k]);
        }
        // verifica intersecao minima entre as deznas
        int distMinima = maxNumIntersecao ;
        for (int i = 0; ok & i < lstGerados.size(); i++) {
            CombOrderDezena co2 = lstGerados.get(i);
            long andnumber = co.longbitSet & co2.longbitSet;
            int distance = Long.bitCount(andnumber);
            ok = distance <= distMinima;
        }
        return ok;
    }

    public static class CombOrderDezena implements Comparable<GeradorDezenasIterator.CombOrderDezena> {
        BigInteger combinadics;
        long num;
        BigInteger bitSet;
        long longbitSet;

        public CombOrderDezena(Long num, BigInteger c, BigInteger b, long l) {
            this.num = num;
            this.combinadics = c;
            this.bitSet = b;
            this.longbitSet = l;
        }

        public BigInteger getCombinadics() {
            return combinadics;
        }

        public BigInteger getBitset() {
            return bitSet;
        }

        public long getNum() {
            return num;
        }

        public long getLongbitSet() {
            return longbitSet;
        }

        public String getDezenas() {
            if (bitSet == null || bitSet.equals(BigInteger.ZERO)) {
                return null;
            } else {
                String dezenas = Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(getBitset()));
                dezenas = dezenas.substring(1, dezenas.length() - 1).replace(", ", "|");
                return dezenas;
            }

        }

        @Override
        public int compareTo(GeradorDezenasIterator.CombOrderDezena o) {
            return combinadics.compareTo(o.combinadics);
        }
    }
}
