package br.sorteioapp.service;

import java.util.Optional;
import java.util.*;

import br.sorteioapp.domain.GrupoLancamentoFinanceiro;
import br.sorteioapp.domain.TipoOperacaoLancamentoFinanceiro;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.repository.GrupoLancamentoFinanceiroRepository;

/**
 * Service Implementation for managing GrupoLancamentoFinanceiro.
 */
@Service
@Transactional
public class GrupoLancamentoFinanceiroService {

	private final Logger log = LoggerFactory.getLogger(GrupoLancamentoFinanceiroService.class);

	private final GrupoLancamentoFinanceiroRepository grupoLancamentoFinanceiroRepository;

	public GrupoLancamentoFinanceiroService(GrupoLancamentoFinanceiroRepository grupoLancamentoRepository) {
		this.grupoLancamentoFinanceiroRepository = grupoLancamentoRepository;
	}

	/**
	 * Save a grupoLancamento.
	 */
	public GrupoLancamentoFinanceiro save(GrupoLancamentoFinanceiro dto) {
		log.debug("Request to save GrupoLancamentoFinanceiro : {}", dto);
		GrupoLancamentoFinanceiro grupoLancamentoFinanceiro = new GrupoLancamentoFinanceiro();
		if (dto.getId() != null) {
			grupoLancamentoFinanceiro = grupoLancamentoFinanceiroRepository.findOne(dto.getId());
		}
		if (dto.getTipoOperacao() != null)
    		grupoLancamentoFinanceiro.setTipoOperacao( grupoLancamentoFinanceiroRepository.findTipoById(dto.getTipoOperacao().getId()).get());
		
		grupoLancamentoFinanceiro.setNomeGrupo(dto.getNomeGrupo());
		grupoLancamentoFinanceiro = grupoLancamentoFinanceiroRepository.save(grupoLancamentoFinanceiro);
		return grupoLancamentoFinanceiro;
	}

	/**
	 * Get all the grupoLancamentos.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<GrupoLancamentoFinanceiro> findAll(Pageable pageable) {
		log.debug("Request to get all GrupoLancamentos");
		return grupoLancamentoFinanceiroRepository.findByOrderByNomeGrupoAsc(pageable);
	}

	@Transactional(readOnly = true)
	public List<TipoOperacaoLancamentoFinanceiro> listAllTipoOperacao() {
		log.debug("Request to get all TipoOperacao");
		return grupoLancamentoFinanceiroRepository.listaTiposOperacoes();
	}	

	@Transactional(readOnly = true)
	public List<GrupoLancamentoFinanceiro> listAllByTipoOperacao(Long idtipo) {
		log.debug("Request to get all GrupoLancamentos of type "+idtipo);
		Optional<TipoOperacaoLancamentoFinanceiro> otipo = grupoLancamentoFinanceiroRepository.findTipoById(idtipo);
		if (otipo.isPresent())		
			return grupoLancamentoFinanceiroRepository.findByTipoOperacaoOrderByNomeGrupoAsc(otipo.get());
		else 
			return new ArrayList(); 	
	}	
	/**
	 * Get one grupoLancamento by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Optional<GrupoLancamentoFinanceiro> findOne(Long id) {
		log.debug("Request to get GrupoLancamentoFinanceiro : {}", id);
		return grupoLancamentoFinanceiroRepository.findById(id);
	}

	/**
	 * Delete the grupoLancamento by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete GrupoLancamentoFinanceiro : {}", id);
		grupoLancamentoFinanceiroRepository.delete(id);
	}
}
