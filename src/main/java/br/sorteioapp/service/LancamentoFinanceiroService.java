package br.sorteioapp.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.sorteioapp.domain.*;
import br.sorteioapp.service.dto.LancamentoFinanceiroDTO;
import br.sorteioapp.service.dto.ThinConcursoDTO;
import br.sorteioapp.service.dto.ThinDistribuicaoBilheteAtualDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.sorteioapp.domain.LancamentoFinanceiro;
import br.sorteioapp.repository.ConcursoRepository;
import br.sorteioapp.repository.LancamentoFinanceiroRepository;
import br.sorteioapp.repository.GrupoLancamentoFinanceiroRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;

/**
 * Service Implementation for managing LancamentoFinanceiro.
 */
@Service
@Transactional
public class LancamentoFinanceiroService {

	private final Logger log = LoggerFactory.getLogger(LancamentoFinanceiroService.class);

	private final LancamentoFinanceiroRepository lancamentoFinanceiroRepository;
	private final GrupoLancamentoFinanceiroRepository grupolancamentoFinanceiroRepository;
	private final ConcursoRepository concursoRepository;
	private final RegionalService regionalService;
	private final AreaRegionalService arearegionalService;
	
	public LancamentoFinanceiroService(LancamentoFinanceiroRepository lancamentoFinanceiroRepository, GrupoLancamentoFinanceiroRepository grupolancamentoFinanceiroRepository,
                                       ConcursoRepository concursoRepository, RegionalService regionalService, AreaRegionalService arearegionalService) {
		this.grupolancamentoFinanceiroRepository = grupolancamentoFinanceiroRepository;
		this.concursoRepository = concursoRepository;
		this.lancamentoFinanceiroRepository = lancamentoFinanceiroRepository;
		this.regionalService = regionalService;
		this.arearegionalService = arearegionalService;
	}

	/**
	 * Save a lancamentoFinanceiro.
	 *
	 * @param dto
	 *            the entity to save
	 * @return the persisted entity
	 */
	public LancamentoFinanceiroDTO save(LancamentoFinanceiroDTO dto) {
		log.debug("Request to save LancamentoFinanceiro : {}", dto);
		// LancamentoFinanceiro lancamentoFinanceiro = lancamentoFinanceiroMapper.toEntity(lancamentoFinanceiroDTO);
		LancamentoFinanceiro lancamentoFinanceiro = null;
		if (dto.getId() == null) {
			lancamentoFinanceiro = new LancamentoFinanceiro();
			lancamentoFinanceiro.setGrupoLancamento(grupolancamentoFinanceiroRepository.findOne(dto.getGrupoLancamento().getId()));
		} else {
			lancamentoFinanceiro = lancamentoFinanceiroRepository.findOne(dto.getId());
			if (lancamentoFinanceiro.getGrupoLancamento().getId() != dto.getGrupoLancamento().getId())
				lancamentoFinanceiro.setGrupoLancamento(grupolancamentoFinanceiroRepository.findOne(dto.getGrupoLancamento().getId()));
		}
        lancamentoFinanceiro.setDataLancamento(dto.getDataLancamento());
        lancamentoFinanceiro.setValor(dto.getValor());
        lancamentoFinanceiro.setDescricao(dto.getDescricao());
		lancamentoFinanceiro = lancamentoFinanceiroRepository.save(lancamentoFinanceiro);
		return new LancamentoFinanceiroDTO(lancamentoFinanceiro);
	}

	/**
	 * Get all the lancamentoFinanceiros.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Transactional(readOnly = true)
	public Page<LancamentoFinanceiroDTO> findAll(Predicate predicate, Pageable pageable) {
		return lancamentoFinanceiroRepository.findAll(predicate, pageable).map(LancamentoFinanceiroDTO::new);
	}

	@Transactional(readOnly = true)
	public Page<LancamentoFinanceiroDTO> findAllBetweenData(LocalDate dataInicio, LocalDate dataFim, Pageable pageable) throws UsuarioSemPermissaoException {
		return lancamentoFinanceiroRepository.findByDataLancamentoBetween(dataInicio, dataFim, pageable).map(LancamentoFinanceiroDTO::new);
	}

    @Transactional(readOnly = true)
	public List<LancamentoFinanceiroDTO> consultaTotalizacaoLancamentos(LocalDate dataInicio, LocalDate dataFim){
        //Concurso conc =  concursoRepository.findOne(concid);
        //ThinConcursoDTO concdto = new ThinConcursoDTO(conc);
	    List<LancamentoFinanceiroDTO> resplst = new ArrayList<>();
        List<Object[]> objlst = lancamentoFinanceiroRepository.consultaTotalizacaoLancamentos(dataInicio, dataFim);
	    for( Object[] objs  : objlst){
            LancamentoFinanceiroDTO dto = new LancamentoFinanceiroDTO();
            dto.setGrupoLancamento((GrupoLancamentoFinanceiro) objs[0]);
            dto.setValor( ((Double) objs[1]).floatValue());
            dto.setDescricao("Totalizacao");
            resplst.add(dto);
        }
	    return resplst;
    }
	/*
	@Transactional(readOnly = true)
	private BooleanExpression lancamentoFinanceirosRegionaisComAcesso(QLancamentoFinanceiro qlancamentoFinanceiro) {
		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_TODOS)) {
			// retorna um predicado sempre verdadeiro
			return qlancamentoFinanceiro.id.isNotNull();
		} else if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
			AreaRegional area = arearegionalService.findByLogin(SecurityUtils.getCurrentUserLogin());			
			return qlancamentoFinanceiro.regional.eq(area.getRegional());
		} else if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_COM_LOGIN)) {
			Regional reg = regionalService.findByLogin(SecurityUtils.getCurrentUserLogin());
			return qlancamentoFinanceiro.regional.eq(reg);
		} else {
			// não deve ter acesso a nada !
			// retorna um predicado sempre falso
			return qlancamentoFinanceiro.id.isNull();
		}
	}
    */

	/**
	 * Get one lancamentoFinanceiro by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Optional<LancamentoFinanceiroDTO> findOne(Long id) {
		log.debug("Request to get LancamentoFinanceiro : {}", id);
		return lancamentoFinanceiroRepository.findById(id).map(LancamentoFinanceiroDTO::new);
	}

	/**
	 * Delete the lancamentoFinanceiro by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	public void delete(Long id) {
		log.debug("Request to delete LancamentoFinanceiro : {}", id);
		lancamentoFinanceiroRepository.delete(id);
	}
}
