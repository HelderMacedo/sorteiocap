package br.sorteioapp.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.LoteGeracaoBilhete;
import br.sorteioapp.domain.TipoBilhete;
import br.sorteioapp.repository.LoteGeracaoBilheteRepository;

@Service
@Transactional
public class LoteGeracaoBilheteService {
	private final Logger log = LoggerFactory.getLogger(LoteGeracaoBilheteService.class);

	@PersistenceContext
	private EntityManager entityManager;

	private final LoteGeracaoBilheteRepository loteGeracaoRepository;
	private final BilheteService bilheteService;

	public LoteGeracaoBilheteService(LoteGeracaoBilheteRepository loteGeracaoRepository,
			BilheteService bilheteService) {
		this.loteGeracaoRepository = loteGeracaoRepository;
		this.bilheteService = bilheteService;
	}

	@Transactional
	public synchronized int geraLoteBilhetes(Concurso conc, int quant, boolean online) {
		try {
			TipoBilhete tipo = conc.getTipo();
			if (quant > 0) {
				conc = entityManager.find(Concurso.class, conc.getId());
				LoteGeracaoBilhete ultimoLote = loteGeracaoRepository
						.findTopByConcursoIdOrderByNumeroInicialDesc(conc.getId());
				long ultimoNumero = (ultimoLote != null) ? ultimoLote.getBilheteFinal().getNumero() : 0;
				Long ultimoNumOrder1 = (ultimoLote != null) ? ultimoLote.getBilheteFinal().getNumorder() : 0;
                Long ultimoNumOrder2 = (ultimoLote != null) ? ultimoLote.getBilheteFinal().getNumorder2() : 0;
                long ultimoNumOrder = Math.max((ultimoNumOrder1!=null)?ultimoNumOrder1.longValue():0,
                                               (ultimoNumOrder2!=null)?ultimoNumOrder2.longValue():0);
				LoteGeracaoBilhete lote;

				Query q = entityManager.createNativeQuery("select dupla_chance from concurso where concurso_id = "+conc.getId());
                Boolean duplaChance = Boolean.valueOf(  q.getSingleResult().toString());
                conc.setDuplaChance(duplaChance);
				int bilhetesPersistidos = bilheteService.geraBilhetes(conc, ultimoNumero, ultimoNumOrder, quant,
						online);
				if (ultimoLote != null && tipo.equals(ultimoLote.getTipo())
						&& !Boolean.logicalXor(online, Boolean.TRUE.equals(ultimoLote.getOnline()))) {
					ultimoLote.setNumeroFinal(ultimoNumero + bilhetesPersistidos);
					entityManager.merge(ultimoLote);
					lote = ultimoLote;
				} else {
					lote = new LoteGeracaoBilhete();
					lote.setTipo(tipo);
					lote.setNumeroInicial(ultimoNumero + 1);
					lote.setNumeroFinal(ultimoNumero + bilhetesPersistidos);
					lote.setConcursoId(conc.getId());
					lote.setOnline(online);
					entityManager.persist(lote);
				}
				return bilhetesPersistidos;
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return 0;
		}
		return 0;
	}

}
