package br.sorteioapp.service;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.RegionalBilheteOnline;
import br.sorteioapp.service.dto.*;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
@Service
@Transactional
public class RegionalBilheteOnlineService {
    private EntityManager entityManager;

    public RegionalBilheteOnlineService(EntityManager entityManager){
        this.entityManager = entityManager;
    }

    public List<RegionalBilheteOnline> buscarRegionalBilheteOnline(Concurso conc, Regional reg){
        
        String sql = 
        "select r.concurso_id, r.regional_id, r.nome, sum(distribuidos),sum(r.validados_on), sum(r.validados_off) "+
        "from(select b.concurso_id, b.regional_id,reg.nome,count(b.numero) as distribuidos,count(b.validado = true) as validados_on, 0 as validados_off "+
        "from bilhete b "+ 
        "inner join regional reg on reg.regional_id = b.regional_id  "+
        "where b.concurso_id = :conc ";
        if(reg != null){
            sql += " and b.regional_id = :reg ";
        }        
        sql += " and b.online = 1 and b.estabelecimento_id is not null "+
        "group by b.regional_id, reg.nome  "+
        "union "+
        "select b.concurso_id, b.regional_id,reg.nome,count(b.numero) as distribuidos,0 as validados_on,count(b.validado = true) as validados_off "+
        "from bilhete b  "+
        "inner join regional reg on reg.regional_id = b.regional_id  "+
        "where b.concurso_id = :conc ";
        if(reg != null){
            sql += " and b.regional_id = :reg ";
        }
        sql += " and coalesce(b.online, 0) = 0 and b.estabelecimento_id is not null "+
        "group by b.regional_id, reg.nome )r group by r.concurso_id, r.nome ";

        Query q = entityManager.createNativeQuery(sql);
        q.setParameter("conc", conc.getId());
        if(reg != null){
            q.setParameter("reg", reg.getId());  
        }
        List<Object[]> lst = q.getResultList();

        List<RegionalBilheteOnline> res = new ArrayList<RegionalBilheteOnline>();

        for(Object[] objs : lst){
            
            RegionalBilheteOnline objRegionalBilheteOnline = new RegionalBilheteOnline();
            Integer rowConcurso = Integer.valueOf((objs[0] == null) ? "0" : objs[0].toString()); 
            objRegionalBilheteOnline.setConcurso_id(rowConcurso);
            Integer rowRegional = Integer.valueOf((objs[1] == null) ? "0" : objs[1].toString());
            objRegionalBilheteOnline.setRegional_id(rowRegional);
            String rowNome = objs[2].toString();
            objRegionalBilheteOnline.setNome(rowNome);
            Integer rowDistribuidos = Integer.valueOf((objs[3] == null) ? "0" : objs[3].toString());
            objRegionalBilheteOnline.setDistribuidos(rowDistribuidos);
            Integer rowValidados_on = Integer.valueOf((objs[4] == null) ? "0" : objs[4].toString());
            objRegionalBilheteOnline.setValidados_on(rowValidados_on);
            Integer rowValidados_off = Integer.valueOf((objs[5] == null) ? "0" : objs[5].toString());
            objRegionalBilheteOnline.setValidados_off(rowValidados_off);

            res.add(objRegionalBilheteOnline);
        }

        return res;
    }
}