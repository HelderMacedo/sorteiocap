package br.sorteioapp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;

import br.sorteioapp.domain.QRegional;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.Regiao;
import br.sorteioapp.domain.User;
import br.sorteioapp.repository.AreaRegionalRepository;
import br.sorteioapp.repository.RegionalRepository;
import br.sorteioapp.repository.UserRepository;
import br.sorteioapp.repository.RegiaoRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.RegionalDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;

/**
 * Service Implementation for managing Regional.
 */
@Service
@Transactional
public class RegionalService {

	// public static final String QUERY_CACHE_TODAS_REGIONAIS = "todasRegionais";
	// public static final String QUERY_CACHE_TODAS_REGIONAIS_ATIVAS =
	// "todasRegionaisAtivas";
	private final Logger log = LoggerFactory.getLogger(RegionalService.class);

	private final RegionalRepository regionalRepository;
	private final AreaRegionalRepository arearegionalRepository;
	private final UserService userService;
    private final RegiaoRepository regiaoRepository;
	private final UserRepository userRepository;

	public RegionalService(RegionalRepository RegionalRepository, 
			AreaRegionalRepository arearegionalRepository,
			UserService userService, RegiaoRepository regiaoRepository, UserRepository userRepository) {
		this.regionalRepository = RegionalRepository;
		this.arearegionalRepository = arearegionalRepository;
		this.userService = userService;
		this.regiaoRepository = regiaoRepository;
		this.userRepository = userRepository;
	}

	/**
	 * Save a regional.
	 *
	 * @param regionaldto
	 *            the entity to save
	 * @return the persisted entity
	 */
	// @Todo avaliar necessidade do cache evict abaixo do cache da entidade
	@Transactional(rollbackFor = { UsuarioSemPermissaoException.class, ErroCriarUsuarioException.class })
	@CacheEvict(value = { "br.sorteioapp.domain.Regional" }, beforeInvocation = true, allEntries = true)
	public Regional save(RegionalDTO regionaldto) throws UsuarioSemPermissaoException, ErroCriarUsuarioException {
		log.debug("Request to save Regional : {}", regionaldto);
		boolean updating = regionaldto.getId() != null;
		boolean newuser = false;
		Regional reg = null;
		User user = null;
		if (updating) {
			reg = regionalRepository.findOne(regionaldto.getId());
			user = reg.getUsuario();
			verificaSeUsuarioTemAcessoSobreRegional(reg);
		} else {
			reg = new Regional();
		}

		if (regionaldto.getLogin() != null && regionaldto.getSenha() != null) {
			if (updating) {
				user = reg.getUsuario();
				if (reg.getUsuario() == null || !regionaldto.getLogin().equals(reg.getUsuario().getLogin())) {
					if (userService.loginIsPresent(regionaldto.getLogin())) {
						throw new ErroCriarUsuarioException("Login já existe!");
					}
					try {
						if (reg.getUsuario() != null) {
							userService.deleteUser(user.getLogin());
						}
						user = userService.createTrustfulUser(regionaldto.getLogin(), regionaldto.getSenha(),
								regionaldto.getNome(), regionaldto.getEmail(), AuthoritiesConstants.ROLE_REGIONAL, regionaldto.isAtivo());
					} catch (Throwable t) {
						throw new ErroCriarUsuarioException(
								"Confirme os dados do usuário. Ou contacte o administrador.");
					}
					newuser = true;
				} else {
					userService.forcePasswordReset(user.getLogin(), regionaldto.getSenha());
				}
			} else {
				try {
					user = userService.createTrustfulUser(regionaldto.getLogin(), regionaldto.getSenha(),
							regionaldto.getNome(), regionaldto.getEmail(), AuthoritiesConstants.ROLE_REGIONAL, regionaldto.isAtivo());
				} catch (Throwable t) {
					throw new ErroCriarUsuarioException("Verifique se já não existe o usuário!");
				}
				newuser = true;
			}
		} else if (user != null && regionaldto.getLogin() != null && regionaldto.getSenha() == null
				&& !regionaldto.getLogin().equals(reg.getUsuario().getLogin())) {
			throw new ErroCriarUsuarioException("Não é possível alterar o usuário sem suas informações de senha!");
		}

		if (user != null && updating && !newuser) {
			user.setActivated(regionaldto.isAtivo());
			if (regionaldto.getEmail() != null)
				user.setEmail(regionaldto.getEmail());
			userService.updateUser(user);
		}

		Regiao regiao = null;
		if (regionaldto.getRegiaoId() != null){
		    regiao = regiaoRepository.findOne(regionaldto.getRegiaoId());
        }

		reg.setAtivo(regionaldto.isAtivo());
		reg.setBairro(regionaldto.getBairro());
		reg.setCep(regionaldto.getCep());
		reg.setCidade(regionaldto.getCidade());
		reg.setUf(regionaldto.getUf());
		reg.setCpf(regionaldto.getCpf());
		reg.setLogradouro(regionaldto.getLogradouro());
		reg.setNome(regionaldto.getNome());
		reg.setNumero(regionaldto.getNumero());
		reg.setPercentualRepasse(regionaldto.getPercentualRepasse());
		reg.setResponsavel(regionaldto.getResponsavel());
		reg.setTelefone(regionaldto.getTelefone());
		reg.setId(regionaldto.getId());
		reg.setUsuario(user);
		reg.setRegiao(regiao);		

		Regional result = regionalRepository.save(reg);
		return result;
	}

	/**
	 * Get all the regionais.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	// @todo melhorar o desempenho da consulta
	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public Page<Regional> findAll(Predicate predicate, Pageable pageable) {
		log.debug("Request to get all Regionais");
		QRegional qreg = QRegional.regional;
		BooleanExpression bexp = qreg.in(regionaisComAcesso());
		predicate = bexp.and(predicate);
		return regionalRepository.findAll(predicate, pageable);
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<Regional> findAll() {
		log.debug("Request to get all Regionais");
		List<Regional> regs = new ArrayList<>();
		for (Regional reg : regionaisComAcesso()) {
			if (reg != null)
				regs.add(reg);
		}
		Collections.sort(regs, new Comparator<Regional>() {
			public int compare(Regional o1, Regional o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		return regs;
	}

	@Transactional(readOnly = true, isolation = Isolation.READ_COMMITTED)
	public List<Regional> findAllActive() {
		log.debug("Request to get all Regionais");
		QRegional qreg = QRegional.regional;
		BooleanExpression bexp = qreg.in(regionaisComAcesso());
		Predicate predicate = bexp.and(qreg.ativo.eq(true));

		List<Regional> regs = new ArrayList<>();
		regionalRepository.findAll(predicate, new Sort(Sort.Direction.ASC, "nome")).forEach(regs::add);
		return regs;
	}

	/**
	 * Get one regional by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Transactional(readOnly = true)
	public Regional findOne(Long id) {
		log.debug("Request to get Regional : {}", id);
		Regional regional = regionalRepository.findOne(id);
		return regional;
	}

	@Transactional
	public List<Regional> findByUsuario(Long usuarioId){
		User user = userRepository.findOne(usuarioId);
		
		return regionalRepository.findByRegionalPorUsuario(user);
	}

	/**
	 * Delete the regional by id.
	 */
	@Transactional
	 public void delete(Long id) { 
		 log.debug("Request to delete Regional : {}",
	 id);
		 Regional reg = regionalRepository.findOne(id);
		 if (reg != null && reg.getUsuario() != null) {
		   userService.deleteUser(reg.getUsuario().getLogin());
		 }
   	     regionalRepository.delete(id); 		 
	 }
	

	@Transactional(readOnly = true)
	public List<Regional> regionaisComAcesso() {
		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_TODOS)) {
			return regionalRepository.findAll();
		} else if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_DA_AREA)) {
			return Collections.singletonList(
					arearegionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin()).getRegional());			
		} else if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_COM_LOGIN)) {
			return Collections.singletonList(regionalRepository.findByLogin(SecurityUtils.getCurrentUserLogin()));
		} else {
			return Collections.emptyList();
		}
	}

	@Transactional(readOnly = true)
	public boolean verificaSeUsuarioTemAcessoSobreRegional(Regional reg) throws UsuarioSemPermissaoException {
		List<Regional> regionaisAcesso = this.regionaisComAcesso();
		boolean found = regionaisAcesso.stream().filter(r -> r.getId().equals(reg.getId())).findAny().isPresent();

		if (!found)
			throw new UsuarioSemPermissaoException(SecurityUtils.getCurrentUserLogin());

		return found;
	}

	/*
	 * private boolean isUsusarioPermitido(Regional reg, boolean isupdating){
	 * boolean ok = true; if (isupdating){ if
	 * (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ROLE_REGIONAL)
	 * ) { User usuarioRegionaldoEstabelecimento = reg.getUsuario(); ok =
	 * (usuarioRegionaldoEstabelecimento != null &&
	 * SecurityUtils.getCurrentUserLogin().equals(usuarioRegionaldoEstabelecimento))
	 * ; } } else { ok =
	 * !(SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ROLE_REGIONAL
	 * )); } return ok; }
	 */

	@Transactional(readOnly = true)
	public Regional findByLogin(String login) {
		
		return regionalRepository.findByLogin(login);
	}

}
