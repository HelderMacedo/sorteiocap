package br.sorteioapp.service;

import br.sorteioapp.config.ApplicationProperties;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.repository.AreaRegionalRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.*;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class TotalizacaoBilhetesService {

    private final Logger log = LoggerFactory.getLogger(TotalizacaoBilhetesService.class);

    private EntityManager entityManager;
    private final ConcursoService concursoService;
    private final RegionalService regionalService;
    private final EstabelecimentoService estabelecimentoService;
    private final AreaRegionalRepository arearegionalRepository;
    private final ApplicationProperties applicationProperties;

    public TotalizacaoBilhetesService(EntityManager entityManager,
                                      ConcursoService concursoService,
                                      RegionalService regionalService, EstabelecimentoService estabelecimentoService,
                                      AreaRegionalRepository arearegionalRepository,
                                      ApplicationProperties applicationProperties
                                      ) {

        this.entityManager = entityManager;
        this.concursoService = concursoService;
        this.regionalService = regionalService;
        this.estabelecimentoService = estabelecimentoService;
        this.arearegionalRepository = arearegionalRepository;
        this.applicationProperties = applicationProperties;
    }

    public List<TotalizacaoRegionalDTO> consultaTotalizacoesRegionais(Concurso conc, LocalDate dataInicio, LocalDate dataFim, Regional reg) throws UsuarioSemPermissaoException {
        if (conc == null && reg == null) {
            throw new IllegalArgumentException();
        } else if (reg == null && !SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ACESSO_REGIONAL_TODOS)) {
            return Collections.emptyList();
        } else if (reg != null) {
            regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);
        }

        Map<Integer, Concurso> mapconc =
            concursoService.findAll().stream().collect(Collectors.toMap(Concurso::getId, Function.identity()));
        System.out.println("TOTALIZACAO param progressivo "+ applicationProperties.getComissaoProgressiva());
        String sqlTotalizacaoReg;
        if (conc == null || conc.getSincronizado()){
            sqlTotalizacaoReg =
                "select tot.concurso_id, reg.regional_id, coalesce(reg.nome, regional_nome) as regnome, " +
                    "total_distribuidos, total_validados, valor_total_arrecadado, valor_total_comissao, valor_total_faturamento, " +
                    "reg.sofre_ajuste, coalesce( conc.perc_ajuste, 0) "+
                    "from totalizacao_concurso_regional tot " +
                    "inner join concurso conc on conc.concurso_id = tot.concurso_id " +
                    "inner join regional reg on reg.regional_id = tot.regional_id " +
                    "where "+
                    ((conc == null) ? "" : " tot.concurso_id = :concid") +
                    ((conc == null || reg == null) ? "" : " and") +
                    ((reg == null) ? "" : " tot.regional_id = :regid") +
                    ((conc != null || dataInicio == null) ? "" : " and conc.data_sorteio >= :dataInicio") +
                    ((conc != null || dataFim == null) ? "" : " and conc.data_sorteio <= :dataFim") +
                    " order by tot.concurso_id desc, regnome asc"
                ;
        } else if (applicationProperties.getComissaoProgressiva()) {
            sqlTotalizacaoReg =
                "select concurso_id, regional_id, nome, distribuidos, validados, total_arrecadacao, total_comissao, "+
                " (total_arrecadacao - total_comissao) as total_faturamento, sofre_ajuste, perc_ajuste "  +
                "from (select conc.concurso_id, reg.regional_id, reg.nome, "+
                    "totbil.distribuidos, totbil.validados,  (totbil.validados*tip.valor) as total_arrecadacao, "+
                    "(case when totbil.validados < conc.lim_faixa_comissao_reg1 then  round( coalesce(totbil.validados*reg.val_comissao_reg_faixa1, 0) , 2) " +
                    "      else round(coalesce(totbil.validados*reg.val_comissao_reg_faixa2, 0), 2) end) as total_comissao, " +
                    "reg.sofre_ajuste, coalesce( conc.perc_ajuste, 0) as perc_ajuste "+
                    "from regional reg "+
                    "join "+
                    "    (select concurso_id, regional_id, count(validado=true) as validados, count(numero) as distribuidos "+
                    "    from bilhete where  estabelecimento_id is not null and  concurso_id = :concid " +
                    "    group by concurso_id, regional_id) totbil on reg.regional_id = totbil.regional_id " +
                    "join concurso conc on conc.concurso_id = totbil.concurso_id " +
                    "join tipobilhete tip on tip.tipo_id = conc.tipo_id " +
                    ((reg == null) ? "" : "where reg.regional_id = :regid") +
                    " order by conc.concurso_id desc, reg.nome asc ) w  ";
        } else {
            sqlTotalizacaoReg =
                "select concurso_id, regional_id, nome, distribuidos, validados, total_arrecadacao, total_comissao, "+
                " (total_arrecadacao - total_comissao) as total_faturamento, sofre_ajuste, perc_ajuste "  +
                "from (select conc.concurso_id, reg.regional_id, reg.nome, "+
                    "totbil.distribuidos, totbil.validados,  (totbil.validados*tip.valor) as total_arrecadacao, "+
                    "round((totbil.validados*tip.valor * reg.percentualrepasse)/100.0, 2) as total_comissao ,"+
                    "reg.sofre_ajuste, coalesce( conc.perc_ajuste, 0)  as perc_ajuste "+
                    "from regional reg "+
                    "join "+
                    "    (select concurso_id, regional_id, count(validado=true) as validados, count(numero) as distribuidos "+
                    "    from bilhete where  estabelecimento_id is not null and  concurso_id = :concid " +
                    "    group by concurso_id, regional_id) totbil on reg.regional_id = totbil.regional_id " +
                    "join concurso conc on conc.concurso_id = totbil.concurso_id " +
                    "join tipobilhete tip on tip.tipo_id = conc.tipo_id " +
                    ((reg == null) ? "" : "where reg.regional_id = :regid") +
                    " order by conc.concurso_id desc, reg.nome asc) w ";
        }

        Query q = entityManager.createNativeQuery(sqlTotalizacaoReg);
        if (conc != null) q.setParameter("concid", conc.getId());
        if (reg != null) q.setParameter("regid", reg.getId());
        if (conc == null && dataInicio != null) q.setParameter("dataInicio", dataInicio);
        if (conc == null && dataFim != null) q.setParameter("dataFim", dataFim);
        List<Object[]> lst = q.getResultList();


        List<TotalizacaoRegionalDTO> res = new ArrayList<TotalizacaoRegionalDTO>();
        for (Object[] objs : lst) {
            Concurso concCorrente = mapconc.get(Integer.valueOf(objs[0].toString()));
            Regional regCorrente = regionalService.findOne(Long.valueOf(objs[1].toString()));

             // Busca totalizacação dos estabelecimentos
            double valorTotalComissaoEstabelecimento = 0;
            try {
                List<TotalizacaoEstabelecimentoDTO> lstTotEst = consultaTotalizacoesEstabelecimentos(concCorrente, regCorrente, null);
                log.info("QTD ESTAB TOT " + lstTotEst.size());
                for (TotalizacaoEstabelecimentoDTO tot : lstTotEst) {
                    valorTotalComissaoEstabelecimento += tot.getValorTotalComissao();
                }
            } catch (Exception e){
                e.printStackTrace();
                log.trace("ERRO na totalizacao dos estbelecimentos. ", e);
            }

            TotalizacaoRegionalDTO totRegional = new TotalizacaoRegionalDTO();
            totRegional.setConcurso(new ThinConcursoDTO(concCorrente));
            totRegional.setRegional(new ThinRegionalDTO(regCorrente));
            Long totalDistribuido = Long.valueOf((objs[3] == null) ? "0" : objs[3].toString());
            totRegional.setTotalDistribuidos(totalDistribuido);
            Long totalValidado = Long.valueOf((objs[4] == null) ? "0" : objs[4].toString());
            Double totalArrecadado = Double.valueOf((objs[5] == null) ? "0" : objs[5].toString());
            Double totalComissao = Double.valueOf((objs[6] == null) ? "0" : objs[6].toString());
            Double totalFaturamento = Double.valueOf((objs[7] == null) ? "0" : objs[7].toString());
            Boolean sofreAjuste = (Boolean) objs[8];
            Long percAjuste = Long.valueOf((objs[9] == null) ? "0" : objs[9].toString());

            if (applicationProperties.getVerAjuste() && sofreAjuste){
                Long totalValidadoOri = totalValidado;
                totalValidado = Math.round( totalValidado * (100.0 - percAjuste)/100.0);
                totalArrecadado = totalArrecadado * ((double)totalValidado/totalValidadoOri);
                totalComissao = totalComissao * ((double)totalValidado/totalValidadoOri);
            }
            totRegional.setTotalValidados(totalValidado);
            totRegional.setValorTotalArrecadado(totalArrecadado);
            totRegional.setValorTotalComissao(totalComissao);
            totRegional.setValorTotalComissaoEstabelecimentos(valorTotalComissaoEstabelecimento);

            if (applicationProperties.getComissaoProgressiva() && !conc.getSincronizado()){
                totRegional.setValorTotalFaturamento(totalFaturamento - valorTotalComissaoEstabelecimento);
            } else {
                totRegional.setValorTotalFaturamento(totalFaturamento);
            }
            res.add(totRegional);
        }
        return res;
    }

    @Transactional(readOnly = true)
    public List<TotalizacaoEstabelecimentoDTO> consultaTotalizacoesEstabelecimentos(Concurso conc, Regional reg, Long areaId)
        throws UsuarioSemPermissaoException {
        regionalService.verificaSeUsuarioTemAcessoSobreRegional(reg);
        String sqlTotalizacao;

        System.out.println("TOTALIZACAO param progressivo "+ applicationProperties.getComissaoProgressiva());
        if (conc == null || conc.getSincronizado()){
            sqlTotalizacao =
            "select tot.estabelecimento_id, coalesce(est.nome, estabelecimento_nome) as estnome, +" +
                    "coalesce(est.endereco_bairro,estabelecimento_bairro), "+
                    "coalesce(est.endereco_cidade,estabelecimento_cidade), coalesce(est.endereco_uf, estabelecimento_uf)," +
                    "total_distribuidos, total_validados, valor_total_arrecadado, coalesce(valor_total_comissao, 0) " +
                    "from totalizacao_concurso_estabelecimento tot " +
                    "join estabelecimento est on est.estabelecimento_id = tot.estabelecimento_id "+
                    "where tot.concurso_id = :concid and tot.regional_id = :regid " +
                    ((areaId==null)?"": " and est.area_id = :areaid ") +
                    " order by tot.concurso_id desc, estnome asc"
            ;
        } else if (applicationProperties.getComissaoProgressiva()) {
            sqlTotalizacao =
                "select est.estabelecimento_id, est.nome, est.endereco_bairro, est.endereco_cidade, est.endereco_uf, " +
                    "coalesce(totbil.distribuidos,0), coalesce(totbil.validados, 0),  coalesce(totbil.validados *tip.valor,0) as total_arrecadacao, "+
                    "(case when coalesce(totbil.validados, 0) < conc.lim_faixa_comissao_est1 then  round(coalesce(totbil.validados * reg.val_comissao_est_faixa1, 0) , 2) " +
                    "      when coalesce(totbil.validados, 0) < conc.lim_faixa_comissao_est2 then  round(coalesce(totbil.validados *reg.val_comissao_est_faixa2, 0) , 2) " +
                    "      else round(coalesce(totbil.validados*reg.val_comissao_est_faixa3, 0), 2) end) as total_comissao " +
                    "from estabelecimento est "+
                    "join regional reg on reg.regional_id = est.regional_id " +
                    "join "+
                    "    (select concurso_id, regional_id, estabelecimento_id, count(validado=true) as validados, count(numero) as distribuidos "+
                    "    from bilhete where  estabelecimento_id is not null and  concurso_id = :concid and regional_id = :regid " +
                    "    group by concurso_id, regional_id, estabelecimento_id) totbil on est.estabelecimento_id = totbil.estabelecimento_id " +
                    "join concurso conc on conc.concurso_id = totbil.concurso_id " +
                    "join tipobilhete tip on tip.tipo_id = conc.tipo_id " +
                    "where est.regional_id = :regid "+ ((areaId==null)?"": " and est.area_id = :areaid ") +
                    " order by conc.concurso_id desc, est.nome asc";
        } else {
            sqlTotalizacao =
                "select est.estabelecimento_id, est.nome, est.endereco_bairro, est.endereco_cidade, est.endereco_uf, " +
                    "coalesce(totbil.distribuidos, 0), coalesce(totbil.validados, 0),  coalesce(totbil.validados * tip.valor, 0) as total_arrecadacao, "+
                    "round((coalesce(totbil.validados * tip.valor , 0) * est.percentualrepasse)/100.0, 2) as total_comissao "+
                    "from estabelecimento est "+
                    "join "+
                    "    (select concurso_id, regional_id, estabelecimento_id, count(validado=true) as validados, count(numero) as distribuidos "+
                    "    from bilhete where  estabelecimento_id is not null and  concurso_id = :concid and regional_id = :regid " +
                    "    group by concurso_id, regional_id, estabelecimento_id) totbil on  est.estabelecimento_id = totbil.estabelecimento_id " +
                    "join concurso conc on conc.concurso_id = totbil.concurso_id " +
                    "join tipobilhete tip on tip.tipo_id = conc.tipo_id " +
                    "where est.regional_id = :regid "+((areaId==null)?"": " and est.area_id = :areaid ") +
                    " order by conc.concurso_id desc, est.nome asc";
        }


        reg = regionalService.findOne(reg.getId());
        conc = concursoService.findOne(conc.getId());
        ThinRegionalDTO treg = new ThinRegionalDTO(reg);
        Query queryTotalLoteVend = entityManager
            .createNativeQuery(sqlTotalizacao);

        queryTotalLoteVend.setParameter("concid", conc.getId());
        queryTotalLoteVend.setParameter("regid", reg.getId());
        if(areaId!=null)
            queryTotalLoteVend.setParameter("areaid", areaId);
        List<Object[]> res = (List<Object[]>) queryTotalLoteVend.getResultList();
        List<TotalizacaoEstabelecimentoDTO> lst = new ArrayList<TotalizacaoEstabelecimentoDTO>();
        Map<Long, TotalizacaoEstabelecimentoDTO> maptotalizacoes = new HashMap();
        long totalRegional = 0;
        for (Object[] tuple : res) {
            ThinEstabelecimentoDTO test = new ThinEstabelecimentoDTO()
                                             .regional(treg)
                                             .id(Long.valueOf(tuple[0].toString()))
                                             .nome(tuple[1].toString())
                                             .bairro(tuple[2].toString())
                                             .cidade(tuple[3].toString())
                                             .uf(tuple[4].toString())
                ;
            TotalizacaoEstabelecimentoDTO tot;
            if (!maptotalizacoes.containsKey(test.getId())) {
                tot = new TotalizacaoEstabelecimentoDTO(test);
                maptotalizacoes.put(test.getId(), tot);
                lst.add(tot);
            } else {
                tot = maptotalizacoes.get(test.getId());
            }
            tot.setTotalDistribuidos(Long.valueOf(tuple[5].toString()));
            tot.setTotalValidados(Long.valueOf(tuple[6].toString()));
            tot.setValorTotalArrecadado(Double.valueOf(tuple[7].toString()));
            tot.setValorTotalComissao(Double.valueOf(tuple[8].toString()));

            totalRegional += tot.getTotalValidados();
        }
        Collections.sort(lst, new Comparator<TotalizacaoEstabelecimentoDTO>() {
            @Override
            public int compare(TotalizacaoEstabelecimentoDTO t0, TotalizacaoEstabelecimentoDTO t1) {
                return (int) (t0.getTotalValidados() - t1.getTotalValidados());
            }
        });
    
        

        //System.out.println("ANTES "+totalRegional);
        totalRegional = Math.round(totalRegional * (100 - conc.getPercAjuste())/ 100.0);
        //System.out.println("DEPOIS "+totalRegional);
        if(applicationProperties.getVerAjuste() && reg.isSofreAjuste()) {
            for( int k = 0 ; k < lst.size(); k++) {
                TotalizacaoEstabelecimentoDTO test = lst.get(k);
                long valTotEstOri = test.getTotalValidados();
                System.out.println("valTotEstOri "+valTotEstOri);
                if(valTotEstOri != 0){
                    long valTotEst = Math.round(test.getTotalValidados() * (100 - conc.getPercAjuste())/ 100.0);
                    System.out.println("valTotEst "+valTotEst);
                    if (k == lst.size()-1 && totalRegional > valTotEst){
                        //System.out.println("ENTROU");
                        valTotEst = totalRegional;
                    }else{
                        valTotEst = Math.min(totalRegional, valTotEst);
                        //System.out.println("totalRegional "+totalRegional);
                        //System.out.println("valTotEst "+valTotEst);
                    }    
                    totalRegional = Math.max(totalRegional - valTotEst, 0);
                    //System.out.println("totalRegional "+totalRegional);
                    test.setTotalValidados(valTotEst);
                    test.setTotalDistribuidos(valTotEst * test.getTotalDistribuidos() / valTotEstOri );
                    test.setValorTotalArrecadado(valTotEst * test.getValorTotalArrecadado() / valTotEstOri );
                    test.setValorTotalComissao(valTotEst  * test.getValorTotalComissao() / valTotEstOri );
                }
            }
        }
        return lst;
    }
}
