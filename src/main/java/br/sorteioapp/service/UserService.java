package br.sorteioapp.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.sorteioapp.config.Constants;
import br.sorteioapp.domain.Authority;
import br.sorteioapp.domain.User;
import br.sorteioapp.repository.AuthorityRepository;
import br.sorteioapp.repository.PersistentTokenRepository;
import br.sorteioapp.repository.UserRepository;
import br.sorteioapp.security.AuthoritiesConstants;
import br.sorteioapp.security.SecurityUtils;
import br.sorteioapp.service.dto.UserDTO;
import br.sorteioapp.service.util.RandomUtil;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

	private final Logger log = LoggerFactory.getLogger(UserService.class);

	private final UserRepository userRepository;

	private final PasswordEncoder passwordEncoder;

	private final PersistentTokenRepository persistentTokenRepository;

	private final AuthorityRepository authorityRepository;
	private final MessageDigestPasswordEncoder passwordMD5Encoder;
	
	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder,
			PersistentTokenRepository persistentTokenRepository, AuthorityRepository authorityRepository) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.persistentTokenRepository = persistentTokenRepository;
		this.authorityRepository = authorityRepository;
		this.passwordMD5Encoder = new MessageDigestPasswordEncoder("MD5");
	}

	public Optional<User> activateRegistration(String key) {
		log.debug("Activating user for activation key {}", key);
		return userRepository.findOneByActivationKey(key).map(user -> {
			// activate given user for the registration key.
			user.setActivated(true);
			user.setActivationKey(null);
			log.debug("Activated user: {}", user);
			return user;
		});
	}

	public Optional<User> completePasswordReset(String newPassword, String key) {
		log.debug("Reset user password for reset key {}", key);

		return userRepository.findOneByResetKey(key).filter(user -> {
			ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
			return user.getResetDate().isAfter(oneDayAgo);
		}).map(user -> {
			user.setPassword(passwordEncoder.encode(newPassword));
			user.setPasswordMD5(passwordMD5Encoder.encodePassword(newPassword, null));
			user.setResetKey(null);
			user.setResetDate(null);
			return user;
		});
	}

	/*
	 * public Optional<User> requestPasswordReset(String mail) { return
	 * userRepository.findOneByEmail(mail) .filter(User::getActivated) .map(user ->
	 * { user.setResetKey(RandomUtil.generateResetKey());
	 * user.setResetDate(ZonedDateTime.now()); return user; }); }
	 */
	public Optional<User> forcePasswordReset(String login, String password) {

		Optional<User> optuser = userRepository.findOneByLogin(login);
		if (optuser.isPresent()) {
			String encryptedPassword = passwordEncoder.encode(password);
			User user = optuser.get();
			user.setPassword(encryptedPassword);
			user.setPasswordMD5(passwordMD5Encoder.encodePassword(password, null));			
			userRepository.save(user);
		}
		return optuser;
	}

	public User createTrustfulUser(String login, String password, String nome, String email, String papel_autoridade,
			boolean activated) {

		User newUser = User.newDefaultUser();
		Authority authority = authorityRepository.findOne(papel_autoridade);
		Set<Authority> authorities = new HashSet<>();
		String encryptedPassword = passwordEncoder.encode(password);
		newUser.setLogin(login);
		// new user gets initially a generated password
		newUser.setPassword(encryptedPassword);
		newUser.setPasswordMD5(passwordMD5Encoder.encodePassword(password, null));		
		newUser.setNome(nome);
		newUser.setEmail(email);
		// new user is not active
		newUser.setActivated(activated);
		// new user gets registration key
		authorities.add(authority);
		newUser.setAuthorities(authorities);
		userRepository.save(newUser);
		log.debug("Created Information for User: {}", newUser);
		return newUser;
	}

	public boolean loginIsPresent(String login) {
		return userRepository.findOneByLogin(login.toLowerCase()).isPresent();
	}

	/*
	 * public User registerUser(String login, String password, String nome, String
	 * email, String imageUrl, String langKey) {
	 * 
	 * User newUser = new User(); Authority authority =
	 * authorityRepository.findOne(AuthoritiesConstants.ROLE_USER); Set<Authority>
	 * authorities = new HashSet<>(); String encryptedPassword =
	 * passwordEncoder.encode(password); newUser.setLogin(login); // new user gets
	 * initially a generated password newUser.setPassword(encryptedPassword);
	 * newUser.setNome(nome); newUser.setEmail(email);
	 * newUser.setImageUrl(imageUrl); newUser.setLangKey(langKey); // new user is
	 * not active newUser.setActivated(false); // new user gets registration key
	 * newUser.setActivationKey(RandomUtil.generateActivationKey());
	 * authorities.add(authority); newUser.setAuthorities(authorities);
	 * userRepository.save(newUser); log.debug("Created Information for User: {}",
	 * newUser); return newUser; }
	 */

	public User createUser(UserDTO userDTO) {
		User user = User.newDefaultUser();
		user.setLogin(userDTO.getLogin());
		user.setNome(userDTO.getNome());
		user.setEmail(userDTO.getEmail());
		user.setImageUrl(userDTO.getImageUrl());
		if (userDTO.getLangKey() != null) {
			user.setLangKey(userDTO.getLangKey());
		}
		if (userDTO.getRoles() != null) {
			Set<Authority> authorities = new HashSet<>();
			userDTO.getRoles().forEach(authority -> authorities.add(authorityRepository.findOne(authority)));
			user.setAuthorities(authorities);
		}

		// @todo retirado RandomUtil.generatePassword() para facilitar a senha inicial
		String encryptedPassword = passwordEncoder.encode(userDTO.getLogin());
		user.setPassword(encryptedPassword);
		user.setPasswordMD5(passwordMD5Encoder.encodePassword(userDTO.getLogin(), null));		
		user.setResetKey(RandomUtil.generateResetKey());
		user.setResetDate(ZonedDateTime.now());
		user.setActivated(true);
		userRepository.save(user);
		log.debug("Created Information for User: {}", user);
		return user;
	}

	/**
	 * Update basic information (first name, last name, email, language) for the
	 * current user.
	 *
	 * @param nome
	 *            name of user
	 * @param email
	 *            email id of user
	 * @param langKey
	 *            language key
	 */
	public void updateUser(String nome, String email, String langKey) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			user.setNome(nome);
			user.setEmail(email);
			user.setLangKey(langKey);
			log.debug("Changed Information for User: {}", user);
		});
	}

	/**
	 * Update all information for a specific user, and return the modified user.
	 *
	 * @param userDTO
	 *            user to update
	 * @return updated user
	 */
	public Optional<UserDTO> updateUser(UserDTO userDTO) {
		return Optional.of(userRepository.findOne(userDTO.getId())).map(user -> {
			user.setLogin(userDTO.getLogin());
			user.setNome(userDTO.getNome());
			user.setEmail(userDTO.getEmail());
			user.setImageUrl(userDTO.getImageUrl());
			user.setActivated(userDTO.isActivated());
			user.setLangKey(userDTO.getLangKey());
			Set<Authority> managedAuthorities = user.getAuthorities();
			managedAuthorities.clear();
			userDTO.getRoles().stream().map(authorityRepository::findOne).forEach(managedAuthorities::add);
			log.debug("Changed Information for User: {}", user);
			return user;
		}).map(UserDTO::new);
	}

	public User updateUser(User user) {
		return userRepository.save(user);
	}

	public void deleteUser(String login) {
		userRepository.findOneByLogin(login).ifPresent(user -> {
			userRepository.delete(user);
			log.debug("Deleted User: {}", user);
		});
	}

	public void changePassword(String password) {
		userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
			String encryptedPassword = passwordEncoder.encode(password);
			user.setPassword(encryptedPassword);
			user.setPasswordMD5(passwordMD5Encoder.encodePassword(password, null));					
			log.debug("Changed password for User: {}", user);
		});
	}

	@Transactional(readOnly = true)
	public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
		if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ROLE_SYSTEM))
			return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
		else
			return userRepository.findNonAdminAndAnonymous(pageable).map(UserDTO::new);
	}

	@Transactional(readOnly = true)
	public Optional<User> getUserWithAuthoritiesByLogin(String login) {
		return userRepository.findOneWithAuthoritiesByLogin(login);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities(Long id) {
		return userRepository.findOneWithAuthoritiesById(id);
	}

	@Transactional(readOnly = true)
	public User getUserWithAuthorities() {
		log.debug("Current user: " + SecurityUtils.getCurrentUserLogin());
		if (Constants.ANONYMOUS_USER.equalsIgnoreCase(SecurityUtils.getCurrentUserLogin()))
			return null;
		return userRepository.findOneWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin()).orElse(null);
	}

	/**
	 * Persistent Token are used for providing automatic authentication, they should
	 * be automatically deleted after 30 days.
	 * <p>
	 * This is scheduled to get fired everyday, at midnight.
	 * </p>
	 */
	@Scheduled(cron = "0 0 0 * * ?")
	public void removeOldPersistentTokens() {
		LocalDate now = LocalDate.now();
		persistentTokenRepository.findByTokenDateBefore(now.minusMonths(1)).forEach(token -> {
			log.debug("Deleting token {}", token.getSeries());
			User user = token.getUser();
			user.getPersistentTokens().remove(token);
			persistentTokenRepository.delete(token);
		});
	}

	/**
	 * Not activated users should be automatically deleted after 3 days.
	 * <p>
	 * This is scheduled to get fired everyday, at 01:00 (am).
	 * </p>
	 */
	@Scheduled(cron = "0 0 1 * * ?")
	public void removeNotActivatedUsers() {
		ZonedDateTime now = ZonedDateTime.now();
		List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
		for (User user : users) {
			log.debug("Deleting not activated user {}", user.getLogin());
			userRepository.delete(user);
		}
	}
}
