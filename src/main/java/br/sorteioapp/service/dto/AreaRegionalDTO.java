package br.sorteioapp.service.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.sorteioapp.config.Constants;
import br.sorteioapp.domain.AreaRegional;

/**
 * Created by rodrigo on 24/04/17.
 */
public class AreaRegionalDTO {
	private Long id;
	@Size(max = 16)
	private String telefone;
	@NotNull
	@Size(max = 64)
	private String nome;
	private float percentualRepasse;
	@Size(max = 64)
	private String responsavel;
	@Size(min = 5, max = 100)
	private String email;
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String login;
	// @todo colocar validacao da senha
	private String senha;
	// @todo colocar validação sobre CPF
	private String cpf;
	private boolean ativo;
	
	private ThinRegionalDTO regional;

	public AreaRegionalDTO() {
	}

	public AreaRegionalDTO(AreaRegional area) {
		this.setCpf(area.getCpf());
		this.setId(area.getId());
		this.setTelefone(area.getTelefone());
		this.setNome(area.getNome());
		this.setPercentualRepasse(area.getPercentualRepasse());
		this.setResponsavel(area.getResponsavel());
		this.ativo = area.isAtivo();
		if (area.getUsuario() != null) {
			this.setEmail(area.getUsuario().getEmail());
			this.setLogin(area.getUsuario().getLogin());
		}
		if(area.getRegional() != null) {
			this.setRegional(new ThinRegionalDTO(area.getRegional()));
		}
	}

	public static List<AreaRegionalDTO> map(List<AreaRegional> listReg) {
		List<AreaRegionalDTO> listDTO = new ArrayList<>();
		for (AreaRegional reg : listReg) {
			listDTO.add(new AreaRegionalDTO(reg));
		}
		return listDTO;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public String getEmail() {
		return email;
	}

	public String getLogin() {
		return login;
	}

	public String getCpf() {
		return cpf;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public ThinRegionalDTO getRegional() {
		return regional;
	}

	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
