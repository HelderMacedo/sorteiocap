package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Bilhete;

import java.time.LocalDateTime;

/**
 * Created by rodrigo on 04/04/17.
 */
public class BilheteDTO {

	private Integer concursoId;
	private String identificacao;
	private Boolean validado;
	private Long loteValidacao;
	private LocalDateTime dataValidacao;

	public BilheteDTO(Bilhete bil) {
		this.setConcursoId(bil.getConcurso().getId());
		this.setIdentificacao(bil.getIdentificacao());
		this.setValidado(bil.getValidado());
		this.setLoteValidacao(bil.getLoteValidacao());
		this.setDataValidacao(bil.getDataValidacao());
	}

	public Integer getConcursoId() {
		return concursoId;
	}

	public void setConcursoId(Integer concursoId) {
		this.concursoId = concursoId;
	}

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public Long getLoteValidacao() {
		return loteValidacao;
	}

	public void setLoteValidacao(Long loteValidacao) {
		this.loteValidacao = loteValidacao;
	}
	
	
	public LocalDateTime getDataValidacao() {
		return dataValidacao;
	}

	public void setDataValidacao(LocalDateTime dataValidacao) {
		this.dataValidacao = dataValidacao;
	}
}
