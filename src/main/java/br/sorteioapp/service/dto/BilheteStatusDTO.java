package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.Regional;

public class BilheteStatusDTO {

    private ThinConcursoDTO concurso;
    private ThinEstabelecimentoDTO estabelecimento;
    private ThinRegionalDTO regional;
	private BilheteDTO bilhete;

	public BilheteStatusDTO(Object[] objs) {
		this(new ThinConcursoDTO(((Bilhete) objs[0]).getConcurso()), new BilheteDTO((Bilhete) objs[0]), 
				(objs[1]==null)?null:(new ThinRegionalDTO((Regional) objs[1])),
				(objs[2]==null)?null:(new ThinEstabelecimentoDTO((Estabelecimento) objs[2])));
	}
	
	public BilheteStatusDTO(ThinConcursoDTO concurso, BilheteDTO bilhete, 
			ThinRegionalDTO regional, ThinEstabelecimentoDTO estabelecimento) {
		this.concurso = concurso;
		this.regional = regional;
		this.estabelecimento = estabelecimento;
		this.bilhete = bilhete;

	}

	public ThinConcursoDTO getConcurso() {
		return concurso;
	}

	public void setConcurso(ThinConcursoDTO concurso) {
		this.concurso = concurso;
	}

	public ThinEstabelecimentoDTO getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(ThinEstabelecimentoDTO estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public ThinRegionalDTO getRegional() {
		return regional;
	}

	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}

	public BilheteDTO getBilhete() {
		return bilhete;
	}

	public void setBilhete(BilheteDTO bilhete) {
		this.bilhete = bilhete;
	}

}
