package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Regional;

import java.util.ArrayList;
import java.util.List;

public class ComissaoListaRegionalDTO {
    private List<Long> regionalIds;

    private Float valorComissaoRegionalFaixa1;

    private Float valorComissaoRegionalFaixa2;

    private Float valorComissaoEstabelecimenotFaixa1;

    private Float valorComissaoEstabelecimenotFaixa2;

    private Float valorComissaoEstabelecimenotFaixa3;

    public ComissaoListaRegionalDTO(){}

    public List<Long> getRegionalIds() {
        return regionalIds;
    }

    public void setRegionalIds(List<Long> regionalIds) {
        this.regionalIds = regionalIds;
    }

    public Float getValorComissaoRegionalFaixa1() {
        return valorComissaoRegionalFaixa1;
    }

    public void setValorComissaoRegionalFaixa1(Float valorComissaoRegionalFaixa1) {
        this.valorComissaoRegionalFaixa1 = valorComissaoRegionalFaixa1;
    }

    public Float getValorComissaoRegionalFaixa2() {
        return valorComissaoRegionalFaixa2;
    }

    public void setValorComissaoRegionalFaixa2(Float valorComissaoRegionalFaixa2) {
        this.valorComissaoRegionalFaixa2 = valorComissaoRegionalFaixa2;
    }

    public Float getValorComissaoEstabelecimenotFaixa1() {
        return valorComissaoEstabelecimenotFaixa1;
    }

    public void setValorComissaoEstabelecimenotFaixa1(Float valorComissaoEstabelecimenotFaixa1) {
        this.valorComissaoEstabelecimenotFaixa1 = valorComissaoEstabelecimenotFaixa1;
    }

    public Float getValorComissaoEstabelecimenotFaixa2() {
        return valorComissaoEstabelecimenotFaixa2;
    }

    public void setValorComissaoEstabelecimenotFaixa2(Float valorComissaoEstabelecimenotFaixa2) {
        this.valorComissaoEstabelecimenotFaixa2 = valorComissaoEstabelecimenotFaixa2;
    }

    public Float getValorComissaoEstabelecimenotFaixa3() {
        return valorComissaoEstabelecimenotFaixa3;
    }

    public void setValorComissaoEstabelecimenotFaixa3(Float valorComissaoEstabelecimenotFaixa3) {
        this.valorComissaoEstabelecimenotFaixa3 = valorComissaoEstabelecimenotFaixa3;
    }
}
