package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Regional;

import java.util.ArrayList;
import java.util.List;

public class ComissaoRegionalDTO {
    private ThinRegionalDTO regional;

    private Float valorComissaoRegionalFaixa1;

    private Float valorComissaoRegionalFaixa2;

    private Float valorComissaoEstabelecimenotFaixa1;

    private Float valorComissaoEstabelecimenotFaixa2;

    private Float valorComissaoEstabelecimenotFaixa3;

    public ComissaoRegionalDTO(){}

    public ComissaoRegionalDTO(Regional reg){
        this.regional = new ThinRegionalDTO(reg);
        this.valorComissaoRegionalFaixa1 = reg.getValorComissaoRegionalFaixa1();
        this.valorComissaoRegionalFaixa2 = reg.getValorComissaoRegionalFaixa2();
        this.valorComissaoEstabelecimenotFaixa1 = reg.getValorComissaoEstabelecimenotFaixa1();
        this.valorComissaoEstabelecimenotFaixa2 = reg.getValorComissaoEstabelecimenotFaixa2();
        this.valorComissaoEstabelecimenotFaixa3 = reg.getValorComissaoEstabelecimenotFaixa3();
    }

    public static List<ComissaoRegionalDTO> map(List<Regional> listReg) {
        List<ComissaoRegionalDTO> listDTO = new ArrayList<>();
        for(Regional reg: listReg){
            listDTO.add(new ComissaoRegionalDTO(reg));
        }
        return listDTO;
    }

    public ThinRegionalDTO getRegional() {
        return regional;
    }

    public void setRegional(ThinRegionalDTO regional) {
        this.regional = regional;
    }

    public Float getValorComissaoRegionalFaixa1() {
        return valorComissaoRegionalFaixa1;
    }

    public void setValorComissaoRegionalFaixa1(Float valorComissaoRegionalFaixa1) {
        this.valorComissaoRegionalFaixa1 = valorComissaoRegionalFaixa1;
    }

    public Float getValorComissaoRegionalFaixa2() {
        return valorComissaoRegionalFaixa2;
    }

    public void setValorComissaoRegionalFaixa2(Float valorComissaoRegionalFaixa2) {
        this.valorComissaoRegionalFaixa2 = valorComissaoRegionalFaixa2;
    }

    public Float getValorComissaoEstabelecimenotFaixa1() {
        return valorComissaoEstabelecimenotFaixa1;
    }

    public void setValorComissaoEstabelecimenotFaixa1(Float valorComissaoEstabelecimenotFaixa1) {
        this.valorComissaoEstabelecimenotFaixa1 = valorComissaoEstabelecimenotFaixa1;
    }

    public Float getValorComissaoEstabelecimenotFaixa2() {
        return valorComissaoEstabelecimenotFaixa2;
    }

    public void setValorComissaoEstabelecimenotFaixa2(Float valorComissaoEstabelecimenotFaixa2) {
        this.valorComissaoEstabelecimenotFaixa2 = valorComissaoEstabelecimenotFaixa2;
    }

    public Float getValorComissaoEstabelecimenotFaixa3() {
        return valorComissaoEstabelecimenotFaixa3;
    }

    public void setValorComissaoEstabelecimenotFaixa3(Float valorComissaoEstabelecimenotFaixa3) {
        this.valorComissaoEstabelecimenotFaixa3 = valorComissaoEstabelecimenotFaixa3;
    }
}
