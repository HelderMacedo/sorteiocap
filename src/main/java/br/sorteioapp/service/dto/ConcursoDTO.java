package br.sorteioapp.service.dto;

import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.LoteGeracaoBilhete;
import br.sorteioapp.domain.TipoBilhete;

import javax.persistence.Column;

/**
 * Created by rodrigo on 08/04/17.
 */
public class ConcursoDTO {

	private Integer id;
	private int ano;
	private int numSorteio;
	private String label;
	private TipoBilhete tipo;
	private ZonedDateTime dataInicioVigencia;
	private ZonedDateTime dataFimVigencia;
	private ZonedDateTime dataSorteio;
	private Long quantidadeBilhetes;
	private Set<LoteGeralDTO> lotebilhetes;

    private Boolean sincronizado;
	private Integer quantidadePremios;

    private Double percentualFixoRegional;
    private String premio1;
    private String premio2;
    private String premio3;
    private String premio4;
    private String premio5;

    private Boolean duplaChance;

    private Float limiteFaixaComissaoRegional1;

    private Float limiteFaixaComissaoEstabelecimento1;

    private Float limiteFaixaComissaoEstabelecimento2;



	public ConcursoDTO() {

	}

	public ConcursoDTO(Concurso conc) {
		this(conc, false);
	}

	public ConcursoDTO(Concurso conc, boolean loadLoteGeracao) {
		this.id = conc.getId();
		this.tipo = conc.getTipo();
		this.lotebilhetes = new HashSet<>();
		if (loadLoteGeracao) {
			for (LoteGeracaoBilhete lote : conc.getLotebilhetes()) {
				lotebilhetes.add(new LoteGeralDTO(lote));
			}
		}
		this.quantidadeBilhetes = conc.getQuantidadeBilhetes();
		this.dataInicioVigencia = conc.getDataInicioVigencia();
		this.dataFimVigencia = conc.getDataFimVigencia();
		this.dataSorteio = conc.getDataSorteio().atStartOfDay().atZone(ZoneId.systemDefault());
		if (conc.getId() != null) {
			this.ano = conc.getAno();
			this.numSorteio = conc.getNumSorteio();
			this.setLabel(conc.getTipo().getLabel() + " - " + new DecimalFormat("#000").format(numSorteio % 1000));
		}
		this.sincronizado = conc.getSincronizado();
		this.quantidadePremios = conc.getQuantidadePremios();
		this.premio1 = conc.getPremio1();
		this.premio2 = conc.getPremio2();
		this.premio3 = conc.getPremio3();
		this.premio4 = conc.getPremio4();
		this.premio5 = conc.getPremio5();
		this.percentualFixoRegional = conc.getPercentualFixoRegional();
		this.duplaChance = conc.getDuplaChance();
		this.limiteFaixaComissaoRegional1 = conc.getLimiteFaixaComissaoRegional1();
		this.limiteFaixaComissaoEstabelecimento1 = conc.getLimiteFaixaComissaoEstabelecimento1();
		this.limiteFaixaComissaoEstabelecimento2 = conc.getLimiteFaixaComissaoEstabelecimento2();
	}

    public static List<ConcursoDTO> map(List<Concurso> listConc) {
        List<ConcursoDTO> listDTO = new ArrayList<>();
        for (Concurso conc : listConc) {
            listDTO.add(new ConcursoDTO(conc));
        }
        return listDTO;
    }

	@Override
	public String toString() {
		return "Concurso{" + "id=" + id + ", ano='" + ano + "'" + ", label ='" + getLabel() + "'" + ", tipo ='" + tipo
				+ "'" + ", dataInicioVigencia ='" + dataInicioVigencia + "'" + ", dataFimVigencia ='" + dataFimVigencia
				+ "'" + '}';
	}

	public Integer getId() {
		return id;
	}

	public ZonedDateTime getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(ZonedDateTime dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public void setTipo(TipoBilhete tipoBilheteId) {
		this.tipo = tipoBilheteId;
	}

	public int getAno() {
		return ano;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Set<LoteGeralDTO> getLotebilhetes() {
		return lotebilhetes;
	}

	public void setLotebilhetes(Set<LoteGeralDTO> lotebilhetes) {
		this.lotebilhetes = lotebilhetes;
	}

	public ZonedDateTime getDataInicioVigencia() {
		return dataInicioVigencia;
	}

	public void setDataInicioVigencia(ZonedDateTime dataInicioVigencia) {
		this.dataInicioVigencia = dataInicioVigencia;
	}

	public ZonedDateTime getDataFimVigencia() {
		return dataFimVigencia;
	}

	public void setDataFimVigencia(ZonedDateTime dataFimVigencia) {
		this.dataFimVigencia = dataFimVigencia;
	}

	public int getNumSorteio() {
		return numSorteio;
	}

	public void setNumSorteio(int numSorteio) {
		this.numSorteio = numSorteio;
	}

	public void setQuantidadeBilhetes(Long qtd){
		this.quantidadeBilhetes = qtd;
	}
	public Long getQuantidadeBilhetes(){
		return this.quantidadeBilhetes;
	}

    public Integer getQuantidadePremios() {
        return quantidadePremios;
    }

    public void setQuantidadePremios(Integer quantidadePremios) {
        this.quantidadePremios = quantidadePremios;
    }

    public Double getPercentualFixoRegional() {
        return percentualFixoRegional;
    }

    public void setPercentualFixoRegional(Double percentualFixoRegional) {
        this.percentualFixoRegional = percentualFixoRegional;
    }

    public String getPremio1() {
        return premio1;
    }

    public void setPremio1(String premio1) {
        this.premio1 = premio1;
    }

    public String getPremio2() {
        return premio2;
    }

    public void setPremio2(String premio2) {
        this.premio2 = premio2;
    }

    public String getPremio3() {
        return premio3;
    }

    public void setPremio3(String premio3) {
        this.premio3 = premio3;
    }

    public String getPremio4() {
        return premio4;
    }

    public void setPremio4(String premio4) {
        this.premio4 = premio4;
    }

    public String getPremio5() {
        return premio5;
    }

    public void setPremio5(String premio5) {
        this.premio5 = premio5;
    }

    public Boolean getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(Boolean sincronizado) {
        this.sincronizado = sincronizado;
    }

    public Boolean getDuplaChance() {
        return duplaChance;
    }

    public void setDuplaChance(Boolean duplaChance) {
        this.duplaChance = duplaChance;
    }

    public Float getLimiteFaixaComissaoRegional1() {
        return limiteFaixaComissaoRegional1;
    }

    public void setLimiteFaixaComissaoRegional1(Float limiteFaixaComissaoRegional1) {
        this.limiteFaixaComissaoRegional1 = limiteFaixaComissaoRegional1;
    }

    public Float getLimiteFaixaComissaoEstabelecimento1() {
        return limiteFaixaComissaoEstabelecimento1;
    }

    public void setLimiteFaixaComissaoEstabelecimento1(Float limiteFaixaComissaoEstabelecimento1) {
        this.limiteFaixaComissaoEstabelecimento1 = limiteFaixaComissaoEstabelecimento1;
    }

    public Float getLimiteFaixaComissaoEstabelecimento2() {
        return limiteFaixaComissaoEstabelecimento2;
    }

    public void setLimiteFaixaComissaoEstabelecimento2(Float limiteFaixaComissaoEstabelecimento2) {
        this.limiteFaixaComissaoEstabelecimento2 = limiteFaixaComissaoEstabelecimento2;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
