package br.sorteioapp.service.dto;

import java.time.ZonedDateTime;

import br.sorteioapp.domain.DistribuicaoBilhete;

public class DistribuicaoBilheteDTO {

	private Long id;
    private ThinConcursoDTO concurso;
	private String bilheteInicio;
	private String bilheteFim;	
    private String novoEstabelecimento;
    private String novaRegional;
    private String antigoEstabelecimento;
    private String antigaRegional;    
    private String usuarioModificacao;
    private ZonedDateTime dataModificacao;
    
    
    public DistribuicaoBilheteDTO( ) {
    	
    }
    public DistribuicaoBilheteDTO (DistribuicaoBilhete dist) {
    	this.id = dist.getId();
    	this.concurso = new ThinConcursoDTO(dist.getConcurso());
    	this.bilheteInicio = dist.getBilheteInicio().getIdentificacao();
    	this.bilheteFim = dist.getBilheteFim().getIdentificacao();
    	this.novoEstabelecimento = (dist.getNovoEstabelecimento()==null)?null: dist.getNovoEstabelecimento().getNome();
    	this.novaRegional = (dist.getNovaRegional()==null)?null:dist.getNovaRegional().getNome();
    	this.antigaRegional = (dist.getAntigaRegional()== null)?null:dist.getAntigaRegional().getNome();
    	this.antigoEstabelecimento = (dist.getAntigoEstabelecimento()==null)?null:dist.getAntigoEstabelecimento().getNome();
    	this.usuarioModificacao = dist.getLastModifiedBy();
    	this.dataModificacao = dist.getLastModifiedDate();
    }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ThinConcursoDTO getConcurso() {
		return concurso;
	}
	public void setConcurso(ThinConcursoDTO concurso) {
		this.concurso = concurso;
	}
	public String getUsuarioModificacao() {
		return usuarioModificacao;
	}
	public void setUsuarioModificacao(String usuarioModificacao) {
		this.usuarioModificacao = usuarioModificacao;
	}
	public ZonedDateTime getDataModificacao() {
		return dataModificacao;
	}
	public void setDataModificacao(ZonedDateTime dataModificacao) {
		this.dataModificacao = dataModificacao;
	}
	public String getBilheteInicio() {
		return bilheteInicio;
	}
	public void setBilheteInicio(String bilheteInicio) {
		this.bilheteInicio = bilheteInicio;
	}
	public String getBilheteFim() {
		return bilheteFim;
	}
	public void setBilheteFim(String bilheteFim) {
		this.bilheteFim = bilheteFim;
	}
	public String getNovoEstabelecimento() {
		return novoEstabelecimento;
	}
	public void setNovoEstabelecimento(String novoEstabelecimento) {
		this.novoEstabelecimento = novoEstabelecimento;
	}
	public String getNovaRegional() {
		return novaRegional;
	}
	public void setNovaRegional(String novaRegional) {
		this.novaRegional = novaRegional;
	}
	public String getAntigoEstabelecimento() {
		return antigoEstabelecimento;
	}
	public void setAntigoEstabelecimento(String antigoEstabelecimento) {
		this.antigoEstabelecimento = antigoEstabelecimento;
	}
	public String getAntigaRegional() {
		return antigaRegional;
	}
	public void setAntigaRegional(String antigaRegional) {
		this.antigaRegional = antigaRegional;
	}
	
}
