package br.sorteioapp.service.dto;

public class DistribuicaoBilheteOnlineDTO {
    private Long regional_id;
    private Integer concurso_id;
    private Integer quantidade;
    private Long area_id;
    private Long estabelecimento_id;

    public Long getRegional_id() {
        return regional_id;
    }

    public void setRegional_id(Long regional_id) {
        this.regional_id = regional_id;
    }

    public Integer getConcurso_id() {
        return concurso_id;
    }

    public void setConcurso_id(Integer concurso_id) {
        this.concurso_id = concurso_id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Long getArea_id() {
        return area_id;
    }

    public void setArea_id(Long area_id) {
        this.area_id = area_id;
    }

    public Long getEstabelecimento_id() {
        return this.estabelecimento_id;
    }
}
