package br.sorteioapp.service.dto;

public class DistribuicaoEstabelecimentoDTO{

    private Long concursoId;
    private Long regionalId;
    private Long areaId;
    private Long estabelecimentoId;

    public DistribuicaoEstabelecimentoDTO(){
        
    }

    public void setConcursoId(Long concursoId){
        this.concursoId = concursoId;
    }

    public Long getConcursoId(){
        return this.concursoId;
    }

    public void setRegionalId(Long regionalId){
        this.regionalId = regionalId;
    }

    public Long getRegionalId(){
        return this.regionalId;
    }

    public void setAreaId(Long areaId){
        this.areaId = areaId;
    }

    public Long getAreaId(){
        return this.areaId;
    }

    public void setEstabelecimentoId(Long estabelecimento){
        this.estabelecimentoId = estabelecimento;
    }

    public Long getEstabelecimentoId(){
        return this.estabelecimentoId;
    }
}