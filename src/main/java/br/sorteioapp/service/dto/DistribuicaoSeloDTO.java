package br.sorteioapp.service.dto;

import br.sorteioapp.domain.DistribuicaoBilhete;
import br.sorteioapp.domain.DistribuicaoSelo;

import java.util.ArrayList;
import java.util.List;
import java.time.ZonedDateTime;


public class DistribuicaoSeloDTO {
    Long id;
    ThinConcursoDTO concurso;
    ThinRegionalDTO regional;
    ThinEstabelecimentoDTO estabelecimento;
    Integer numeroInicio;
    Integer numeroFim;
    Character operacao;
    private String usuarioModificacao;
    private ZonedDateTime dataModificacao;

    public DistribuicaoSeloDTO( ) {

    }
    public DistribuicaoSeloDTO (DistribuicaoSelo dist) {
        this.id = dist.getId();
        this.concurso = new ThinConcursoDTO(dist.getConcurso());
        this.regional = new ThinRegionalDTO(dist.getRegional());
        this.estabelecimento = (dist.getEstabelecimento() != null)?new ThinEstabelecimentoDTO(dist.getEstabelecimento()):null;
        this.numeroInicio = dist.getNumeroInicio();
        this.numeroFim = dist.getNumeroFim();
        this.operacao = dist.getOperacao();
    	this.usuarioModificacao = dist.getLastModifiedBy();
    	this.dataModificacao = dist.getLastModifiedDate();        
    }

    public static List<DistribuicaoSeloDTO> map( List<DistribuicaoSelo> lst){
        List<DistribuicaoSeloDTO> dtos = new ArrayList<>();
        for(DistribuicaoSelo dist : lst){
            dtos.add(new DistribuicaoSeloDTO(dist));
        }
        return dtos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ThinConcursoDTO getConcurso() {
        return concurso;
    }

    public void setConcurso(ThinConcursoDTO concurso) {
        this.concurso = concurso;
    }

    public ThinRegionalDTO getRegional() {
        return regional;
    }

    public void setRegional(ThinRegionalDTO regional) {
        this.regional = regional;
    }

    public ThinEstabelecimentoDTO getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(ThinEstabelecimentoDTO estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public Integer getNumeroInicio() {
        return numeroInicio;
    }

    public void setNumeroInicio(Integer numeroInicio) {
        this.numeroInicio = numeroInicio;
    }

    public Integer getNumeroFim() {
        return numeroFim;
    }

    public void setNumeroFim(Integer numeroFim) {
        this.numeroFim = numeroFim;
    }

    public Character getOperacao() {
        return operacao;
    }

    public void setOperacao(Character operacao) {
        this.operacao = operacao;
    }

    public String getUsuarioModificacao() {
		return usuarioModificacao;
	}
	public void setUsuarioModificacao(String usuarioModificacao) {
		this.usuarioModificacao = usuarioModificacao;
	}
	public ZonedDateTime getDataModificacao() {
		return dataModificacao;
	}
	public void setDataModificacao(ZonedDateTime dataModificacao) {
		this.dataModificacao = dataModificacao;
	}
}
