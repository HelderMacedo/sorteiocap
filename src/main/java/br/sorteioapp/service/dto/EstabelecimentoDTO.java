package br.sorteioapp.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.sorteioapp.config.Constants;
import br.sorteioapp.domain.Estabelecimento;

/**
 * Created by rodrigo on 24/04/17.
 */
public class EstabelecimentoDTO {

	private Long id;
	@NotNull
	@Size(max = 128)
	private String nome;
	private String pontoReferencia;
	@Size(max = 16)
	private String telefone;
	private ThinRegionalDTO regional;
	private ThinAreaRegionalDTO arearegional;
	@NotNull
	@Size(max = 32)
	private String bairro;
	@Pattern(regexp = "(\\d{8})")
	private String cep;
	@NotNull
	@Size(max = 32)
	private String cidade;
	@NotNull
	@Size(min = 2, max = 2)
	private String uf;
	@Size(max = 128)
	private String logradouro;
	@Size(max = 8)
	// @Pattern(regexp = "^[0-9]*$")
	private String numero;
	@Size(min = 5, max = 100)
	private String email;
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String login;
	// @todo colocar validacao senha
	private String senha;
	// @todo colocar validacao cpf
	private String cpf;
	private float percentualRepasse;
	private boolean ativo;
	private boolean padrao;
	private String responsavel;
	private Boolean online;

	public EstabelecimentoDTO() {
	}

	public EstabelecimentoDTO(Estabelecimento estabelecimento) {
		this.setId(estabelecimento.getId());
		this.setBairro(estabelecimento.getBairro());
		this.setCep(estabelecimento.getCep());
		this.setCidade(estabelecimento.getCidade());
		this.setUf(estabelecimento.getUf());
		this.setCpf(estabelecimento.getCpf());
		this.setLogradouro(estabelecimento.getLogradouro());
		this.setNome(estabelecimento.getNome());
		this.setResponsavel(estabelecimento.getResponsavel());
		this.setNumero(estabelecimento.getNumero());
		this.setPontoReferencia(estabelecimento.getPontoReferencia());
		this.setTelefone(estabelecimento.getTelefone());
		this.setPercentualRepasse(estabelecimento.getPercentualRepasse());
		if (estabelecimento.getRegional() != null) {
			this.setRegional(new ThinRegionalDTO(estabelecimento.getRegional()));
		}
		if (estabelecimento.getArea() != null) {
			this.setArearegional(new ThinAreaRegionalDTO(estabelecimento.getArea()));
		}
		this.padrao = estabelecimento.isPadraoRegional();
		this.ativo = estabelecimento.isAtivo();
		this.online = estabelecimento.getOnline();
	}

	public boolean isPadrao() {
		return padrao;
	}

	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPontoReferencia() {
		return pontoReferencia;
	}

	public void setPontoReferencia(String pontoReferencia) {
		this.pontoReferencia = pontoReferencia;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public ThinRegionalDTO getRegional() {
		return regional;
	}

	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

	public ThinAreaRegionalDTO getArearegional() {
		return arearegional;
	}

	public void setArearegional(ThinAreaRegionalDTO arearegional) {
		this.arearegional = arearegional;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public Boolean getOnline() {
		return this.online;
	}

	public void setOnline(Boolean online) {
		this.online = online;
	}
}
