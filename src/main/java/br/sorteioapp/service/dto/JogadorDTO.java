package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Jogador;

/**
 * Created by rodrigo on 25/04/17.
 */
public class JogadorDTO {

    private String nome;
    private String cpf;
    private String telefone;
    private String identificacao;

    public JogadorDTO(){}
    public JogadorDTO(Jogador jog){
        this.setNome(jog.getNome());
        this.setCpf(jog.getCpf());
        this.setTelefone(jog.getTelefone());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }
}
