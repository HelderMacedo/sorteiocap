package br.sorteioapp.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

import br.sorteioapp.domain.GrupoLancamentoFinanceiro;
import br.sorteioapp.domain.LancamentoFinanceiro;
import java.time.LocalDateTime;
/**
 * A DTO for the LancamentoFinanceiro entity.
 */
public class LancamentoFinanceiroDTO implements Serializable {

	private Long id;

	private String descricao;

	private GrupoLancamentoFinanceiro grupoLancamento;

    private LocalDate dataLancamento;

	private float valor;

	public LancamentoFinanceiroDTO() {

	}

	public LancamentoFinanceiroDTO(LancamentoFinanceiro lancamento) {
		this.setDescricao(lancamento.getDescricao());
		this.setId(lancamento.getId());
		this.setDataLancamento(lancamento.getDataLancamento());
		this.setGrupoLancamento(lancamento.getGrupoLancamento());
		this.setValor(lancamento.getValor());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

    public GrupoLancamentoFinanceiro getGrupoLancamento() {
        return grupoLancamento;
    }

    public void setGrupoLancamento(GrupoLancamentoFinanceiro grupoLancamento) {
        this.grupoLancamento = grupoLancamento;
    }

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(LocalDate dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		LancamentoFinanceiroDTO lancamentoDTO = (LancamentoFinanceiroDTO) o;
		if (lancamentoDTO.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), lancamentoDTO.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "LancamentoFinanceiroDTO{" + "id=" + getId() + ", valor='" + getValor() + "'" + ", dataLancamento=" + getDataLancamento() + "}";
	}
}
