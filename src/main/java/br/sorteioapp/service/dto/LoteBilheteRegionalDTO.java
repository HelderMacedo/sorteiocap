package br.sorteioapp.service.dto;

import br.sorteioapp.domain.DistribuicaoBilhete;

/**
 * Created by rodrigo on 16/04/17.
 */
public class LoteBilheteRegionalDTO {

    private Long id;
    private int concursoId;
    private Long regionalId;
    private Float valor;
    protected String bilheteInicial;
    protected String bilheteFinal;
    protected String seloInicial;
    protected String seloFinal;
    protected Character motivoDistribuicao;

    public LoteBilheteRegionalDTO(){
        this.motivoDistribuicao = DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_REGIONAL;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(int concursoId) {
        this.concursoId = concursoId;
    }

    public Long getRegionalId() {
        return regionalId;
    }

    public void setRegionalId(Long regionalId) {
        this.regionalId = regionalId;
    }

    public String getBilheteInicial() {
        return bilheteInicial;
    }

    public void setBilheteInicial(String bilheteInicial) {
        this.bilheteInicial = bilheteInicial;
    }

    public String getBilheteFinal() {
        return bilheteFinal;
    }

    public void setBilheteFinal(String bilheteFinal) {
        this.bilheteFinal = bilheteFinal;
    }

    public String getSeloInicial() {
        return seloInicial;
    }

    public void setSeloInicial(String seloInicial) {
        this.seloInicial = seloInicial;
    }

    public String getSeloFinal() {
        return seloFinal;
    }

    public void setSeloFinal(String seloFinal) {
        this.seloFinal = seloFinal;
    }
    
    @Override
    public String toString() {
        return "LoteBilheteRegionalDTO{" +
            "id=" + id +
            ", concursoId='" + concursoId + "'" +
            ", regionalId='" + regionalId + "'" +
            '}';
    }


    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Character getMotivoDistribuicao() {
        return motivoDistribuicao;
    }

    public void setMotivoDistribuicao(Character motivoDistribuicao) {
        this.motivoDistribuicao = motivoDistribuicao;
    }
}
