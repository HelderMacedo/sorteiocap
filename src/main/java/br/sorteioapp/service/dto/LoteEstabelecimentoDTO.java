package br.sorteioapp.service.dto;

import br.sorteioapp.domain.DistribuicaoBilhete;

import java.util.Arrays;

/**
 * Created by rodrigo on 16/04/17.
 */
public class LoteEstabelecimentoDTO extends LoteGeralDTO {

    private int concursoId;
    private long estabelecimentoId;
    private Character motivoDistribuicao;
    private Long areaRegionalId;

    public LoteEstabelecimentoDTO(){
        super();
        this.motivoDistribuicao = DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_ESTABELECIMENTO;
    }

    @Override
    public String toString() {
        return "LoteEstabelecimentoDTO{" +
            ", concursoId='" + getConcursoId() + "'" +
            ", estabelecimentoId='" + getEstabelecimentoId() + "'" +
            super.toString()+
            '}';
    }

    public int getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(int concursoId) {
        this.concursoId = concursoId;
    }

    public long getEstabelecimentoId() {
        return estabelecimentoId;
    }

    public void setEstabelecimentoId(long estabelecimentoId) {
        this.estabelecimentoId = estabelecimentoId;
    }

    public Character getMotivoDistribuicao() {
        return motivoDistribuicao;
    }

    public void setMotivoDistribuicao(Character motivoDistribuicao) {
        this.motivoDistribuicao = motivoDistribuicao;
    }

    public void setAreaRegionalId(Long area){
        this.areaRegionalId = area;
    }

    public Long getAreaRegionalId(){
        return this.areaRegionalId;
    }
}
