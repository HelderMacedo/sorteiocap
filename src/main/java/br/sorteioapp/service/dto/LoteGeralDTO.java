package br.sorteioapp.service.dto;

import br.sorteioapp.domain.LoteGeracaoBilhete;
import br.sorteioapp.domain.TipoBilhete;

/**
 * Created by rodrigo on 16/04/17.
 */
public class LoteGeralDTO {
	protected TipoBilhete tipo;
	protected String bilheteInicial;
	protected String bilheteFinal;
	protected int quantidade = 0;
	protected boolean online;

	public LoteGeralDTO() {
	}

	public LoteGeralDTO(LoteGeracaoBilhete lote) {
		this.tipo = lote.getTipo();
		if (lote.getBilheteInicial() != null) {
			this.bilheteInicial = lote.getBilheteInicial().getIdentificacao();
		}
		if (lote.getBilheteFinal() != null) {
			this.bilheteFinal = lote.getBilheteFinal().getIdentificacao();
		}
		this.setQuantidade((int) (lote.getNumeroFinal() - lote.getNumeroInicial()) + 1);
		this.setOnline(Boolean.TRUE.equals(lote.getOnline()));
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public void setTipo(TipoBilhete tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "LoteGeralDTO{" + "tipo=" + tipo + ", bilheteInicial='" + getBilheteInicial() + "'" + ", bilheteFinal='"
				+ getBilheteFinal() + "'" + '}';
	}

	public String getBilheteInicial() {
		return bilheteInicial;
	}

	public void setBilheteInicial(String bilheteInicial) {
		this.bilheteInicial = bilheteInicial;
	}

	public String getBilheteFinal() {
		return bilheteFinal;
	}

	public void setBilheteFinal(String bilheteFinal) {
		this.bilheteFinal = bilheteFinal;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}
}
