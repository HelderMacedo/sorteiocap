package br.sorteioapp.service.dto;

/**
 * Created by rodrigo on 24/04/17.
 */
public class LoteRegionalBilheteDTO extends LoteGeralDTO {
    private Long id;
    private ThinConcursoDTO concurso;
    private ThinRegionalDTO regional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ThinConcursoDTO getConcurso() {
        return concurso;
    }

    public void setConcurso(ThinConcursoDTO concurso) {
        this.concurso = concurso;
    }

    public ThinRegionalDTO getRegional() {
        return regional;
    }

    public void setRegional(ThinRegionalDTO regional) {
        this.regional = regional;
    }
}
