package br.sorteioapp.service.dto;

import br.sorteioapp.domain.DistribuicaoBilhete;

public class LoteSubRegionalDTO extends LoteGeralDTO {
    private int concursoId;
    private Long areaId;
    private Character motivoDistribuicao;

    public LoteSubRegionalDTO(){
        super();
        this.motivoDistribuicao =DistribuicaoBilhete.MOTIVO_DISTRIBUICAO_SUBREGIONAL;
    }

    @Override
    public String toString() {
        return "LoteSubRegionalDTO{" +
            ", concursoId='" + getConcursoId() + "'" +
            ", areaId='" + getAreaId() + "'" +
            super.toString()+
            '}';
    }

    public int getConcursoId() {
        return concursoId;
    }

    public void setConcursoId(int concursoId) {
        this.concursoId = concursoId;
    }

    public long getAreaId() {
        return areaId;
    }

    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }

    public Character getMotivoDistribuicao() {
        return motivoDistribuicao;
    }

    public void setMotivoDistribuicao(Character motivoDistribuicao) {
        this.motivoDistribuicao = motivoDistribuicao;
    }
}