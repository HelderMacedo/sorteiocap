package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.domain.Regional;

import java.time.LocalDateTime;

public class LoteValidacaoStatusDTO {

    private ThinConcursoDTO concurso;
    private Long loteValidacao;
    private Long quantidade;
    private LocalDateTime dataValidacao;

    public LoteValidacaoStatusDTO(Object[] objs) {
        this(new ThinConcursoDTO(((Concurso) objs[0])), (Long) objs[1],
            (Long) objs[2],(LocalDateTime) objs[3]);
    }

    public LoteValidacaoStatusDTO(ThinConcursoDTO concurso, Long loteValidacao, Long quantidade, LocalDateTime dataValidacao) {
        this.concurso = concurso;
        this.loteValidacao = loteValidacao;
        this.quantidade = quantidade;
        this.dataValidacao = dataValidacao;
    }

    public ThinConcursoDTO getConcurso() {
        return concurso;
    }

    public void setConcurso(ThinConcursoDTO concurso) {
        this.concurso = concurso;
    }

    public Long getLoteValidacao() {
        return loteValidacao;
    }

    public void setLoteValidacao(Long loteValidacao) {
        this.loteValidacao = loteValidacao;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getDataValidacao() {
        return dataValidacao;
    }

    public void setDataValidacao(LocalDateTime dataValidacao) {
        this.dataValidacao = dataValidacao;
    }
}
