package br.sorteioapp.service.dto;

/**
 * Created by rodrigo on 24/04/17.
 */
public class LoteVendaBilheteDTO extends LoteGeralDTO {
    private Long id;
    private ThinConcursoDTO concurso;
    private ThinEstabelecimentoDTO estabelecimento;
    private Long loteRegionalId;
    private ThinRegionalDTO regional;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ThinConcursoDTO getConcurso() {
        return concurso;
    }

    public void setConcurso(ThinConcursoDTO concurso) {
        this.concurso = concurso;
    }

    public ThinEstabelecimentoDTO getEstabelecimento() {
        return estabelecimento;
    }

    public void setEstabelecimento(ThinEstabelecimentoDTO estabelecimento) {
        this.estabelecimento = estabelecimento;
    }

    public Long getLoteRegionalId() {
        return loteRegionalId;
    }

    public void setLoteRegionalId(Long loteRegionalId) {
        this.loteRegionalId = loteRegionalId;
    }

    public ThinRegionalDTO getRegional() {
        return regional;
    }

    public void setRegional(ThinRegionalDTO regional) {
        this.regional = regional;
    }
}
