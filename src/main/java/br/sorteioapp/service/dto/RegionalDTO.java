package br.sorteioapp.service.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.sorteioapp.config.Constants;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.Regiao;

/**
 * Created by rodrigo on 24/04/17.
 */
public class RegionalDTO {
	private Long id;
	@Size(max = 16)
	private String telefone;
	@Size(max = 32)
	private String bairro;
	@Pattern(regexp = "(\\d{8})")
	private String cep;
	@Size(max = 32)
	private String cidade;
	@Size(min = 2, max = 2)
	private String uf;
	@Size(max = 128)
	private String logradouro;
	@NotNull
	@Size(max = 64)
	private String nome;
	@Size(max = 8)
	@Pattern(regexp = "^[0-9]*$")
	private String numero;
	private float percentualRepasse;
	@Size(max = 64)
	private String responsavel;
	@Size(min = 5, max = 100)
	private String email;
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String login;
	// @todo colocar validacao da senha
	private String senha;
	// @todo colocar validação sobre CPF
	private String cpf;
	private boolean ativo;
	private Long regiaoId;	

	public RegionalDTO() {
	}

	public RegionalDTO(Regional regional) {
		this.setBairro(regional.getBairro());
		this.setCep(regional.getCep());
		this.setCidade(regional.getCidade());
		this.setUf(regional.getUf());
		this.setCpf(regional.getCpf());
		this.setId(regional.getId());
		this.setTelefone(regional.getTelefone());
		this.setLogradouro(regional.getLogradouro());
		this.setNome(regional.getNome());
		this.setNumero(regional.getNumero());
		this.setPercentualRepasse(regional.getPercentualRepasse());
		this.setResponsavel(regional.getResponsavel());
		this.ativo = regional.isAtivo();
		if (regional.getUsuario() != null) {
			this.setEmail(regional.getUsuario().getEmail());
			this.setLogin(regional.getUsuario().getLogin());
			// this.setSenha(regional.getUsuario().getPassword());
		}
		if(regional.getRegiao() != null)
	    	this.regiaoId = regional.getRegiao().getId();		
	}

	public static List<RegionalDTO> map(List<Regional> listReg) {
		List<RegionalDTO> listDTO = new ArrayList<>();
		for (Regional reg : listReg) {
			listDTO.add(new RegionalDTO(reg));
		}
		return listDTO;
	}

	public String getBairro() {
		return bairro;
	}

	public String getCep() {
		return cep;
	}

	public String getCidade() {
		return cidade;
	}

	public Long getId() {
		return id;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public String getNome() {
		return nome;
	}

	public String getNumero() {
		return numero;
	}

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public String getResponsavel() {
		return responsavel;
	}

	public String getEmail() {
		return email;
	}

	public String getLogin() {
		return login;
	}

	public String getCpf() {
		return cpf;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

    public Long getRegiaoId() {
        return regiaoId;
    }

    public void setRegiaoId(Long regiaoId) {
        this.regiaoId = regiaoId;
    }
}
