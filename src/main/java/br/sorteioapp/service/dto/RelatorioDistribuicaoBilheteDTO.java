package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Concurso;

public class RelatorioDistribuicaoBilheteDTO{

    private Concurso concurso;
    private Integer numInicial;
    private Integer numFinal;
    private String identificacaoInicial;
    private String identificacaoFinal;
    private String estabelecimento;
    private String area;
    private String regional;

    public void setConcurso(Concurso concurso){
        this.concurso = concurso;
    }

    public Concurso getConcurso(){
        return this.concurso;
    }

    public void setNumInicial(Integer num){
        this.numInicial = num;
    }

    public Integer getNumInicial(){
        return this.numInicial;
    }

    public void setNumFinal(Integer num){
        this.numFinal = num;
    }

    public Integer getNumFinal(){
        return this.numFinal;
    }

    public void setIdentificacaoInicial(String ident){
        this.identificacaoInicial = ident;
    }

    public String getIdentificacaoInicial(){
        return this.identificacaoInicial;
    }

    public void setIdentificacaoFinal(String ident){
        this.identificacaoFinal = ident;
    }

    public String getIdentificacaoFinal(){
        return this.identificacaoFinal;
    }

    public void setEstabelecimento(String est){
        this.estabelecimento = est;
    }

    public String getEstabelecimento(){
        return this.estabelecimento;
    }

    public void setRegional(String reg){
        this.regional = reg;
    }

    public String getRegional(){
        return this.regional;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getArea(){
        return this.area;
    }



    
}