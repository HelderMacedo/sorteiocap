package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Concurso;

public class RelatorioDistribuicaoSubRegionalDTO{

    private Concurso concurso;
    private Integer numInicial;
    private Integer numFinal;
    private String identificacaoInicial;
    private String identificacaoFinal;
    private String area;

    public void setConcurso(Concurso concurso){
        this.concurso = concurso;
    }

    public Concurso getConcurso(){
        return this.concurso;
    }

    public void setNumInicial(Integer num){
        this.numInicial = num;
    }

    public Integer getNumInicial(){
        return this.numInicial;
    }

    public void setNumFinal(Integer num){
        this.numFinal = num;
    }

    public Integer getNumFinal(){
        return this.numFinal;
    }

    public void setIdentificacaoInicial(String ident){
        this.identificacaoInicial = ident;
    }

    public String getIdentificacaoInicial(){
        return this.identificacaoInicial;
    }

    public void setIdentificacaoFinal(String ident){
        this.identificacaoFinal = ident;
    }

    public String getIdentificacaoFinal(){
        return this.identificacaoFinal;
    }

    public void setArea(String area){
        this.area = area;
    }

    public String getArea(){
        return this.area;
    }

}