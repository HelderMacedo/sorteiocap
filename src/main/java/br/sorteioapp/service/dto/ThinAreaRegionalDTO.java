package br.sorteioapp.service.dto;

import java.util.ArrayList;
import java.util.List;

import br.sorteioapp.domain.AreaRegional;

/**
 * Created by rodrigo on 14/05/17.
 */
public class ThinAreaRegionalDTO {
    private Long id;
    private String nome;
    private String login;
    private String responsavel;
    private float percentualRepasse;
	private ThinRegionalDTO regional;
    private boolean ativo;

    public ThinAreaRegionalDTO(){}
    public ThinAreaRegionalDTO(AreaRegional area) {
        this.setId(area.getId());
        this.setNome(area.getNome());
        this.setResponsavel(area.getResponsavel());
        this.setPercentualRepasse(area.getPercentualRepasse());
        this.ativo = area.isAtivo();
        if(area.getUsuario()!= null) {
            this.setLogin(area.getUsuario().getLogin());
        }
		if(area.getRegional() != null) {
			this.setRegional(new ThinRegionalDTO(area.getRegional()));
		}        
    }
    public ThinAreaRegionalDTO(Long id, String nome) {
    	this.id = id;
    	this.nome = nome;
    }

    public static List<ThinAreaRegionalDTO> map(List<AreaRegional> listReg) {
        List<ThinAreaRegionalDTO> listDTO = new ArrayList<>();
        for(AreaRegional reg: listReg){
            listDTO.add(new ThinAreaRegionalDTO(reg));
        }
        return listDTO;
    }


    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getLogin() {
        return login;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public float getPercentualRepasse() {
        return percentualRepasse;
    }

    public void setPercentualRepasse(float percentualRepasse) {
        this.percentualRepasse = percentualRepasse;
    }
	public ThinRegionalDTO getRegional() {
		return regional;
	}
	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}
    
}
