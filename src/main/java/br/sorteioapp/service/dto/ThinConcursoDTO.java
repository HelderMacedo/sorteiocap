package br.sorteioapp.service.dto;

import java.text.DecimalFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.TipoBilhete;

/**
 * Created by rodrigo on 14/05/17.
 */
public class ThinConcursoDTO {

	private Integer id;
	private int ano;
	private String label;
	private TipoBilhete tipo;
	private ZonedDateTime dataSorteio;

	private Boolean sincronizado;
    private Integer quantidadePremios;

    private Double percentualFixoRegional;
    private String premio1;
    private String premio2;
    private String premio3;
    private String premio4;
    private String premio5;


	public ThinConcursoDTO() {

	}

	public ThinConcursoDTO(Concurso conc) {
		this.id = conc.getId();
		if (conc.getId() != null) {
			this.ano = conc.getId() / 100;
			this.setLabel(
					conc.getTipo().getLabel() + " - " + new DecimalFormat("#000").format(conc.getNumSorteio() % 1000));
			this.setTipo(conc.getTipo());
			this.dataSorteio = conc.getDataSorteio().atStartOfDay().atZone(ZoneId.systemDefault());
			this.sincronizado = conc.getSincronizado();
            this.quantidadePremios = conc.getQuantidadePremios();
            this.premio1 = conc.getPremio1();
            this.premio2 = conc.getPremio2();
            this.premio3 = conc.getPremio3();
            this.premio4 = conc.getPremio4();
            this.premio5 = conc.getPremio5();
            this.percentualFixoRegional = conc.getPercentualFixoRegional();

		}
	}

	public static List<ThinConcursoDTO> map(List<Concurso> listConc) {
		List<ThinConcursoDTO> listDTO = new ArrayList<>();
		for (Concurso conc : listConc) {
			listDTO.add(new ThinConcursoDTO(conc));
		}
		return listDTO;
	}

	@Override
	public String toString() {
		return "Concurso{" + "id=" + id + ", ano='" + ano + "'" + ", label='" + getLabel() + "'" + '}';
	}

	public Integer getId() {
		return id;
	}

	public ZonedDateTime getDataSorteio() {
		return dataSorteio;
	}

	public void setDataSorteio(ZonedDateTime dataSorteio) {
		this.dataSorteio = dataSorteio;
	}

	public int getAno() {
		return ano;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public TipoBilhete getTipo() {
		return tipo;
	}

	public void setTipo(TipoBilhete tipo) {
		this.tipo = tipo;
	}

    public Boolean getSincronizado() {
        return sincronizado;
    }

    public void setSincronizado(Boolean sincronizado) {
        this.sincronizado = sincronizado;
    }

    public Integer getQuantidadePremios() {
        return quantidadePremios;
    }

    public void setQuantidadePremios(Integer quantidadePremios) {
        this.quantidadePremios = quantidadePremios;
    }

    public Double getPercentualFixoRegional() {
        return percentualFixoRegional;
    }

    public void setPercentualFixoRegional(Double percentualFixoRegional) {
        this.percentualFixoRegional = percentualFixoRegional;
    }

    public String getPremio1() {
        return premio1;
    }

    public void setPremio1(String premio1) {
        this.premio1 = premio1;
    }

    public String getPremio2() {
        return premio2;
    }

    public void setPremio2(String premio2) {
        this.premio2 = premio2;
    }

    public String getPremio3() {
        return premio3;
    }

    public void setPremio3(String premio3) {
        this.premio3 = premio3;
    }

    public String getPremio4() {
        return premio4;
    }

    public void setPremio4(String premio4) {
        this.premio4 = premio4;
    }

    public String getPremio5() {
        return premio5;
    }

    public void setPremio5(String premio5) {
        this.premio5 = premio5;
    }
}
