package br.sorteioapp.service.dto;

public class ThinDistribuicaoBilheteAtualDTO {

	protected String bilheteInicial;
	protected String bilheteFinal;
    private ThinConcursoDTO concurso;
    private ThinRegionalDTO regional;	
    private ThinEstabelecimentoDTO estabelecimento;    
	protected int quantidade = 0;
	
	public ThinDistribuicaoBilheteAtualDTO(ThinConcursoDTO concurso, String bilheteInicial, String bilheteFinal, ThinRegionalDTO regional, ThinEstabelecimentoDTO estabelecimento, int quantidade){
		this.concurso = concurso;
		this.bilheteInicial = bilheteInicial;
		this.bilheteFinal = bilheteFinal;
		this.regional = regional;
		this.estabelecimento = estabelecimento;
		this.quantidade = quantidade;
	}
	
	public String getBilheteInicial() {
		return bilheteInicial;
	}
	public void setBilheteInicial(String bilheteInicial) {
		this.bilheteInicial = bilheteInicial;
	}
	public String getBilheteFinal() {
		return bilheteFinal;
	}
	public void setBilheteFinal(String bilheteFinal) {
		this.bilheteFinal = bilheteFinal;
	}
	public ThinConcursoDTO getConcurso() {
		return concurso;
	}
	public void setConcurso(ThinConcursoDTO concurso) {
		this.concurso = concurso;
	}
	public ThinRegionalDTO getRegional() {
		return regional;
	}
	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}
	public ThinEstabelecimentoDTO getEstabelecimento() {
		return estabelecimento;
	}
	public void setEstabelecimento(ThinEstabelecimentoDTO estabelecimento) {
		this.estabelecimento = estabelecimento;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}	

}
