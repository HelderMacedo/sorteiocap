package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Estabelecimento;

/**
 * Created by rodrigo on 25/04/17.
 */
public class ThinEstabelecimentoDTO {
	private Long id;
	private String bairro;
	private String cidade;
	private String uf;
	private String telefone;
	private String nome;
	private float percentualRepasse;
	private ThinRegionalDTO regional;
	private ThinAreaRegionalDTO areaRegional;
	private boolean ativo;
	private boolean padrao;

	public ThinEstabelecimentoDTO(){}
	public ThinEstabelecimentoDTO(Long id, String nome) {
		this.id = id;
		this.nome = nome;
	}
	public ThinEstabelecimentoDTO(Estabelecimento estabelecimento) {
		this(estabelecimento, true);
	}
	public ThinEstabelecimentoDTO(Estabelecimento estabelecimento, boolean loadRegional) {
		this.setId(estabelecimento.getId());
		this.setBairro(estabelecimento.getBairro());
		this.setCidade(estabelecimento.getCidade());
		this.setUf(estabelecimento.getUf());
		this.setTelefone(estabelecimento.getTelefone());
		this.setNome(estabelecimento.getNome());
		this.setPercentualRepasse(estabelecimento.getPercentualRepasse());
		this.setAtivo(estabelecimento.isAtivo());
		this.setPadrao(estabelecimento.isPadraoRegional());
		if (loadRegional && estabelecimento.getRegional() != null) {
			this.setRegional(new ThinRegionalDTO(estabelecimento.getRegional()));
		}
		if (loadRegional && estabelecimento.getArea() != null) {
			this.setAreaRegional(new ThinAreaRegionalDTO(estabelecimento.getArea()));
		}
	}

	public static ThinEstabelecimentoDTO createWithoutRegional(Estabelecimento estabelecimento) {
		return new ThinEstabelecimentoDTO(estabelecimento, false);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

    public ThinEstabelecimentoDTO id(Long id) {
        this.id = id;
        return this;
    }

	public boolean isPadrao() {
		return padrao;
	}
	public void setPadrao(boolean padrao) {
		this.padrao = padrao;
	}
	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
    public ThinEstabelecimentoDTO bairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
    public ThinEstabelecimentoDTO cidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}
    public ThinEstabelecimentoDTO uf(String uf) {
        this.uf = uf;
        return this;
    }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
    public ThinEstabelecimentoDTO nome(String nome) {
        this.nome = nome;
        return this;
    }

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean situacao) {
		this.ativo = situacao;
	}

	public ThinRegionalDTO getRegional() {
		return regional;
	}

	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}
    public ThinEstabelecimentoDTO regional(ThinRegionalDTO regional) {
        this.regional = regional;
        return this;
    }

	public float getPercentualRepasse() {
		return percentualRepasse;
	}

	public void setPercentualRepasse(float percentualRepasse) {
		this.percentualRepasse = percentualRepasse;
	}
	public ThinAreaRegionalDTO getAreaRegional() {
		return areaRegional;
	}
	public void setAreaRegional(ThinAreaRegionalDTO areaRegional) {
		this.areaRegional = areaRegional;
	}
	
}
