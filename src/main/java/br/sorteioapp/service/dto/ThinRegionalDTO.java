package br.sorteioapp.service.dto;

import java.util.ArrayList;
import java.util.List;

import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.Regiao;
/**
 * Created by rodrigo on 14/05/17.
 */
public class ThinRegionalDTO {
    private Long id;
    private String nome;
    private String login;
    private String responsavel;
    private float percentualRepasse;
    private boolean ativo;
    private Regiao regiao;

    public ThinRegionalDTO(){}
    public ThinRegionalDTO(Regional regional) {
        this.setId(regional.getId());
        this.setNome(regional.getNome());
        this.setResponsavel(regional.getResponsavel());
        this.setPercentualRepasse(regional.getPercentualRepasse());
        this.ativo = regional.isAtivo();
        if(regional.getUsuario()!= null) {
            this.setLogin(regional.getUsuario().getLogin());
        }
        this.regiao = regional.getRegiao();        
    }
    public ThinRegionalDTO(Long id, String nome) {
    	this.id = id;
    	this.nome = nome;
    }

    public static List<ThinRegionalDTO> map(List<Regional> listReg) {
        List<ThinRegionalDTO> listDTO = new ArrayList<>();
        for(Regional reg: listReg){
            listDTO.add(new ThinRegionalDTO(reg));
        }
        return listDTO;
    }


    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getLogin() {
        return login;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public ThinRegionalDTO id(Long id) {
        this.id = id;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    public ThinRegionalDTO nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public float getPercentualRepasse() {
        return percentualRepasse;
    }

    public void setPercentualRepasse(float percentualRepasse) {
        this.percentualRepasse = percentualRepasse;
    }

    public Regiao getRegiao() {
        return regiao;
    }

    public void setRegiao(Regiao regiao) {
        this.regiao = regiao;
    }
}
