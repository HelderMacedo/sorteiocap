package br.sorteioapp.service.dto;

import br.sorteioapp.domain.Estabelecimento;

/**
 * Created by rodrigo on 29/05/17.
 */
public class TotalizacaoEstabelecimentoDTO {
    private ThinEstabelecimentoDTO estabelecimento;
    private long totalDistribuidos;
    private long totalValidados;
    private double valorTotalArrecadado;
    private double valorTotalComissao;

    public TotalizacaoEstabelecimentoDTO() {
        this.totalDistribuidos = 0;
        this.totalValidados = 0;
        this.valorTotalArrecadado = 0;
        this.valorTotalComissao = 0;
    }

    public TotalizacaoEstabelecimentoDTO(ThinEstabelecimentoDTO test) {
        this();
        this.estabelecimento = test;
    }

    public TotalizacaoEstabelecimentoDTO(Estabelecimento est) {
        this(new ThinEstabelecimentoDTO(est));
    }

    public ThinEstabelecimentoDTO getEstabelecimento() {
        return estabelecimento;
    }

    public long getTotalDistribuidos() {
        return totalDistribuidos;
    }

    public void setTotalDistribuidos(long totalDistribuidos) {
        this.totalDistribuidos = totalDistribuidos;
    }

    public long getTotalValidados() {
        return totalValidados;
    }

    public void setTotalValidados(long totalValidados) {
        this.totalValidados = totalValidados;
    }

    public double getValorTotalArrecadado() {
        return valorTotalArrecadado;
    }

    public void setValorTotalArrecadado(double valorTotalArrecadado) {
        this.valorTotalArrecadado = valorTotalArrecadado;
    }

    public double getValorTotalComissao() {
        return valorTotalComissao;
    }

    public void setValorTotalComissao(double valorTotalComissao) {
        this.valorTotalComissao = valorTotalComissao;
    }
}
