package br.sorteioapp.service.dto;

import java.util.List;

/**
 * Created by rodrigo on 29/05/17.
 */
public class TotalizacaoRegionalDTO {
	private ThinConcursoDTO concurso;
	private ThinRegionalDTO regional;

	private long totalDistribuidos;
	private long totalValidados;
	private double valorTotalArrecadado;
    private double valorTotalComissao;
    private double valorTotalComissaoEstabelecimentos;
    private double ValorTotalFaturamento;

	private List<LancamentoFinanceiroDTO> lancamentos;

    public double getValorTotalFaturamento() {
        return ValorTotalFaturamento;
    }

    public void setValorTotalFaturamento(double valorTotalFaturamento) {
        ValorTotalFaturamento = valorTotalFaturamento;
    }

    public ThinConcursoDTO getConcurso() {
		return concurso;
	}

	public void setConcurso(ThinConcursoDTO concurso) {
		this.concurso = concurso;
	}

	public ThinRegionalDTO getRegional() {
		return regional;
	}

	public void setRegional(ThinRegionalDTO regional) {
		this.regional = regional;
	}

	public long getTotalValidados() {
		return totalValidados;
	}

	public void setTotalValidados(long totalValidados) {
		this.totalValidados = totalValidados;
	}

	public List<LancamentoFinanceiroDTO> getLancamentos() {
		return lancamentos;
	}

	public void setLancamentos(List<LancamentoFinanceiroDTO> lancamentos) {
		this.lancamentos = lancamentos;
	}

    public long getTotalDistribuidos() {
        return totalDistribuidos;
    }

    public void setTotalDistribuidos(long totalDistribuidos) {
        this.totalDistribuidos = totalDistribuidos;
    }

    public double getValorTotalArrecadado() {
        return valorTotalArrecadado;
    }

    public void setValorTotalArrecadado(double valorTotalArrecadado) {
        this.valorTotalArrecadado = valorTotalArrecadado;
    }

    public double getValorTotalComissao() {
        return valorTotalComissao;
    }

    public void setValorTotalComissao(double valorTotalComissao) {
        this.valorTotalComissao = valorTotalComissao;
    }

    public double getValorTotalComissaoEstabelecimentos() {
        return valorTotalComissaoEstabelecimentos;
    }

    public void setValorTotalComissaoEstabelecimentos(double valorTotalComissaoEstabelecimentos) {
        this.valorTotalComissaoEstabelecimentos = valorTotalComissaoEstabelecimentos;
    }
}
