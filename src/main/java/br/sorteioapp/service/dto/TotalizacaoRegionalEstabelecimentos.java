package br.sorteioapp.service.dto;
import java.util.List;

public class TotalizacaoRegionalEstabelecimentos {
    private ThinConcursoDTO concurso;
    private ThinRegionalDTO regional;

    private List<TotalizacaoEstabelecimentoDTO> totaisEstabelecimentos;

    public ThinConcursoDTO getConcurso() {
        return concurso;
    }

    public void setConcurso(ThinConcursoDTO concurso) {
        this.concurso = concurso;
    }

    public ThinRegionalDTO getRegional() {
        return regional;
    }

    public void setRegional(ThinRegionalDTO regional) {
        this.regional = regional;
    }

    public List<TotalizacaoEstabelecimentoDTO> getTotaisEstabelecimentos() {
        return totaisEstabelecimentos;
    }

    public void setTotaisEstabelecimentos(List<TotalizacaoEstabelecimentoDTO> totaisEstabelecimentos) {
        this.totaisEstabelecimentos = totaisEstabelecimentos;
    }
}
