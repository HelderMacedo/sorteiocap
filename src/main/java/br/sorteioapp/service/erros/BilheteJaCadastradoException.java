package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 19/05/17.
 */
public class BilheteJaCadastradoException extends Exception {
    private final String nomeJogador;
    private final String identificacaoBilhete;

    public BilheteJaCadastradoException(String nomeJogador, String identificacaoBilhete){
        this.nomeJogador = nomeJogador;
        this.identificacaoBilhete = identificacaoBilhete;
    }

    public String getNomeJogador() {
        return nomeJogador;
    }

    public String getIdentificacaoBilhete() {
        return identificacaoBilhete;
    }
}
