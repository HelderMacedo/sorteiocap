package br.sorteioapp.service.erros;

public class BilheteJaValidadoException extends Exception {

	public BilheteJaValidadoException(String identificacao) {
		super(identificacao);
	}

}
