package br.sorteioapp.service.erros;

public class BilheteNaoCorrespondeConcurso extends Exception {
    private String identificacao;

    public BilheteNaoCorrespondeConcurso(String identificacao){
        super("Bilhete "+ identificacao+" não corresponde ao concurso selecionado.");
        this.identificacao = identificacao;
    }

    public String getIdentificacao(){
        return this.identificacao;
    }
}
