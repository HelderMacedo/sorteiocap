package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 22/05/17.
 */
public class BilhetesJaDevolvidosException extends Exception {

    private final String identificacaoInicio;
    private final String identificacaoFim;

    public BilhetesJaDevolvidosException(String identificacaoInicio, String identificacaoFim){
        super(geraMsg(identificacaoInicio, identificacaoFim ));
        this.identificacaoInicio = identificacaoInicio;
        this.identificacaoFim = identificacaoFim;
    }

    private static String geraMsg(String identificacaoInicio, String identificacaoFim) {
        return "Bilhetes na faixa ["+identificacaoInicio+", "+identificacaoFim+" já foram devolvidos.";
    }

    public String getIdentificacaoInicio() {
        return identificacaoInicio;
    }

    public String getIdentificacaoFim() {
        return identificacaoFim;
    }
}
