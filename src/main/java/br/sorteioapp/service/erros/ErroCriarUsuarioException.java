package br.sorteioapp.service.erros;

public class ErroCriarUsuarioException extends Exception {

	public ErroCriarUsuarioException() {
		// TODO Auto-generated constructor stub
	}

	public ErroCriarUsuarioException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ErroCriarUsuarioException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ErroCriarUsuarioException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ErroCriarUsuarioException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
