package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 12/06/17.
 */
public class FaixaDevolucaoInvalidaException extends Exception {
    private final String identificacaoInicio;
    private final String identificacaoFim;

    public FaixaDevolucaoInvalidaException(String identificacaoInicio, String identificacaoFim){
        super(geraMsg(identificacaoInicio, identificacaoFim ));
        this.identificacaoInicio = identificacaoInicio;
        this.identificacaoFim = identificacaoFim;
    }

    private static String geraMsg(String identificacaoInicio, String identificacaoFim) {
        return "A faixa ["+identificacaoInicio+", "+identificacaoFim+"] possui bilhetes vendidos e não vendidos";
    }

    public String getIdentificacaoInicio() {
        return identificacaoInicio;
    }

    public String getIdentificacaoFim() {
        return identificacaoFim;
    }

}
