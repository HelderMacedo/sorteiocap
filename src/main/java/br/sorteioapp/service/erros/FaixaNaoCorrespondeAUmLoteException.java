package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 22/05/17.
 */
public class FaixaNaoCorrespondeAUmLoteException extends Exception {

    private final String identificacaoInicio;
    private final String identificacaoFim;

    public FaixaNaoCorrespondeAUmLoteException(String identificacaoInicio, String identificacaoFim){
        super(geraMsg(identificacaoInicio, identificacaoFim ));
        this.identificacaoInicio = identificacaoInicio;
        this.identificacaoFim = identificacaoFim;
    }

    private static String geraMsg(String identificacaoInicio, String identificacaoFim) {
        return "A faixa de bilhetes ["+identificacaoInicio+", "+identificacaoFim+" não pertence a nenhum lote";
    }

    public String getIdentificacaoInicio() {
        return identificacaoInicio;
    }

    public String getIdentificacaoFim() {
        return identificacaoFim;
    }
}
