package br.sorteioapp.service.erros;

import java.util.Formatter;

/**
 * Created by rodrigo on 23/04/17.
 */
public class IdentificacaoBilheteInvalida extends Exception {
    private static final String MSG_IDENTIFICACAO_TAM_INVALIDO = "Identificação '%s' com tamanho inválido.";
    private static final String MSG_IDENTIFICACAO_SO_ALGARISMOS = "A identificação '%s' deve possuir apenas algarismos numéricos.";
    private static final String MSG_IDENTIFICACAO_INEXISTENTE = "O bilhete '%s' não existe ou é inválido.";
    public static final int INDEFINIDO = 0;
    public static final int TAM_INVALIDO = 1;
    public static final int SO_ALGARISMOS = 2;

    private static int codigoErro = INDEFINIDO;
    private String identificacao;

    public IdentificacaoBilheteInvalida( String identificacao){
    	this(INDEFINIDO, identificacao);
    }
    
    public IdentificacaoBilheteInvalida(int codigoErro, String identificacao){
        super(geraMsg(codigoErro, identificacao));
        this.identificacao = identificacao;
    }

    private static String geraMsg(int codigoErro, String identificacao) {
        switch (codigoErro){
            case TAM_INVALIDO:
               return new Formatter().format(MSG_IDENTIFICACAO_TAM_INVALIDO, identificacao).toString();
            case SO_ALGARISMOS:
               return new Formatter().format(MSG_IDENTIFICACAO_SO_ALGARISMOS, identificacao).toString();
            default:
                return new Formatter().format(MSG_IDENTIFICACAO_INEXISTENTE, identificacao).toString();
        }
    }

    public static int getCodigoErro() {
        return codigoErro;
    }

    public String getIdentificacao() {
        return identificacao;
    }
}
