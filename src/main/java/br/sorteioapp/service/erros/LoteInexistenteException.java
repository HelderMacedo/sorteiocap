package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 27/05/17.
 */
public class LoteInexistenteException extends Exception {

    private final Integer concursoId;
    private final Long loteid;

    public LoteInexistenteException(Integer concursoId, Long loteid){
        super(geraMsg(concursoId, loteid ));
        this.concursoId = concursoId;
        this.loteid = loteid;
    }

    private static String geraMsg(Integer concursoId, Long loteid) {
        return "O lote "+loteid+" inexiste no concurso "+concursoId+"";
    }

    public Integer getConcursoId() {
        return concursoId;
    }

    public Long getLoteid() {
        return loteid;
    }
}
