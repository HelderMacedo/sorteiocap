package br.sorteioapp.service.erros;

public class LoteJaPertenceAUmEstabelecimentoException extends Exception{

    private String estabelecimento;

    public LoteJaPertenceAUmEstabelecimentoException(String  estabelecimento){
        this.estabelecimento = estabelecimento;
    }

    private static String geraMsg(String estabelecimento) {
        return "A faixa de bilhetes pertence ao estalecimento "+estabelecimento;
    }

}