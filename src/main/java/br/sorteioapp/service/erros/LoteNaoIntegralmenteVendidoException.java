package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 27/05/17.
 */
public class LoteNaoIntegralmenteVendidoException extends Exception {

    private final Integer concursoId;
    private final Long loteid;

    public LoteNaoIntegralmenteVendidoException(Integer concursoId, Long loteid){
        super(geraMsg(concursoId, loteid ));
        this.concursoId = concursoId;
        this.loteid = loteid;
    }

    private static String geraMsg(Integer concursoId, Long loteid) {
        return "O lote "+loteid+" do concurso "+concursoId+" não foi integralmente vendido";
    }

    public Integer getConcursoId() {
        return concursoId;
    }

    public Long getLoteid() {
        return loteid;
    }
}
