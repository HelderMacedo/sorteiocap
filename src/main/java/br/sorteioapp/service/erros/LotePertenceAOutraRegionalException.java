package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 22/05/17.
 */
public class LotePertenceAOutraRegionalException extends Exception  {

    private String nomeRegional;

    public LotePertenceAOutraRegionalException(){
    }

    private static String geraMsg(String nomeRegional) {
        return "O lote pertence à regional "+nomeRegional+".";
    }

    public String getNomeRegional() {
        return nomeRegional;
    }

}
