package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 22/05/17.
 */
public class LotePertenceAOutraSubRegionalException extends Exception  {

    private String subregional;

    public LotePertenceAOutraSubRegionalException(String subregional){
        this.subregional = subregional;
    }

    private static String geraMsg(String subregional) {
        return "O lote pertence à subregional "+subregional+".";
    }

    public String getNomeSubRegional() {
        return subregional;
    }

}