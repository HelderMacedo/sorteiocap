package br.sorteioapp.service.erros;

public class LoteRegionalJaVendidoException extends Exception {

	public LoteRegionalJaVendidoException() {
		// TODO Auto-generated constructor stub
	}

	public LoteRegionalJaVendidoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public LoteRegionalJaVendidoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public LoteRegionalJaVendidoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public LoteRegionalJaVendidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
