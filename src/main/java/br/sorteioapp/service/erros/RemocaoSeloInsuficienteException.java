package br.sorteioapp.service.erros;

public class RemocaoSeloInsuficienteException extends Exception {

    public RemocaoSeloInsuficienteException (String msg){
        super(msg);
    }
}
