package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 10/06/17.
 */
public class UsuarioSemPermissaoException extends Exception {
    private final String usuario;

    public UsuarioSemPermissaoException(String usuario){
        super("Usuario "+usuario+" sem permissão para um conjunto de dados");
        this.usuario = usuario;
    }


    public String getUsuario() {
        return usuario;
    }
}
