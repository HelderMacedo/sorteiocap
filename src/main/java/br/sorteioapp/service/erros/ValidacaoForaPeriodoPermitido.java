package br.sorteioapp.service.erros;

import java.time.ZonedDateTime;

public class ValidacaoForaPeriodoPermitido extends Exception {

	public ValidacaoForaPeriodoPermitido(ZonedDateTime dataInicioVigencia, ZonedDateTime dataFimVigencia) {
		super(dataInicioVigencia + " - " + dataFimVigencia);
	}

}
