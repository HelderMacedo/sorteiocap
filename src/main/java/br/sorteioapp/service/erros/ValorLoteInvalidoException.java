package br.sorteioapp.service.erros;

/**
 * Created by rodrigo on 09/06/17.
 */
public class ValorLoteInvalidoException extends Exception {

    private final float valorApurado;
    private final float valorIndicado;

    public ValorLoteInvalidoException(float valorApurado, float valorIndicado){
        super(geraMsg(valorApurado, valorIndicado));
        this.valorApurado = valorApurado;
        this.valorIndicado = valorIndicado;
    }

    private static String geraMsg(float valorApurado, float valorIndicado) {
        return "Valor do lote ("+valorApurado+") diferente do indicado ("+valorIndicado+").";
    }

    public float getValorApurado() {
        return valorApurado;
    }

    public float getValorIndicado() {
        return valorIndicado;
    }
}
