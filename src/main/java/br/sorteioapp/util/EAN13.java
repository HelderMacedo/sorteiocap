package br.sorteioapp.util;

/**
 * Created by rodrigo on 22/04/17.
 */
public class EAN13 {

    public static int calcDigitoVerificador(String referencia){
        if (referencia == null || referencia.length() != 12)
            throw new IllegalArgumentException("Não possível calcular o digito verificador do código '"+referencia+"'.");

        int soma=0;
        for (int a =0; a < referencia.length(); a+=2)
            soma += (referencia.charAt(a)-'0');
        for (int a =1; a < referencia.length(); a+=2)
            soma += (referencia.charAt(a)-'0')*3;

        int diferencaParaProxMultiplo10 = 0;
        if (soma%10 != 0)
            diferencaParaProxMultiplo10  = 10 - (soma%10);
        return diferencaParaProxMultiplo10;
    }
}
