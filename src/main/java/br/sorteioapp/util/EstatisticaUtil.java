package br.sorteioapp.util;

/**
 * Created by rodrigo on 30/05/17.
 */
public class EstatisticaUtil {

    public static double calcMedia(int[] numeros) {
        float media = 0;
        for (int b = 0; b < numeros.length; b++) {
            media += numeros[b];
        }
        return media / numeros.length;
    }

    public static double calcDesvio(int[] numeros, double media) {
        double desvio = 0;
        for (int b = 0; b < numeros.length; b++) {
            desvio += Math.pow(numeros[b] - media, 2);
        }
        return Math.sqrt(desvio / numeros.length);
    }
}
