package br.sorteioapp.util;

import java.util.Arrays;

public class NaiveXSSFilter {

	private static String[] SQLkeyWords = { ";", "\"", "\'", "/*", "*/", "|", "--", "exec", "select", "update",
			"delete", "insert", "alter", "drop", "create", "shutdown" };

	private static String[] HTMLkeyWords = { "&gt;", "&lt;", "&quot;", "&amp;", "<script>", "</script>", "script" };

	private static String[] TrashkeyWords = { "\n", "\t", "  ", "   ", "    " };

	private static String[] unsafeKeywords = mergeArrays(mergeArrays(SQLkeyWords, HTMLkeyWords), TrashkeyWords);

	static String[] mergeArrays(String[] a, String[] b) {
		int alen = a.length;
		int blen = b.length;
		String[] c = new String[alen + blen];

		System.arraycopy(a, 0, c, 0, alen);
		System.arraycopy(b, 0, c, alen, blen);
		return c;
	}

	public static boolean isUnsafe(String value) {
		String lowerCase = value.toLowerCase();
		for (int i = 0; i < unsafeKeywords.length; i++) {
			if (lowerCase.indexOf(unsafeKeywords[i]) >= 0) {
				return true;
			}
		}
		return false;
	}

	public static String getSafeValue(String oldValue) {
		if (oldValue == null || oldValue.isEmpty()) {
			return oldValue;
		}
		StringBuilder sb = new StringBuilder(oldValue);
		String lowerCase = oldValue.toLowerCase();
		for (int i = 0; i < unsafeKeywords.length; i++) {
			int x = -1;
			while ((x = lowerCase.indexOf(unsafeKeywords[i])) >= 0) {
				if (unsafeKeywords[i].length() == 1) {
					sb.replace(x, x + 1, " ");
					lowerCase = sb.toString().toLowerCase();
					continue;
				}
				sb.replace(x, x + unsafeKeywords[i].length(), " ");
				lowerCase = sb.toString().toLowerCase();
			}
		}
		return sb.toString().trim();
	}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(unsafeKeywords));
		System.out.println(getSafeValue("<script > </script> "));
		System.out.println(getSafeValue("; -- drop"));
		System.out.println(getSafeValue("' and 1=1"));
	}

}
