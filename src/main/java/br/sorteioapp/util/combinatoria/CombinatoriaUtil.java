package br.sorteioapp.util.combinatoria;

import java.math.BigInteger;

/**
 * Parte dos métodos baseia-se em Combinadics:
 * https://en.wikipedia.org/wiki/Combinatorial_number_system
 * 
 * @author Rodrigo
 */
public class CombinatoriaUtil {
	private static BigInteger[][] memCombinacoes = null;

	private static BigInteger fatRange(int a, int b) throws ArithmeticException {
		if (a == 0 || b == 0 || a > b)
			return BigInteger.ZERO;
		BigInteger prd = BigInteger.ONE;
		for (int x = a; x <= b; ++x) {
			prd = prd.multiply(BigInteger.valueOf(x));
		}
		return prd;
	}

	public static BigInteger totalCombinacoes(int totalNumeros, int qtdCombinacao) throws ArithmeticException {
		if (memCombinacoes == null) {
			memCombinacoes = new BigInteger[200][200];
		} else if (totalNumeros < 200 && qtdCombinacao < 200 && memCombinacoes[totalNumeros][qtdCombinacao] != null) {
			return memCombinacoes[totalNumeros][qtdCombinacao];
		}

		int a = totalNumeros - qtdCombinacao, b = qtdCombinacao;
		if (totalNumeros == 0 || qtdCombinacao == 0 || totalNumeros < qtdCombinacao)
			return BigInteger.ZERO;
		if (totalNumeros == qtdCombinacao)
			return BigInteger.ONE;
		// if (b > a) {
		// int c = a;
		// a = b;
		// b = c;
		// }
		BigInteger x = fatRange(a + 1, totalNumeros);
		BigInteger y = fatRange(1, b);
		// System.out.println("("+(a+1)+","+totalNumeros+")="+x+" (1,"+b+")="+y);
		BigInteger ret = x.divide(y);
		if (totalNumeros < 200 && qtdCombinacao < 200) {
			memCombinacoes[totalNumeros][qtdCombinacao] = ret;
		}
		return ret;
	}

	public static int[] bitSetParaCombinacao(BigInteger bitset) {
		int j = 0, numeroBits = bitset.bitCount(), totalBits = bitset.bitLength();
		int[] ret = new int[numeroBits];
		int x = 0;
		for (; j < numeroBits && x < totalBits; ++x) {
			if (bitset.testBit(x))
				ret[j++] = x + 1;
		}
		return ret;
	}

	public static BigInteger mergeBitsets(BigInteger bita, BigInteger bitb, int totalNumber, int numberSelectNumbers) {
		BigInteger result = bita.add(BigInteger.ZERO);
		BigInteger draftb = bitb.add(BigInteger.ZERO);
		BigInteger mask = BigInteger.ZERO;
		mask = mask.setBit(totalNumber).subtract(BigInteger.ONE);
		BigInteger inva = bita.not().and(mask);

		// limite utilizando numberSelectNumbers para evitar loop infinitos
		int lastp = 0;
		for (int j = 0; j < numberSelectNumbers; j++) {
			int p = draftb.getLowestSetBit();
			if (p < 0)
				break;
			int target = 0;
			for (int k = lastp; k <= p; k++) {
				target = inva.getLowestSetBit();
				inva = inva.clearBit(target);
			}
			result = result.setBit(target);
			draftb = draftb.clearBit(p);
			lastp = p + 1;
		}
		return result;
	}
	/*
	 * public static long ordemCombinacao(int[] comb, int setsize){ long sum = 0;
	 * for (int i=0; i <comb.length; i++){ long t = totalCombinacoes(comb[i],
	 * comb.length - i); sum += t; } return sum; }
	 */

	public static BigInteger rankParaBitSet(BigInteger rank, int numerosPorBilhete) {
		int i = 0, x = 0, lastX = 0;
		BigInteger bitset = BigInteger.ZERO, lastTotal = BigInteger.ZERO;
		while (i < numerosPorBilhete) {
			BigInteger total = totalCombinacoes(x, numerosPorBilhete - i);
			if (total.compareTo(rank) > 0) {
				bitset = bitset.setBit(lastX);
				i++;
				rank = rank.subtract(lastTotal);
				x = 0;
				total = BigInteger.ZERO;
			}
			lastX = x++;
			lastTotal = total;
		}
		return bitset;
	}

	public static long rankParaLongBitSet(BigInteger rank, int numerosPorBilhete) {
		int i = 0, x = 0;
		BigInteger lastTotal = BigInteger.ZERO;
		long bitset = 0, lastMask = 1, mask = 1; 
		while (i < numerosPorBilhete) {
			BigInteger total = totalCombinacoes(x, numerosPorBilhete - i);
			if (total.compareTo(rank) > 0) {
				bitset += lastMask;
				i++;
				rank = rank.subtract(lastTotal);
				x = 0;
				mask = 1;
				total = BigInteger.ZERO;
			}
			x++;
			lastMask = mask;
			mask *= 2;
			lastTotal = total;
		}
		return bitset;
	}
	public static void main(String[] args) {
		int num = 15;
		BigInteger bita = BigInteger.valueOf(0);
		for (int i = 1; i <= 6; i++) {
			bita = bita.setBit(i * 10 - 1);
		}
		System.out.println(bita.bitCount());
		System.out.println(bita.toString(2));
		BigInteger bitb = BigInteger.valueOf(0);
		for (int i = 0; i < 9; i++) {
			bitb = bitb.setBit(i * 2);
		}
		System.out.println(bitb.bitCount());
		System.out.println(bitb.toString(2));
		BigInteger finalbit = CombinatoriaUtil.mergeBitsets(bita, bitb, 60, 10);
		System.out.println(finalbit.bitCount());
		System.out.println(finalbit.toString(2));
		/*
		 * System.out.println(tot); BigInteger target =
		 * CombinatoriaUtil.totalCombinacoes(59, 15); BigInteger bitSet0 =
		 * CombinatoriaUtil.rankParaBitSet(target, num); String dezenas0 =
		 * Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(bitSet0));
		 * System.out.println(dezenas0); System.out.println(tot.divide(target));
		 * System.out.println("..."); for (int i = 0; i < 100; i++) { BigInteger bitSet
		 * = CombinatoriaUtil.rankParaBitSet(tot.subtract(BigInteger.valueOf(100 - i)),
		 * num); String dezenas =
		 * Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(bitSet));
		 * System.out.println(dezenas); }
		 */
	}
}
