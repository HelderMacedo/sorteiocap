package br.sorteioapp.util.criptografia;
/*
 * New BSD License (BSD)
 * Copyright (c) 2014,2015, Rob Shepherd
 *
 * All rights reserved.
 *
 * Parts derived from source code Copyright (c) 2012, Caio Yuri da Silva Costa
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and
 *    the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NavigableSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import br.sorteioapp.util.combinatoria.CombinatoriaUtil;

/**
 * Software derived from a New-BSD licensed implementation for .NET
 * http://dotfpe.codeplex.com ... That in turn was ported from the Botan library
 * http://botan.randombit.net/fpe.html. ... Using the scheme FE1 from the paper
 * "Format-Preserving Encryption" by Bellare, Rogaway, et al.
 * (http://eprint.iacr.org/2009/251)
 *
 * @author Rob Shepherd
 *
 */
public class FPE {
	public static class CombOrder implements Comparable<CombOrder> {
		BigInteger combinadics;
		BigInteger bitset;
		long longbitSet;
		int nearDistance = 0;
		CombOrder nearest = null;

		public CombOrder(BigInteger c, BigInteger b) {
			this.combinadics = c;
			this.bitset = b;
		}

		@Override
		public int compareTo(CombOrder o) {
			return combinadics.compareTo(o.combinadics);
		}
	}

	public static Set<CombOrder> generateNaive(int total) throws Exception {
		Random rand = new Random();
		Set<CombOrder> todosBilhetes = new HashSet();
		while (todosBilhetes.size() < total) {
			BigInteger bigSet = BigInteger.ZERO;
			while (bigSet.bitCount() < 15) {
				int z = rand.nextInt(60);
				if (!bigSet.testBit(z)) {
					bigSet = bigSet.setBit(z);
				}
			}
			todosBilhetes.add(new CombOrder(bigSet, bigSet));
		}
		return todosBilhetes;
	}

	public static Set<CombOrder> generateFPE(int total) throws Exception {
		String chave = "RXfHPoqnLVuyx6Um0PI9fNAkvffuvejLtNTaYPx3rqE="; // GeradorChave.geraChaveBase64();
		BigInteger combinacoes = CombinatoriaUtil.totalCombinacoes(60, 15);
		FPE.MultiEncryptorX enc = new FPE.MultiEncryptorX(combinacoes, chave);

		Set<CombOrder> todosBilhetes = new HashSet();
		int x = 0;
		while (x < total) { // Random rand = new Random();
			BigInteger biga = BigInteger.valueOf(x++);
			BigInteger combinadics1 = enc.encrypt(biga);
			long longbitSet = CombinatoriaUtil.rankParaLongBitSet(combinadics1, 15);
			BigInteger bitsetf = BigInteger.valueOf(longbitSet);
			CombOrder co = new CombOrder(combinadics1, bitsetf);
			co.longbitSet = longbitSet;
			todosBilhetes.add(co);
		}
		return todosBilhetes;
	}

	public static void main(String[] args) throws Exception {
		int total = 150000;
		int target = 70000;
		List<BigInteger[]> lst = new ArrayList();
		List<CombOrder> todosBilhetes = new ArrayList();
		List<CombOrder> validadosBilhetes = new ArrayList();
		int numerosTotais = 60;
		Random rand = new Random();
		int z = 0;
		long t0 = System.currentTimeMillis();
		todosBilhetes.addAll(generateFPE(total));
		long t1 = System.currentTimeMillis();
		System.out.println("## Gerados " + total + " bilhetes (" + (t1 - t0) + ")");
		// validadosBilhetes.addAll(todosBilhetes);
		int j = 0;
		t0 = System.currentTimeMillis();
		
		long[] simseq = new long[numerosTotais - 4];
		long mask = 0b1111l;
		for (int i=0; i < simseq.length; i++) {
			simseq[i] = mask;
			mask = mask << 1;
		}
		for (; validadosBilhetes.size() < target && j < total; j++) {
			CombOrder co = todosBilhetes.get(j);
			boolean ok = true;
			for (int k=0; ok && k < simseq.length; k++) {
				ok = ((simseq[k] & co.longbitSet) != simseq[k]);
			}
			for (int i = 0; ok & i < validadosBilhetes.size(); i++) {
				CombOrder co2 = validadosBilhetes.get(i);
				long andnumber = co.longbitSet & co2.longbitSet;
				int distance = Long.bitCount(andnumber);				
				ok = distance <= 10;
			}
			if (ok)
				validadosBilhetes.add(co);
		}
		t1 = System.currentTimeMillis();
		System.out.println(" Filtrados " + validadosBilhetes.size() + " utilizados " + j + " (" + (t1 - t0) + ")");
		
		
		List validadosBilhetes2 = new ArrayList();
		validadosBilhetes2.addAll(	validadosBilhetes.subList(0, 5000));
		validadosBilhetes2.addAll(	validadosBilhetes.subList(10000, 15000));
		validadosBilhetes2.addAll(	validadosBilhetes.subList(20000, 25000));
		validadosBilhetes2.addAll(	validadosBilhetes.subList(30000, 35000));		
		validadosBilhetes = validadosBilhetes2;
		// todosBilhetes.sort(new Comparator<CombOrder>() {
		// public int compare(CombOrder o1, CombOrder o2) {
		// return o1.nearDistance - o2.nearDistance;
		// }
		// });

		// for (int j = 0; validadosBilhetes.size() < total * 0.2; j++) {
		// validadosBilhetes.add(todosBilhetes.get(j));
		// }

		// for (CombOrder co : todosBilhetes) {
		// if (rand.nextFloat() <= 0.2)
		// validadosBilhetes.add(co);
		// }
		/*
		 * ExponentialDistribution logNormal = new ExponentialDistribution(3); while
		 * (validadosBilhetes.size() < 0.2 * total) { int j = Math.min(total - 1, (int)
		 * (logNormal.sample() * total / 5.0)); System.out.println("#valdaods : " +
		 * logNormal.sample() + " " + j + " " + validadosBilhetes.size());
		 * 
		 * validadosBilhetes.add(todosBilhetes.get(j)); }
		 */
		System.out.println("## Bilhetes validados " + validadosBilhetes.size());

		for (int s = 1; s <= 5; s++) {
			Set<CombOrder> sorteados = new HashSet();
			List<Integer> bolasint = new ArrayList();
			BigInteger bolasBitSet = BigInteger.ZERO;
			while (sorteados.isEmpty()) {
				int bola = rand.nextInt(60);
				bolasint.add(bola + 1);
				bolasBitSet = bolasBitSet.setBit(bola);
				for (CombOrder co : validadosBilhetes) {
					if (co.bitset.and(bolasBitSet).bitCount() == 15) {
						sorteados.add(co);
					}
				}
			}
			System.out.println("# " + s + " Bolas sorteadas [" + bolasBitSet.bitCount() + "] :"
					+ Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(bolasBitSet)));

			for (CombOrder co : sorteados) {
				System.out.println("Bolas == " + Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(co.bitset)));
			}
		}
	}

	public static void main2(String[] args) throws Exception {

		String chave = "RXfHPoqnLVuyx6Um0PI9fNAkvffuvejLtNTaYPx3rqE="; // GeradorChave.geraChaveBase64();
		String chave2 = "ot9j6h8xh82TJurscdBE8Q==";
		BigInteger combinacoes1 = CombinatoriaUtil.totalCombinacoes(80, 5);
		FPE.MultiEncryptorX enc = new FPE.MultiEncryptorX(combinacoes1, chave);

		// teste decript
		PrintWriter pw = new PrintWriter(new File("combinadics-test1.csv"));
		PrintWriter pw2 = new PrintWriter(new File("combinadics-test2.csv"));
		System.out.println(combinacoes1);
		pw.println("num,combs,maxcom");
		int total = 1000000;
		List<BigInteger[]> lst = new ArrayList();
		TreeSet<CombOrder> tree = new TreeSet();

		int x = 0;
		int numberSkips = 0;
		while (lst.size() < total) { // Random rand = new Random();
			BigInteger biga = BigInteger.valueOf(x++); // rand.nextInt(1915383762));
			BigInteger combinadics1 = enc.encrypt(biga);

			BigInteger bitsetf = CombinatoriaUtil.rankParaBitSet(combinadics1, 5);
			boolean skip = false;
			CombOrder co = new CombOrder(combinadics1, bitsetf);
			tree.add(co);
			NavigableSet<CombOrder> ns = tree.tailSet(co, false);
			int k = 0;
			int t = 10;
			for (CombOrder ctmp : ns) {
				if (skip || k > 15)
					break;
				else if (k < 5)
					t = 3;
				else if (k >= 10)
					t = 4;
				skip = (ctmp.bitset.and(bitsetf).bitCount() >= t);
				k++;
			}
			k = 0;
			NavigableSet<CombOrder> hs = tree.headSet(co, false);
			for (CombOrder ctmp : hs) {
				if (skip || k > 15)
					break;
				else if (k < 5)
					t = 3;
				else if (k >= 10)
					t = 4;
				skip = (ctmp.bitset.and(bitsetf).bitCount() >= t);
				k++;
			}
			if (!skip) {
				BigInteger[] obj = new BigInteger[5];
				obj[1] = bitsetf;
				obj[0] = combinadics1;
				lst.add(obj);
			} else {
				if (numberSkips % 10000 == 0)
					System.out.println("SKIP " + numberSkips);
				numberSkips++;
				tree.remove(co);
			}
		}
		System.out.println("TOTAL " + total + " / " + numberSkips);
		long[] qtnumeros = new long[60];
		pw2.println("num,qtd");
		for (int a = 0; a < total; a++) {
			long max = 0;
			long countcount = 0;
			BigInteger comb = lst.get(a)[0];
			BigInteger bita = lst.get(a)[1];
			int[] sem = new int[60];
			for (int b = 0; b < total; b++) {
				BigInteger bitb = lst.get(b)[1];
				int count = bita.and(bitb).bitCount();
				countcount += count;
				if (a != b) {
					sem[count] += 1;
					if (count > max) {
						max = count;
						// if (count >= 8) {
						// System.out.println(
						// comb + " ~ " + Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(bitb)));
						// }
					}
				}
			}
			/*
			 * if (max >= 8) { // System.out.println(comb + " = " + //
			 * Arrays.toString(CombinatoriaUtil.bitSetParaCombinacao(bita)));
			 * System.out.println("---------------------------------"); for (int i = 10; i <
			 * sem.length; i++) { if (sem[i] > 0) System.out.println("# " + i + " = " +
			 * sem[i]); } }
			 */
			lst.get(a)[2] = BigInteger.valueOf(max);
			pw.println(a + "," + comb + "," + max);// + "," + (countcount / (total - 1)));
			for (int j = 0; j < 60; j++) {
				qtnumeros[j] += bita.testBit(j) ? 1 : 0;
			}
		}
		pw.close();

		for (int j = 0; j < 60; j++) {
			pw2.println(j + 1 + "," + qtnumeros[j]);
		}
		pw2.close();
		System.out.println("FIM");

		/*
		 * final byte[] key = "Here is my secret key".getBytes(); final byte[] tweak =
		 * "Here is my tweak1".getBytes();
		 * 
		 * 
		 * final int range = 10000; final BigInteger modulus =
		 * BigInteger.valueOf(range);
		 * 
		 * Set<BigInteger> results = new HashSet<BigInteger>();
		 * 
		 * for(int i=0;i<range;i++){ BigInteger enc = encrypt(modulus,
		 * BigInteger.valueOf(i), key, tweak); BigInteger dec = decrypt(modulus, enc,
		 * key, tweak); System.out.println(i + ": " + enc + " " + dec); if(
		 * dec.longValue() != i ){ throw new IllegalStateException("enc (" + enc +
		 * ") != i(" + i + ")"); } results.add(enc); if(results.size() != i+1){ throw
		 * new IllegalStateException("duplicate enc: " + enc); } if(enc.longValue() < 0
		 * || enc.longValue() > range){ throw new IllegalStateException("enc " + enc +
		 * " out of range " + range); } }
		 */
	}

	// Normally FPE is for SSNs, CC#s, etc, nothing too big
	private static final int MAX_N_BYTES = 128 / 8;

	/// <summary>
	/// Generic Z_n FPE decryption, FD1 scheme
	/// </summary>
	/// <param name="modulus">Use to determine the range of the numbers. Example, if
	/// the
	/// numbers range from 0 to 999, use "1000" here.</param>
	/// <param name="ciphertext">The number to decrypt.</param>
	/// <param name="key">Secret key</param>
	/// <param name="tweak">Non-secret parameter, think of it as an IV - use the
	/// same one used to encrypt</param>
	/// <returns>The decrypted number</returns>
	public static BigInteger decrypt(BigInteger modulus, BigInteger ciphertext, byte[] key, byte[] tweak)
			throws Exception {
		FPE_Encryptor F = new FPE_Encryptor(key, modulus, tweak);

		BigInteger[] a_b = NumberTheory.factor(modulus);

		BigInteger a = a_b[0];
		BigInteger b = a_b[1];

		int r = rounds(a, b);

		BigInteger X = ciphertext;

		for (int i = 0; i != r; ++i) {
			BigInteger W = X.mod(a);
			BigInteger R = X.divide(a);

			BigInteger bigInteger = (W.subtract(F.F(r - i - 1, R)));

			BigInteger L = bigInteger.mod(a);
			X = b.multiply(L).add(R);
		}

		return X;
	}

	// Alterado para melhorar a distribuição de probabilidade sobre o dominio
	// (numero modulo)
	//
	public static class MultiEncryptorX {
		EncryptorX[] encs;

		public MultiEncryptorX(BigInteger modulus, String strkey) throws Exception {
			this(modulus, strkey, 1);
		}

		public MultiEncryptorX(BigInteger modulus, String strkey, int count) throws Exception {
			this.encs = new EncryptorX[count];
			for (int i = 0; i < count; i++) {
				encs[i] = new EncryptorX(modulus, (strkey + i).getBytes());
			}
		}

		public BigInteger encrypt(BigInteger plaintext) throws Exception {
			BigInteger comb = plaintext;
			for (int i = 0; i < encs.length; i++) {
				comb = encs[i].encrypt(comb);
			}
			return comb;
		}
	}

	public static class EncryptorX {
		FPE_Encryptor F;
		BigInteger a;
		BigInteger b;
		int r;
		BigInteger memF[][];
		BigInteger modulus;

		/// <param name="modulus">Use to determine the range of the numbers. Example, if
		/// the
		/// numbers range from 0 to 999, use "1000" here.</param>
		public EncryptorX(BigInteger modulus, byte[] key) throws Exception {
			this.F = new FPE_Encryptor(key, modulus, new byte[0]);
            this.modulus = modulus;
			BigInteger[] a_b = NumberTheory.factor(modulus);

			this.a = a_b[0];
			this.b = a_b[1];
			System.out.println("A = " + a + "; B = " + b);
			this.r = FPE.rounds(a, b);
			// Adaptação sobre o algoritmo original, para guardar os hash utilizados
			// visto que b em geral é um valor bem pequeno.
			int bint = b.intValue();
			memF = new BigInteger[r][bint];
			for (int i = 0; i != r; ++i) {
				for (int j = 0; j != bint; ++j) {
					memF[i][j] = F.F(i, BigInteger.valueOf(j));
				}
			}
		}

		public BigInteger getModulus(){
		    return modulus;
        }

		public BigInteger encrypt(BigInteger plaintext) throws Exception {
			BigInteger X = plaintext;

			for (int i = 0; i != r; ++i) {
				BigInteger L = X.divide(b);
				BigInteger R = X.mod(b);
				// BigInteger W = (L.add(F.F(i, R))).mod(a);
				int rint = R.intValue();
				BigInteger W = (L.add(memF[i][rint])).mod(a);
				X = a.multiply(R).add(W);
			}

			return X;
		}
	}

	/// <summary>
	/// Generic Z_n FPE encryption, FE1 scheme
	/// </summary>
	/// <param name="modulus">Use to determine the range of the numbers. Example, if
	/// the
	/// numbers range from 0 to 999, use "1000" here.</param>
	/// <param name="plaintext">The number to encrypt.</param>
	/// <param name="key">Secret key</param>
	/// <param name="tweak">Non-secret parameter, think of it as an IV</param>
	/// <returns>The encrypted number.</returns>
	public static BigInteger encrypt(BigInteger modulus, BigInteger plaintext, byte[] key, byte[] tweak)
			throws Exception {
		FPE_Encryptor F = new FPE_Encryptor(key, modulus, tweak);

		BigInteger[] a_b = NumberTheory.factor(modulus);

		BigInteger a = a_b[0];
		BigInteger b = a_b[1];
		int r = rounds(a, b);

		BigInteger X = plaintext;

		for (int i = 0; i != r; ++i) {
			BigInteger L = X.divide(b);
			BigInteger R = X.mod(b);

			BigInteger W = (L.add(F.F(i, R))).mod(a);
			X = a.multiply(R).add(W);
		}

		return X;
	}

	/// <summary>
	/// According to a paper by Rogaway, Bellare, etc, the min safe number
	/// of rounds to use for FPE is 2+log_a(b). If a >= b then log_a(b) &lt;= 1
	/// so 3 rounds is safe. The FPE factorization routine should always
	/// return a >= b, so just confirm that and return 3.
	/// </summary>
	/// <param name="a"></param>
	/// <param name="b"></param>
	/// <returns></returns>
	private static int rounds(BigInteger a, BigInteger b) throws Exception {
		if (a.compareTo(b) < 0)
			throw new Exception("FPE rounds: a < b");
		// [RODRIGO] Após alguns testes, a distribuição começou a melhor por volta de
		// rounds 9
		return 12;
	}

	/// <summary>
	/// A simple round function based on HMAC(SHA-256)
	/// </summary>
	private static class FPE_Encryptor {
		private Mac mac;

		private byte[] mac_n_t;

		public FPE_Encryptor(byte[] key, BigInteger n, byte[] tweak) throws Exception {
			mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(key, "HmacSHA256");
			mac.init(secret_key);

			byte[] n_bin = n.toByteArray();

			if (n_bin.length > MAX_N_BYTES)
				throw new Exception("N is too large for FPE encryption");

			ByteArrayOutputStream ms = new ByteArrayOutputStream();

			ms.write(n_bin.length);
			ms.write(n_bin);

			ms.write(tweak.length);
			ms.write(tweak);

			mac.reset();
			mac_n_t = mac.doFinal(ms.toByteArray());
		}

		public BigInteger F(int round_no, BigInteger R) throws IOException {
			byte[] r_bin = R.toByteArray();
			ByteArrayOutputStream ms = new ByteArrayOutputStream();
			ms.write(mac_n_t);
			ms.write(round_no);

			ms.write(r_bin.length);
			ms.write(r_bin);

			mac.reset();
			byte[] X = mac.doFinal(ms.toByteArray());

			byte[] X_ = new byte[X.length + 1];
			X_[0] = 0; // set the first byte to 0

			for (int i = 0; i < X.length; i++) {
				X_[i + 1] = X[i];
			}

			BigInteger ret = new BigInteger(X_);
			return ret;
		}
	}

	private static class NumberTheory {
		private static final BigInteger MAX_PRIME = BigInteger.valueOf(65535);

		/// <summary>
		/// Factor n into a and b which are as close together as possible.
		/// Assumes n is composed mostly of small factors which is the case for
		/// typical uses of FPE (typically, n is a power of 10)
		///
		/// Want a >= b since the safe number of rounds is 2+log_a(b); if a >= b
		/// then this is always 3
		/// </summary>
		/// <param name="n"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public static BigInteger[] factor(BigInteger n) throws Exception {
			BigInteger a = BigInteger.ONE;
			BigInteger b = BigInteger.ONE;

			int n_low_zero = low_zero_bits(n);

			a = a.shiftLeft(n_low_zero / 2);
			b = b.shiftLeft(n_low_zero - (n_low_zero / 2));

			n = n.shiftRight(n_low_zero);

			// for (int i = 0; i != PRIMES.length; ++i)
			BigInteger prime = BigInteger.ONE;
			while (prime.compareTo(MAX_PRIME) <= 0) {
				prime = prime.nextProbablePrime();
				while (n.mod(prime).compareTo(BigInteger.ZERO) == 0) {
					a = a.multiply(prime);
					if (a.compareTo(b) > 0) {
						BigInteger old_b = b;
						b = a;
						a = old_b;
					}
					n = n.divide(prime);
				}
				if (a.compareTo(BigInteger.ONE) > 0 && b.compareTo(BigInteger.ONE) > 0) {
					break;
				}
			}

			if (a.compareTo(b) > 0) {
				BigInteger old_b = b;
				b = a;
				a = old_b;
			}
			a = a.multiply(n);
			if (a.compareTo(b) < 0) {
				BigInteger old_b = b;
				b = a;
				a = old_b;
			}

			if (a.compareTo(BigInteger.ONE) < 0 || b.compareTo(BigInteger.ONE) < 0) {
				throw new Exception("Could not factor n for use in FPE");
			}

			// return
			return new BigInteger[] { a, b };
		}

		/// <summary>
		/// Counts the trailing zeroes of a byte
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		private static int ctz(byte n) {
			for (int i = 0; i != 8; ++i) {
				if (((n >> i) & 0x01) > 0) {
					return i;
				}
			}
			return 8;
		}

		/// <summary>
		/// Return the number of 0 bits at the end of n
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		private static int low_zero_bits(BigInteger n) {
			int low_zero = 0;

			if (n.signum() > 0) {
				byte[] bytes = n.toByteArray();

				for (int i = bytes.length - 1; i >= 0; i--) {
					int x = (bytes[i] & 0xFF);

					if (x > 0) {
						low_zero += ctz((byte) x);
						break;
					} else
						low_zero += 8;
				}
			}

			return low_zero;
		}
	}

}
