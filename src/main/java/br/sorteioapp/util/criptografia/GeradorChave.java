package br.sorteioapp.util.criptografia;


    import java.security.NoSuchAlgorithmException;
    import java.util.Base64;
    import java.util.logging.Level;
    import java.util.logging.Logger;
    import javax.crypto.KeyGenerator;
    import javax.crypto.SecretKey;

/**
 *
 * @author Rodrigo
 */
public class GeradorChave {

    public static byte[] geraChave() {
        KeyGenerator keyGen;
        try {
            keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(256);

            SecretKey key = keyGen.generateKey();
            return key.getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(GeradorChave.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new byte[0];
    }

    public static String geraChaveBase64() {
        return Base64.getEncoder().encodeToString(geraChave());
    }
}
