package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.querydsl.core.types.Predicate;

import br.sorteioapp.domain.AreaRegional;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.service.AreaRegionalService;
import br.sorteioapp.service.dto.AreaRegionalDTO;
import br.sorteioapp.service.dto.ThinAreaRegionalDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing AreaRegional.
 */
@RestController
@RequestMapping("/api")
public class AreaRegionalResource {

	private final Logger log = LoggerFactory.getLogger(AreaRegionalResource.class);

	private static final String ENTITY_NAME = "areaRegional";

	private final AreaRegionalService areaRegionalService;

	public AreaRegionalResource(AreaRegionalService areaRegionalService) {
		this.areaRegionalService = areaRegionalService;
	}

	/**
	 * POST /arearegional : Create a new AreaRegional.
	 *
	 * @param AreaRegionaldto
	 *            the AreaRegional to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         AreaRegional, or with status 400 (Bad Request) if the
	 *         AreaRegional has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/arearegional")
	@Timed
	public ResponseEntity<AreaRegionalDTO> createAreaRegional(
			@Valid @RequestBody AreaRegionalDTO AreaRegionaldto) throws URISyntaxException {
		log.debug("REST request to save AreaRegional : {}", AreaRegionaldto);
		if (AreaRegionaldto.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new AreaRegional cannot already have an ID")).body(null);
		}
		try {
			AreaRegionalDTO result = new AreaRegionalDTO(areaRegionalService.save(AreaRegionaldto));
			return ResponseEntity.created(new URI("/api/arearegional/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(AreaRegionaldto.getLogin(),
					"errocriarusuario", "Erro ao criar usuario do AreaRegional")).body(null);
		}
	}

	/**
	 * PUT /arearegional : Updates an existing AreaRegional.
	 *
	 * @param AreaRegionaldto
	 *            the AreaRegional to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         AreaRegional, or with status 400 (Bad Request) if the
	 *         AreaRegional is not valid, or with status 500 (Internal Server
	 *         Error) if the AreaRegional couldnt be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/arearegional")
	@Timed
	public ResponseEntity<AreaRegionalDTO> updateAreaRegional(
			@Valid @RequestBody AreaRegionalDTO AreaRegionaldto) throws URISyntaxException {
		log.debug("REST request to update AreaRegional : {}", AreaRegionaldto);
		try {
			if (AreaRegionaldto.getId() == null) {
				return createAreaRegional(AreaRegionaldto);
			}
			AreaRegionalDTO result = new AreaRegionalDTO(areaRegionalService.save(AreaRegionaldto));
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, AreaRegionaldto.getId().toString()))
					.body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(AreaRegionaldto.getLogin(),
					"errocriarusuario", "Erro ao criar usuario do AreaRegional")).body(null);
		}
	}

	/**
	 * GET /arearegional : get all the AreaRegionals.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         AreaRegionals in body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/arearegional")
	@Timed
	public ResponseEntity<List<ThinAreaRegionalDTO>> getAllAreaRegionals(
			@QuerydslPredicate(root = AreaRegional.class) Predicate predicate, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of AreaRegionals");
		Page<ThinAreaRegionalDTO> page = areaRegionalService.findAll(predicate, pageable)
				.map(ThinAreaRegionalDTO::new);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/arearegional");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /arearegional/:id : get the "id" AreaRegional.
	 *
	 * @param id
	 *            the id of the AreaRegional to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         AreaRegional, or with status 404 (Not Found)
	 */
	@GetMapping("/arearegional/{id}")
	@Timed
	public ResponseEntity<AreaRegionalDTO> getAreaRegional(@PathVariable Long id) {
		log.debug("REST request to get AreaRegional : {}", id);
		AreaRegionalDTO AreaRegional = new AreaRegionalDTO(areaRegionalService.findOne(id));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(AreaRegional));
	}

	/**
	 * DELETE /arearegional/:id : delete the "id" AreaRegional.
	 *
	 * @return the ResponseEntity with status 200 (OK)
	 * @throws IllegalAccessException 
	 */
	
	  @DeleteMapping("/arearegional/{id}")
	@Timed
	public ResponseEntity<Void> deleteAreaRegional(@PathVariable Long id) throws IllegalAccessException {
		log.debug("REST request to delete AreaRegional : {}", id);

		try {
			areaRegionalService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();

		} catch (Exception e) {
			throw new IllegalAccessException("errodelecao");			
		}
	}

	@GetMapping("/arearegional/regional")
	@Timed
	public ResponseEntity<List<ThinAreaRegionalDTO>> getAreaRegionals(@ApiParam Pageable pageable) {
		return getAllAreaRegionals(null, pageable);
	}

	@GetMapping("/arearegional/regional/{regionalId}")
	@Timed
	public ResponseEntity<List<AreaRegionalDTO>> getAreaRegionalsPorRegional(@PathVariable Long regionalId) {
		log.debug("REST request to get a page of Concursos");
		List<AreaRegional> lst = areaRegionalService.findAll(new Regional(regionalId));
		return new ResponseEntity(lst, null, HttpStatus.OK);
	}

	@GetMapping("/arearegional/ativos/regional/{regionalId}")
	@Timed
	public ResponseEntity<List<AreaRegionalDTO>> getTodosAreaRegionalsAtivosPorRegional(@PathVariable Long regionalId) {
		log.debug("REST request to get a page of Concursos");
		List<AreaRegional> lst = areaRegionalService.findAllActive(new Regional(regionalId));
		return new ResponseEntity(lst, null, HttpStatus.OK);
	}

	@GetMapping("/arearegional/porusuario/{usuarioId}")
	@Timed
	public ResponseEntity<List<AreaRegional>> getSubRegionalPorUsuario(@PathVariable Long usuarioId){
		log.debug("REST request to get a page of Concursos");
		List<AreaRegional> lst = areaRegionalService.findByUsuario(usuarioId);
		return new ResponseEntity(lst,null, HttpStatus.OK);
	}

	@GetMapping("/arearegional/ativos")
	@Timed
	public ResponseEntity<List<AreaRegional>> getSubRegionalAtivos(){
		List<AreaRegional> lst = areaRegionalService.findAllAtivos();
		return  new ResponseEntity(lst, null, HttpStatus.OK);
	}
	
}
