package br.sorteioapp.web.rest;
import com.codahale.metrics.annotation.Timed;

import java.util.List;
import java.util.Optional;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.Dezenas;
import br.sorteioapp.repository.ConcursoRepository;
import br.sorteioapp.service.BilheteService;
import io.github.jhipster.web.util.ResponseUtil;


@RestController
@RequestMapping("/api")
public class BilheteResource {
    private final BilheteService bilheteService;
    private final ConcursoRepository concursoRepository;

    public BilheteResource(BilheteService bilheteService, ConcursoRepository concursoRepository){
        this.bilheteService = bilheteService;
        this.concursoRepository = concursoRepository;
    }  
    
    @GetMapping("/bilhete/{concurso}/{numero}/{identificador}")
    @Timed
    public List<Dezenas> getBilhete(@PathVariable Integer concurso, @PathVariable Integer numero, @PathVariable String identificador){
        //Concurso conc = concursoRepository.getOne(concurso);
        List<Dezenas> dezenas = bilheteService.getConcursoAndNumero(concurso, numero, identificador);
        return dezenas;
    }
}