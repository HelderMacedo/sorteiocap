package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import br.sorteioapp.service.dto.LoteValidacaoStatusDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;

import br.sorteioapp.service.BilheteService;
import br.sorteioapp.service.ConcursoService;
import br.sorteioapp.service.dto.BilheteStatusDTO;
import br.sorteioapp.service.dto.ConcursoDTO;
import br.sorteioapp.service.dto.ThinConcursoDTO;
import br.sorteioapp.service.erros.BilheteJaValidadoException;
import br.sorteioapp.service.erros.FaixaNaoCorrespondeAUmLoteException;
import br.sorteioapp.service.erros.IdentificacaoBilheteInvalida;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import br.sorteioapp.service.erros.ValidacaoForaPeriodoPermitido;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Concurso.
 */
@RestController
@RequestMapping("/api")
public class ConcursoResource {

	private final Logger log = LoggerFactory.getLogger(ConcursoResource.class);

	private static final String ENTITY_NAME = "concurso";

	private final ConcursoService concursoService;
	private final BilheteService bilheteService;

	public ConcursoResource(ConcursoService concursoService, BilheteService bilheteService) {
		this.bilheteService = bilheteService;
		this.concursoService = concursoService;
	}

	/**
	 * POST /concursos : Create a new concurso.
	 *
	 * @param concurso
	 *            the concurso to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         concurso, or with status 400 (Bad Request) if the concurso has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/concursos")
	@Timed
	public ResponseEntity<ConcursoDTO> createConcurso(@RequestBody ConcursoDTO concurso) throws URISyntaxException {
		log.debug("REST request to save Concurso : {}", concurso);
		if (concurso.getId() != null && concurso.getId() > 0) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new concurso cannot already have an ID"))
					.body(null);
		}
		ConcursoDTO result = new ConcursoDTO(concursoService.insert(concurso));
		return ResponseEntity.created(new URI("/api/concursos/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

    @PutMapping("/concursos")
    @Timed
    public ResponseEntity<ConcursoDTO> updateConcurso(@RequestBody ConcursoDTO concurso) throws URISyntaxException {
        log.debug("REST request to save Concurso : {}", concurso);
        if (concurso.getId() == null || concurso.getId() <= 0) {
            return ResponseEntity.badRequest().headers(
                HeaderUtil.createFailureAlert(ENTITY_NAME, "idnotexists", "A existing concurso must have an ID"))
                .body(null);
        }
        ConcursoDTO result = new ConcursoDTO(concursoService.update(concurso));
        return ResponseEntity.created(new URI("/api/concursos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
    }

	/**
	 * GET /concursos : get all the concursos.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of concursos in
	 *         body
	 * @throws URISyntaxException
	 *             if there is an error to generate the pagination HTTP headers
	 */
	@GetMapping("/concursos")
	@Timed
	public ResponseEntity<List<ConcursoDTO>> getAllConcursos() {
		log.debug("REST request to get a page of Concursos");
		List<ConcursoDTO> lst = ConcursoDTO.map( concursoService.findAllQtd());
		return new ResponseEntity<>(lst, null, HttpStatus.OK);
	}

	@GetMapping("/concursos-short")
	@Timed
	public ResponseEntity<List<ThinConcursoDTO>> getAllThinConcursos(@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		List<ThinConcursoDTO> lst = ThinConcursoDTO.map(concursoService.findAll());
		return new ResponseEntity<>(lst, null, HttpStatus.OK);
	}

    @GetMapping("/concursos-naosinc")
    @Timed
    public ResponseEntity<List<ThinConcursoDTO>> getAllConcursosNaoSinc(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Concursos");
        List<ThinConcursoDTO> lst = ThinConcursoDTO.map(concursoService.findAllNaoSincronizado());
        return new ResponseEntity<>(lst, null, HttpStatus.OK);
    }

	/**
	 * GET /concursos/:id : get the "id" concurso.
	 *
	 * @param id
	 *            the id of the concurso to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the concurso,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/concursos/{id}")
	@Timed
	public ResponseEntity<ConcursoDTO> getConcurso(@PathVariable Integer id) {
		log.debug("REST request to get Concurso : {}", id);
		ConcursoDTO concurso = new ConcursoDTO(concursoService.findOne(id), true);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(concurso));
	}

	@GetMapping("/bilhetes-status-dist/{concursoId}/{numinicio}/{numfim}")
	@Timed
	public ResponseEntity<List<BilheteStatusDTO>> getStatusBilhetesDistribuidos(@PathVariable Integer concursoId,
			@PathVariable Long numinicio, @PathVariable Long numfim, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<BilheteStatusDTO> page = bilheteService.getStatusBilhetesDistribuidos(concursoId, numinicio, numfim,
				pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bilhetes-status");
		headers.set("qtd_validados", "" + bilheteService.quantidadeValidados(concursoId, numinicio, numfim));
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
	
	@GetMapping("/bilhetes-status-lote/{concursoId}/{lotevalidacao}")
	@Timed
	public ResponseEntity<List<BilheteStatusDTO>> getStatusBilhetesDistribuidosLote(@PathVariable Integer concursoId,
			@PathVariable Long lotevalidacao, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<BilheteStatusDTO> page = bilheteService.getStatusBilhetesDistribuidos(concursoId, lotevalidacao,
				pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bilhetes-status-lote");
		headers.set("qtd_validados", "" + bilheteService.quantidadeValidadosLote(concursoId, lotevalidacao));
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/bilhetes-status-faixa/{concursoId}/{numinicio}/{numfim}")
	@Timed
	public ResponseEntity<List<BilheteStatusDTO>> getStatusBilhetesFaixa(@PathVariable Integer concursoId,
			@PathVariable Long numinicio, @PathVariable Long numfim, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<BilheteStatusDTO> page = bilheteService.getStatusBilhetesFaixa(concursoId, numinicio, numfim, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bilhetes-status");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

    @GetMapping("/lotevalidacao-status/{concursoId}")
    @Timed
    public ResponseEntity<List<LoteValidacaoStatusDTO>> getStatusBilhetesDistribuidosRegional(
          @PathVariable Integer concursoId, @ApiParam Pageable pageable) {
        return getStatusBilhetesDistribuidosRegional(concursoId, null, pageable);
    }

    @GetMapping("/lotevalidacao-status/{concursoId}/{regionalId}")
    @Timed
    public ResponseEntity<List<LoteValidacaoStatusDTO>> getStatusBilhetesDistribuidosRegional(
          @PathVariable Integer concursoId, @PathVariable Long regionalId, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Concursos");
        try {
            Page<LoteValidacaoStatusDTO> page = bilheteService.consultaLotesValidacao(concursoId, regionalId,
                pageable);
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/bilhetes-status");
            headers.set("qtd_validados", "" + bilheteService.quantidadeValidadosRegional(concursoId, regionalId));
            return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
        } catch (UsuarioSemPermissaoException usp) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
                .body(null);
        }

    }


	@GetMapping("/validarbilhete/{concursoId}/{identificacaoBilhete}")
	@Timed
	public ResponseEntity validaBilhete(@PathVariable Integer concursoId, @PathVariable String identificacaoBilhete)
			throws URISyntaxException {
		try {
			String identificacaoValidada = bilheteService.validaVenda(concursoId, identificacaoBilhete);
			if (identificacaoValidada != null) {
				return ResponseEntity.created(new URI("/api/validarbilhete/"))
						.headers(HeaderUtil.createEntityCreationAlert("validado", identificacaoBilhete)).body(null);
			} else {
				return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(identificacaoBilhete,
						"nenhumupdate", "Não foi possível realizadar update de validacao")).body(null);
			}
		} catch (IdentificacaoBilheteInvalida eib) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(eib.getIdentificacao(), "bilheteInexistente", eib.getMessage()))
					.body(null);
		} catch (FaixaNaoCorrespondeAUmLoteException fncl) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(
							fncl.getIdentificacaoInicio() + " a " + fncl.getIdentificacaoFim(), "faixaNenhumLote",
							fncl.getMessage()))
					.body(null);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ValidacaoForaPeriodoPermitido vfpp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(vfpp.getMessage(), "periodoinvalido", vfpp.getMessage()))
					.body(null);
		} catch (BilheteJaValidadoException bjv) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(bjv.getMessage(), "javalidado", bjv.getMessage()))
					.body(null);
		}
	}


}
