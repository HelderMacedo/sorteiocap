package br.sorteioapp.web.rest;

import java.util.List;

import javax.xml.ws.Response;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;

import br.sorteioapp.service.DistribuicaoBilheteService;
import br.sorteioapp.service.dto.DistribuicaoBilheteOnlineDTO;
import com.codahale.metrics.annotation.Timed;
import br.sorteioapp.web.rest.util.HeaderUtil;

@RestController
@RequestMapping("/api")
public class DistribuicaoBilheteOnlineResource {

    private final DistribuicaoBilheteService distribuicaoBilheteService;

    public DistribuicaoBilheteOnlineResource(DistribuicaoBilheteService distribuicaoBilheteService) {
        this.distribuicaoBilheteService = distribuicaoBilheteService;
    }

    @PostMapping("/distribuicaoBilhetes/online")
    @Timed
    public ResponseEntity<List<String>> distibuicaoBilheteOnline(
            @RequestBody DistribuicaoBilheteOnlineDTO distribuicaoBilheteOnlineDTO) {

        try {
            List<String> response = distribuicaoBilheteService
                    .distiruirbilhetesOnlineService(distribuicaoBilheteOnlineDTO);
            return new ResponseEntity<>(response, null, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("distribuicaoBilheteOnline.error", e.getMessage(),
                            e.getMessage()))
                    .body(null);
        }

    }

}
