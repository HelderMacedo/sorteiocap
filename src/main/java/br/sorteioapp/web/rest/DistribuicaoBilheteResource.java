package br.sorteioapp.web.rest;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import br.sorteioapp.domain.*;
import br.sorteioapp.service.*;
import br.sorteioapp.service.erros.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;

import br.sorteioapp.service.dto.DistribuicaoBilheteDTO;
import br.sorteioapp.service.dto.DistribuicaoEstabelecimentoDTO;
import br.sorteioapp.service.dto.LoteBilheteRegionalDTO;
import br.sorteioapp.service.dto.LoteEstabelecimentoDTO;
import br.sorteioapp.service.dto.LoteRegionalBilheteDTO;
import br.sorteioapp.service.dto.LoteSubRegionalDTO;
import br.sorteioapp.service.dto.RelatorioDistribuicaoBilheteDTO;
import br.sorteioapp.service.dto.RelatorioDistribuicaoSubRegionalDTO;
import br.sorteioapp.service.dto.ThinConcursoDTO;
import br.sorteioapp.service.dto.ThinDistribuicaoBilheteAtualDTO;
import br.sorteioapp.service.dto.ThinRegionalDTO;
import br.sorteioapp.service.dto.TotalizacaoRegionalDTO;
import br.sorteioapp.service.dto.TotalizacaoRegionalEstabelecimentos;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class DistribuicaoBilheteResource {
	private final Logger log = LoggerFactory.getLogger(DistribuicaoBilheteResource.class);

	private final ConcursoService concursoService;
	private final RegionalService regionalService;
	private final EstabelecimentoService estabelecimentoService;
	private final DistribuicaoBilheteService distribuicaoBilheteService;
	private final TotalizacaoBilhetesService totalizacaoBilhetesService;

	public DistribuicaoBilheteResource(DistribuicaoBilheteService distribuicaoBilheteService,
			ConcursoService concursoService, RegionalService regionalService,
			EstabelecimentoService estabelecimentoService, TotalizacaoBilhetesService totalizacaoBilhetesService) {
		this.distribuicaoBilheteService = distribuicaoBilheteService;
		this.concursoService = concursoService;
		this.regionalService = regionalService;
		this.estabelecimentoService = estabelecimentoService;
		this.totalizacaoBilhetesService = totalizacaoBilhetesService;
	}

	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}

	@PostMapping("/distribuicaobilhetes/loteregionais")
	@Timed
	public ResponseEntity<LoteRegionalBilheteDTO> distribuiRegional(@RequestBody LoteBilheteRegionalDTO dto)
			throws URISyntaxException {
		log.debug("REST request to save LoteDistribuicaoBilhete : {}", dto);

		try {
			distribuicaoBilheteService.saveBilheteSeloRegional(dto.getConcursoId(), dto.getBilheteInicial(),
					dto.getBilheteFinal(),
					dto.getSeloInicial(), dto.getSeloFinal(),
					(dto.getRegionalId() == null) ? null : new Regional(dto.getRegionalId()), null,
					dto.getMotivoDistribuicao());

			return new ResponseEntity<>(null, null, HttpStatus.OK);
		} catch (BilheteNaoCorrespondeConcurso bncc) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(bncc.getIdentificacao(),
					"bilhetenaocorrespondeconcurso", bncc.getMessage())).body(null);
		} catch (FaixaJaCorrespondeAUmLoteException fjc) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(null,
					"faixapertenceoutrolote", fjc.getMessage())).body(null);
		} catch (IdentificacaoBilheteInvalida ibie) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ibie.getIdentificacao(), "bilheteinvalido", ibie.getMessage()))
					.body(null);
		} catch (LotePertenceAOutraRegionalException lpor) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(lpor.getNomeRegional(), "loteOutraRegional", lpor.getMessage()))
					.body(null);

		} catch (IllegalArgumentException iae) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(iae.getMessage(), "loteinvalido", iae.getMessage()))
					.body(null);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (BilheteJaValidadoException e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(e.getMessage(), "bilhetejavalidado", e.getMessage()))
					.body(null);
		} catch (SeloInvalidoException sie) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("selo",
					"seloinvalido", sie.getMessage())).body(null);
		}
	}

	@GetMapping("/bilhetesdistribuidos/{concurso}/{numInicial}/{numFinal}/{estabelecimento}")
	@Timed
	public ResponseEntity getBilhetesDistribuidos(
			@PathVariable Integer concurso, @PathVariable Long numInicial, @PathVariable Long numFinal,
			@PathVariable Long estabelecimento) {
		Concurso conc = new Concurso(concurso);

		List<Bilhete> lotes = distribuicaoBilheteService.findAllBilheteDistribuidos(conc, numInicial, numFinal,
				estabelecimento);

		return new ResponseEntity<>(lotes, null, HttpStatus.OK);
	}

	@GetMapping("/subregionaldistribuidos/{concurso}/{subRegional}")
	@Timed
	public ResponseEntity getSubRegionalDistribuidos(@PathVariable Integer concurso, @PathVariable Long subRegional) {
		Concurso conc = new Concurso(concurso);

		List<Bilhete> bilhetes = distribuicaoBilheteService.findAllSubRegionalDistribuidos(conc, subRegional);

		return new ResponseEntity<>(bilhetes, null, HttpStatus.OK);
	}

	@PostMapping("/distribuicaobilhetes/loteestabelecimentos")
	@Timed
	public ResponseEntity vendeEstabelecimento(@RequestBody LoteEstabelecimentoDTO packlote) throws URISyntaxException {
		log.debug("REST request to save LoteDistribuicaoBilhete : {}", packlote);
		try {
			if (packlote.getEstabelecimentoId() <= 0)
				throw new IllegalArgumentException("Est null");
			distribuicaoBilheteService.save(packlote.getConcursoId(), packlote.getBilheteInicial(),
					packlote.getBilheteFinal(),
					new Estabelecimento(packlote.getEstabelecimentoId()), packlote.getMotivoDistribuicao(),
					packlote.getAreaRegionalId());
			return new ResponseEntity(null, null, HttpStatus.OK);
		} catch (IllegalArgumentException iee) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(iee.getMessage(), "periodoinvalido", iee.getMessage()))
					.body(null);
		} catch (IdentificacaoBilheteInvalida ibie) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ibie.getIdentificacao(), "bilheteinvalido", ibie.getMessage()))
					.body(null);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (FaixaJaCorrespondeAUmLoteException fjc) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(null,
					"faixapertenceoutrolote", fjc.getMessage())).body(null);
		} catch (LotePertenceAOutraRegionalException lpor) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(lpor.getNomeRegional(), "loteOutraRegional", lpor.getMessage()))
					.body(null);
		} catch (BilheteJaValidadoException e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(e.getMessage(), "bilhetejavalidado", e.getMessage()))
					.body(null);
		} catch (BilheteNaoCorrespondeConcurso e) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(e.getIdentificacao(),
					"bilhetenaocorrespondeconcurso", e.getMessage())).body(null);
		}
	}

	@PostMapping("/distribuicaobilhetes/subregional")
	@Timed
	public ResponseEntity distribuicaoSubRegional(@RequestBody LoteSubRegionalDTO packlote) throws URISyntaxException,
			IdentificacaoBilheteInvalida, UsuarioSemPermissaoException, BilheteJaValidadoException,
			BilheteNaoCorrespondeConcurso, LotePertenceAOutraRegionalException, FaixaJaCorrespondeAUmLoteException,
			LotePertenceAOutraSubRegionalException {
		log.debug("REST request to save LoteSubRegionalDTO : {}", packlote);

		if (packlote.getAreaId() <= 0)
			throw new IllegalArgumentException("Sub null");

		try {
			distribuicaoBilheteService.saveDistSubRegional(packlote.getConcursoId(), packlote.getBilheteInicial(),
					packlote.getBilheteFinal(), new AreaRegional(packlote.getAreaId()),
					packlote.getMotivoDistribuicao());

			return new ResponseEntity(null, null, HttpStatus.OK);
		} catch (FaixaJaCorrespondeAUmLoteException e) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(null, "faixapertenceoutrolote", e.getMessage())).body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/{idconcurso}/{idregional}")
	@Timed
	public ResponseEntity<List<DistribuicaoBilheteDTO>> consultaLogDistribuicoes(@PathVariable Integer idconcurso,
			@PathVariable Long idregional, @ApiParam Pageable pageable)
			throws URISyntaxException {
		if (idconcurso != null && idregional != null) {
			Page<DistribuicaoBilheteDTO> page = distribuicaoBilheteService.findAll(new Concurso(idconcurso),
					new Regional(idregional), pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"api//distribuicaobilhetes/" + idconcurso + "/" + idregional);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(null, "sempermissao", null))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/bilhete/{idconcurso}/{numero}")
	@Timed
	public ResponseEntity<List<DistribuicaoBilheteDTO>> consultaLogDistribuicoesBilhete(
			@PathVariable Integer idconcurso, @PathVariable Long numero, @ApiParam Pageable pageable)
			throws URISyntaxException {
		if (idconcurso != null && numero != null) {
			Bilhete bil = new Bilhete();
			bil.setConcurso(new Concurso(idconcurso));
			bil.setNumero(numero);
			Page<DistribuicaoBilheteDTO> page = distribuicaoBilheteService.findAll(bil, pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"api//distribuicaobilhetes/bilhete/" + idconcurso + "/" + numero);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(null, "sempermissao", null))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/{idconcurso}")
	@Timed
	public ResponseEntity<List<DistribuicaoBilheteDTO>> consultaLogDistribuicoes(@PathVariable Integer idconcurso,
			@ApiParam Pageable pageable)
			throws URISyntaxException {
		if (idconcurso != null) {
			Page<DistribuicaoBilheteDTO> page = distribuicaoBilheteService.findAll(new Concurso(idconcurso), pageable);
			HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
					"api//distribuicaobilhetes/" + idconcurso);
			return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
		} else {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(null, "sempermissao", null))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/loteregionais-totalizacao/{concursoId}/{regionalId}")
	@Timed
	public ResponseEntity<TotalizacaoRegionalDTO> getTotalizacaoRegional(@PathVariable Integer concursoId,
			@PathVariable Long regionalId) {
		try {
			Concurso conc = concursoService.findOne(concursoId);
			Regional reg = regionalService.findOne(regionalId);
			List<TotalizacaoRegionalDTO> lsttotais = totalizacaoBilhetesService.consultaTotalizacoesRegionais(conc,
					null, null, reg);

			return new ResponseEntity<>((lsttotais.size() > 0) ? lsttotais.get(0) : null, null, HttpStatus.OK);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/loteregionais-totalizacao/{concursoId}")
	@Timed
	public ResponseEntity<List<TotalizacaoRegionalDTO>> getTotalizacoesRegionais(@PathVariable Integer concursoId) {
		try {
			Concurso conc = concursoService.findOne(concursoId);
			List<TotalizacaoRegionalDTO> lsttotais = totalizacaoBilhetesService.consultaTotalizacoesRegionais(conc,
					null, null, null);
			return new ResponseEntity<>(lsttotais, null, HttpStatus.OK);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/loteregionais-tendencia/{regionalId}")
	@Timed
	public ResponseEntity<List<TotalizacaoRegionalDTO>> getTotalizacoesRegionais(@PathVariable Long regionalId) {
		try {
			Regional reg = regionalService.findOne(regionalId);
			List<TotalizacaoRegionalDTO> lsttotais = totalizacaoBilhetesService.consultaTotalizacoesRegionais(null,
					null, null, reg);
			return new ResponseEntity<>(lsttotais, null, HttpStatus.OK);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/loteregionais-tendencia-periodo/{regionalId}")
	@Timed
	public ResponseEntity<List<TotalizacaoRegionalDTO>> getTotalizacoesRegionais(@PathVariable Long regionalId,
			@RequestParam(value = "dataInicio", required = true) LocalDate datainicio,
			@RequestParam(value = "dataFim", required = true) LocalDate datafim) {
		try {
			Regional reg = regionalService.findOne(regionalId);
			List<TotalizacaoRegionalDTO> lsttotais = totalizacaoBilhetesService.consultaTotalizacoesRegionais(null,
					datainicio, datafim, reg);
			return new ResponseEntity<>(lsttotais, null, HttpStatus.OK);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-reg/{idconcurso}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicoesTodosEstabelecimentoTodasRegionais(
			@PathVariable Integer idconcurso, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoTodosEstabelecimentosTodasRegionais(idconcurso, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-reg");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-reg/{idconcurso}/{idregional}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicoesTodosEstabelecimentoRegional(
			@PathVariable Integer idconcurso, @PathVariable Long idregional, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoTodosEstabelecimentosRegional(idconcurso, idregional, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-reg");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-est/{idconcurso}/{idestabelecimento}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicoesEstabelecimentoRegional(
			@PathVariable Integer idconcurso, @PathVariable Long idestabelecimento, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoEstabelecimentoRegional(idconcurso, idestabelecimento, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-est");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-reg/{idconcurso}/{idregional}/{idarea}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicaoTodosEstabelecimentosAreaRegional(
			@PathVariable Integer idconcurso, @PathVariable Long idregional, @PathVariable Long idarea,
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoTodosEstabelecimentosAreaRegional(idconcurso, idregional, idarea, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-est");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-reg/{idconcurso}/{idregional}/{idarea}/{idestabelecimento}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultarListaDistribuicaoEstabelecimento(
			@PathVariable Integer idconcurso, @PathVariable Long idregional, @PathVariable Long idarea,
			@PathVariable Long idestabelecimento, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultarDistribuicaoEstabelecimentoTodosOsFiltros(idconcurso, idregional, idarea, idestabelecimento,
						pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-est");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/relatorioestabelecimentos")
	@Timed
	public ResponseEntity<List<RelatorioDistribuicaoBilheteDTO>> relatorioDistribuicaoEstabelecimento(
			@RequestParam(value = "concursoId", required = true) Integer concurso,
			@RequestParam(value = "regionalId", required = false) Integer regional,
			@RequestParam(value = "areaId", required = false) Integer area,
			@RequestParam(value = "estabelecimentoId", required = false) Integer estabelecimento) {
		log.debug("REST request to get a page of Regionais");
		List<RelatorioDistribuicaoBilheteDTO> page = distribuicaoBilheteService
				.consultarRelatorioDistribuicaoEstabelecimento(concurso, regional, area, estabelecimento);
		return new ResponseEntity<>(page, null, HttpStatus.OK);
	}

	@GetMapping("/distrbuicaobilhetes/lotesubregional")
	@Timed
	public ResponseEntity<List<RelatorioDistribuicaoSubRegionalDTO>> relatorioDistribuicaoSubRegional(
			@RequestParam(value = "concursoId", required = true) Integer concursoId,
			@RequestParam(value = "areaId", required = false) Integer areaId) {
		log.debug("REST request to get a page of SubRegional");
		List<RelatorioDistribuicaoSubRegionalDTO> list = distribuicaoBilheteService
				.consultarRelatorioDistribuicaoSubRegional(concursoId, areaId);
		return new ResponseEntity<>(list, null, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteregionais/{idconcurso}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicaoTodasRegional(
			@PathVariable Integer idconcurso, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoAtualTodasRegional(idconcurso, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"api/distribuicaobilhetes/loteregionais/" + idconcurso);
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteregionais/{idconcurso}/{idregional}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicaoRegional(
			@PathVariable Integer idconcurso, @PathVariable Long idregional, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoAtualRegional(idconcurso, idregional, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteregionais");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-disponiveis/{idconcurso}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicoesDisponiveisTodasRegional(
			@PathVariable Integer idconcurso, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoDisponivelTodasRegional(idconcurso, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"api/distribuicaobilhetes/loteestabelecimentos-disponiveis/" + idconcurso);
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-disponiveis/{idconcurso}/{idregional}")
	@Timed
	public ResponseEntity<List<ThinDistribuicaoBilheteAtualDTO>> consultaDistribuicoesDisponiveisNaRegional(
			@PathVariable Integer idconcurso, @PathVariable Long idregional, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinDistribuicaoBilheteAtualDTO> page = distribuicaoBilheteService
				.consultaDistribuicaoDisponivelRegional(idconcurso, idregional, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/api/distribuicaobilhetes/loteestabelecimentos-disponiveis");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-totalizacao/{concursoId}/{regionalId}")
	@Timed
	public ResponseEntity<TotalizacaoRegionalEstabelecimentos> consultaTotalizacaoEstabelecimentos(
			@PathVariable Integer concursoId, @PathVariable Long regionalId) {
		return consultaTotalizacaoEstabelecimentos(concursoId, regionalId, null);
	}

	@GetMapping("/distribuicaobilhetes/loteestabelecimentos-totalizacao/{concursoId}/{regionalId}/{areaId}")
	@Timed
	public ResponseEntity<TotalizacaoRegionalEstabelecimentos> consultaTotalizacaoEstabelecimentos(
			@PathVariable Integer concursoId, @PathVariable Long regionalId, @PathVariable Long areaId) {
		try {
			Concurso conc = concursoService.findOne(concursoId);
			Regional reg = regionalService.findOne(regionalId);
			TotalizacaoRegionalEstabelecimentos totRegional = new TotalizacaoRegionalEstabelecimentos();
			totRegional.setConcurso(new ThinConcursoDTO(conc));
			totRegional.setRegional(new ThinRegionalDTO(reg));
			totRegional.setTotaisEstabelecimentos(
					totalizacaoBilhetesService.consultaTotalizacoesEstabelecimentos(conc, reg, areaId));

			return new ResponseEntity<>(totRegional, null, HttpStatus.OK);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}
}
