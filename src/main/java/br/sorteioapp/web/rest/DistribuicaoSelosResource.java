package br.sorteioapp.web.rest;

import br.sorteioapp.domain.*;
import br.sorteioapp.service.*;
import br.sorteioapp.service.dto.*;
import br.sorteioapp.service.erros.*;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DistribuicaoSelosResource {
	private final Logger log = LoggerFactory.getLogger(DistribuicaoSelosResource.class);

	private final ConcursoService concursoService;
	private final RegionalService regionalService;
	private final EstabelecimentoService estabelecimentoService;
	private final DistribuicaoSeloService distribuicaoSeloService;

	public DistribuicaoSelosResource(DistribuicaoSeloService distribuicaoSeloService, ConcursoService concursoService, RegionalService regionalService, EstabelecimentoService estabelecimentoService) {
		this.distribuicaoSeloService = distribuicaoSeloService;
		this.concursoService = concursoService;
		this.regionalService = regionalService;
		this.estabelecimentoService = estabelecimentoService;
	}
	
	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}	
	
	@PostMapping("/distribuicaoselos")
	@Timed
	public ResponseEntity<DistribuicaoSeloDTO> distribuirSelo(@RequestBody DistribuicaoSeloDTO dto)
			throws URISyntaxException {
		log.debug("REST request to save LoteDistribuicaoBilhete : {}", dto);

		try {
		    if (dto.getOperacao() == DistribuicaoSelo.OPERACAO_INSERCAO) {
                distribuicaoSeloService.distribuirSelos(
                    new Concurso(dto.getConcurso().getId()),
                    (dto.getRegional() != null) ? new Regional(dto.getRegional().getId()) : null,
                    (dto.getEstabelecimento() != null) ? new Estabelecimento(dto.getEstabelecimento().getId()) : null,
                    dto.getNumeroInicio(),
                    dto.getNumeroFim());

			    return new ResponseEntity<DistribuicaoSeloDTO>(null, null, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(null, "operacaoinvalida", "Operacao deve ser 'I'"))
                    .body(null);
            }
		} catch (SeloInvalidoException sie) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert( "selo",
					"seloinvalido", sie.getMessage())).body(null);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		}
	}


    @PutMapping("/distribuicaoselos")
    @Timed
    public ResponseEntity<DistribuicaoSeloDTO> devolverSelo(@RequestBody DistribuicaoSeloDTO dto)
        throws URISyntaxException {
        log.debug("REST request to save LoteDistribuicaoBilhete : {}", dto);

        try {
            if (dto.getOperacao() == DistribuicaoSelo.OPERACAO_REMOCAO) {
                distribuicaoSeloService.devolverSelos(
                    new Concurso(dto.getConcurso().getId()),
                    (dto.getRegional() != null) ? new Regional(dto.getRegional().getId()) : null,
                    (dto.getEstabelecimento() != null) ? new Estabelecimento(dto.getEstabelecimento().getId()) : null,
                    dto.getNumeroInicio(),
                    dto.getNumeroFim());

                return new ResponseEntity<DistribuicaoSeloDTO>(null, null, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert("selo", "operacaoinvalida", "Operacao deve ser 'R'"))
                    .body(null);
            }
        } catch (SeloInvalidoException sie) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert( "selo",
                "seloinvalido", sie.getMessage())).body(null);
        } catch (RemocaoSeloInsuficienteException rsie) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert( "selo",
                "selosinsuficiente", rsie.getMessage())).body(null);
        } catch (UsuarioSemPermissaoException usp) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
                .body(null);
        }
    }


	@GetMapping("/distribuicaoselos/regional/{idconcurso}/{idregional}")
	@Timed
	public ResponseEntity<List<DistribuicaoSeloDTO>> listarSelosRegional(@PathVariable Integer idconcurso,
                                                                 @PathVariable Long idregional)
			throws URISyntaxException {
	    try {
            if (idconcurso != null && idregional != null) {
                List<DistribuicaoSeloDTO> lst =
                    DistribuicaoSeloDTO.map(distribuicaoSeloService.listarSelos(new Concurso(idconcurso),
                        new Regional(idregional), null));
                return new ResponseEntity<List<DistribuicaoSeloDTO>>(lst, null, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(null, "sempermissao", null))
                    .body(null);
            }
        } catch (UsuarioSemPermissaoException usp) {
            return ResponseEntity.badRequest()
                .headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
                .body(null);
        }
	}

    @GetMapping("/distribuicaoselos/estabelecimento/{idconcurso}/{idestabelecimento}")
    @Timed
    public ResponseEntity<List<DistribuicaoSeloDTO>> listarSelosEstabelecimento(@PathVariable Integer idconcurso,
                                                                         @PathVariable Long idestabelecimento)
        throws URISyntaxException {
        try {
            if (idconcurso != null && idestabelecimento != null) {
                List<DistribuicaoSeloDTO> lst =
                    DistribuicaoSeloDTO.map(distribuicaoSeloService.listarSelos(new Concurso(idconcurso),
                        null, new Estabelecimento(idestabelecimento)));
                return new ResponseEntity<List<DistribuicaoSeloDTO>>(lst, null, HttpStatus.OK);
            } else {
                return ResponseEntity.badRequest()
                    .headers(HeaderUtil.createFailureAlert(null, "sempermissao", null))
                    .body(null);
            }
        } catch (UsuarioSemPermissaoException usp) {
        return ResponseEntity.badRequest()
            .headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
            .body(null);
        }
    }

}
