package br.sorteioapp.web.rest;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.codahale.metrics.annotation.Timed;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.EstabelecimentoBilheteOnline;
import br.sorteioapp.domain.Regional;
import br.sorteioapp.service.ConcursoService;
import br.sorteioapp.service.EstabalecimentoBilheteOnlineService;
import br.sorteioapp.service.RegionalService;

@RestController
@RequestMapping("/api")
public class EstabelecimentoBilheteOnlineResource {
    private final EstabalecimentoBilheteOnlineService estabalecimentoBilheteOnlineService;
    private final ConcursoService concursoService;
    private final RegionalService regionalService;

    public EstabelecimentoBilheteOnlineResource(
            EstabalecimentoBilheteOnlineService estabalecimentoBilheteOnlineService,
            ConcursoService concursoService,
            RegionalService regionalService) {
        this.estabalecimentoBilheteOnlineService = estabalecimentoBilheteOnlineService;
        this.concursoService = concursoService;
        this.regionalService = regionalService;
    }

    @ExceptionHandler
    void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value());
    }

    @GetMapping("/estabelecimentobilheteonline/{concursoId}/{regionalId}/{isOnline}")
    @Timed
    public ResponseEntity<List<EstabelecimentoBilheteOnline>> getEstabelecimentoBilheteOnline(
            @PathVariable Integer concursoId,
            @PathVariable Long regionalId, @PathVariable Boolean isOnline) {
        System.out.println("concursoId: " + concursoId + " regionalId: " + regionalId + " isOnline: " + isOnline);
        Concurso concurso = concursoService.findOne(concursoId);
        Regional regional = regionalService.findOne(regionalId);

        List<EstabelecimentoBilheteOnline> lstEstabalecimentoBilheteOnline = estabalecimentoBilheteOnlineService
                .buscarEstabelecimentoBilheteOnline(concurso, regional, null, isOnline);
        return new ResponseEntity<>(lstEstabalecimentoBilheteOnline, null, HttpStatus.OK);
    }

    @GetMapping("/estabelecimentobilheteonline/{concursoId}/{regionalId}/{areaId}/{isOnline}")
    @Timed
    public ResponseEntity<List<EstabelecimentoBilheteOnline>> getEstabelecimentoBilheteOnline(
            @PathVariable Integer concursoId,
            @PathVariable Long regionalId,
            @PathVariable Long areaId,
            @PathVariable Boolean isOnline) {
        System.out.println("concursoId: " + concursoId + " regionalId: " + regionalId + " areaId: " + areaId
                + " isOnline: " + isOnline);
        Concurso concurso = concursoService.findOne(concursoId);
        Regional regional = regionalService.findOne(regionalId);

        List<EstabelecimentoBilheteOnline> lstEstabalecimentoBilheteOnline = estabalecimentoBilheteOnlineService
                .buscarEstabelecimentoBilheteOnline(concurso, regional, areaId, isOnline);
        return new ResponseEntity<>(lstEstabalecimentoBilheteOnline, null, HttpStatus.OK);
    }
}