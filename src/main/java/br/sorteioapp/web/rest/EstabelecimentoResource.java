package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import com.querydsl.core.types.Predicate;

import br.sorteioapp.domain.Estabelecimento;
import br.sorteioapp.service.EstabelecimentoService;
import br.sorteioapp.service.dto.EstabelecimentoDTO;
import br.sorteioapp.service.dto.ThinEstabelecimentoDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Estabelecimento.
 */
@RestController
@RequestMapping("/api")
public class EstabelecimentoResource {

	private final Logger log = LoggerFactory.getLogger(EstabelecimentoResource.class);

	private static final String ENTITY_NAME = "estabelecimento";

	private final EstabelecimentoService estabelecimentoService;

	public EstabelecimentoResource(EstabelecimentoService estabelecimentoService) {
		this.estabelecimentoService = estabelecimentoService;
	}

	/**
	 * POST /estabelecimentos : Create a new estabelecimento.
	 *
	 * @param estabelecimentodto
	 *                           the estabelecimento to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         estabelecimento, or with status 400 (Bad Request) if the
	 *         estabelecimento has already an ID
	 * @throws URISyntaxException
	 *                            if the Location URI syntax is incorrect
	 */
	@PostMapping("/estabelecimentos")
	@Timed
	public ResponseEntity<EstabelecimentoDTO> createEstabelecimento(
			@Valid @RequestBody EstabelecimentoDTO estabelecimentodto) throws URISyntaxException {
		log.debug("REST request to save Estabelecimento : {}", estabelecimentodto);
		if (estabelecimentodto.getId() != null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists",
					"A new estabelecimento cannot already have an ID")).body(null);
		}
		try {
			EstabelecimentoDTO result = new EstabelecimentoDTO(estabelecimentoService.save(estabelecimentodto));
			return ResponseEntity.created(new URI("/api/estabelecimentos/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(estabelecimentodto.getLogin(),
					"errocriarusuario", "Erro ao criar usuario do estabelecimento")).body(null);
		}
	}

	/**
	 * PUT /estabelecimentos : Updates an existing estabelecimento.
	 *
	 * @param estabelecimentodto
	 *                           the estabelecimento to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         estabelecimento, or with status 400 (Bad Request) if the
	 *         estabelecimento is not valid, or with status 500 (Internal Server
	 *         Error) if the estabelecimento couldnt be updated
	 * @throws URISyntaxException
	 *                            if the Location URI syntax is incorrect
	 */
	@PutMapping("/estabelecimentos")
	@Timed
	public ResponseEntity<EstabelecimentoDTO> updateEstabelecimento(
			@Valid @RequestBody EstabelecimentoDTO estabelecimentodto) throws URISyntaxException {
		log.debug("REST request to update Estabelecimento : {}", estabelecimentodto);
		try {
			if (estabelecimentodto.getId() == null) {
				return createEstabelecimento(estabelecimentodto);
			}
			EstabelecimentoDTO result = new EstabelecimentoDTO(estabelecimentoService.save(estabelecimentodto));
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, estabelecimentodto.getId().toString()))
					.body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(estabelecimentodto.getLogin(),
					"errocriarusuario", "Erro ao criar usuario do estabelecimento")).body(null);
		}
	}

	/**
	 * GET /estabelecimentos : get all the estabelecimentos.
	 *
	 * @param pageable
	 *                 the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         estabelecimentos in body
	 * @throws URISyntaxException
	 *                            if there is an error to generate the pagination
	 *                            HTTP headers
	 */
	@GetMapping("/estabelecimentos")
	@Timed
	public ResponseEntity<List<ThinEstabelecimentoDTO>> getAllEstabelecimentos(
			@QuerydslPredicate(root = Estabelecimento.class) Predicate predicate, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Estabelecimentos");
		Page<ThinEstabelecimentoDTO> page = estabelecimentoService.findAll(predicate, pageable)
				.map(ThinEstabelecimentoDTO::new);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/estabelecimentos");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos/online/{regionalId}")
	@Timed
	public ResponseEntity<List<Estabelecimento>> getEstabelecimentosOnlinePorArea(@PathVariable Long regionalId,
			@RequestParam(value = "areaId", required = false) Long areaId,
			@RequestParam(value = "isOnline", required = false) Boolean isOnline) {

		List<Estabelecimento> lst = estabelecimentoService.getEstabelecimentosOnlinePorRegionalEArea(regionalId,
				areaId, isOnline);
		return new ResponseEntity<>(lst, HttpStatus.OK);
	}

	/**
	 * GET /estabelecimentos/:id : get the "id" estabelecimento.
	 *
	 * @param id
	 *           the id of the estabelecimento to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         estabelecimento, or with status 404 (Not Found)
	 */
	@GetMapping("/estabelecimentos/{id}")
	@Timed
	public ResponseEntity<EstabelecimentoDTO> getEstabelecimento(@PathVariable Long id) {
		log.debug("REST request to get Estabelecimento : {}", id);
		EstabelecimentoDTO estabelecimento = new EstabelecimentoDTO(estabelecimentoService.findOne(id));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(estabelecimento));
	}

	/**
	 * DELETE /estabelecimentos/:id : delete the "id" estabelecimento.
	 *
	 * @return the ResponseEntity with status 200 (OK)
	 * @throws IllegalAccessException
	 */

	@DeleteMapping("/estabelecimentos/{id}")
	@Timed
	public ResponseEntity<Void> deleteEstabelecimento(@PathVariable Long id) throws IllegalAccessException {
		log.debug("REST request to delete Estabelecimento : {}", id);

		try {
			estabelecimentoService.delete(id);
			return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString()))
					.build();

		} catch (Exception e) {
			throw new IllegalAccessException("errodelecao");
		}
	}

	@GetMapping("/estabelecimentos/regional")
	@Timed
	public ResponseEntity<List<ThinEstabelecimentoDTO>> getEstabelecimentos(@ApiParam Pageable pageable) {
		return getAllEstabelecimentos(null, pageable);
	}

	@GetMapping("/estabelecimentos/regional/{regionalId}")
	@Timed
	public ResponseEntity<List<EstabelecimentoDTO>> getEstabelecimentosPorRegional(@PathVariable Long regionalId,
			@ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Concursos");
		Page<Estabelecimento> page = estabelecimentoService.consultaEstabelecimentosPorRegional(regionalId, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/estabelecimentos/estabelecimento/" + regionalId);
		return new ResponseEntity(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos/ativos/regional/{regionalId}")
	@Timed
	public ResponseEntity<List<EstabelecimentoDTO>> getTodosEstabelecimentosAtivosPorRegional(
			@PathVariable Long regionalId) {
		log.debug("REST request to get a page of Concursos");
		List<Estabelecimento> lst = estabelecimentoService.consultaEstabelecimentosAtivosPorRegional(regionalId);
		return new ResponseEntity(lst, null, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos/ativos/subrebional/{subregional}")
	@Timed
	public ResponseEntity<List<EstabelecimentoDTO>> getTodosEstabelecimentosPorArea(@PathVariable Long subregional) {
		log.debug("REST request to get a page of Concursos");
		List<Estabelecimento> lst = estabelecimentoService.consultarEstabelecimentosPorSubRegional(subregional);
		return new ResponseEntity(lst, null, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos-enderecos")
	@Timed
	public ResponseEntity<List<String>> queryAllCidades() {
		List<String> lista = estabelecimentoService.queryAllCidades();
		return new ResponseEntity(lista, null, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos-enderecos/{cidade}")
	@Timed
	public ResponseEntity<List<String>> queryAllBairros(@PathVariable String cidade) {
		List<String> lista = estabelecimentoService.queryAllBairros(cidade);
		return new ResponseEntity(lista, null, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos-por-enderecos")
	public ResponseEntity<List<ThinEstabelecimentoDTO>> queryAllByCidade(Pageable pageable) {
		Page<ThinEstabelecimentoDTO> page = estabelecimentoService.findAll(null, pageable)
				.map(ThinEstabelecimentoDTO::createWithoutRegional);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/estabelecimentos-por-enderecos");
		return new ResponseEntity(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos-por-enderecos/{cidade}")
	public ResponseEntity<List<ThinEstabelecimentoDTO>> queryAllByCidade(@PathVariable String cidade,
			Pageable pageable) {
		Page<ThinEstabelecimentoDTO> page = estabelecimentoService.queryAllByCidade(cidade, pageable)
				.map(ThinEstabelecimentoDTO::createWithoutRegional);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/estabelecimentos-por-enderecos/" + cidade);
		return new ResponseEntity(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/estabelecimentos-por-enderecos/{cidade}/{bairro}")
	public ResponseEntity<List<ThinEstabelecimentoDTO>> queryAllByCidadeAndBairro(@PathVariable String cidade,
			@PathVariable String bairro, Pageable pageable) {
		Page<ThinEstabelecimentoDTO> page = estabelecimentoService.queryAllByCidadeAndBairro(cidade, bairro, pageable)
				.map(ThinEstabelecimentoDTO::createWithoutRegional);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page,
				"/estabelecimentos-por-enderecos/" + cidade + "/" + bairro);
		return new ResponseEntity(page.getContent(), headers, HttpStatus.OK);
	}
}
