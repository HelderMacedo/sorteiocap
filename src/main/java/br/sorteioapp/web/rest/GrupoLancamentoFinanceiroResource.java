package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.sorteioapp.service.GrupoLancamentoFinanceiroService;
import br.sorteioapp.domain.GrupoLancamentoFinanceiro;
import br.sorteioapp.domain.TipoOperacaoLancamentoFinanceiro;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing GrupoLancamentoFinanceiro.
 */
@RestController
@RequestMapping("/api")
public class GrupoLancamentoFinanceiroResource {

	private final Logger log = LoggerFactory.getLogger(GrupoLancamentoFinanceiroResource.class);

	private static final String ENTITY_NAME = "grupoLancamentoFinanceiro";

	private final GrupoLancamentoFinanceiroService grupoLancamentoFinanceiroService;

	public GrupoLancamentoFinanceiroResource(GrupoLancamentoFinanceiroService grupoLancamentoFinanceiroService) {
		this.grupoLancamentoFinanceiroService = grupoLancamentoFinanceiroService;
	}

	/**
	 * POST /grupo-lancamento : Create a new grupoLancamentoFinanceiro.
	 *
	 * @param grupoLancamentoFinanceiro
	 *            the grupoLancamentoFinanceiroDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         grupoLancamentoFinanceiroDTO, or with status 400 (Bad Request) if the grupoLancamentoFinanceiro
	 *         has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/grupo-lancamento")
	@Timed
	public ResponseEntity<GrupoLancamentoFinanceiro> createGrupoLancamentoFinanceiro(@RequestBody GrupoLancamentoFinanceiro grupoLancamentoFinanceiro)
			throws URISyntaxException, IllegalArgumentException {
		log.debug("REST request to save GrupoLancamentoFinanceiro : {}", grupoLancamentoFinanceiro);
		if (grupoLancamentoFinanceiro.getId() != null) {
			throw new IllegalArgumentException("A new grupoLancamentoFinanceiro cannot already have an ID");
		}
		GrupoLancamentoFinanceiro result = grupoLancamentoFinanceiroService.save(grupoLancamentoFinanceiro);
		return ResponseEntity.created(new URI("/api/grupo-lancamento/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /grupo-lancamento : Updates an existing grupoLancamentoFinanceiro.
	 *
	 * @param grupoLancamentoFinanceiro
	 *            the grupoLancamentoFinanceiroDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         grupoLancamentoFinanceiroDTO, or with status 400 (Bad Request) if the
	 *         grupoLancamentoFinanceiroDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the grupoLancamentoFinanceiroDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/grupo-lancamento")
	@Timed
	public ResponseEntity<GrupoLancamentoFinanceiro> updateGrupoLancamentoFinanceiro(@RequestBody GrupoLancamentoFinanceiro grupoLancamentoFinanceiro)
			throws URISyntaxException {
		log.debug("REST request to update GrupoLancamentoFinanceiro : {}", grupoLancamentoFinanceiro);
		if (grupoLancamentoFinanceiro.getId() == null) {
			throw new IllegalArgumentException("Invalid id" + ENTITY_NAME);
		}
		GrupoLancamentoFinanceiro result = grupoLancamentoFinanceiroService.save(grupoLancamentoFinanceiro);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, grupoLancamentoFinanceiro.getId().toString()))
				.body(result);
	}

	/**
	 * GET /grupo-lancamento : get all the grupoLancamentoFinanceiros.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of grupoLancamentoFinanceiros
	 *         in body
	 */
	@GetMapping("/grupo-lancamento")
	@Timed
	public ResponseEntity<List<GrupoLancamentoFinanceiro>> getAllGrupoLancamentoFinanceiros(Pageable pageable) {
		log.debug("REST request to get a page of GrupoLancamentoFinanceiros");
		Page<GrupoLancamentoFinanceiro> page = grupoLancamentoFinanceiroService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/grupo-lancamento");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/tipo-operacaoFinanceira")
	@Timed
	public ResponseEntity<List<TipoOperacaoLancamentoFinanceiro>> getAllTipoOperacao() {
		log.debug("REST request to get a page of TipoOperacaoLancamentoFinanceiro");
		List<TipoOperacaoLancamentoFinanceiro> lst = grupoLancamentoFinanceiroService.listAllTipoOperacao();
		return new ResponseEntity<>(lst, null, HttpStatus.OK);
	}

	@GetMapping("/grupo-lancamento/tipo/{idtipo}")
	@Timed
	public ResponseEntity<List<GrupoLancamentoFinanceiro>> getAllGrupoLancamentoFinanceiros(@PathVariable Long idtipo) {
		log.debug("REST request to get a page of GrupoLancamentoFinanceiros por tipo "+idtipo );
		List<GrupoLancamentoFinanceiro> lst = grupoLancamentoFinanceiroService.listAllByTipoOperacao(idtipo);
		return new ResponseEntity<>(lst, null, HttpStatus.OK);
	}
	/**
	 * GET /grupo-lancamento/:id : get the "id" grupoLancamentoFinanceiro.
	 *
	 * @param id
	 *            the id of the grupoLancamentoFinanceiroDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         grupoLancamentoFinanceiroDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/grupo-lancamento/{id}")
	@Timed
	public ResponseEntity<GrupoLancamentoFinanceiro> getGrupoLancamentoFinanceiro(@PathVariable Long id) {
		log.debug("REST request to get GrupoLancamentoFinanceiro : {}", id);
		Optional<GrupoLancamentoFinanceiro> grupoLancamentoFinanceiro = grupoLancamentoFinanceiroService.findOne(id);
		return ResponseUtil.wrapOrNotFound(grupoLancamentoFinanceiro);
	}

	/**
	 * DELETE /grupo-lancamento/:id : delete the "id" grupoLancamentoFinanceiro.
	 *
	 * @param id
	 *            the id of the grupoLancamentoFinanceiroDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/grupo-lancamento/{id}")
	@Timed
	public ResponseEntity<Void> deleteGrupoLancamentoFinanceiro(@PathVariable Long id) {
		log.debug("REST request to delete GrupoLancamentoFinanceiro : {}", id);
		grupoLancamentoFinanceiroService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
