package br.sorteioapp.web.rest;

import br.sorteioapp.domain.Bilhete;
import br.sorteioapp.service.BilheteService;
import br.sorteioapp.service.erros.BilheteJaCadastradoException;
import br.sorteioapp.service.erros.IdentificacaoBilheteInvalida;
import br.sorteioapp.service.dto.JogadorDTO;
import com.codahale.metrics.annotation.Timed;
import br.sorteioapp.domain.Jogador;
import br.sorteioapp.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * REST controller for managing Jogador.
 */
@RestController
@RequestMapping("/api")
public class JogadorResource {

    private final Logger log = LoggerFactory.getLogger(JogadorResource.class);

    private static final String ENTITY_NAME = "jogador";

    private final BilheteService bilheteService;

    public JogadorResource(BilheteService bilheteService) {
        this.bilheteService = bilheteService;
    }

    /**
     * POST  /jogadors : Create a new jogador.
     *
     * @return the ResponseEntity with status 201 (Created) and with body the new jogador, or with status 400 (Bad Request) if the jogador has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect

    @CrossOrigin
    @PostMapping("/jogador")
    @Timed
    public ResponseEntity<Jogador> createJogador(@RequestBody JogadorDTO jogadordto) throws URISyntaxException {
        log.debug("REST request to save Jogador : {}", jogadordto);
        try {
            Bilhete bilhete= bilheteService.cadastrarJogador(jogadordto);
            return ResponseEntity.created(new URI("/api/jogadors/" ))
                .body(null);
        } catch (IdentificacaoBilheteInvalida eib) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(jogadordto.getIdentificacao(), "bilheteInexistente", eib.getMessage())).body(null);
        } catch (BilheteJaCadastradoException bje) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(bje.getNomeJogador(), "bilheteJaCadastrado", bje.getMessage())).body(null);
        } catch (IllegalArgumentException iae){
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(jogadordto.getNome(), "nomeInvalido", iae.getMessage())).body(null);
        }
    }
    */

}
