package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import br.sorteioapp.service.dto.LancamentoFinanceiroDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.codahale.metrics.annotation.Timed;
import com.querydsl.core.types.Predicate;

import br.sorteioapp.domain.LancamentoFinanceiro;
import br.sorteioapp.service.LancamentoFinanceiroService;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing LancamentoFinanceiro.
 */
@RestController
@RequestMapping("/api")
public class LancamentoFinanceiroResource {

	private final Logger log = LoggerFactory.getLogger(LancamentoFinanceiroResource.class);

	private static final String ENTITY_NAME = "lancamentoFinanceiro";

	private final LancamentoFinanceiroService lancamentoFinanceiroService;

	public LancamentoFinanceiroResource(LancamentoFinanceiroService lancamentoFinanceiroService) {
		this.lancamentoFinanceiroService = lancamentoFinanceiroService;
	}

	/**
	 * POST/
	 : Create a new lancamentoFinanceiro.
	 *
	 * @param lancamentoFinanceiroDTO
	 *            the lancamentoFinanceiroDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         lancamentoFinanceiroDTO, or with status 400 (Bad Request) if the lancamentoFinanceiro has
	 *         already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/lancamentos")
	@Timed
	public ResponseEntity<LancamentoFinanceiroDTO> createLancamentoFinanceiro(@RequestBody LancamentoFinanceiroDTO lancamentoFinanceiroDTO) throws URISyntaxException {
		log.debug("REST request to save LancamentoFinanceiro : {}", lancamentoFinanceiroDTO);
		if (lancamentoFinanceiroDTO.getId() != null) {
			throw new IllegalArgumentException("A new lancamentoFinanceiro cannot already have an ID");
		}
		LancamentoFinanceiroDTO result = lancamentoFinanceiroService.save(lancamentoFinanceiroDTO);
		return ResponseEntity.created(new URI("/api/lancamentos/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT/lancamento : Updates an existing lancamentoFinanceiro.
	 *
	 * @param lancamentoFinanceiroDTO
	 *            the lancamentoFinanceiroDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         lancamentoFinanceiroDTO, or with status 400 (Bad Request) if the lancamentoFinanceiroDTO is not
	 *         valid, or with status 500 (Internal Server Error) if the lancamentoFinanceiroDTO
	 *         couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/lancamentos")
	@Timed
	public ResponseEntity<LancamentoFinanceiroDTO> updateLancamentoFinanceiro(@RequestBody LancamentoFinanceiroDTO lancamentoFinanceiroDTO) throws URISyntaxException {
		log.debug("REST request to update LancamentoFinanceiro : {}", lancamentoFinanceiroDTO);
		if (lancamentoFinanceiroDTO.getId() == null) {
			throw new IllegalArgumentException("Invalid id");
		}
		LancamentoFinanceiroDTO result = lancamentoFinanceiroService.save(lancamentoFinanceiroDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, lancamentoFinanceiroDTO.getId().toString())).body(result);
	}

	/**
	 * GET/lancamento : get all the lancamento.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of lancamento in
	 *         body
	 */
	@GetMapping("/lancamentos")
	@Timed
	public ResponseEntity<List<LancamentoFinanceiroDTO>> getAllLancamentoFinanceiros(@QuerydslPredicate(root = LancamentoFinanceiro.class) Predicate predicate,
                                                                        @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of LancamentoFinanceiros");
		Page<LancamentoFinanceiroDTO> page = lancamentoFinanceiroService.findAll(predicate, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/lancamentos");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

    @GetMapping("/lancamentos/totalizacao")
    @Timed
    public ResponseEntity<List<LancamentoFinanceiroDTO>> getAllLancamentoFinanceiros(
                            @RequestParam(value = "datainicio", required = false) LocalDate datainicio,
                            @RequestParam(value = "datafim", required = false) LocalDate datafim) {
        log.debug("REST request to get a totalizacao of LancamentoFinanceiros");
        List<LancamentoFinanceiroDTO> lst = lancamentoFinanceiroService.consultaTotalizacaoLancamentos(datainicio, datafim);
        return new ResponseEntity<>(lst, null, HttpStatus.OK);
    }

	/**
	 * GET/lancamento/:id : get the "id" lancamentoFinanceiro.
	 *
	 * @param id
	 *            the id of the lancamentoFinanceiroDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the lancamentoFinanceiroDTO,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/lancamentos/{id}")
	@Timed
	public ResponseEntity<LancamentoFinanceiroDTO> getLancamentoFinanceiro(@PathVariable Long id) {
		log.debug("REST request to get LancamentoFinanceiro : {}", id);
		Optional<LancamentoFinanceiroDTO> lancamentoFinanceiroDTO = lancamentoFinanceiroService.findOne(id);
		return ResponseUtil.wrapOrNotFound(lancamentoFinanceiroDTO);
	}

	/**
	 * DELETE/lancamento/:id : delete the "id" lancamentoFinanceiro.
	 *
	 * @param id
	 *            the id of the lancamentoFinanceiroDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/lancamentos/{id}")
	@Timed
	public ResponseEntity<Void> deleteLancamentoFinanceiro(@PathVariable Long id) {
		log.debug("REST request to delete LancamentoFinanceiro : {}", id);
		lancamentoFinanceiroService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
