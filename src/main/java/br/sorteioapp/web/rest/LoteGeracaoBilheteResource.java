package br.sorteioapp.web.rest;

import java.net.URISyntaxException;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.service.ConcursoService;
import br.sorteioapp.service.LoteGeracaoBilheteService;
import br.sorteioapp.web.rest.util.HeaderUtil;

/**
 * Created by rodrigo on 23/04/17.
 */

@RestController
@RequestMapping("/api")
public class LoteGeracaoBilheteResource {

	private final org.slf4j.Logger log = LoggerFactory.getLogger(LoteGeracaoBilheteResource.class);

	private final LoteGeracaoBilheteService loteGeracaoService;
	private final ConcursoService concursoService;

	public LoteGeracaoBilheteResource(ConcursoService concursoService, LoteGeracaoBilheteService loteGeracaoService) {
		this.concursoService = concursoService;
		this.loteGeracaoService = loteGeracaoService;
	}

	/**
	 * POST /bilhetes : Create new bilhetes.
	 */
	@PostMapping(value = "/lotebilhete-geracao/{idconcurso}", params = { "quantidade", "online" })
	@Timed
	public ResponseEntity createBilhete(@PathVariable int idconcurso, @RequestParam int quantidade,
			@RequestParam boolean online) throws URISyntaxException {
		Concurso concurso = concursoService.findOne(idconcurso);

		System.out.println("Chegou solicitacao " + concurso.getId() + " " + quantidade);
		if (concurso.getId() == null) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("LoteGeracaoBilheteResource",
					"idexists", "Nenhum concurso foi informado")).body(null);
		}
		int res = loteGeracaoService.geraLoteBilhetes(concurso, quantidade, online);
		return new ResponseEntity((res == quantidade) ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
	}

/*	@GetMapping("/lotebilhete/faixas-disponiveis/{idconcurso}")
	@Timed
	public ResponseEntity<List<LoteGeralDTO>> getLotesFaixasDisponiveisParaVenda(@PathVariable Integer idconcurso,
			@PathVariable Long regionalid) {
		log.debug("REST request to get a list of lotes intgegralmente nao distribuidos");
		List<LoteGeralDTO> resultdto = loteGeracaoService.lotesGeracaoComDisponibilidade(idconcurso);
		return new ResponseEntity<>(resultdto, null, HttpStatus.OK);
	}
*/	
}
