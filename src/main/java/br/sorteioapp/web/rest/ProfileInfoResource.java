package br.sorteioapp.web.rest;

import br.sorteioapp.config.ApplicationProperties;
import br.sorteioapp.config.DefaultProfileUtil;

import io.github.jhipster.config.JHipsterProperties;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Resource to return information about the currently running Spring profiles.
 */
@RestController
@RequestMapping("/api")
public class ProfileInfoResource {

    private final Environment env;

    private final JHipsterProperties jHipsterProperties;
    private final ApplicationProperties applicationProperties;

    public ProfileInfoResource(Environment env, JHipsterProperties jHipsterProperties, ApplicationProperties applicationProperties) {
        this.env = env;
        this.jHipsterProperties = jHipsterProperties;
        this.applicationProperties = applicationProperties;
    }

    @GetMapping("/profile-info")
    public ProfileInfoVM getActiveProfiles() {
        String[] activeProfiles = DefaultProfileUtil.getActiveProfiles(env);
        return new ProfileInfoVM(activeProfiles, getRibbonEnv(activeProfiles), applicationProperties.getVerAjuste(), applicationProperties.getComissaoProgressiva());
    }

    private String getRibbonEnv(String[] activeProfiles) {
        String[] displayOnActiveProfiles = jHipsterProperties.getRibbon().getDisplayOnActiveProfiles();
        if (displayOnActiveProfiles == null) {
            return null;
        }
        List<String> ribbonProfiles = new ArrayList<>(Arrays.asList(displayOnActiveProfiles));
        List<String> springBootProfiles = Arrays.asList(activeProfiles);
        ribbonProfiles.retainAll(springBootProfiles);
        if (!ribbonProfiles.isEmpty()) {
            return ribbonProfiles.get(0);
        }
        return null;
    }

    class ProfileInfoVM {

        private String[] activeProfiles;
        private String ribbonEnv;
        private Boolean reportWithAdjustment;
        private Boolean comissaoProgressiva;

        ProfileInfoVM(String[] activeProfiles, String ribbonEnv, Boolean reportWithAdjustment, Boolean comissaoProgressiva) {
            this.activeProfiles = activeProfiles;
            this.ribbonEnv = ribbonEnv;
            this.reportWithAdjustment = reportWithAdjustment;
            this.comissaoProgressiva = comissaoProgressiva;
        }

        public String[] getActiveProfiles() {
            return activeProfiles;
        }

        public String getRibbonEnv() {
            return ribbonEnv;
        }

        public Boolean getReportWithAdjustment() { return reportWithAdjustment;  }

        public Boolean getComissaoProgressiva() {
            return comissaoProgressiva;
        }
    }
}
