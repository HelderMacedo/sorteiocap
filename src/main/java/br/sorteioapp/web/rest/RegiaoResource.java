package br.sorteioapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import br.sorteioapp.domain.Regiao;

import br.sorteioapp.repository.RegiaoRepository;
import br.sorteioapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Regiao.
 */
@RestController
@RequestMapping("/api")
public class RegiaoResource {

    private final Logger log = LoggerFactory.getLogger(RegiaoResource.class);

    private static final String ENTITY_NAME = "regiao";

    private final RegiaoRepository regiaoRepository;

    public RegiaoResource(RegiaoRepository regiaoRepository) {
        this.regiaoRepository = regiaoRepository;
    }

    /**
     * POST  /regioes : Create a new regiao.
     *
     * @param regiao the regiao to create
     * @return the ResponseEntity with status 201 (Created) and with body the new regiao, or with status 400 (Bad Request) if the regiao has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/regioes")
    @Timed
    public ResponseEntity<Regiao> createRegiao(@RequestBody Regiao regiao) throws URISyntaxException {
        log.debug("REST request to save Regiao : {}", regiao);
        if (regiao.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new regiao cannot already have an ID")).body(null);            
        }
        Regiao result = regiaoRepository.save(regiao);
        return ResponseEntity.created(new URI("/api/regioes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /regioes : Updates an existing regiao.
     *
     * @param regiao the regiao to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated regiao,
     * or with status 400 (Bad Request) if the regiao is not valid,
     * or with status 500 (Internal Server Error) if the regiao couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/regioes")
    @Timed
    public ResponseEntity<Regiao> updateRegiao(@RequestBody Regiao regiao) throws URISyntaxException {
        log.debug("REST request to update Regiao : {}", regiao);
        if (regiao.getId() == null) {
            return createRegiao(regiao);
        }
        Regiao result = regiaoRepository.save(regiao);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, regiao.getId().toString()))
            .body(result);
    }

    /**
     * GET  /regioes : get all the regioes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of regioes in body
     */
    @GetMapping("/regioes")
    @Timed
    public List<Regiao> getAllregioes() {
        log.debug("REST request to get all regioes");
        return regiaoRepository.findAll();
        }

    /**
     * GET  /regioes/:id : get the "id" regiao.
     *
     * @param id the id of the regiao to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the regiao, or with status 404 (Not Found)
     */
    @GetMapping("/regioes/{id}")
    @Timed
    public ResponseEntity<Regiao> getRegiao(@PathVariable Long id) {
        log.debug("REST request to get Regiao : {}", id);
        Regiao regiao = regiaoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(regiao));
    }

    /**
     * DELETE  /regioes/:id : delete the "id" regiao.
     *
     * @param id the id of the regiao to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/regioes/{id}")
    @Timed
    public ResponseEntity<Void> deleteRegiao(@PathVariable Long id) {
        log.debug("REST request to delete Regiao : {}", id);
        regiaoRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
