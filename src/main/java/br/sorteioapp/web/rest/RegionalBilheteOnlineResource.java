package br.sorteioapp.web.rest;
import org.springframework.web.bind.annotation.RestController;
import br.sorteioapp.domain.*;
import br.sorteioapp.service.ConcursoService;
import br.sorteioapp.service.RegionalBilheteOnlineService;
import br.sorteioapp.service.RegionalService;
import br.sorteioapp.web.rest.util.HeaderUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.codahale.metrics.annotation.Timed;


@RestController
@RequestMapping("/api")
public class RegionalBilheteOnlineResource {
    private final RegionalBilheteOnlineService regionalBilheteOnlineService;
    private final ConcursoService concursoService;
	private final RegionalService regionalService;

    public RegionalBilheteOnlineResource(   RegionalBilheteOnlineService regionalBilheteOnlineService,
                                            ConcursoService concursoService,
                                            RegionalService regionalService){
        this.regionalBilheteOnlineService = regionalBilheteOnlineService;
        this.concursoService = concursoService;
        this.regionalService = regionalService;
    }

    @ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
    }
    
    @GetMapping("/regionalbilheteonline/{concursoId}")
    @Timed
    public ResponseEntity<List<RegionalBilheteOnline>> getRegionalBilheteOnline(@PathVariable Integer concursoId)
    {        
        Concurso concurso = concursoService.findOne(concursoId);
        //System.out.println("CONCURSO "+ concursoId);
        List<RegionalBilheteOnline> lstRegionalBilheteOnline = regionalBilheteOnlineService.buscarRegionalBilheteOnline(concurso, null);
        return new ResponseEntity<>(lstRegionalBilheteOnline, null, HttpStatus.OK);
            
    }

    @GetMapping("/regionalbilheteonline/{concursoId}/{regionalId}")
    @Timed
    public ResponseEntity<List<RegionalBilheteOnline>> getRegionalBilheteOnline(@PathVariable Integer concursoId,
                                                                                @PathVariable Long regionalId)
    {        
        Concurso concurso = concursoService.findOne(concursoId);
        Regional regional = regionalService.findOne(regionalId); 
        
        List<RegionalBilheteOnline> lstRegionalBilheteOnline = regionalBilheteOnlineService.buscarRegionalBilheteOnline(concurso, regional);
        return new ResponseEntity<>(lstRegionalBilheteOnline, null, HttpStatus.OK);
            
    }
}
