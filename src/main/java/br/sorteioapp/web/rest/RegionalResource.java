package br.sorteioapp.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import br.sorteioapp.repository.RegionalRepository;
import br.sorteioapp.repository.RegiaoRepository;
import br.sorteioapp.service.dto.ComissaoListaRegionalDTO;
import br.sorteioapp.service.dto.ComissaoRegionalDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.querydsl.core.types.Predicate;

import br.sorteioapp.domain.Regional;
import br.sorteioapp.domain.Regiao;
import br.sorteioapp.service.RegionalService;
import br.sorteioapp.service.dto.RegionalDTO;
import br.sorteioapp.service.dto.ThinRegionalDTO;
import br.sorteioapp.service.erros.ErroCriarUsuarioException;
import br.sorteioapp.service.erros.UsuarioSemPermissaoException;
import br.sorteioapp.web.rest.util.HeaderUtil;
import br.sorteioapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing Regional.
 */
@RestController
@RequestMapping("/api")
public class RegionalResource {

	private final Logger log = LoggerFactory.getLogger(RegionalResource.class);

	private static final String ENTITY_NAME = "regional";

	private final RegionalService regionalService;
    private final RegionalRepository regionalRepository;
    private final RegiaoRepository regiaoRepository;

	public RegionalResource(RegionalService regionalService, RegionalRepository regionalRepository,
                            RegiaoRepository regiaoRepository) {
		this.regionalService = regionalService;
        this.regionalRepository = regionalRepository;
        this.regiaoRepository = regiaoRepository;
	}

	/**
	 * POST /regionais : Create a new regional.
	 *
	 * @param regionaldto the regional to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         regional, or with status 400 (Bad Request) if the regional has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/regionais")
	@Timed
	public ResponseEntity<RegionalDTO> createRegional(@Valid @RequestBody RegionalDTO regionaldto)
			throws URISyntaxException {
		log.debug("REST request to save Regional : {}", regionaldto);
		if (regionaldto.getId() != null) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new regional cannot already have an ID"))
					.body(null);
		}
		try {
			RegionalDTO result = new RegionalDTO(regionalService.save(regionaldto));
			return ResponseEntity.created(new URI("/api/regionais/" + result.getId()))
					.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(regionaldto.getLogin(),
					"errocriarusuario", "Erro ao criar usuario da regional")).body(null);
		}
	}

	/**
	 * PUT /regionais : Updates an existing regional.
	 *
	 * @param regionaldto the regional to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         regional, or with status 400 (Bad Request) if the regional is not
	 *         valid, or with status 500 (Internal Server Error) if the regional
	 *         couldnt be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/regionais")
	@Timed
	public ResponseEntity<RegionalDTO> updateRegional(@Valid @RequestBody RegionalDTO regionaldto)
			throws URISyntaxException {
		log.debug("REST request to update Regional : {}", regionaldto);
		if (regionaldto.getId() == null) {
			return createRegional(regionaldto);
		}
		try {
			RegionalDTO result = new RegionalDTO(regionalService.save(regionaldto));
			return ResponseEntity.ok()
					.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, regionaldto.getId().toString()))
					.body(result);
		} catch (UsuarioSemPermissaoException usp) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert(usp.getUsuario(), "sempermissao", usp.getMessage()))
					.body(null);
		} catch (ErroCriarUsuarioException ecue) {
			return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ecue.getMessage(),
					"errocriarusuario", "Erro ao criar usuario da regional")).body(null);
		}
	}

	/**
	 * DELETE /estabelecimentos/:id : delete the "id" estabelecimento.
	 *
	 * @return the ResponseEntity with status 200 (OK)
	 * @throws IllegalAccessException 
	 */

	@DeleteMapping("/regionais/{id}")
	@Timed
	public ResponseEntity<Void> deleteRegional(@PathVariable Long id) throws IllegalAccessException {
		log.debug("REST request to delete Estabelecimento : {}", id);
		try {
		  regionalService.delete(id);
		  return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
		} catch (Exception e) {
			throw new IllegalAccessException("errodelecao");			
		}
	}

	/**
	 * GET /regionais : get all the regionais.
	 *
	 * @param pageable the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of regionais in
	 *         body
	 * @throws URISyntaxException if there is an error to generate the pagination
	 *                            HTTP headers
	 */
	@GetMapping("/regionais")
	@Timed
	public ResponseEntity<List<ThinRegionalDTO>> getAllRegionais(
			@QuerydslPredicate(root = Regional.class) Predicate predicate, @ApiParam Pageable pageable) {
		log.debug("REST request to get a page of Regionais");
		Page<ThinRegionalDTO> page = regionalService.findAll(predicate, pageable).map(ThinRegionalDTO::new);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/regionais");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /regionais/:id : get the "id" regional.
	 *
	 * @param id the id of the regional to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the regional,
	 *         or with status 404 (Not Found)
	 */
	@GetMapping("/regionais/{id}")
	@Timed
	public ResponseEntity<RegionalDTO> getRegional(@PathVariable Long id) {
		log.debug("REST request to get Regional : {}", id);
		RegionalDTO regionaldto = new RegionalDTO(regionalService.findOne(id));
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(regionaldto));
	}

	@GetMapping("/regional-por-usuario/{usuarioId}")
	@Timed
	public ResponseEntity<List<Regional>> getRegionalPorUsuario(@PathVariable Long usuarioId) {
		log.debug("REST request to get Regional : {}", usuarioId);
		List<Regional> regional = regionalService.findByUsuario(usuarioId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(regional));
	}


	@GetMapping("/regionais-ativas")
	@Timed
	public ResponseEntity<List<RegionalDTO>> getAllRegionaisAtivas() {
		log.debug("REST request to get a page of Regionais");
		List<RegionalDTO> list = RegionalDTO.map(regionalService.findAllActive());
		return new ResponseEntity<>(list, null, HttpStatus.OK);
	}

	@GetMapping("/regionais-todas")
	@Timed
	public ResponseEntity<List<ThinRegionalDTO>> getAllRegionais() {
		log.debug("REST request to get a page of Regionais");
		List<Regional> list;
		/*if (SecurityUtils.isCurrentUserWithAuthority(AuthoritiesConstants.ROLE_REGIONAL)) {
			Regional reg = regionalService.findByLogin(SecurityUtils.getCurrentUserLogin());
			list = new ArrayList<>();
			if (reg != null) {
				list.add(reg);
			}
		} else {
		*/
			list = regionalService.findAll();
		//}
		List<ThinRegionalDTO> listdto = ThinRegionalDTO.map(list);
		return new ResponseEntity<>(listdto, null, HttpStatus.OK);
	}

    @GetMapping("/comissoes")
    @Timed
    public ResponseEntity<List<ComissaoRegionalDTO>> getComissoes() {
        log.debug("REST request to get a page of comissoes");
        List<Regional> lst = regionalService.findAllActive();
        List<ComissaoRegionalDTO> listdto = ComissaoRegionalDTO.map(lst);
        return new ResponseEntity<>(listdto, null, HttpStatus.OK);
    }

    @PutMapping("/comissoes")
    @Timed
    @Transactional
    public  ResponseEntity<ComissaoListaRegionalDTO>  updateComissao(@Valid @RequestBody ComissaoListaRegionalDTO dto) {
        log.debug("REST request to get a page of comissoes");
        for(Long id : dto.getRegionalIds()) {
            Regional reg = regionalService.findOne(id);
            reg.setValorComissaoEstabelecimenotFaixa1(dto.getValorComissaoEstabelecimenotFaixa1());
            reg.setValorComissaoEstabelecimenotFaixa2(dto.getValorComissaoEstabelecimenotFaixa2());
            reg.setValorComissaoEstabelecimenotFaixa3(dto.getValorComissaoEstabelecimenotFaixa3());
            reg.setValorComissaoRegionalFaixa1(dto.getValorComissaoRegionalFaixa1());
            reg.setValorComissaoRegionalFaixa2(dto.getValorComissaoRegionalFaixa2());

            regionalRepository.save(reg);
        }
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert( ENTITY_NAME, "'Regionais selecionadas'"))
            .body(dto);
    }


}
