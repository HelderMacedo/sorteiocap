package br.sorteioapp.web.rest;

import br.sorteioapp.domain.TipoBilhete;
import br.sorteioapp.repository.TipoBilheteRepository;
import br.sorteioapp.web.rest.util.PaginationUtil;
import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class TipoBilheteResource {

    private final Logger log = LoggerFactory.getLogger(ConcursoResource.class);

    private static final String ENTITY_NAME = "tipobilhete";

    private final TipoBilheteRepository bilheteRepository;

    public TipoBilheteResource(TipoBilheteRepository bilheteRepository) {
        this.bilheteRepository = bilheteRepository;
    }

    @GetMapping("/tiposbilhete")
    @Timed
    public ResponseEntity<List<TipoBilhete>> getAllConcursos() {
        log.debug("REST request to get a page of Concursos");
        List<TipoBilhete> lst = bilheteRepository.findAllActive();
        return new ResponseEntity<>(lst, null, HttpStatus.OK);
    }
}
