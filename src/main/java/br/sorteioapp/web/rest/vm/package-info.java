/**
 * View Models used by Spring MVC REST controllers.
 */
package br.sorteioapp.web.rest.vm;
