/**
 * Data Access Objects used by WebSocket services.
 */
package br.sorteioapp.web.websocket.dto;
