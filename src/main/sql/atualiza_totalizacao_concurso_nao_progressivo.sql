CREATE TRIGGER `armazena_hist_totalizacao_comissao_concurso_naoprogressivo` AFTER UPDATE ON `concurso` FOR EACH ROW 
begin
	   IF OLD.sincronizado = FALSE and new.sincronizado = TRUE THEN
      -- Grava comissoes das regionais
      insert into  historico_comissao_regional (concurso_id, faixa_inicio, faixa_fim, regional_id, regional_nome, valor_comissao, timestamp)
        select conc.concurso_id, 1, (new.lim_faixa_comissao_reg1 - 1), reg.regional_id, reg.nome, reg.val_comissao_reg_faixa1, now()
        from concurso conc, regional reg, tipobilhete tip
        where conc.tipo_id = tip.tipo_id and conc.concurso_id = new.concurso_id and conc.`Sincronizado` = True;
      insert into  historico_comissao_regional (concurso_id, faixa_inicio, faixa_fim, regional_id, regional_nome, valor_comissao, timestamp)
        select conc.concurso_id, new.lim_faixa_comissao_reg1, 1000000, reg.regional_id, reg.nome, reg.val_comissao_reg_faixa2, now()
        from concurso conc, regional reg, tipobilhete tip
        where conc.tipo_id = tip.tipo_id and conc.concurso_id = new.concurso_id and conc.`Sincronizado` = True;

        -- Grava comissoes dos estabelecimentos
      insert into  historico_comissao_estabelecimento (concurso_id, faixa_inicio, faixa_fim, regional_id, regional_nome, estabelecimento_id, estabelecimento_nome, 
                                                 estabelecimento_bairro, estabelecimento_cidade, estabelecimento_uf, valor_comissao, timestamp)
        select conc.concurso_id, 1, (new.lim_faixa_comissao_est1 - 1), reg.regional_id, reg.nome, est.estabelecimento_id, est.nome, 
                est.endereco_bairro, est.endereco_cidade, est.endereco_uf, reg.val_comissao_est_faixa1, now()
        from concurso conc, regional reg, tipobilhete tip, estabelecimento est
        where conc.tipo_id = tip.tipo_id and reg.regional_id = est.regional_id and conc.concurso_id = new.concurso_id and conc.`Sincronizado` = True;
      insert into  historico_comissao_estabelecimento (concurso_id, faixa_inicio, faixa_fim, regional_id, regional_nome, estabelecimento_id, estabelecimento_nome, 
                                                 estabelecimento_bairro, estabelecimento_cidade, estabelecimento_uf, valor_comissao, timestamp)
        select conc.concurso_id, lim_faixa_comissao_est1, (new.lim_faixa_comissao_est2 - 1), reg.regional_id, reg.nome, est.estabelecimento_id, est.nome, 
                est.endereco_bairro, est.endereco_cidade, est.endereco_uf, reg.val_comissao_est_faixa2, now()
        from concurso conc, regional reg, tipobilhete tip, estabelecimento est
        where conc.tipo_id = tip.tipo_id and reg.regional_id = est.regional_id and conc.concurso_id = new.concurso_id and conc.`Sincronizado` = True;
      insert into  historico_comissao_estabelecimento (concurso_id, faixa_inicio, faixa_fim, regional_id, regional_nome, estabelecimento_id, estabelecimento_nome, 
                                                 estabelecimento_bairro, estabelecimento_cidade, estabelecimento_uf, valor_comissao, timestamp)
        select conc.concurso_id, new.lim_faixa_comissao_est2, 1000000, reg.regional_id, reg.nome, est.estabelecimento_id, est.nome, 
                est.endereco_bairro, est.endereco_cidade, est.endereco_uf, reg.val_comissao_est_faixa3, now()
        from concurso conc, regional reg, tipobilhete tip, estabelecimento est
        where conc.tipo_id = tip.tipo_id and reg.regional_id = est.regional_id and conc.concurso_id = new.concurso_id and conc.`Sincronizado` = True;


        
        -- Grava totalizacoes dos estabelecimentos
        delete from totalizacao_concurso_estabelecimento where concurso_id = new.concurso_id;        
        insert into totalizacao_concurso_estabelecimento  (regional_nome, regional_id, estabelecimento_id, estabelecimento_nome, estabelecimento_bairro, estabelecimento_cidade,estabelecimento_uf, 
                                                            concurso_id, `timestamp`, total_distribuidos, total_validados, valor_total_arrecadado, valor_total_comissao) 
        select reg.nome, reg.regional_id, est.estabelecimento_id, est.nome, est.endereco_bairro, est.endereco_cidade, est.endereco_uf, conc.concurso_id, now(),
          totbil.distribuidos, totbil.validados,  (totbil.validados*tip.valor) as total_arrecadacao, 
          round((coalesce(totbil.validados * tip.valor , 0) * est.percentualrepasse)/100.0, 2) as total_comissao
        from regional reg
         join estabelecimento est on est.regional_id = reg.regional_id
         join 
	        (select concurso_id, regional_id, estabelecimento_id, count(validado=true) as validados, count(numero) as distribuidos
	        from bilhete where  estabelecimento_id is not null and concurso_id = new.concurso_id
	        group by concurso_id, regional_id, estabelecimento_id) totbil on reg.regional_id = totbil.regional_id and est.estabelecimento_id = totbil.estabelecimento_id
         join concurso conc on conc.concurso_id = totbil.concurso_id 
         join tipobilhete tip on tip.tipo_id = conc.tipo_id
         where conc.`Sincronizado` = True;

        -- Grava totalizacoes das regionais
        delete from totalizacao_concurso_regional where concurso_id = new.concurso_id;
        insert into totalizacao_concurso_regional  (regional_nome, regional_id, concurso_id, `timestamp`, total_distribuidos, total_validados, valor_total_arrecadado, valor_total_comissao, valor_total_faturamento) 
        select nome, regional_id, concurso_id, timestamp, distribuidos, validados, total_arrecadacao, total_comissao, (total_arrecadacao - total_comissao) as total_faturamento
        from (
          select reg.nome, reg.regional_id, conc.concurso_id, now() as timestamp,
            totbil.distribuidos, totbil.validados,  (totbil.validados*tip.valor) as total_arrecadacao, 
              round((totbil.validados*tip.valor * reg.percentualrepasse)/100.0, 2) as total_comissao 
          from regional reg
          join 
            (select concurso_id, regional_id, count(validado=true) as validados, count(numero) as distribuidos
            from bilhete where  estabelecimento_id is not null and concurso_id = new.concurso_id
            group by concurso_id, regional_id) totbil on reg.regional_id = totbil.regional_id
          join concurso conc on conc.concurso_id = totbil.concurso_id 
          join tipobilhete tip on tip.tipo_id = conc.tipo_id
          where conc.`Sincronizado` = True
        ) w;

   END IF;
END
