(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
