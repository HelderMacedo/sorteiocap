(function () {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('DistribuicaoDialogControllerAreaRegional', DistribuicaoDialogControllerAreaRegional);

    DistribuicaoDialogControllerAreaRegional.$inject = ['$timeout', '$scope', '$stateParams', '$http', '$cookies', '$uibModalInstance',
        'entity', 'Concurso', 'DistribuicaoBilhetes', 'AlertService'];

    function DistribuicaoDialogControllerAreaRegional($timeout, $scope, $stateParams, $http, $cookies, $uibModalInstance,
        entity, Concurso, DistribuicaoBilhetes, AlertService) {
        var vm = this;

        vm.loading = true;
        vm.lotedistribuicao = entity;
        vm.lotedistribuicao.lista = [];
        vm.concursos = [];
        vm.formlote = {};
        vm.lotes = [];
        vm.alerts = [];
        vm.clear = clear;
        vm.save = save;
        vm.changeconcurso = changeconcurso;
        vm.changeBilhete = changeBilhete;

        function positionFirstInput() {
            $timeout(function () {
                angular.element('.form-group:eq(1)>input').focus();
            });
        };
        positionFirstInput();

        Concurso.querynaosinc().$promise.then(function (res) {
            vm.concursos = res;
            console.log(vm.lotedistribuicao.regional);
            if (vm.concursos.length > 0) {
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });

        function changeconcurso() {
            salvaConncursoCookie();
            vm.lotedistribuicao.bilheteInicial = null;
            vm.lotedistribuicao.bilheteFinal = null;
            vm.loading = false;
        }

        function changeBilhete() {
            if (vm.lotedistribuicao.bilheteInicial && vm.lotedistribuicao.bilheteInicial.trim().length > 0) {
                vm.lotedistribuicao.bilheteInicial = formatIdentificacaoBilhete(vm.lotedistribuicao.bilheteInicial);
            }
            if (vm.lotedistribuicao.bilheteFinal && vm.lotedistribuicao.bilheteFinal.trim().length > 0) {
                vm.lotedistribuicao.bilheteFinal = formatIdentificacaoBilhete(vm.lotedistribuicao.bilheteFinal);
            }
        }

        function formatIdentificacaoBilhete(element) {
            console.log("Bil.A: " + element);
            element = element.replace(/\D/g, '').trim();
            element = ('0000000000000' + element).substring(element.length, element.length + 13);
            element = element.substring(0, 11) + "-" + element.substring(11, 13);
            console.log('Bil.D: ' + element);
            return element;
        }

        function addErrorAlert(mensagem) {
            vm.alerts.push({ type: 'danger', msg: mensagem });
            $timeout(function () {
                vm.closeAlert(vm.alerts.length - 1);
            }, 5000);
        };

        vm.closeAlert = function (index) {
            vm.alerts.splice(index, 1);
        };

        function clear() {
            $uibModalInstance.dismiss('cancel');
        }

        function save() {
            vm.isSaving = true;
            changeBilhete();
            DistribuicaoBilhetes.distribuicaoSubRegional({
                id: null,
                regionalId : (vm.lotedistribuicao.regional.id)? vm.lotedistribuicao.regional.id : null,
                concursoId: (vm.lotedistribuicao.concurso)? vm.lotedistribuicao.concurso.id : null,
                areaId: vm.lotedistribuicao.id,
                bilheteInicial : vm.lotedistribuicao.bilheteInicial,
                bilheteFinal : vm.lotedistribuicao.bilheteFinal
            }, onSaveSuccess, onSaveError);
        }

        function onSaveSuccess(result) {
            // $scope.$emit('sorteioApp:geracaoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            AlertService.success('Lote de venda criado ');
        }

        function onSaveError(response) {
            vm.isSaving = false;
            var msg = 'Houve um erro na criação do lote de venda de bilhetes!';
            if (response.status == 400) {
                var errorkey = response.headers('X-sorteioApp-error');
                var errorparam = response.headers('X-sorteioApp-params');

                if (errorkey === 'error.bilheteinvalido') {
                    msg = 'Bilhete inválido: ' + errorparam;
                } else if (errorkey === 'error.periodoinvalido') {
                    msg = 'Faixa não disponível! Verifique se existem bilhetes entregues a outra regional.';
                } else if (errorkey === 'error.sempermissao') {
                    msg = 'O usuário não tem permissão para alterar todos os bilhetes deste intervalo.';
                } else if (errorkey === 'error.bilhetejavalidado') {
                    msg = 'Existem bilhetes validados no intervalo informado.';
                } else if (errorkey === 'error.bilhetenaocorrespondeconcurso') {
                    msg = 'Bilhete ' + errorparam + ' não corresponde ao concurso selecionado.';
                } else if (errorkey === 'error.faixapertenceoutrolote') {
                    msg = 'Parte dos bilhetes já atribuidos a um estabelecimento diferente.';
                } else if (errorkey === 'error.loteOutraRegional') {
                    msg = 'Parte dos bilhetes já atribuidos a uma outra regional.';
                }
            }
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: msg
            })
        }

        function selecionaConcursoPadrao() {
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie) {
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.lotedistribuicao.concurso = vm.concursos[(idx < 0) ? 0 : idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.lotedistribuicao.concurso);
        }

    }
})();