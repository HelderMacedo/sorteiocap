(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('arearegional', {
            parent: 'entity',
            url: '/arearegional?page&sort',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL','PERM_CONSULTAR_AREA_REGIONAL'],
                pageTitle: 'sorteioApp.arearegional.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/arearegional/arearegional.html',
                    controller: 'AreaRegionalController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'nome,asc',
                    squash: true
                },
                filtro: {nome: null, bairro: null, uf:null, cidade: null, telefone: null, regional : null }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }], 
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('arearegional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('arearegional-detail', {
            url: '/arearegional/{id}',
            parent: 'entity',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL','PERM_CONSULTAR_AREA_REGIONAL'],
                pageTitle: 'sorteioApp.arearegional.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/arearegional/arearegional-detail.html',
                    controller: 'AreaRegionalDetailController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'loteRegionalId,asc',
                    squash: true
                },
                filtro: {concurso : null }
            },
            resolve: {
                pagingParams: ['$state', '$stateParams', 'PaginationUtil', function ($state, $stateParams, PaginationUtil) {
                    //if ($state.current.name === 'arearegional-detail') 
                     return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                    //else return {page: 1, sort: 'id,asc'}
                }],                 
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('arearegional');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'AreaRegional', function($stateParams, AreaRegional) {
                    return AreaRegional.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'arearegional',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        }).state('arearegional.distribuicao', {
                parent: 'arearegional',
                url: '/{id}/distribuicao',
                data: {
                    authorities: ['PERM_CADASTRO_AREA_REGIONAL']
                },
                params: {id: null, regional: null},
                onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                    $uibModal.open({
                       templateUrl:'app/entities/arearegional/arearegional-distribuicao.html',
                       controller: 'DistribuicaoDialogControllerAreaRegional',
                       controllerAs: 'vm',
                       backdrop: 'static',
                       size: 'lg',
                       resolve: {
                          entity: ['AreaRegional', function(AreaRegional){
                              return AreaRegional.get({id : $stateParams.id}).$promise;
                          }]
                       }
                    }).result.then(function() {
                        $state.go('arearegional', null, { reload: 'arearegional',  notify: false } );
                    }, function() {
                        $state.go('^');
                    });
                }]
            }
        ).state('arearegional-detail.edit', {
            parent: 'arearegional-detail',
            url: '/detail/edit',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arearegional/arearegional-dialog.html',
                    controller: 'AreaRegionalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AreaRegional', function(AreaRegional) {
                            return AreaRegional.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('arearegional.new', {
            parent: 'arearegional',
            url: '/new',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arearegional/arearegional-dialog.html',
                    controller: 'AreaRegionalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                cpf: null,
                                nome: null,
                                email: null,
                                telefone: null,
                                logradouro: null,
                                numero: null,
                                bairro: null,
                                cidade: null,
                                uf:null,
                                cep: null,
                                pontoReferencia: null,
                                ativo: true,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('arearegional', null, { reload: 'arearegional' });
                }, function() {
                    $state.go('arearegional');
                });
            }]
        })
        .state('arearegional.edit', {
            parent: 'arearegional',
            url: '/{id}/edit',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arearegional/arearegional-dialog.html',
                    controller: 'AreaRegionalDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['AreaRegional', function(AreaRegional) {
                            return AreaRegional.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('arearegional', null, { reload: 'arearegional' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('arearegional.delete', {
            parent: 'arearegional',
            url: '/{id}/delete',
            data: {
                authorities: ['PERM_CADASTRO_AREA_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/arearegional/arearegional-delete-dialog.html',
                    controller: 'AreaRegionalDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['AreaRegional', function(AreaRegional) {
                            return AreaRegional.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('arearegional', null, { reload: 'arearegional' });
                }, function() {
                    $state.go('^');
                });
            }] 
        })
        ;        
    }

})();
