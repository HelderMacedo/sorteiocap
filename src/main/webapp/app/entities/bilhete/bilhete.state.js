(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('validacao-bilhete', {
            parent: 'bilhete',
            url: '/validacao?page&sort',
            data: {
                authorities: ['PERM_VALIDAR_BILHETES'],
                pageTitle: 'sorteioApp.validacao.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bilhete/bilhete-validacao.html',
                    controller: 'ValidacaoBilheteDialogController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],                
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('validacaobilhete');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('status-bilhetes', {
            parent: 'bilhete',
            url: '/status-bilhetes?page&sort',
            data: {
                authorities: ['PERM_CONFECCAO_LOTES'],
                pageTitle: 'sorteioApp.status-bilhetes.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bilhete/consulta_bilhete.html',
                    controller: 'ConsultaStatusBilhetesController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                filtro: {concursoid: null, numinicio:null, numfim:null }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('loteregional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })   
        .state('status-bilhetes-dist', {
            parent: 'bilhete',
            url: '/status-bilhetes-dist?page&sort',
            data: {
                authorities: ['PERM_DISTRIBUICAO_ESTABELECIMENTO'],
                pageTitle: 'sorteioApp.status-bilhetes-dist.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bilhete/consulta_bilhete_dist.html',
                    controller: 'ConsultaStatusBilhetesDistribuidosController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                filtro: {concursoid: null, numinicio:null, numfim:null }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('loteregional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('status-lotesvalidacao', {
            parent: 'bilhete',
            url: '/status-lotevalidacao?page&sort',
            data: {
                authorities: ['PERM_DISTRIBUICAO_ESTABELECIMENTO'],
                pageTitle: 'sorteioApp.status-bilhetes-dist.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/bilhete/consulta_lotesvalidacao.html',
                    controller: 'ConsultaStatusLoteValidacaoController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'lotevalidacao,asc',
                    squash: true
                },
                filtro: {concursoid: null, regionalid:null }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('loteregional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })                        
        ;
    }

})();
