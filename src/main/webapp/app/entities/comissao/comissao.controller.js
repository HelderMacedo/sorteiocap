(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('ComissaoController', ComissaoController);

        ComissaoController.$inject = ['$scope','$state', 'Principal', 'Comissao', 'AlertService'];

    function ComissaoController($scope, $state, Principal, Comissao, 
                      AlertService) {
        var vm = this;

        vm.regionaiscomissoes = [];
        vm.reset = reset;
        vm.startEdit = startEdit;
        vm.save = save;
        vm.toggleAll = toggleAll;
        vm.oneSelected = oneSelected;
        vm.onedit = false;
        vm.comissaoregionaledicao = {};
        vm.allSelected = false;

        reset();
        
        function loadAll () {
            Comissao.query({}, onSuccess, onError);

            function onError(error) {
                AlertService.error(error.data.message);
            }
            function onSuccess(data, headers) {
                vm.regionaiscomissoes = data;
            }
        }
        function save () {
            console.log("alterando");
            var regionalIds = [];
            for(var j=0;j<vm.regionaiscomissoes.length;j++){
                if(vm.regionaiscomissoes[j].onedit){
                    regionalIds.push(vm.regionaiscomissoes[j].regional.id);
                }            
            }
            var obj = {
                'regionalIds': regionalIds,
                'valorComissaoRegionalFaixa1': vm.comissaoregionaledicao.valorComissaoRegionalFaixa1,
                'valorComissaoRegionalFaixa2': vm.comissaoregionaledicao.valorComissaoRegionalFaixa2,
                'valorComissaoEstabelecimenotFaixa1': vm.comissaoregionaledicao.valorComissaoEstabelecimenotFaixa1,
                'valorComissaoEstabelecimenotFaixa2': vm.comissaoregionaledicao.valorComissaoEstabelecimenotFaixa2,
                'valorComissaoEstabelecimenotFaixa3': vm.comissaoregionaledicao.valorComissaoEstabelecimenotFaixa3
            };

            Comissao.update(obj, onSuccess, onError);

            function onError(error) {
                AlertService.error(error.data.message);
            }
            function onSuccess(data, headers) {
                vm.regionaiscomissoes = data;
                reset();
            }
        }

        function toggleAll(){
            var bool = true;
            if (vm.allSelected){
                bool = false;
            } 
            for(var j=0;j<vm.regionaiscomissoes.length;j++){
                vm.regionaiscomissoes[j].onedit = !bool;
                vm.allSelected = !bool;
            }
        }
        function oneSelected(){
            vm.allSelected = true;
            for(var j=0;j<vm.regionaiscomissoes.length;j++){
                if(!vm.regionaiscomissoes[j].onedit){
                    vm.allSelected = false;                    
                }
            }
        }

        function startEdit(){
            vm.onedit = false;
            for(var j=0;j<vm.regionaiscomissoes.length;j++){
                if(vm.regionaiscomissoes[j].onedit && !vm.onedit){
                    vm.onedit = true;                    
                    vm.comissaoregionaledicao = JSON.parse(JSON.stringify(vm.regionaiscomissoes[j]));
                    vm.comissaoregionaledicao.regional.id = null;
                    vm.comissaoregionaledicao.regional.nome = 'Rascunho';
                    break;
                }
            }            
        }

        function reset () {
            vm.regionaiscomissoes = [];
            vm.onedit = false;
            vm.allSelected = false;
            vm.comissaoregionaledicao = {};
            loadAll();
        }
    }
})();
