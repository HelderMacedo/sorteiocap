(function() {
    'use strict';
    angular
        .module('sorteioApp')
        .factory('Comissao', Comissao);

        Comissao.$inject = ['$resource'];

    function Comissao ($resource) {
        var urlResource =  'api/comissoes';

        var resourcePrincipal = $resource(urlResource, {}, {
            'query': { method: 'GET', isArray: true},
            'update': { method:'PUT' }
        });

        return {
            query  : resourcePrincipal.query,
            update : resourcePrincipal.update
        }        
    }
})();
