(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('comissao', {
            parent: 'entity',
            url: '/comissao',
            data: {
                authorities: ['PERM_EDICAO_COMISSOES'],
                pageTitle: 'sorteioApp.comissao.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/comissao/comissoes.html',
                    controller: 'ComissaoController',
                    controllerAs: 'vm'
                }
            },params: {
                
            },
            resolve: {            
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('regional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        ;
    }

})();
