(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('ConcursoDeleteController',ConcursoDeleteController);

    ConcursoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Concurso'];

    function ConcursoDeleteController($uibModalInstance, entity, Concurso) {
        var vm = this;

        vm.concurso = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Concurso.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
