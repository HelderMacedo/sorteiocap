(function () {
    "use strict";

    angular
        .module("sorteioApp")
        .controller(
            "DistribuicaoBilhetesOnlineController",
            DistribuicaoBilhetesOnlineController
        );

    DistribuicaoBilhetesOnlineController.$inject = [
        "$timeout",
        "$scope",
        "$stateParams",
        "$http",
        "$cookies",
        "Concurso",
        "Regional",
        "Estabelecimento",
        "AreaRegional",
        "AlertService",
        "DistribuicaoBilhetesOnlineService",
        "Principal",
    ];

    function DistribuicaoBilhetesOnlineController(
        $timeout,
        $scope,
        $stateParams,
        $http,
        $cookies,
        Concurso,
        Regional,
        Estabelecimento,
        AreaRegional,
        AlertService,
        DistribuicaoBilhetesOnlineService,
        Principal
    ) {
        var vm = this;

        vm.concursos = [];
        vm.regionais = [];
        vm.areasregional = [];
        vm.estabelecimentos = [];
        vm.alerts = [];
        vm.bilhetes = [];
        vm.lote = {};
        vm.changeregional = changeRegional;
        vm.changeArea = changeArea;
        vm.distribuir = distribuir;
        vm.account = null;
        vm.disabledArea = false;

        Principal.identity().then(function (account) {
            vm.account = account;
        });

        function positionFirstInput() {
            $timeout(function () {
                angular.element("#quantidade_bilhete").focus();
            });
        }
        positionFirstInput();
        Concurso.querynaosinc().$promise.then(function (res) {
            vm.concursos = res;
            if (vm.lote.concurso == null) {
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });

        function changeRegional() {
            if (vm.lote.regional && vm.lote.regional.id) {
                AreaRegional.queryPorRegional({
                    regionalId: vm.lote.regional.id,
                }).$promise.then(function (res) {
                    vm.areasregional = _.orderBy(res, ["nome"], ["asc"]);

                    if (vm.areasregional.length == 1) {
                        vm.lote.areasregional = vm.areasregional[0];
                    }

                    if (
                        vm.areasregional.length == 1 &&
                        vm.account.roles.includes("ROLE_AREA_REGIONAL")
                    ) {
                        vm.disabledArea = true;
                    }

                    changeArea();
                });
            } else {
                changeArea();
                vm.areasregional = [];
                vm.estabelecimentos = [];
            }
        }

        function changeArea() {
            if (vm.lote.regional && vm.lote.regional.id) {
                var areaId = vm.lote.areasregional
                    ? vm.lote.areasregional.id
                    : null;
                Estabelecimento.queryOnlinePorRegionalEArea({
                    regionalId: vm.lote.regional.id,
                    areaId: areaId,
                    isOnline: true,
                }).$promise.then(function (res) {
                    vm.estabelecimentos = _.orderBy(res, ["nome"], ["asc"]);
                });
            } else {
                vm.estabelecimentos = [];
            }
        }

        Regional.queryAtivos().$promise.then(function (res) {
            vm.regionais = _.orderBy(res, ["nome"], ["asc"]);
            if (vm.regionais.length === 1) {
                vm.lote.regional = vm.regionais[0];
            }

            changeRegional();
        });

        function distribuir() {
            if (alertFieldRequired(vm.lote)) {
                var data = {
                    concurso_id: vm.lote.concurso.id,
                    quantidade: vm.lote.quantidade,
                    regional_id: vm.lote.regional.id,
                    area_id: vm.lote.areasregional
                        ? vm.lote.areasregional.id
                        : null,
                    estabelecimento_id: vm.lote.estabelecimento
                        ? vm.lote.estabelecimento.id
                        : null,
                };

                DistribuicaoBilhetesOnlineService.queryDistribuirOnline(data)
                    .$promise.then(function (res) {
                        vm.bilhetes = res;
                        AlertService.success(
                            "Distribuição realizada com sucesso!"
                        );
                    })
                    .catch(function (error) {
                        vm.bilhetes = [];
                        var errorMessage =
                            error.headers("X-sorteioApp-error") ||
                            "Erro desconhecido";
                        AlertService.error(errorMessage);
                    });
            }
        }

        function alertFieldRequired(fields) {
            if (fields.concurso.id === null) {
                AlertService.error("O campo concurso é obrigatório.");

                return false;
            } else if (fields.quantidade === null || fields.quantidade <= 0) {
                AlertService.error("A quantidade de bilhetes é obrigatória.");

                return false;
            } else if (fields.regional.id === null) {
                AlertService.addAlert("A regional é obrigatória.");

                return false;
            }

            return true;
        }

        function changeconcurso() {
            salvaConncursoCookie();
            vm.lote.quantidade = null;
        }

        function selecionaConcursoPadrao() {
            var concursoCookie = $cookies.getObject("concursoSelecionado");
            var idx = 0;
            if (concursoCookie) {
                idx = _.findIndex(vm.concursos, ["id", concursoCookie.id]);
            }
            vm.lote.concurso = vm.concursos[idx < 0 ? 0 : idx];
        }

        function salvaConncursoCookie() {
            $cookies.putObject("concursoSelecionado", vm.lote.concurso);
        }
    }
})();
