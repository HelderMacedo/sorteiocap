(function () {
    'use strict';

    angular.module('sorteioApp').factory('DistribuicaoBilhetesOnlineService', DistribuicaoBilhetesOnlineService);

    DistribuicaoBilhetesOnlineService.$inject = ['$resource'];

    function DistribuicaoBilhetesOnlineService($resource) {
        var url = 'api/distribuicaoBilhetes/online';

        var resourceDistribuirOnline = $resource(url, {}, {
            'post': {
                method: 'POST',
                isArray: true
            }
        });

        return {
            queryDistribuirOnline: resourceDistribuirOnline.post
        };
    }
})();