(function () {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('distribuicao-bilhetes-online', {
            parent: 'bilhete',
            url: '/distribuicao-bilhetes-online',
            data: {
                pageTitle: 'Distribuicao Bilhetes Online'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribuicao-bilhetes-online/distribuicao-bilhetes-online.html',
                    controller: 'DistribuicaoBilhetesOnlineController',
                    controllerAs: 'vm'
                }
            }
        });
    }
})();