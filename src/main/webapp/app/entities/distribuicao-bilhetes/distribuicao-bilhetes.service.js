(function() {
    'use strict';
    angular
        .module('sorteioApp')
        .factory('DistribuicaoBilhetes', DistribuicaoBilhetes);

        DistribuicaoBilhetes.$inject = ['$resource'];

    function DistribuicaoBilhetes ($resource) {
        var urlResourceDistribuicaoBilheteNumeracao = 'api/distribuicaobilhetes/bilhete/:concursoId/:numero'; 
        var urlResourceDistribuicaoBilhetes = 'api/distribuicaobilhetes/:concursoId/:regionalId';
        var urlResourceLotesRegionais = 'api/distribuicaobilhetes/loteregionais';
        var urlResourceSubRegional = 'api/distribuicaobilhetes/subregional';
        var urlResourceLotesEstabelecimentos = 'api/distribuicaobilhetes/loteestabelecimentos';        
        var urlResourceTotalizacaoTodasRegionais = 'api/distribuicaobilhetes/loteregionais-totalizacao/:concursoId';
        var urlResourceTotalizacaoRegional = 'api/distribuicaobilhetes/loteregionais-totalizacao/:concursoId/:regionalId';
        var urlResourceTendenciaTotalizacaoRegional = 'api/distribuicaobilhetes/loteregionais-tendencia/:regionalId';
        var urlResourceTendenciaTotalizacaoRegionalPeriodo = 'api/distribuicaobilhetes/loteregionais-tendencia-periodo/:regionalId';
        var urlResourceTotalizacaoEstabelecimentos = 'api/distribuicaobilhetes/loteestabelecimentos-totalizacao/:concursoId/:regionalId/:areaId';
        var urlResourceDistribuicaoRegional = 'api/distribuicaobilhetes/loteregionais/:concursoId/:regionalId';
        var urlResourceGetDistribuicaoSubRegional = 'api/distrbuicaobilhetes/lotesubregional';
        var urlResourceDistribuicaoDisponivelRegional = 'api/distribuicaobilhetes/loteestabelecimentos-disponiveis/:concursoId/:regionalId';        
        //var urlResourceDistribuicaoEstabelecimentoPorRegional = "api/distribuicaobilhetes/loteestabelecimentos-reg/:concursoId/:regionalId/:areaId/:estabelecimentoId";
        var urlResourceDistribuicaoEstabelecimentoPorRegional = "api/distribuicaobilhetes/relatorioestabelecimentos";
        var urlResourceDistribuicaoEstabelecimentoPorEstabelecimento = "api/distribuicaobilhetes/loteestabelecimentos-est/:concursoId/:estabelecimentoId";
        var urlResourceConcurso = 'api/regionalbilheteonline/:concursoId';
        var urlResource = 'api/regionalbilheteonline/:concursoId/:regionalId';    
        var urlResourceEst = 'api/estabelecimentobilheteonline/:concursoId/:regionalId/:isOnline';
        var urlResourceArea = 'api/estabelecimentobilheteonline/:concursoId/:regionalId/:areaId/:isOnline';

        var resourceLoteRegional = $resource(urlResourceLotesRegionais, {}, {
            'query': {method: 'GET', isArray: true},
            'save':  {method:'POST', isArray: false}
        });

        var resourceSubRegional = $resource(urlResourceSubRegional, {}, {
            'save': {method:'POST', isArray: false}
        })

        var resourceLoteEstabelecimento = $resource(urlResourceLotesEstabelecimentos, {}, {
            'query': {method: 'GET', isArray: true},
            'save':  {method:'POST', isArray: false}
        });

        function getQueryResurce(url){
            return $resource(url, {}, {
                    'query': {method: 'GET', isArray: true}
                }).query;
        }
        
        var resourcePrincipal = $resource(urlResource, {}, {
            'query' : { method: 'GET', isArray: true }
        });

        var resourceConcurso = $resource(urlResourceConcurso, {}, {
            'query' : { method: 'GET', isArray: true }
        });

        var resourcePrincipall = $resource(urlResourceEst, {}, {
            'query' : { method: 'GET', isArray: true }
        });

        var resourceArea = $resource(urlResourceArea, {}, {
            'query' : { method: 'GET', isArray: true }
        });

        var resouceGetDistribuicaoSubRegional = $resource(urlResourceGetDistribuicaoSubRegional, {},{
            'get':  { method: 'GET', isArray: true }
        });
        
        return {
            queryLogDistribuicaoBilhetePorNumeracao: getQueryResurce(urlResourceDistribuicaoBilheteNumeracao),
            queryLogDistribuicaoBilhetes: getQueryResurce(urlResourceDistribuicaoBilhetes),            
            queryRegional : resourceLoteRegional.query,
            distribuiRegional : resourceLoteRegional.save,
            distribuicaoSubRegional: resourceSubRegional.save,
            queryEstabelecimento : resourceLoteEstabelecimento.query,
            queryGetDistribuicaoSubRegional: resouceGetDistribuicaoSubRegional.get,
            distribuiEstabelecimento : resourceLoteEstabelecimento.save, //DISTRIBUIÇÃO DE BILHETES ESTABELECIMENTO
            queryDistribuicaoRegional: getQueryResurce(urlResourceDistribuicaoRegional),//DISTRIBUIÇÃO DE BILHETES REIONAL             
            queryDistribuicaoDisponivel: getQueryResurce(urlResourceDistribuicaoDisponivelRegional),                                                       
            queryDistribuicaoEstabelecimentoPorRegional: getQueryResurce(urlResourceDistribuicaoEstabelecimentoPorRegional),
            queryDistribuicaoEstabelecimentoPorEstabelecimento: getQueryResurce(urlResourceDistribuicaoEstabelecimentoPorEstabelecimento),
            queryTotalizacaoRegional: $resource(urlResourceTotalizacaoRegional, {}, {
                                        'query': {method: 'GET', isArray: false}
                                    }).query,   
            queryTotalizacaoTodasRegional: $resource( urlResourceTotalizacaoTodasRegionais, {}, {
                                        'query': {method: 'GET', isArray: true}
                                    }).query,
            queryTendenciaTotalizacaoRegional: $resource(urlResourceTendenciaTotalizacaoRegional, {}, {
                                        'query': {method: 'GET', isArray: true}
                                    }).query,                                     
            queryTendenciaTotalizacaoRegionalPeriodo: $resource(urlResourceTendenciaTotalizacaoRegionalPeriodo, {}, {
                                        'query': {method: 'GET', isArray: true}
                                    }).query,                                                                                                                                                   
            queryTotalizacaoEstabelecimentos: $resource( urlResourceTotalizacaoEstabelecimentos, {}, {
                                        'query': {method: 'GET', isArray: false}
                                    }).query ,
            queryRegionalBilheteOnline: resourcePrincipal.query,
            queryRegionalBilheteOnlineConcurso: resourceConcurso.query,
            queryEstabelecimentoBilheteOnline: resourcePrincipall.query,
            queryEstabelecimentoBilheteArea: resourceArea.query
        }

    }
})();
