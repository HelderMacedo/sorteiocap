(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('DistribuicaoSelosController', DistribuicaoSelosController);

        DistribuicaoSelosController.$inject = ['$timeout', '$scope', '$stateParams',  '$http', '$cookies',
       'Concurso', 'Regional', 'Estabelecimento', 'AlertService', 'DistribuicaoSelos'];

    function DistribuicaoSelosController ($timeout, $scope, $stateParams,  $http, $cookies,
        Concurso, Regional, Estabelecimento, AlertService, DistribuicaoSelos) {
        var vm = this;

        vm.concursos = [];
        vm.lastValidations = [];
        vm.alerts = [];        
        vm.limpar = limpar;
        vm.distribuir = distribuir;
        vm.buscar = buscar;
        vm.changeconcurso = changeconcurso;
        vm.changeregional = changeregional;
        vm.changenumero = changenumero;
        vm.lote = {};
        limpar();

        function positionFirstInput(){
          $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
          });
        };

        Concurso.querynaosinc().$promise.then(function(res){
            vm.concursos = res;
            if(vm.lote.concurso == null){
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });
        Regional.queryAtivos().$promise.then(function(res){
            vm.regionais = _.orderBy(res, ['nome'], ['asc']);
            if (vm.regionais.length == 1){
                vm.lote.regional = vm.regionais[0];
            }
        });        

        function changeconcurso(){
            salvaConncursoCookie();
            vm.lote.numeroInicio = null;
            vm.lote.numeroFim = null;            
        }

        function changeregional(){
            if (vm.lote.regional){
                Estabelecimento.queryTodosAtivosPorRegional({'regionalId': vm.lote.regional.id}).$promise.then(function(res){
                    vm.estabelecimentos = _.orderBy(res, ['nome'], ['asc']);
                    if (vm.estabelecimentos.length == 1){
                        vm.lote.estabelecimento = vm.estabelecimentos[0];
                    }    
                });        
            } else {
                vm.lote.estabelecimento = null;
            }
        }
        function changenumero(){
            if(vm.lote.numeroInicio){
                var strNumInicio = vm.lote.numeroInicio.toString();
                console.log(strNumInicio, strNumInicio.replace( /\D+/g, '') );
                vm.lote.numeroInicio = strNumInicio.replace( /\D+/g, '');
            }
            if(vm.lote.numeroFim){
                var strNumFim = vm.lote.numeroFim.toString();
                vm.lote.numeroFim = strNumFim.replace( /\D+/g, '');            
            }
        }

       function addErrorAlert(mensagem) {
            vm.alerts.push({type: 'danger', msg: mensagem});
            $timeout(function () {
                  vm.closeAlert(vm.alerts.length-1);
            }, 5000);
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        function limpar () {
            vm.lote.numeroInicio = null;
            vm.lote.numeroFim = null;
            vm.lote.regional = null;
            vm.lote.estabelecimento = null;
            vm.lote.operacao = 'I';                                                
        }

        function distribuir () {
            console.log(vm.lote);
            if (vm.lote.numeroInicio > vm.lote.numeroFim){
                AlertService.error('O número inicial é maior que o final.');     
            }

            if (vm.lote.concurso.id && vm.lote.regional && vm.lote.numeroInicio && 
                vm.lote.numeroFim && vm.lote.operacao && vm.lote.numeroInicio <= vm.lote.numeroFim){
                vm.isSaving = true;
                console.log('step1');
                if (vm.lote.operacao === "I"){
                    DistribuicaoSelos.distribuirSelos({id: null,                      
                        concurso: vm.lote.concurso,
                        estabelecimento: vm.lote.estabelecimento,
                        regional: vm.lote.regional,                        
                        numeroInicio : vm.lote.numeroInicio,
                        numeroFim: vm.lote.numeroFim,
                        operacao: vm.lote.operacao
                    }).$promise.then( onSaveSuccess).catch(onSaveError);
                } else {
                    DistribuicaoSelos.devolverSelos({id: null,                      
                        concurso: vm.lote.concurso,
                        estabelecimento: vm.lote.estabelecimento,
                        regional: vm.lote.regional,                        
                        numeroInicio : vm.lote.numeroInicio,
                        numeroFim: vm.lote.numeroFim,
                        operacao: vm.lote.operacao
                    }).$promise.then( onSaveSuccess).catch(onSaveError);                    
                }
            } else {
                AlertService.error('Revise os campos obrigatórios do formulário.');
            }
        }
            function onSaveSuccess (result) {
                vm.isSaving = false;
                AlertService.success('Lote de selos movimentado!');
                buscar();
            }
    
            function onSaveError (response) {
                vm.isSaving = false;
                var msg = 'Houve um erro na movimentação do lote de selos!';
                if (response.status == 400){
                    var errorkey = response.headers('X-sorteioApp-error');
                    var errorparam = response.headers('X-sorteioApp-params');
                    
                    if (errorkey === 'error.operacaoinvalida'){
                        msg = 'Houve um erro inesperado. Contacte um administrador do sistema!'; 
                    } else if (errorkey === 'error.seloinvalido'){
                        msg = 'Movimentação inválida. Verifique, por exemplo, se a numeração inicial é menor que a final.'; 
                    } else if (errorkey === 'error.selosinsuficiente'){
                        msg = 'Não foi possível efetuar a devolução, pois há selos insuficientes no momento com a regional/estabelecimento.';                    
                    } else if (errorkey === 'error.sempermissao'){
                        msg = 'O usuário não tem permissão para alterar todos os bilhetes deste intervalo.';                    
                    }

                    
                    
                } 
                AlertService.error(msg);      
            }

        function  buscar (){
            if (vm.lote.concurso.id && vm.lote.regional ){
                if (vm.lote.estabelecimento){

                    DistribuicaoSelos.queryPorEstabelecimento(
                        {'idconcurso': vm.lote.concurso.id,
                        'idestabelecimento': vm.lote.estabelecimento.id})
                    .$promise.then( function(data) {
                        vm.logdistribuicao = data;
                        console.log(data);
                    }).catch(function(){
                        vm.logdistribuicao = [];
                        AlertService.error("Não foi possível coletar o log de distribuições");
                    });
                } else {
                    DistribuicaoSelos.queryPorRegional(
                        {'idconcurso': vm.lote.concurso.id,
                        'idregional': vm.lote.regional.id})
                    .$promise.then( function(data) {
                        vm.logdistribuicao = data;
                        console.log(data);
                    }).catch(function(){
                        vm.logdistribuicao = [];
                        AlertService.error("Não foi possível coletar o log de distribuições");
                    });
                }
            }
        }

        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.lote.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.lote.concurso);
        }          
    }
})();
