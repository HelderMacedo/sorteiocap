(function() {
    'use strict';
    angular
        .module('sorteioApp')
        .factory('DistribuicaoSelos', DistribuicaoSelos);

        DistribuicaoSelos.$inject = ['$resource'];

    function DistribuicaoSelos ($resource) {
        var urlResourceDistribuicaoSelos = 'api/distribuicaoselos'; 
        var urlResourceDistribuicaoRegional = 'api/distribuicaoselos/regional/:idconcurso/:idregional';
        var urlResourceDistribuicaoEstabelecimento = "api/distribuicaoselos/estabelecimento/:idconcurso/:idestabelecimento";
   
        var resourceDistribuicaoSelos = $resource(urlResourceDistribuicaoSelos, {}, {
            'delete': {method: 'PUT', isArray: false},
            'save':  {method:'POST', isArray: false}
        }); 
        
        return {
            distribuirSelos : resourceDistribuicaoSelos.save,
            devolverSelos : resourceDistribuicaoSelos.delete,            
            queryPorRegional: $resource(urlResourceDistribuicaoRegional, {}, {
                                        'query': {method: 'GET', isArray: true}
                                    }).query,                                       
            queryPorEstabelecimento: $resource( urlResourceDistribuicaoEstabelecimento, {}, {
                                        'query': {method: 'GET', isArray: true}
                                    }).query    
        }

    }
})();
