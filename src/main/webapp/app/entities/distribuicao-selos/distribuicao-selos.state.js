(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('distribuicao-selos', {
            parent: 'bilhete',
            url: '/distribuicao-selos?page&sort',
            data: {
                authorities: ['PERM_CONFECCAO_LOTES'],
                pageTitle: 'sorteioApp.distribuicao-selos.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribuicao-selos/distribuicao-selos.html',
                    controller: 'DistribuicaoSelosController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],                
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('distribuicao-selos');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('historico-distribuicao-selo', {
            parent: 'bilhete',
            url: '/historico-distribuicao-selo',
            data: {
                authorities: ['PERM_CONFECCAO_LOTES'],
                pageTitle: 'sorteioApp.hist_distribuicao-selos.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/distribuicao-selos/hist_distribuicao-selos.html',
                    controller: 'HistoricoDistribuicaoSelosController',
                    controllerAs: 'vm'
                }
            },params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                filtro: {concurso : null, regional : null}                
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,                        
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],                
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('hist_distribuicao-selos');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })           
        ;
    }

})();
