(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('HistoricoDistribuicaoSelosController', HistoricoDistribuicaoSelosController);

        HistoricoDistribuicaoSelosController.$inject = ['$timeout', '$scope', '$stateParams',  '$http', '$state', '$cookies',
       'Concurso', 'Regional', 'Estabelecimento', 'AlertService', 'DistribuicaoSelos', 'ParseLinks', 'pagingParams', 'paginationConstants'];

    function HistoricoDistribuicaoSelosController ($timeout, $scope, $stateParams,  $http, $state, $cookies,
        Concurso, Regional, Estabelecimento, AlertService, DistribuicaoSelos, ParseLinks, pagingParams, paginationConstants) {
        var vm = this;

        vm.concursos = [];
        vm.alerts = [];        
        vm.loadAll = loadAll;
        vm.changeconcurso = changeconcurso;
        vm.changeregional = changeregional;
        vm.filtro = pagingParams.filtro;
        vm.totalSelos = 0;    
        vm.loadPage = loadPage;
        vm.transition = transition;

        vm.queryCount = 1;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.page = pagingParams.page;
        vm.links = {
            last: 0
        };
        vm.predicate = 'id';
        vm.reset = reset;
        vm.reverse = true;            
        
        function positionFirstInput(){
          $timeout(function (){
            angular.element('.form-group:eq(0)>input').focus();
          });
        };

        Concurso.queryshort().$promise.then(function(res){
            vm.concursos = res;
            if(vm.filtro.concurso == null){
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });
        Regional.queryAtivos().$promise.then(function(res){
            vm.regionais = _.orderBy(res, ['nome'], ['asc']);
            if (vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0];
            }
            loadAll ();
        });        

        function changeconcurso(){
            salvaConncursoCookie();    
        }
        function changeregional(){
            if (vm.filtro.regional){
                vm.estabelecimentos = [];
                vm.filtro.estabelecimento = null;
                Estabelecimento.queryTodosAtivosPorRegional({'regionalId': vm.filtro.regional.id}).$promise.then(function(res){
                    vm.estabelecimentos = _.orderBy(res, ['nome'], ['asc']);
                    if (vm.estabelecimentos.length == 1){
                        vm.filtro.estabelecimento = vm.estabelecimentos[0];
                    }    
                });        
            } else {
                vm.filtro.estabelecimento = null;
            }
        }
       function addErrorAlert(mensagem) {
            vm.alerts.push({type: 'danger', msg: mensagem});
            $timeout(function () {
                  vm.closeAlert(vm.alerts.length-1);
            }, 5000);
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };
        function reset () {
            vm.page = 0;
            vm.filtro.regional = null;
            loadAll();
        }
        function loadPage(page) {
            vm.page = page;
            loadAll();
        }        
        function  loadAll (){
            if (vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0];
            }      
            if (vm.filtro.concurso.id && vm.filtro.regional ){
                vm.totalSelos = 0;
                if (vm.filtro.estabelecimento){

                    DistribuicaoSelos.queryPorEstabelecimento(
                        {'idconcurso': vm.filtro.concurso.id,
                        'idestabelecimento': vm.filtro.estabelecimento.id})
                    .$promise.then( function(data) {
                        vm.logdistribuicao = data;
                        somarSelos();
                    }).catch(function(){
                        vm.logdistribuicao = [];
                        AlertService.error("Não foi possível coletar o log de distribuições");
                    });
                } else {
                    DistribuicaoSelos.queryPorRegional(
                        {'idconcurso': vm.filtro.concurso.id,
                        'idregional': vm.filtro.regional.id})
                    .$promise.then( function(data) {
                        vm.logdistribuicao = data;
                        somarSelos();
                    }).catch(function(){
                        vm.logdistribuicao = [];
                        AlertService.error("Não foi possível coletar o log de distribuições");
                    });
                }
            }
        }
        function somarSelos(){
            vm.totalSelos = 0
            if (vm.logdistribuicao){
                console.log(vm.logdistribuicao);
                for (var k=0; k < vm.logdistribuicao.length; k++){
                    
                    var somalote = (vm.logdistribuicao[k].numeroFim - vm.logdistribuicao[k].numeroInicio + 1);
                    somalote = somalote * (vm.logdistribuicao[k].operacao=='I'?1:-1)
                    vm.totalSelos = vm.totalSelos + somalote;
                }
            }
        }
        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: 'id,asc',
                search: vm.currentSearch
            });
        }   
        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.filtro.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.filtro.concurso);
        }          
    }
})();
