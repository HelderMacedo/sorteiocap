(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('EstabelecimentoDeleteController',EstabelecimentoDeleteController);

    EstabelecimentoDeleteController.$inject = ['$rootScope', 'AlertService','$uibModalInstance', 'entity', 'Estabelecimento'];

    function EstabelecimentoDeleteController($rootScope, AlertService, $uibModalInstance, entity, Estabelecimento) {
        var vm = this;

        vm.estabelecimento = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Estabelecimento.delete({id: id},
                function () {
                    $rootScope.$broadcast('estabelecimentoUpdate');                    
                    AlertService.success('Estabelecimento excluido com sucesso');    
                    $uibModalInstance.close(true);
                },
                function(){
                    AlertService.error("Não foi possível excluir o estabelecimento!\nVerifique se não existe um lote de distribuição associado a ele.");
                });
        }        
    }
})();
