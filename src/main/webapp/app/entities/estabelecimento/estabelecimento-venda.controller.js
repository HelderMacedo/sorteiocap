(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('VendaDialogController', VendaDialogController);

    VendaDialogController.$inject = ['$timeout', '$scope','$state' ,'$stateParams', '$http', '$cookies', '$uibModalInstance', 
       'entity', 'Concurso', 'Estabelecimento' ,'AlertService', 'DistribuicaoBilhetes', 'Principal'];

    function VendaDialogController ($timeout, $scope, $state,$stateParams,  $http, $cookies, $uibModalInstance, 
       entity, Concurso, Estabelecimento,AlertService, DistribuicaoBilhetes, Principal) {
        var vm = this;
        vm.loading = true;
        vm.regional = {id: entity.regionalId }
        vm.lotevenda = entity;
        vm.lotevenda.lista = [];        
        vm.formlote = {};
        vm.concursos = [];
        vm.alerts = [];        
        vm.clear = clear;
        vm.save = save;
        vm.getDistribuicaoBilhetes = getDistribuicaoBilhetes;
        vm.changeconcurso= changeconcurso;
        vm.changeBilhete = changeBilhete;
        vm.bilhetesDistribuidos = null;
        vm.account = {};    
        vm.est = null;
        vm.areaRegionalId = null;

        if(vm.lotevenda.estabelecimentoId){
            Estabelecimento.get({
                id: vm.lotevenda.estabelecimentoId
            }).$promise.then(function(res){
                console.log(res);
                vm.areaRegionalId = res.arearegional.id;
            });
        }
        
        function positionFirstInput(){
          $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
          });
        };
        positionFirstInput();
        Concurso.querynaosinc().$promise.then(function(res){
            vm.concursos = res;
            if(vm.concursos.length > 0){
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });

        function changeconcurso(){
            salvaConncursoCookie();
            vm.lotevenda.bilheteInicial = null;
            vm.lotevenda.bilheteFinal = null;
            vm.loading = false;                    
        } 

        function changeBilhete(){
            if (vm.lotevenda.bilheteInicial && vm.lotevenda.bilheteInicial.trim().length > 0){
                vm.lotevenda.bilheteInicial = formatIdentificacaoBilhete(vm.lotevenda.bilheteInicial);
            }
            if (vm.lotevenda.bilheteFinal && vm.lotevenda.bilheteFinal.trim().length > 0){    
                vm.lotevenda.bilheteFinal = formatIdentificacaoBilhete(vm.lotevenda.bilheteFinal);
            }
        }

        function formatIdentificacaoBilhete(element){
            console.log("Bil.A: "+element);
            element = element.replace(/\D/g,'').trim();      
            element = ('0000000000000'+ element).substring(element.length,element.length+13);
            element = element.substring(0,11)+"-"+element.substring(11,13);
            console.log('Bil.D: '+ element);
            return element;
        } 

       function addErrorAlert(mensagem) {
            vm.alerts.push({type: 'danger', msg: mensagem});
            $timeout(function () {
                  vm.closeAlert(vm.alerts.length-1);
            }, 5000);
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function getDistribuicaoBilhetes() {
            
            var numeroInicial = getNumero(vm.lotevenda.bilheteInicial);
            var numeroFinal = getNumero(vm.lotevenda.bilheteFinal);
            
            Principal.identity().then(function(account){
                vm.account = account;

                if(!vm.account.roles.includes("ROLE_AREA_REGIONAL")){
                    distribuicaoEstabelecimento(numeroInicial, numeroFinal);
                }else{

                    Estabelecimento.queryGetSubRegionalDistribuidos({
                        concurso: vm.lotevenda.concurso.id,
                        subRegional: vm.areaRegionalId
                    }).$promise.then(response => { 

                        verificaOsBilhetesPertecemAArea(numeroInicial, numeroFinal, response);                              
                        
                    });
                    
                }
            });          

            
        }
        
        function verificaOsBilhetesPertecemAArea(numeroInicial, numeroFinal, subRegionalBilhetes)
        {
            let numberInicial = Math.round(numeroInicial);
            let numberfinal = Math.round(numeroFinal);
            let isSubRegional = true; 
            for(let i = numberInicial; i <= numberfinal; i++){
                if(!subRegionalBilhetes.includes(i)){
                    isSubRegional = false;
                }
            }

            if(isSubRegional){
                distribuicaoEstabelecimento(numeroInicial, numeroFinal);
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Sequência indisponível para está SubRegional'
                })
            }
            
        }

        function distribuicaoEstabelecimento(numeroInicial, numeroFinal){
            Estabelecimento.queryGetBilhetesDistribuidos({
                concurso: vm.lotevenda.concurso.id,
                numInicial: numeroInicial,
                numFinal: numeroFinal,
                estabelecimento: vm.lotevenda.estabelecimentoId
            }).$promise.then(response => {
                var tamanho = response.length;
                vm.bilhetesDistribuidos = response;
                if(tamanho >= 1){
                   
                    const res = getEstabelecimento(vm.bilhetesDistribuidos[0].estabelecimento_id);
                    res.then(response => {
                        vm.est = response.nome;
                        Swal.fire({
                            title: 'Sequência já distribuida',
                            text: "Sequência já foi distribuída para o estabelecimento "+vm.est+", deseja tranferir?",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Cancelar',
                            confirmButtonText: 'Sim'
                          }).then((result) => {
                            if (result.isConfirmed) {
                                save();
                              Swal.fire(
                                'Lote distribuído',
                                'Lote Criado.',
                                'success'
                              )
                            }
                          })
                    });
                    
                }else{
                    
                    save();
                }
            });
        }

        function getEstabelecimento(id){
            return Estabelecimento.get({id: id}).$promise;
        }

        function getNumero(numero){
            return numero.substring(4,11);
        }

        function save () {
            vm.isSaving = true;
          
            changeBilhete();    
            
            DistribuicaoBilhetes.distribuiEstabelecimento({id: null,                      
                estabelecimentoId : vm.lotevenda.estabelecimentoId,
                concursoId: vm.lotevenda.concurso.id,
                bilheteInicial : vm.lotevenda.bilheteInicial,
                bilheteFinal: vm.lotevenda.bilheteFinal,
                motivoDistribuicao : 'X', // MOTIVO_DISTRIBUICAO_REGIONAL
                areaRegionalId: vm.areaRegionalId
            }).$promise.then( onSaveSuccess).catch(onSaveError);
        }

        function onSaveSuccess (result) {
           // $scope.$emit('sorteioApp:geracaoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            AlertService.success('Lote de venda criado ');
        }

        function onSaveError (response) {
            vm.isSaving = false;
            var msg = 'Houve um erro na criação do lote de venda de bilhetes!';
            if (response.status == 400){
                var errorkey = response.headers('X-sorteioApp-error');
                var errorparam = response.headers('X-sorteioApp-params');
                
                if (errorkey === 'error.bilheteinvalido'){
                    msg = 'Bilhete inválido: ' + errorparam; 
                } else if (errorkey === 'error.periodoinvalido'){
                    msg = 'Faixa não disponível! Verifique se existem bilhetes entregues a outra regional.'; 
                } else if (errorkey === 'error.sempermissao'){
                    msg = 'O usuário não tem permissão para alterar todos os bilhetes deste intervalo.';                    
                } else if (errorkey === 'error.bilhetejavalidado'){
                    msg = 'Existem bilhetes validados no intervalo informado.';                    
                }  else if (errorkey === 'error.bilhetenaocorrespondeconcurso'){
                    msg = 'Bilhete ' + errorparam + ' não corresponde ao concurso selecionado.'; 
                } else if (errorkey === 'error.faixapertenceoutrolote'){
                    msg = 'Parte dos bilhetes já atribuidos a uma regional diferente deste estabelecimento.';
                } else if (errorkey === 'error.loteOutraRegional'){
                    msg = 'Parte dos bilhetes já atribuidos a uma outra regional.';
                }
            } 
            AlertService.error(msg);      
        }

        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.lotevenda.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.lotevenda.concurso);
        }
    }
})();
