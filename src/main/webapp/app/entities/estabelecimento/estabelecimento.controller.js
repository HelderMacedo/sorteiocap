(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('EstabelecimentoController', EstabelecimentoController);

    EstabelecimentoController.$inject = ['$scope','$state', 'Estabelecimento', 'Regional', 'AreaRegional', 'ParseLinks', 'AlertService', 
        'pagingParams', 'paginationConstants'];

    function EstabelecimentoController($scope, $state ,Estabelecimento, Regional, AreaRegional, ParseLinks, AlertService, pagingParams, paginationConstants) {

        var vm = this;

        vm.estabelecimentos = [];
        vm.regionais = [];
        vm.areasregional = [];        
        vm.filtrar = filtrar;
        vm.limparFiltro = limparFiltro;
        vm.filtro = pagingParams.filtro;
        vm.saveCSV = saveCSV;        

        vm.loadPage = loadPage;
        vm.transition = transition;
        vm.transitonTo = transitonTo;
        vm.queryCount = 1;
        vm.page = pagingParams.page;
        vm.itemsPerPage = paginationConstants.itemsPerPage;
        vm.changeregional = changeregional;

        vm.links = {
            last: 0
        };
        vm.predicate = 'nome';
        vm.reset = reset;
        vm.reverse = true;

        if (vm.filtro.ativo === undefined) vm.filtro.ativo  = true;
        Regional.queryAtivos().$promise.then(function(res){
            vm.regionais = _.orderBy(res, ['nome'], ['asc']);            
            if (vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0].id;
            } 
            changeregional ();
        });

        function changeregional () {
            if (vm.filtro.regional){
                AreaRegional.queryPorRegional({'regionalId': vm.filtro.regional }).$promise.then(function(res){
                    vm.areasregional = _.orderBy(res, ['nome'], ['asc']);
                });
            }
        }
        
        loadAll();
        function limparFiltro(){
            vm.filtro = {nome: null, bairro: null, uf:null, cidade: null, telefone: null, regional : null};
            loadAll();
        }
        function filtrar(){
            vm.page = 1;
            vm.links = { last: 0 };
            load(vm.filtro, onSuccessSearchRequest, vm.page - 1, vm.itemsPerPage);
        }
        $scope.$on("estabelecimentoUpdate",function(){
            console.log('updating');
            loadAll();
         });

        function loadAll () {
            load(vm.filtro, onSuccessSearchRequest, vm.page - 1, vm.itemsPerPage);
        }

        function saveCSV(){
            load(vm.filtro, onSuccessSaveCSVRequest, 0, 10000000);
        }    

        function load(filtro, successFunction, startPage, pageSize){
            var obj = JSON.parse(JSON.stringify(filtro));            
            obj.page = startPage;     
            obj.size = pageSize;
            obj.sort = sort();
            if (!obj.ativo) obj.ativo = null;
            Estabelecimento.query( obj, successFunction, onError);
            function sort() {
                var result = [vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc')];
                if (vm.predicate !== 'nome') {
                    result.push('nome');
                }
                return result;
            }

            function onError(error) {
                console.log('onError %o', error);   
                AlertService.error(error.data.message);
            }
        }

        function onSuccessSearchRequest(data, headers) {
            vm.links = ParseLinks.parse(headers('link'));
            vm.totalItems = headers('X-Total-Count');
            vm.queryCount = vm.totalItems;              
            vm.estabelecimentos = [];      
          
            for (var i = 0; i < data.length; i++) {
                vm.estabelecimentos.push(data[i]);
            }
        }        

        function onSuccessSaveCSVRequest(data, headers) {             
            var lista = data;
            _.sortBy(lista, ['nome']);
            var content = 'ID;Nome;Telefone;Regional;Área;Bairro;Cidade;Comissão;Situação\n';
            for (var i = 0; i < lista.length; i++) {
                content += lista[i].id+';';
                content += (lista[i].nome).replace(/[,;]/g,'')+';';
                content += ((lista[i].telefone)?lista[i].telefone:'').replace(/[,;]/g,'')+';';
                content += (lista[i].regional.nome).replace(/[,;]/g,'')+';';
                content += ((lista[i].areaAegional!=null)?lista[i].areaAegional.nome:'').replace(/[,;]/g,'')+';';
                content += (lista[i].bairro).replace(/[,;]/g,'')+';';
                content += (lista[i].cidade+'/'+lista[i].uf).replace(/[,;]/g,'')+';';
                content += lista[i].percentualRepasse+';';                
                content += (lista[i].ativo?'Ativo':'Inativo')+'\n';
            }
            //var hiddenElement = document.createElement('a');
            //hiddenElement.href =  encodeURI('data:text/csv;charset=utf-8,' + content);
            //hiddenElement.download = 'lista_estabelecimentos.csv';
            //document.body.appendChild(hiddenElement);
            //hiddenElement.click();
            var fileName = 'lista_estabelecimentos.csv';
            var  textEncoder = new CustomTextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true});              
            var csvContentEncoded = textEncoder.encode([content]);
            var blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252;'});
            saveAs(blob, fileName);               
        }

        function reset () {
            vm.page = 1;
            vm.estabelecimentos = [];
            loadAll();
        }

        function loadPage(page) {
            vm.page = page;
            loadAll();
        }

        function transition () {
            $state.transitionTo($state.$current, {
                page: vm.page,
                sort: vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc'),
                filtro: vm.filtro
            });
        }   
        function transitonTo(statename, stateparams){
            $state.params.filtro = vm.filtro;
            $state.params.page = vm.page;
            $state.params.sort = vm.predicate + ',' + (vm.reverse ? 'asc' : 'desc');
            $state.transitionTo(statename, stateparams);
        }              
    }
})();
