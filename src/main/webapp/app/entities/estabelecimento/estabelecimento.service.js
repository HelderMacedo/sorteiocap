(function() {
    'use strict';
    angular
        .module('sorteioApp')
        .factory('Estabelecimento', Estabelecimento);

    Estabelecimento.$inject = ['$resource'];

    function Estabelecimento ($resource) {
        var resourceUrl =  'api/estabelecimentos/:id';
        var urlResourceEstabRegional = 'api/estabelecimentos/regional/:regionalId';        
        var urlResourceTodosEstabAtivosRegional = 'api/estabelecimentos/ativos/regional/:regionalId';
        var urlEstabalecimentosPorSubRegional = 'api/estabelecimentos/ativos/subrebional/:subregional'
        var urlBilhetesDistribuidos = 'api/bilhetesdistribuidos/:concurso/:numInicial/:numFinal/:estabelecimento';
        var urlSubRegionalDistribuidos = 'api/subregionaldistribuidos/:concurso/:subRegional';
        var urlOnlinePorRegionalEArea = 'api/estabelecimentos/online/:regionalId';

        var resourceprincipal =  $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });

        var resourceEstabelecimento = $resource(urlEstabalecimentosPorSubRegional,{},{
            'get': {method: 'GET', isArray: true }
        })

        function getQueryResurce(url){
            return $resource(url, {}, {
                    'query': {method: 'GET', isArray: true}
                }).query;
        };

        var resourceGetBilhetesDistribuidos = $resource(urlBilhetesDistribuidos, {}, {
            'get' : { method: 'GET', isArray: true }
        });
        var resourceGetSubRegionalDistribuidos = $resource(urlSubRegionalDistribuidos, {} ,{
            'get' : { method: 'GET', isArray: true }
        });

        var resourceOnlinePorRegionalEArea = $resource(urlOnlinePorRegionalEArea, {
            regionalId: '@regionalId',
            areaId: '@areaId' // Envia areaId como query param
        }, {
            'get': { method: 'GET', isArray: true}
        });
    
        return {
            query : resourceprincipal.query,
            save : resourceprincipal.save,
            update : resourceprincipal.update,
            get : resourceprincipal.get,
            delete : resourceprincipal.delete,            
            queryPorRegional : getQueryResurce(urlResourceEstabRegional),
            queryTodosAtivosPorRegional : getQueryResurce(urlResourceTodosEstabAtivosRegional),
            queryTodosAtivosPorSubRegional: resourceEstabelecimento.get,
            queryGetBilhetesDistribuidos : resourceGetBilhetesDistribuidos.get,
            queryGetSubRegionalDistribuidos: resourceGetSubRegionalDistribuidos.get,
            queryOnlinePorRegionalEArea: resourceOnlinePorRegionalEArea.get
        };
    }
})();
