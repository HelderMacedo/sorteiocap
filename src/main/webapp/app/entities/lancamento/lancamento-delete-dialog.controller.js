(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('LancamentoDeleteController',LancamentoDeleteController);

    LancamentoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Lancamento'];

    function LancamentoDeleteController($uibModalInstance, entity, Lancamento) {
        var vm = this;

        vm.lancamento = entity;
        console.log(entity);
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Lancamento.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
