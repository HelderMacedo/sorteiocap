(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('lancamento', {
            parent: 'entity',
            url: '/lancamento',
            data: {
                authorities: ['PERM_CADASTRO_LANCAMENTO'],
                pageTitle: 'sorteioApp.lancamento.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/lancamento/lancamento.html',
                    controller: 'LancamentoController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                filtro: {dataLancamento: null, id: null, }
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        filtro: $stateParams.filtro,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort)
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('regional');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            },           
        })
        .state('lancamento.edit', {
            parent: 'lancamento',
            url: '/detail/edit',
            data: {
                authorities: ['PERM_CADASTRO_LANCAMENTO']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/lancamento/lancamento-dialog.html',
                    controller: 'LancamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: ['Lancamento', function(Lancamento) {
                            return Lancamento.get({id : $state.params.id}).$promise;
                        }]                        
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('lancamento.delete', {
            parent: 'lancamento',
            url: '/detail/delete',
            data: {
                authorities: ['PERM_CADASTRO_LANCAMENTO']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/lancamento/lancamento-delete-dialog.html',
                    controller: 'LancamentoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: {id : $state.params.id}
                    }
                }).result.then(function() {
                    $state.go('lancamento', null, { reload: 'lancamento' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('lancamento.new', {
            parent: 'lancamento',
            url: '/new',
            data: {
                authorities: ['PERM_CADASTRO_LANCAMENTO']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/lancamento/lancamento-dialog.html',
                    controller: 'LancamentoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'md',
                    resolve: {
                        entity: function () {
                            return {
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('lancamento', null, { reload: 'lancamento' });
                }, function() {
                    $state.go('lancamento');
                });
            }]
        })
        ;
    }

})();
