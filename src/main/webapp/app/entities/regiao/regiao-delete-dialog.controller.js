(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('RegiaoDeleteController',RegiaoDeleteController);

    RegiaoDeleteController.$inject = ['$uibModalInstance', 'entity', 'Regiao'];

    function RegiaoDeleteController($uibModalInstance, entity, Regiao) {
        var vm = this;

        vm.regiao = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Regiao.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
