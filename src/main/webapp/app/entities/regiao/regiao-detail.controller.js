(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('RegiaoDetailController', RegiaoDetailController);

    RegiaoDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Regiao'];

    function RegiaoDetailController($scope, $rootScope, $stateParams, previousState, entity, Regiao) {
        var vm = this;

        vm.regiao = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('sorteioApp:regiaoUpdate', function(event, result) {
            vm.regiao = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
