(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('RegiaoDialogController', RegiaoDialogController);

    RegiaoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Regiao'];

    function RegiaoDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Regiao) {
        var vm = this;

        vm.regiao = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.regiao.id !== null) {
                Regiao.update(vm.regiao, onSaveSuccess, onSaveError);
            } else {
                Regiao.save(vm.regiao, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('sorteioApp:regiaoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
