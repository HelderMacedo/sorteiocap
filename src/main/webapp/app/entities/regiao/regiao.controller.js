(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('RegiaoController', RegiaoController);

    RegiaoController.$inject = ['Regiao'];

    function RegiaoController(Regiao) {

        var vm = this;

        vm.regiaos = [];

        loadAll();

        function loadAll() {
            Regiao.query(function(result) {
                vm.regiaos = result;
                vm.searchQuery = null;
            });
        }
    }
})();
