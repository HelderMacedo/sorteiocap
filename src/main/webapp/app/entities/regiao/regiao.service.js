(function() {
    'use strict';
    angular
        .module('sorteioApp')
        .factory('Regiao', Regiao);

    Regiao.$inject = ['$resource'];

    function Regiao ($resource) {
        var resourceUrl =  'api/regioes/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
