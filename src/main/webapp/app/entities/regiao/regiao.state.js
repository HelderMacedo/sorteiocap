(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('regiao', {
            parent: 'entity',
            url: '/regiao',
            data: {
                authorities: ['PERM_CADASTRO_REGIONAL','PERM_CONSULTAR_REGIONAL'],
                pageTitle: 'sorteioApp.regiao.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/regiao/regiaos.html',
                    controller: 'RegiaoController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('regiao');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('regiao-detail', {
            parent: 'regiao',
            url: '/regiao/{id}',
            data: {
                authorities: ['PERM_CONSULTAR_REGIONAL'],
                pageTitle: 'sorteioApp.regiao.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/regiao/regiao-detail.html',
                    controller: 'RegiaoDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('regiao');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Regiao', function($stateParams, Regiao) {
                    return Regiao.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'regiao',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('regiao-detail.edit', {
            parent: 'regiao-detail',
            url: '/detail/edit',
            data: {
                authorities: ['PERM_CADASTRO_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/regiao/regiao-dialog.html',
                    controller: 'RegiaoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'sm',
                    resolve: {
                        entity: ['Regiao', function(Regiao) {
                            return Regiao.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('regiao.new', {
            parent: 'regiao',
            url: '/new',
            data: {
                authorities: ['PERM_CADASTRO_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/regiao/regiao-dialog.html',
                    controller: 'RegiaoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'sm',
                    resolve: {
                        entity: function () {
                            return {
                                nome: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('regiao', null, { reload: 'regiao' });
                }, function() {
                    $state.go('regiao');
                });
            }]
        })
        .state('regiao.edit', {
            parent: 'regiao',
            url: '/{id}/edit',
            data: {
                authorities: ['PERM_CADASTRO_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/regiao/regiao-dialog.html',
                    controller: 'RegiaoDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Regiao', function(Regiao) {
                            return Regiao.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('regiao', null, { reload: 'regiao' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('regiao.delete', {
            parent: 'regiao',
            url: '/{id}/delete',
            data: {
                authorities: ['PERM_CADASTRO_REGIONAL']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/regiao/regiao-delete-dialog.html',
                    controller: 'RegiaoDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Regiao', function(Regiao) {
                            return Regiao.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('regiao', null, { reload: 'regiao' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
