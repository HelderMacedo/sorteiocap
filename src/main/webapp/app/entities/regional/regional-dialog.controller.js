(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('RegionalDialogController', RegionalDialogController);

    RegionalDialogController.$inject = ['ProfileService','$timeout', '$scope', '$stateParams', '$uibModalInstance',
     'entity', 'Regional', 'Regiao', 'AlertService',];

    function RegionalDialogController (ProfileService, $timeout, $scope, $stateParams, $uibModalInstance, 
        entity, Regional, Regiao, AlertService) {
        var vm = this;

        vm.regional = entity;
        vm.regioes = [];
        vm.clear = clear;
        vm.save = save;
        vm.editForm;
        vm.passwordDoNotMatch = null;
        vm.comissaoProgressiva = true;

        ProfileService.getProfileInfo().then(function(response) {
            vm.comissaoProgressiva = response.comissaoProgressiva;
        });

        Regiao.query().$promise.then(function(res){
            vm.regioes = _.orderBy(res, ['nome'], ['asc']);
            console.log(vm.regioes);
            if (vm.regional.regiaoId){
                for(var i =0 ; i < vm.regioes.length; i++){
                    if (vm.regioes[i].id === vm.regional.regiaoId){
                        vm.regional.regiaoId = vm.regioes[i].id;
                        console.log('changed!!');
                    }
                }
            }
            console.log(vm.regional);
        });

        if(!vm.regional.id || vm.regional.id < 1){
           vm.regional.ativo = true;
           vm.regional.login = null;
           vm.regional.senha = null;
        }
        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function nullfyEmptyProperty(field){
            if (!vm.regional[field] || (vm.regional[field] != null && 0 === vm.regional[field].length)) vm.regional[field] = null;
        }
        function save () {
           nullfyEmptyProperty('senha');
           if (vm.regional.senha != null   &&
                vm.regional.senha !== vm.confirmaSenha) {
               console.log('senha diferente');
               vm.editForm.senha.$invalid = true;
               vm.editForm.senha.$error.different = true;
               vm.editForm.confirmPassword.$invalid = true;
               vm.editForm.confirmPassword.$error.different = true;
               vm.doNotMatch = 'ERROR';
            } else {
                vm.isSaving = true;
                nullfyEmptyProperty('responsavel');
                nullfyEmptyProperty('telefone');
                nullfyEmptyProperty('email');
                nullfyEmptyProperty('login');
                nullfyEmptyProperty('logradouro');
                nullfyEmptyProperty('numero');
                nullfyEmptyProperty('bairro');
                nullfyEmptyProperty('cidade'); 
                nullfyEmptyProperty('uf');                        
                nullfyEmptyProperty('cep');
                if (vm.regional.id !== null) {
                    Regional.update(vm.regional, onSaveSuccess, onSaveError);
                } else {
                    Regional.save(vm.regional, onSaveSuccess, onSaveError);
                }
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('sorteioApp:regionalUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError (response) {
            vm.isSaving = false;
            var msg = 'Problema na criação do regional!';
            if (response.status == 400){
                var errorkey = response.headers('X-sorteioApp-error');
                var errorparam = response.headers('X-sorteioApp-params');
                
                if (errorkey === 'error.sempermissao'){
                    msg = 'Sem permissão para realizar a ação'; 
                } else if (errorkey === 'error.errocriarusuario'){
                    msg = 'Erro ao criar usuário da regional. '+ errorparam; 
                } 
            } 
            AlertService.error(msg);
        }


    }
})();
