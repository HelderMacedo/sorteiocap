(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('DistribuicaoDialogController', DistribuicaoDialogController);

    DistribuicaoDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$http', '$cookies', '$uibModalInstance', 
        'entity', 'Concurso', 'DistribuicaoBilhetes', 'AlertService'];

    function DistribuicaoDialogController ($timeout, $scope, $stateParams, $http, $cookies, $uibModalInstance,
       entity, Concurso, DistribuicaoBilhetes, AlertService) {
        var vm = this;

        vm.lotedistribuicao = entity;
        vm.lotedistribuicao.regionalNome = $stateParams.nome;
        vm.lotedistribuicao.lista = [];
        
        vm.concursos = [];
        vm.lotesid = [];
        vm.lotes = [];
        vm.alerts = [];
        vm.clear = clear;
        vm.save = save;
        vm.changeconcurso = changeconcurso;
        vm.changeNumeroSelo = changeNumeroSelo;
        vm.reset = reset; 
        vm.changeBilhete = changeBilhete;
        
        function reset(){
           vm.lotedistribuicao.lista = [];            
           vm.alerts = [];
           vm.lotedistribuicao.loteid = null;
        }

        function positionFirstInput(){
            $timeout(function (){
                angular.element('#field_bilheteInicial').focus();
              });
        };
        positionFirstInput();

        Concurso.querynaosinc().$promise.then(function(res){
            vm.concursos = res;
            if(vm.concursos.length > 0){
                selecionaConcursoPadrao();
                changeconcurso();
            }
        });

        function changeconcurso(){
            salvaConncursoCookie();                 
            vm.lotedistribuicao.bilheteInicial = null;
            vm.lotedistribuicao.bilheteFinal = null;

        } 

        function changeNumeroSelo(){
            if(vm.lotedistribuicao.seloInicial){
                var strNumInicio = vm.lotedistribuicao.seloInicial.toString();
                vm.lotedistribuicao.seloInicial = strNumInicio.replace( /\D+/g, '');
                if (vm.lotedistribuicao.seloInicial.length == 0){
                    vm.lotedistribuicao.seloInicial = null;
                }
            } 

            if(vm.lotedistribuicao.seloFinal){
                var strNumFim = vm.lotedistribuicao.seloFinal.toString();
                vm.lotedistribuicao.seloFinal = strNumFim.replace( /\D+/g, '');            
                if (vm.lotedistribuicao.seloFinal.length == 0){
                    vm.lotedistribuicao.seloFinal = null;
                }
            }
        }
        function changeBilhete(){
            if (vm.lotedistribuicao.bilheteInicial && vm.lotedistribuicao.bilheteInicial.trim().length > 0){
                vm.lotedistribuicao.bilheteInicial = formatIdentificacaoBilhete(vm.lotedistribuicao.bilheteInicial);
            }
            if (vm.lotedistribuicao.bilheteFinal && vm.lotedistribuicao.bilheteFinal.trim().length > 0){    
                vm.lotedistribuicao.bilheteFinal = formatIdentificacaoBilhete(vm.lotedistribuicao.bilheteFinal);
            }
        }

        function formatIdentificacaoBilhete(element){
            console.log("Bil.A: "+element);
            element = element.replace(/\D/g,'').trim();      
            element = ('0000000000000'+ element).substring(element.length,element.length+13);
            element = element.substring(0,11)+"-"+element.substring(11,13);
            console.log('Bil.D: '+ element);
            return element;
        }

        function clear () {
            reset();
            $uibModalInstance.dismiss('cancel');
        }


        function save () {
            vm.isSaving = true;
            changeBilhete();
            DistribuicaoBilhetes.distribuiRegional({id: null,
                regionalId : (vm.lotedistribuicao.regionalId)? vm.lotedistribuicao.regionalId : null,
                concursoId: (vm.lotedistribuicao.concurso)? vm.lotedistribuicao.concurso.id : null,
                valor : 0,
                bilheteInicial : vm.lotedistribuicao.bilheteInicial,
                bilheteFinal : vm.lotedistribuicao.bilheteFinal,
                seloInicial : vm.lotedistribuicao.seloInicial,
                seloFinal : vm.lotedistribuicao.seloFinal,                
                motivoDistribuicao: 'X' // MOTIVO_DISTRIBUICAO_REGIONAL
               }, onSaveSuccess, onSaveError);
        }


        function onSaveSuccess (result) {
           // $scope.$emit('sorteioApp:geracaoUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
            AlertService.success('Lote do concurso '+vm.lotedistribuicao.concurso.label+' distribuido à Regional.');
        }

        function onSaveError (response) {
            vm.isSaving = false;
            var msg = 'Problema na criação do lote!';
            if (response.status == 400){
                var errorkey = response.headers('X-sorteioApp-error');
                var errorparam = response.headers('X-sorteioApp-params');
                
                if (errorkey === 'error.bilheteinvalido'){
                    msg = 'Bilhete inválido: ' + errorparam; 
                } else if (errorkey === 'error.bilhetenaocorrespondeconcurso'){
                    msg = 'Bilhete ' + errorparam + ' não corresponde ao concurso selecionado.'; 
                } else if (errorkey === 'error.loteinvalido'){
                    msg = 'Lote inválido: ' + errorparam;
                } else if (errorkey === 'error.valorloteinvalido'){
                    msg = 'Lote com valor diferente do indicado: ' + errorparam;                    
                }  else if (errorkey === 'error.sempermissao'){
                    msg = 'O usuário não tem permissão para alterar todos os bilhetes deste intervalo.';                    
                } else if (errorkey === 'error.bilhetejavalidado'){
                    msg = 'Existem bilhetes validados no intervalo informado.';                    
                } else if (errorkey === 'error.faixapertenceoutrolote'){
                    msg = 'Parte dos bilhetes já atribuidos a uma regional diferente deste estabelecimento.';
                } else if (errorkey === 'error.loteOutraRegional'){
                    msg = 'Parte dos bilhetes já atribuidos a uma outra regional.';
                } else if (errorkey === 'error.seloinvalido'){
                    msg = 'Movimentação inválida. Verifique, por exemplo, se a numeração inicial é menor que a final.'; 
                }
            } 
            AlertService.error(msg);
        }


        function addErrorAlert(mensagem) {
            vm.alerts.push({type: 'danger', msg: mensagem});
            $timeout(function () {
                  vm.closeAlert(vm.alerts.length-1);
            }, 5000);
        };

        vm.closeAlert = function(index) {
            vm.alerts.splice(index, 1);
        };

        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.lotedistribuicao.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.lotedistribuicao.concurso);
        }             
    }
})();
