 (function() {
    'use strict';

    angular
        .module('sorteioApp')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['$scope', '$location', '$http' ,'Bilhete','Principal', 'LoginService', 'ProfileService', '$state'];

    function HomeController ($scope, $location, $http ,Bilhete, Principal, LoginService, ProfileService, $state) {
        var vm = this;

        vm.account = null;
        vm.isAuthenticated = null;
        vm.login = LoginService.open;
        vm.register = register;
        vm.bilhete = [];

        ProfileService.getProfileInfo().then(function(response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
            vm.appname =  response.appname;
            vm.applargelogo = response.applargelogo;
        });

        $scope.$on('authenticationSuccess', function() {
            getAccount();
        });

        var url = $location.search();

        if(url.id){
            var concurso = getConcurso(url.id);
            var numero   = getNumero(url.id); 
            var identificador = getIdentificador(url.id);
            //if(concurso > 2021100){
                
                Bilhete.query_get({
                    concurso: concurso,
                    numero: numero,
                    identificador: identificador
                }).$promise.then(function(res){
                    vm.bilhete = res;
                    vm.bilhete[0][0] = vm.bilhete[0][0].toString().substring(4);
                    var primeiraChance = vm.bilhete[0][2].split("|");
                    var arrayPrimeira = [];
                    var segundaChance = vm.bilhete[0][3].split("|");
                    var arraySegunda = [];
                    primeiraChance.forEach(element => {
                        arrayPrimeira.push(("00"+element).slice(-2));                    
                    });

                    segundaChance.forEach(element => {
                        arraySegunda.push(("00"+element).slice(-2));                    
                    });

                    vm.bilhete[0][2] = arrayPrimeira.join(" ");
                    vm.bilhete[0][3] = arraySegunda.join(" ");

                    console.log(vm.bilhete);
                    
                });
            //}else{
               // window.location.replace("http://solidariedadesorte.ddns.net/solid-esp/#/bilhete?id="+url.id);
            //}
        }
        function getConcurso(id){
            return id.substring(0,7);
        }

        function getNumero(id){
           return id.substring(11,18);
        }

        function getIdentificador(id){
            return id.substring(7,18)+"-"+id.substring(18);
        }

        getAccount();

        function getAccount() {
            Principal.identity().then(function(account) {
                vm.account = account;
                vm.isAuthenticated = Principal.isAuthenticated;
            });
        }
        function register () {
            $state.go('register');
        }
    }
})();
