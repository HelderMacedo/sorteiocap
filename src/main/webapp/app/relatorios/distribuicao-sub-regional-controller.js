(function (){
    'use strict';

    angular.module('sorteioApp')
        .controller('RelatorioDistribuicaoSubRegionalController', RelatorioDistribuicaoSubRegionalController);

    RelatorioDistribuicaoSubRegionalController.$inject = ['$timeout', '$scope', '$stateParams',  '$http', '$state', '$cookies', 
    'Concurso', 'Regional', 'AreaRegional','DistribuicaoBilhetes', 'Principal', 'AlertService'];

    function RelatorioDistribuicaoSubRegionalController($timeout, $scope, $stateParams,  $http, $state, $cookies,
        Concurso, Regional, AreaRegional ,DistribuicaoBilhetes, Principal, AlertService){
        
       var vm = this;
       vm.account = {};
       vm.concursos = [];
       vm.subregionais = [];
       vm.filtro = {concurso: null, subregional: null};
       vm.relatorio = null;
       vm.loadAll = loadAll;
       vm.distribuicaoBilhetes = [];
       vm.disabledArea = false;
       //vm.changeconcurso = changeconcurso;
       
        

        Principal.identity().then(function(account) {
            vm.account = account;
            
            if(vm.account.roles.includes("ROLE_REGIONAL")){
                Regional.regionalPorUsuario({usuarioId: vm.account.id}).$promise.then(function(res){
                    subRegionalPorRegional(res[0].id);
                });
            }else if(vm.account.roles.includes("ROLE_AREA_REGIONAL")){
                subRegionalPorUsuario();
            }else{
                subRegionalGeral();
            }
            
        });

        function subRegionalPorRegional(id){
            Concurso.queryshort().$promise.then(function(res){
                vm.concursos = res;
                AreaRegional.queryPorRegional({regionalId: id}).$promise.then(function(res){
                    vm.subregionais = res;
                })

                if(vm.concursos.length > 0){
                    if(vm.filtro.concurso == null){
                        selecionaConcursoPadrao();
                    } 
                   }
            });
            
        }

        function subRegionalPorUsuario(){
            Concurso.queryshort().$promise.then(function(res){
                vm.concursos = res;

                AreaRegional.queryPorUsuario({usuarioId: vm.account.id}).$promise.then(function(res){
                    vm.subregionais = res;
        
                    if(vm.subregionais.length == 1){
                        vm.filtro.subregional = vm.subregionais[0];
                    }

                    vm.disabledArea = true;

                    loadAll();                    
                })

                if(vm.concursos.length > 0){
                 if(vm.filtro.concurso == null){
                     selecionaConcursoPadrao();
                 } 
                }

                
            });
        }

        function subRegionalGeral(){
            Concurso.queryshort().$promise.then(function(res){
                vm.concursos = res;

                AreaRegional.getAtivos().$promise.then(function(res){
                    vm.subregionais = _.orderBy(res, ['nome'], ['asc']);                    
                    var area = _.find(vm.subregionais, function(ar){ return vm.filtro.area && ar.id ==  vm.filtro.area.id;});
                });
                if(vm.concursos.length > 0){
                    if(vm.filtro.concurso == null){
                        selecionaConcursoPadrao();
                    } 
                }

                loadAll();
            });
        }

        function loadAll(){
            if(vm.account.roles.includes("ROLE_REGIONAL") && vm.filtro.subregional == null){
                return;
            }
                       
            DistribuicaoBilhetes.queryGetDistribuicaoSubRegional({
                concursoId:  vm.filtro.concurso.id,
                areaId: (vm.filtro.subregional == null) ? null : vm.filtro.subregional.id
            }).$promise.then((res) => {
                vm.distribuicaoBilhetes = res;
            });
        }
       
        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.filtro.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.filtro.concurso);
        }
    }

})();