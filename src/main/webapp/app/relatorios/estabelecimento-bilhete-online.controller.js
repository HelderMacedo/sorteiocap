(function(){
    'use strict';

    angular
        .module('sorteioApp')
        .controller('EstabelecimentoBilheteOnlineController', EstabelecimentoBilheteOnlineController);

    EstabelecimentoBilheteOnlineController.$inject = ['$timeout', '$scope', '$http', '$cookies', 'DistribuicaoBilhetes','Concurso', 'Regional','AreaRegional', 'Principal'];

    function EstabelecimentoBilheteOnlineController ($timeout, $scope, $http, $cookies, DistribuicaoBilhetes,Concurso, Regional , AreaRegional, Principal){
        var vm = this;

        vm.somar = somar;
        vm.totalValidadosBilhete;
        vm.totalBilhetesDistribuidos;
        vm.totalDistribuidosOnline;
        vm.totalValidadosOnline;
        vm.concursos = [];
        vm.regionais = [];
        vm.areasregional = [];
        vm.resultados = [];
        vm.filtro  = {concurso: null, regional:null, isOnline: false};
        vm.changeregional = changeregional;
        vm.changearea = changearea;
        vm.changeconcurso = changeconcurso;
        vm.loadAll = loadAll;
        vm.disabledFiledArea = false;
        vm.account = null;


        Principal.identity().then(function(account) {
            vm.account = account;            
        });

        Regional.queryAtivos().$promise.then(function(res){
            vm.regionais = _.orderBy(res, ['nome'], ['asc']); 
            if(vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0]; 
            }
            if (vm.regionais.length > 1){
                vm.regionais.unshift({'id': -1, 'nome': "Todas Regionais"});
                vm.filtro.regional = vm.regionais[0];
            }
            Concurso.queryshort().$promise.then(function(res){
                vm.concursos = res;
                if(vm.concursos.length > 0){
                    selecionaConcursoPadrao();
                    changeconcurso();
                }
            });
        });

        function changeconcurso(){
            salvaConncursoCookie();
            vm.filtro.regional = null;
            changeregional();
        }

        function changeregional(){
            if (vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0];
            }            
            vm.filtro.area = null; 
            if (vm.filtro.regional){
                AreaRegional.queryPorRegional({'regionalId': vm.filtro.regional.id }).$promise.then(function(res){
                    vm.areasregional = _.orderBy(res, ['nome'], ['asc']);
                    if (vm.areasregional.length === 1) {
                        vm.filtro.area = vm.areasregional[0];
                    }
                    console.log(vm.account);
                    if (vm.areasregional.length === 1 && vm.account.roles.includes("ROLE_AREA_REGIONAL")) {
                        vm.disabledFiledArea = true;
                    }
                    changearea();
                });
            }            
            
        }
        function changearea(){
            if (!vm.filtro.regional){
                vm.areasregional = [];
            }           
            loadAll();
        }   
        
        function loadAll(){
            if(vm.filtro.concurso && vm.filtro.regional && vm.filtro.area){
                DistribuicaoBilhetes.queryEstabelecimentoBilheteArea({
                    concursoId: vm.filtro.concurso.id,
                    regionalId: vm.filtro.regional.id,
                    areaId: (vm.filtro.area)? vm.filtro.area.id : null,
                    isOnline: vm.filtro.isOnline
                }).$promise.then(function(res){
                    vm.resultados = res;
                    console.log(vm.resultados);
                    somar();
                });
            }
            
            if(vm.filtro.concurso && vm.filtro.regional && !vm.filtro.area){
                DistribuicaoBilhetes.queryEstabelecimentoBilheteOnline({
                    concursoId: vm.filtro.concurso.id,
                    regionalId: vm.filtro.regional.id,
                    isOnline: vm.filtro.isOnline
                }).$promise.then(function(res){
                    vm.resultados = res;
                    console.log(vm.resultados);
                    somar();
                    
                });
            }
        }

        function somar(){
            vm.totalValidadosBilhete = 0;
            vm.totalBilhetesDistribuidos = 0;
            vm.totalDistribuidosOnline = 0;
            vm.totalValidadosOnline = 0;
            for(var key of Object.keys(vm.resultados)){
                if(vm.resultados[key]['bilhetesDistribuidos'] != undefined){
                    vm.totalBilhetesDistribuidos += Number(vm.resultados[key]['bilhetesDistribuidos']);
                }

                if (vm.resultados[key]['bilhetesValidados']) {
                    vm.totalValidadosBilhete += Number(vm.resultados[key]['bilhetesValidados']);
                }
                
                if(vm.resultados[key]['bilhetesDistribuidosOnline'] != undefined){
                    vm.totalDistribuidosOnline += Number(vm.resultados[key]['bilhetesDistribuidosOnline']);
                }

                if(vm.resultados[key]['validados_on'] != undefined){
                    vm.totalValidadosOnline += Number(vm.resultados[key]['validados_on']);
                }
                    
            }
            
        }

        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.filtro.concurso = vm.concursos[(idx<0)?0:idx];
        }
        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.filtro.concurso);
        }    

    }  

})();