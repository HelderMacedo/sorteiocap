(function() {
    'use strict';

    angular
    .module('sorteioApp')
    .controller('RegionalBilheteOnlineController', RegionalBilheteOnlineController);

    RegionalBilheteOnlineController.$inject = ['$timeout', '$scope', '$stateParams',  '$http','$cookies', 'DistribuicaoBilhetes' ,'Concurso', 'Regional'];

    function RegionalBilheteOnlineController ($timeout, $scope, $stateParams,  $http, $cookies, DistribuicaoBilhetes,Concurso, Regional){
        var vm = this;

        vm.somar = somar;
        vm.totalValidado_on;
        vm.totalValidados_off;
        vm.totalDistribuidos;
        vm.regionais = [];
        vm.concursos = [];
        vm.resultados = [];
        vm.filtro = {concurso: null, regional: null};
        vm.changeregional = changeregional;
        vm.changeconcurso = changeconcurso;
        vm.loadAll = loadAll;
        


        Regional.queryAtivos().$promise.then(function(res){
            vm.regionais = _.orderBy(res, ['nome'], ['asc']);
            if(vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0]; 
            }
            if (vm.regionais.length > 1){
                vm.regionais.unshift({'id': -1, 'nome': "Todas Regionais"});
                vm.filtro.regional = vm.regionais[0];
            }
            Concurso.queryshort().$promise.then(function(res){
                vm.concursos = res;
                if(vm.concursos.length > 0){
                    selecionaConcursoPadrao(); 
                    changeconcurso();               
                }
            });
            
        });

        

        function changeconcurso(){
            salvaConncursoCookie();
            changeregional();
        }

        function changeregional(){
            if(vm.filtro.concurso){
                loadAll();
            }
        }

        function loadAll(){
            
            if(vm.regionais.length == 1){
                vm.filtro.regional = vm.regionais[0]; 
            }
            if(vm.filtro.concurso && vm.filtro.regional){
                DistribuicaoBilhetes.queryRegionalBilheteOnline({
                    concursoId: vm.filtro.concurso.id,
                    regionalId: vm.filtro.regional.id
                }).$promise.then(function(res){
                    vm.resultados = res;
                    somar();
                });
            }else{
                DistribuicaoBilhetes.queryRegionalBilheteOnlineConcurso({
                    concursoId: vm.filtro.concurso.id
                }).$promise.then(function(res){
                    vm.resultados = res;
                    somar();
                });
            }
           
        }


        function somar(){
            vm.totalValidados_off = 0;
            vm.totalValidado_on = 0;
            vm.totalDistribuidos = 0
            for(var key of Object.keys(vm.resultados)){
                if(vm.resultados[key]['distribuidos'] != undefined){
                    vm.totalDistribuidos += Number(vm.resultados[key]['distribuidos']);
                }

                if(vm.resultados[key]['validados_off'] != undefined){
                    vm.totalValidados_off += Number(vm.resultados[key]['validados_off']);
                }

                if(vm.resultados[key]['validados_on'] != undefined){
                    vm.totalValidado_on += Number(vm.resultados[key]['validados_on']);
                }
                    
            }
            
        }
        function selecionaConcursoPadrao(){
            var concursoCookie = $cookies.getObject('concursoSelecionado');
            var idx = 0;
            if (concursoCookie){
                idx = _.findIndex(vm.concursos, ['id', concursoCookie.id]);
            }
            vm.filtro.concurso = vm.concursos[(idx<0)?0:idx];
        }

        function salvaConncursoCookie() {
            $cookies.putObject('concursoSelecionado', vm.filtro.concurso);
        } 
    }

})();