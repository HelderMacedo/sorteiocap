(function() {
    'use strict';

    angular
        .module('sorteioApp')
        .factory('ProfileService', ProfileService);

    ProfileService.$inject = ['$http'];

    function ProfileService($http) {

        var dataPromise;

        var service = {
            getProfileInfo : getProfileInfo
        };

        return service;

        function getProfileInfo() {
            if (angular.isUndefined(dataPromise)) {
                dataPromise = $http.get('api/profile-info').then(function(result) {
                    if (result.data.activeProfiles) {
                        var response = {};
                        response.activeProfiles = result.data.activeProfiles;
                        response.ribbonEnv = result.data.ribbonEnv;
                        response.inProduction = result.data.activeProfiles.indexOf("dev") == -1;
                        response.swaggerEnabled = result.data.activeProfiles.indexOf("swagger") !== -1;
                        response.comissaoProgressiva = result.data.comissaoProgressiva;
                        
                        if (result.data.activeProfiles.indexOf("prod-sobral_sort") !== -1){
                            response.appname = 'DOAÇÃO DÁ $ORTE';
                            response.applogo = 'logo_apae_sma.jpg';
                            response.applargelogo = 'imagem_doacao_sobral_med.png';    
                            response.Bootswatchtheme = 'Flatly';                        
                        } else if (result.data.activeProfiles.indexOf("prod-sobral_esp") !== -1){
                            response.appname = 'DOAÇÃO DÁ $ORTE - QUINTA ESPECIAL';
                            response.applogo = 'logo_apae_sma.jpg';
                            response.applargelogo = 'imagem_doacao_sobral_med.png';    
                            response.Bootswatchtheme = 'Sandstone';                            
                        } else if (result.data.activeProfiles.indexOf("prod-solid_sort") !== -1){
                            response.appname = 'SOLIDARIEDADE DÁ $ORTE';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_solidariedade_med.png';
                            response.Bootswatchtheme = 'Yeti';                            
                        } else if (result.data.activeProfiles.indexOf("prod-solid_apae") !== -1){
                            response.appname = 'SOLIDARIEDADE DÁ $ORTE';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_solidariedade_med.png';
                            response.Bootswatchtheme = 'Cerulean';                            
                        } else if (result.data.activeProfiles.indexOf("prod-solid_rasp") !== -1){
                            response.appname = 'DOAÇÃO DÁ $ORTE';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_doacao_med.png';
                            response.Bootswatchtheme = 'Flatly';                            
                        } else if (result.data.activeProfiles.indexOf("prod-solid_esp") !== -1){
                            response.appname = 'SOLID. DA $ORTE - QUINTA ESPECIAL';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_solidariedade_bilhete_solidario_med.jpeg';    
                            response.Bootswatchtheme = 'Flatly';                            
                        } else if (result.data.activeProfiles.indexOf("prod-ceara_solid") !== -1){
                            response.appname = 'DOAÇÃO SOLIDÁRIA';
                            response.applogo = 'logo_apae_sma.jpg';
                            response.applargelogo = 'imagem_doacao_sobral_med.png';
                            response.Bootswatchtheme = 'Journal';      
                        } else if (result.data.activeProfiles.indexOf("prod-rasp_norte") !== -1){
                            response.appname = 'RASPADINHA DO NORTE';
                            response.applogo = 'logo-sorteio_sma.jpg';
                            response.applargelogo = 'bolas_sorteio_med.jpg';
                            response.Bootswatchtheme = 'Default';                                                             
                        } else {
                            //Perfil sorteiocap
                            /*
                            response.appname = 'SOLIDARIEDADE DÁ $ORTE';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_solidariedade_med.png';
                            response.Bootswatchtheme = 'Yeti';
                            */
                            response.appname = 'DOAÇÃO DÁ $ORTE';
                            response.applogo = 'logo-slb_sma.png';
                            response.applargelogo = 'imagem_doacao_med.png';
                            response.Bootswatchtheme = 'Flatly';                        
                        }  

                        return response;
                    }

                    //Perfil sorteiocap
                    //response.appname = 'SOLIDARIEDADE DÁ $ORTE';
                    //response.applogo = 'logo-slb_sma.png';
                    //response.applargelogo = 'imagem_solidariedade_med.png';
                    //response.Bootswatchtheme = 'Yeti';

                    //perfil quinta especial
                    //response.appname = 'DOAÇÃO DÁ $ORTE';
                    //response.applogo = 'logo-slb_sma.png';
                    //response.applargelogo = 'imagem_solidariedade_bilhete_solidario_med.jpeg';
                    //response.Bootswatchtheme = 'Flatly';     

                    //Perfil desenvolvimento
                    //response.appname = 'APP DESENVOLVIMENTO';
                    //response.applogo = 'logo-sorteio_sma.jpg';
                    //response.applargelogo = 'bolas_sorteio_med.jpg';
                    //response.Bootswatchtheme = 'Default';
                });
            }
            return dataPromise;
        }
    }
})();
