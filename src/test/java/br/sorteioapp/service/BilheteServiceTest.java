package br.sorteioapp.service;

import br.sorteioapp.service.erros.IdentificacaoBilheteInvalida;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by rodrigo on 22/04/17.
 */
public class BilheteServiceTest {
    @Test
    public void testExtraiNumeroDeIdentificacao() throws IdentificacaoBilheteInvalida {
        int numero  = BilheteService.extraiNumeroDeIdentificacao("70511030002-33");
        assertEquals(numero, 1030002);
    }
    @Test
    public void testExtraiConcursoDeIdentificacao() throws IdentificacaoBilheteInvalida {
        int concurso  = BilheteService.extraiConcursoDeIdentificacao("70511000002-33");
        assertEquals(concurso, 201705);
    }

    @Test
    public void testFormater(){

    }
}
