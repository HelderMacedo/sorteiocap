package br.sorteioapp.service;

import br.sorteioapp.domain.Concurso;
import br.sorteioapp.domain.TipoBilhete;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import static org.junit.Assert.*;

public class GeradorDezenasIteratorTest {
    Concurso conc;
    GeradorDezenasIterator gerador;
    @Before
    public void setUp() throws Exception {
        TipoBilhete tipo = new TipoBilhete().quantidadeNumeros(60).numerosBilhete(20);
        conc = new Concurso();
        conc.setChaveCriptografica("teste");
        conc.setTipo(tipo);

        gerador = new GeradorDezenasIterator(1, conc, true);

    }

    @Test
    public void hasNext() {
        assertTrue(gerador.hasNext());
    }

    @Test
    public void next() {
        GeradorDezenasIterator.CombOrderDezena co = gerador.next();
        assertThat(co.getNum(), greaterThanOrEqualTo(1l));
        assertNotNull(co.getDezenas());
        assertThat(co.getDezenas().length(), greaterThan(40));
    }
}
