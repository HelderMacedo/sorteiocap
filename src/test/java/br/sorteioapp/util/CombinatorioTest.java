package br.sorteioapp.util;

import br.sorteioapp.util.combinatoria.CombinatoriaUtil;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by rodrigo on 30/05/17.
 */
public class CombinatorioTest {


    @Test
    public void testExemplo1() {
        BigInteger combinacoes = CombinatoriaUtil.totalCombinacoes(60, 10);
        BigInteger esperado = new BigInteger("75394027566");
        assertEquals(combinacoes,esperado);
    }
}
