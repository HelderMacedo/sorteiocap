package br.sorteioapp.util;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Created by rodrigo on 22/04/17.
 */
public class EAN13Test {

    @Test
    public void testExemplo1() {
        String codigo = "705100002386";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 8);
    }

    @Test
    public void testExemplo2() {
        String codigo = "701100000024";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 5);
    }

    @Test
    public void testDigitoVerificadorComSomaMaiorQue10() {
        String codigo = "789100031550";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 7);
    }

    @Test
    public void testDigitoVerificadorComSomaMenorQue10() {
        String codigo = "101010101010";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 4);
    }

    @Test
    public void testDigitoVerificadorComSomaIgual10() {
        String codigo = "101010101050";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 0);
    }

    @Test
    public void testDigitoVerificadorComSomaIgual0() {
        String codigo = "000000000000";
        int verificador = EAN13.calcDigitoVerificador(codigo);
        assertEquals(verificador, 0);
    }
}
