'use strict';

describe('Controller Tests', function() {

    describe('Bilhete Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockBilhete, MockConcurso, MockJogador, MockVenda, MockRegional;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockBilhete = jasmine.createSpy('MockBilhete');
            MockConcurso = jasmine.createSpy('MockConcurso');
            MockJogador = jasmine.createSpy('MockJogador');
            MockVenda = jasmine.createSpy('MockVenda');
            MockRegional = jasmine.createSpy('MockRegional');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Bilhete': MockBilhete,
                'Concurso': MockConcurso,
                'Jogador': MockJogador,
                'Venda': MockVenda,
                'Regional': MockRegional
            };
            createController = function() {
                $injector.get('$controller')("BilheteDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'sorteioApp:bilheteUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
