'use strict';

describe('Controller Tests', function() {

    describe('Regional Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockRegional, MockBilhete, MockVenda;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockRegional = jasmine.createSpy('MockRegional');
            MockBilhete = jasmine.createSpy('MockBilhete');
            MockVenda = jasmine.createSpy('MockVenda');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Regional': MockRegional,
                'Bilhete': MockBilhete,
                'Venda': MockVenda
            };
            createController = function() {
                $injector.get('$controller')("RegionalDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'sorteioApp:regionalUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
