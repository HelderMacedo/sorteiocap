'use strict';

describe('Controller Tests', function() {

    describe('Venda Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockPreviousState, MockVenda, MockRegional, MockBilhete, MockEstabelecimento;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockPreviousState = jasmine.createSpy('MockPreviousState');
            MockVenda = jasmine.createSpy('MockVenda');
            MockRegional = jasmine.createSpy('MockRegional');
            MockBilhete = jasmine.createSpy('MockBilhete');
            MockEstabelecimento = jasmine.createSpy('MockEstabelecimento');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity,
                'previousState': MockPreviousState,
                'Venda': MockVenda,
                'Regional': MockRegional,
                'Bilhete': MockBilhete,
                'Estabelecimento': MockEstabelecimento
            };
            createController = function() {
                $injector.get('$controller')("VendaDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'sorteioApp:vendaUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});
