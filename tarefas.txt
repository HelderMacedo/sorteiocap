Dia 30/06/2018

[ok]- Adicionado o tipo do bilhete ao concurso
[ok]- Mudanças cosmeticas na tela de concurso
[ok]- Remocao de dados de jogador do bilhete e o serviço de jogador

Meta 01/07
[ok]- Adicionado a quantidade de numeros por bilhete no tipo do bilhete
[ok]- Regerar entidades
[ok]- A geração de bilhetes deve ter o mesmo tipo


Meta 03/07
[ok] - Testar mudanças realizadas
[ok] - simplificar distribuição para regional 
[ok] - Mudar campos de estabelecimento (cpf, nome)
[ok] - Eliminar campo de indisponibilidade no bilhete e colocar campo validado
[ok] - Mudar concurso para incluir o numero do sorteio no ano e para que as datas de vigencia sejam DATETIME
[ok] - Colocar atributo tipoBilhete ativo, 

Meta 04/07
[ok] Mudar o nome de estabelecimento para vendedor-bilhetes
[ok] simplificar distribuicao para vendedor-bilhetes

Meta 08/07
[ok] Eliminar serviço de devolução
[ok] Criar serviço de validacao de bilhete
[ok] Pagina lote para estabelecimento (a parte) 
[ok]- Indicar campos obrigatórios

Dias 09, 11 e 12/07 Infra no servidor contratado (habilitação de https)

Meta 16/07
[ok] Testar com multiplos concursos,regionais, distribuicao
[ok] Melhorar mensagens de erro e de sucesso para distribuicao de regional
[ok] - Colocar comissão nos estabelecimentos.
[ok]- Fazer testes para estabelecimento, venda-estabelecimento, e validacao
[ok] - Relatorios básicos Distribuição
  - Distribuicao Regionais [ok]
  - Distribuição Regional Sem Estabelecimento [ ok]
  - Endereços dos Estabelecimentos de uma Regional [ok]
  - Distribuição Estabelecimento [ok]

Metas 17/07 s
[ok] Mudar relatório de totalização pelo relatório financeiro de regional
[ok] relatório financeiro de totalização de estabelecimentos 
[ok] adicionar cache para concurso regional e estabelecimento

Meta 18/07
[ok] - revisar permissões 
[ok] - evitar que tecnologia possa visualizar botoes de alteracao de admin
[ok] - ordenar os concursos desc pelo id
[ok] - fazer com que o perfil tecnologia possa alterar senhas, mas nao crie admin

Meta 11/08:
[OK] - Pendencia para msg sucesso distribuição regional : sorteioApp.loteregional.created
[OK] Colocar um campo a mais na distribuições de lotes com quantidade ou valor ?
[ok]- verificação da data do concurso (validação)
[ok] - Pendencia para msg sucesso e erro distribuição estabelecimento

Meta 13/09
[OK] - criar perfil VALIDADOR (considerar no liquibase que o banco já foi alterado);
[OK] - detalhar erro quando cadastrar uma regional que já tenha um usuário;
[OK] - Permitir que um lote seja mudada a regional para NULL (remover regional)
[OK] - testar exclusão simples de um lote  de regional (apenas se não houver uma regional associada nem lotes vendidos)

Meta 12/09
[doing] Ao alterar uma regional, apresentar o login do mesmo. Caso a senha seja preenchida, devemos altera-la. Caso o login mude, temos que EXCLUIR o usuário antigo.

Meta 23/09
[OK] - Verificar preservação dos filtros na navegação dos relatórios; 
[OK] - Tela de mudança de estabelecimento em loteestabelecimento
[OK] - Quando terminar de editar a tela anterior não pode perder o filtro (olhar lote de regionais)
[OK] - Tela de exclusão de lotes de venda (e alterar estabelecimento associado)

Meta 29/09
[OK] - Relatorios com o identificação completa de uma faixa de bilhetes e o seu estado (validado)
       para bilhetes distribuidos, indicando além da regional associada e do estabelecimento. 
       Mostrar o total de bilhetes validados no intervalo.  

Meta 03/10
[OK] - corrigir filtro no cadastro de estabelecimento, quando distribui apaga o filtro
[OK] - recuperar escolha do concurso do cookie
[OK] - Relatorio de totalização com todos os bilhetes

Meta 06/10
[OK] - Mudar sequencial do concurso

Meta 08/10
[OK] - Rever tabelas para armazenar ganhadores e premios;
      Sorteio.Tabelas_Premios_Ganhadores
[OK] - Adicionar colunas que o João colocou no banco
[OK] - Bilhetes online e impressao; Adicionar campo a bilhetes e no lotegeracaobilhete;


Meta 09/10
[OK] - Lote Validação na consulta


Meta 16/10
[OK] - Adicional quantidade total de bilhetes validados na totalização
[OK] - Correção da paginacao em lote de regional

Meta 29/10
[OK] - Tela grupo despesa
[OK] - Filtrar strings SQL Maliciosas
[OK] - Trigger regional_id e estabelecimento_id no bilhete

Meta semana 24/10
[Ok] Não permitir que nenhum bilhete gerado possua mais do que 10 dezenas em comum;
     e não pode possuir mais do que 3 números sequencias.

[OK] - Exclusão de regional e estabelecimentos (caso não existam lotes)
[OK] - Quando uma regional estiver inativa, o usuario também deve ficar inativo
[OK] - Desabilitar distribuição para regionais e estabelecimentos inativos
[OK] - Novos usuários soliciados pelo Bento
[OK] - Correção da exclusao de lotes
[OK] - Reduzir a largura da tabela de totalização para permitir a impressão

[OK] - CSS por profile
[OK] - Testar excluir lote regional com usuario padrão (logistica, regional, etc)
[OK] - Incluir novas modificações do banco na tabela bilhete
[OK] - update regional_id e estabelecimento_id em bilhete no cadastro/atualizacao de lotes
[OK] a) Mudar relatórios de totalização para utilizar apenas bihete

[CANCELED] - Criar Lote Regional tem que notificar atualizacao na lista de lotes
[OK]  b) Mudar estado bilhete (mostrar distribuições e excluir relatório redundante)
[CANCELED]  Na tela de validação apresentar o bilhete validado pelo servidor e não o digitado pelo usuário;
    Testar mensagem de erro;
[CANCELED] apagar usuario e senha dos estabelecimentos!

TODO 07/07/2019

[OK] Enviar planilha para Carol / Solidariedade
[OK] - Gerar concursos novos (todo domingo)
[OK] Pedido de Bento: Mudar colunas da tela de totalização regional
[OK] APLICAÇÃO Solidariedade : Colocar um Filtro de Área na pesquisa e Cadastro de Estabelecimento 
    Já existe a funcionliade: foi verificado que os usuários conseguem visualizar, PORÉM vamos deixar mais explícita.  
    => Se só tiver uma regional, selecionar ela automaticamente
OK] APLICAÇÃO: Disponibilizar um Relatório de Totalização por Área [JÁ EXISTE NA TOTALIZAÇÃO DE ESTABELECIMENTO]
    Já existe a funcionliade: foi verificado que os usuários conseguem visualizar, PORÉM vamos deixar mais explícita.
    => Mudar o nome do menu para totalização Estabelecimento / Área 
[OK] FALHA: Em Relatórios - Distribuição Estabelecimento: quando usa o filtro Área e é mais de 20 lançamentos NÃO CONSEGUIMOS mudar de página. Ou não acontece nada ou sai o filtro.

[  ] - nova consulta para atender o que o Bento deseja
[ ] Avisar ao usuário quando houver um erro de coneção ou quando a sessão terminar



Pendência:
- apagar caches do sistema
- instalar a versão mais nova do tomcat (evitar erros que aparecem no log)
- erro em alterar data do concurso (pendencia baixa)
- apagar trechos/servicos de codigo comentados ou não utilizados.
[DONE] - Retirar triggers e constraints para as tabelas lotes de distribuicao
[OK] - Estabelecimento padrão (campo unique)
[OK] - Totalização no relatório de estabelecimento
[DOING] - Criar view mv_loteregionalbilhete mv_lotevendabilhete   
[DOING] - Adicionar triggers na tabela de distribuicao para atualizar view
[PENDING] - Transferência e exclusão de lote: adicionar opção na tela de relatório de distribuições

[CANCELED] - Cadastro replicado em Jacobina (Doacao e Solidariedade)
[ ] - Distribuição de um ponto de venda (estabelecimento) - Compreender melhor
[OK] - Criar área (associacao nullable em estabelecimento) ok
    - Criar ROLE_AREA_REGIONAL ok
    - Alterar permissão ACESSO_REGIONAL_SEM_SUPER para BLOQUEIA_VISUALIZ_TOTALIZACAO_TODAS_REGIONAIS ok
    - Alterar permissão ACESSO_REGIONAL_COM_SUPER para ACESSO_REGIONAL_DA_AREA ok
    - Não permitir delete cascade ok
    - Permitir que COORDENADOR VEJA TODAS REGIONAIS (e que BLOQUEIA VISUALIZ_TOTALIZ ...) ok
    - Permissão PERM_CADASTRO_AREA 
    - Filtro por area em cadastro de estabelecimento e nos relatorios de estabelecimento
[CANCELED] - Campo de data-hora fim no cadastro de concurso
[ ] - criar coluna MD5_hash também na tabela jhi_user
[ ] - verificar permissões de distribuição regional, estabelecimento e confecção de lotes. 
       - ROUTER x PERM
       - lista de regionais no relatório de distrbuição
[OK] - Instalar FTP Server no servidor (pasta c:\are_transf_clientes  | usuario: grafica | senha: graf9102
                                                                     | grupo clientes | ftp simples não habilitado 

BUGs:
[ok] relatorios: se houver apenas uma regional, selecionar ela.
[ok] Regional deve ver os relatorios financeiros relacionados a ele
[ok]  Adicionada a permissão PERM_REL_FINANCEIRO (15) ao ROLE_REGIONAL
[ok] diretoria nao ve nada
[FALTA] Restrigir o acesso ao rel. financeiro de outras regionais no servidor
[ok] edicao de estabelecimento não está aparecendo a comissão
- estabelecimento não vê nada (deveria ver alguns relatorios)
[ok] - Bug data do concurso com 1 dia a menos: O serviço tomcat levantou com o fuso horário errado

------------------------------------
Perguntas:
- só mostrar nos combos os tipos de ativos (colocar alguma nota (talvez no banco) que os tipos não podem ser alterados os valores, pois isto pode mudar os calculos de tudo. Eles devem ser criados novos.
